/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.6.15-log : Database - escola
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` longtext NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('0547e64b707d4e1c12c0a28727c97c2e','::1','Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.18 Safari/537.36',1429729010,'');

/*Table structure for table `costs` */

DROP TABLE IF EXISTS `costs`;

CREATE TABLE `costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('user','object','input') DEFAULT 'user',
  `name` varchar(60) DEFAULT NULL,
  `value` varchar(40) DEFAULT NULL,
  `stdrvalue` varchar(40) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `iduserfrom` int(11) DEFAULT NULL,
  `iduserto` int(11) DEFAULT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `idorg` smallint(6) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `costs` */

/*Table structure for table `docs` */

DROP TABLE IF EXISTS `docs`;

CREATE TABLE `docs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(160) DEFAULT NULL,
  `desc` text,
  `file1` mediumblob,
  `file1_info` varchar(255) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `iduserto` int(11) DEFAULT NULL,
  `idgroup` int(11) DEFAULT NULL,
  `idproduct` int(11) DEFAULT NULL,
  `category` varchar(160) DEFAULT NULL,
  `permission` varchar(5) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `idtarget` tinyint(4) DEFAULT '1',
  `idorg` int(11) DEFAULT '1',
  `idfolder` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `docs` */

/*Table structure for table `docsuser` */

DROP TABLE IF EXISTS `docsuser`;

CREATE TABLE `docsuser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idorg` int(4) DEFAULT '1',
  `desc` varchar(250) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `content` mediumblob,
  `idtpl` int(11) DEFAULT NULL,
  `contenttpl` text,
  `date_created` datetime DEFAULT NULL,
  `locked` enum('yes','no') DEFAULT 'no',
  `idusr` int(4) DEFAULT NULL,
  `idgrp` int(4) DEFAULT NULL,
  `idserv` int(4) DEFAULT NULL,
  `readonly` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `docsuser` */

/*Table structure for table `entities` */

DROP TABLE IF EXISTS `entities`;

CREATE TABLE `entities` (
  `guid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') NOT NULL DEFAULT 'object',
  `subtype` int(11) DEFAULT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL DEFAULT '1',
  `container_guid` bigint(20) unsigned NOT NULL,
  `access_id` int(11) NOT NULL,
  `time_created` int(11) NOT NULL,
  `time_updated` int(11) NOT NULL,
  `last_action` int(11) NOT NULL DEFAULT '0',
  `enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`guid`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `owner_guid` (`owner_guid`),
  KEY `site_guid` (`site_guid`),
  KEY `container_guid` (`container_guid`),
  KEY `access_id` (`access_id`),
  KEY `time_created` (`time_created`),
  KEY `time_updated` (`time_updated`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `entities` */

/*Table structure for table `entity_subtypes` */

DROP TABLE IF EXISTS `entity_subtypes`;

CREATE TABLE `entity_subtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') NOT NULL DEFAULT 'object',
  `subtype` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`subtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `entity_subtypes` */

/*Table structure for table `financial` */

DROP TABLE IF EXISTS `financial`;

CREATE TABLE `financial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `obs` varchar(255) DEFAULT NULL,
  `status` enum('pending','paid','released') DEFAULT 'pending',
  `percent` varchar(10) DEFAULT NULL,
  `commission` varchar(40) DEFAULT '0',
  `idservicefrom` int(11) DEFAULT NULL,
  `iduserfrom` int(11) DEFAULT NULL,
  `idserviceto` int(11) DEFAULT NULL,
  `iduserto` int(11) DEFAULT NULL,
  `debit` varchar(40) DEFAULT NULL,
  `credit` varchar(40) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_pgto` date DEFAULT NULL,
  `date_rcb` datetime DEFAULT NULL,
  `num_rcb` varchar(10) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `financial` */

/*Table structure for table `folders` */

DROP TABLE IF EXISTS `folders`;

CREATE TABLE `folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nameid` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `folder_level` tinyint(4) DEFAULT NULL,
  `folder_parent` int(11) DEFAULT NULL,
  `desc` varchar(200) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `num` int(4) DEFAULT '0',
  `owner` int(4) DEFAULT NULL,
  `permission` varchar(10) DEFAULT 'write',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `folders` */

insert  into `folders`(`id`,`nameid`,`name`,`folder_level`,`folder_parent`,`desc`,`idorg`,`date_created`,`num`,`owner`,`permission`) values (1,'666666','Users',1,0,'Pasta fixa de Usuários',1,'2015-04-21 14:33:18',0,0,'read'),(2,'990505','usu791164',2,666666,'Pasta do Luiz Tinoco',1,'2015-04-21 14:49:02',0,186,'write'),(3,'938883','docs',3,990505,'Pasta fixa para contratos e propostas para Luiz Tinoco',1,'2015-04-21 14:49:02',0,186,'read'),(4,'964843','escola',3,990505,'Pasta privativa para anexo de documentos do usuário Luiz Tinoco',1,'2015-04-21 14:49:02',0,186,'sol'),(5,'140543','usu7699798',2,666666,'Pasta do Ronan',1,'2015-04-21 19:25:55',0,187,'write'),(6,'251356','docs',3,140543,'Pasta fixa para contratos e propostas para Ronan',1,'2015-04-21 19:25:55',0,187,'read'),(7,'149386','escola',3,140543,'Pasta privativa para anexo de documentos do usuário Ronan',1,'2015-04-21 19:25:55',0,187,'sol');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `desc` varchar(160) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `is_user` enum('yes','no') DEFAULT 'yes',
  `kind` tinyint(4) DEFAULT '1',
  `roles` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='Armazena os tipos ou papéis dos usuários no sistema';

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`desc`,`idorg`,`date_created`,`code`,`is_user`,`kind`,`roles`) values (3,'Professor','',1,'2013-06-14 10:27:27','OPER','yes',3,''),(5,'Outros profissionais','',1,'2013-06-14 10:27:44','PROF','no',4,''),(6,'Administrador','',1,'2013-06-14 10:27:52','ADM','yes',5,''),(7,'Aluno','',1,'2013-06-14 10:28:03','PAC','yes',2,''),(9,'Insumo',NULL,1,'2012-09-25 10:00:11','INS','no',1,NULL),(16,'Auxiliar',NULL,1,'2013-11-22 09:14:58','AUX','no',6,NULL),(17,'Turma',NULL,1,'2013-11-22 09:16:04','TURMA','no',7,NULL);

/*Table structure for table `historic` */

DROP TABLE IF EXISTS `historic`;

CREATE TABLE `historic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusu` int(4) DEFAULT NULL,
  `module` varchar(60) DEFAULT NULL,
  `action` varchar(160) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `info` varchar(60) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Histórico de ações dos usuários no sistema';

/*Data for the table `historic` */

/*Table structure for table `metadata` */

DROP TABLE IF EXISTS `metadata`;

CREATE TABLE `metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) unsigned NOT NULL,
  `name_id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  `value_type` enum('integer','text','money') NOT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `access_id` int(11) NOT NULL,
  `time_created` int(11) NOT NULL,
  `enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `entity_guid` (`entity_guid`),
  KEY `name_id` (`name_id`),
  KEY `value_id` (`value_id`),
  KEY `owner_guid` (`owner_guid`),
  KEY `access_id` (`access_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `metadata` */

/*Table structure for table `metastrings` */

DROP TABLE IF EXISTS `metastrings`;

CREATE TABLE `metastrings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `string` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `string` (`string`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `metastrings` */

/*Table structure for table `organization` */

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `desc` text,
  `folder` varchar(50) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `logo` varchar(80) DEFAULT NULL,
  `options` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabela-base para que o sistema execute em várias instancias';

/*Data for the table `organization` */

insert  into `organization`(`id`,`name`,`desc`,`folder`,`email`,`logo`,`options`) values (1,'Escola','','uploads',NULL,'logo.jpg',NULL);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `desc` text,
  `group` varchar(20) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `abrev` varchar(6) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`desc`,`group`,`date_created`,`active`,`abrev`,`idorg`) values (1,'Matemática','<p>Isto &eacute; apenas um teste.</p>\r\n','Básico','2015-04-02 15:30:02',1,'MAT',1),(2,'Português','<p>Teste</p>\r\n','Básico','2015-04-02 15:30:21',1,'PORT',1),(3,'Geografia','<p>Peguem o material de geografia aqui!</p>\r\n','Básico','2015-04-21 18:34:03',1,'GEO',1);

/*Table structure for table `relationship` */

DROP TABLE IF EXISTS `relationship`;

CREATE TABLE `relationship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idobj1` int(11) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `idobj2` int(11) DEFAULT NULL,
  `permission` varchar(60) DEFAULT 'none',
  `date_created` datetime DEFAULT NULL,
  `content` text,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=951 DEFAULT CHARSET=latin1 COMMENT='Entidade principal para relação entre tabelas';

/*Data for the table `relationship` */

insert  into `relationship`(`id`,`idobj1`,`type`,`idobj2`,`permission`,`date_created`,`content`,`idorg`) values (939,1,'users___groups',6,'none','2015-04-21 18:05:45','',1),(948,3,'products___users',179,'none','2015-04-21 18:35:13','professor',1),(941,186,'users___groups',7,'none','2015-04-21 18:08:03','',1),(890,167,'users___groups',17,'none','2014-01-22 20:17:22','',1),(923,167,'users___services',2,'none','2014-01-30 18:20:29','',1),(873,2,'resources___products',1,'none','2013-11-24 12:13:55','',1),(931,2,'products___users',178,'none','2015-04-02 15:39:54','turma',1),(940,179,'users___groups',3,'none','2015-04-21 18:06:53','',1),(915,181,'users___users',167,'none','2014-01-24 09:40:13','',1),(909,178,'users___groups',17,'none','2014-01-23 15:27:12','',1),(916,167,'users___services',1,'none','2014-01-30 15:41:16','',1),(917,2,'resources___services',1,'0','2014-01-30 15:41:16','',1),(945,1,'products___users',178,'none','2015-04-21 18:32:37','turma',1),(946,3,'products___users',178,'none','2015-04-21 18:34:55','turma',1),(929,2,'products___users',179,'none','2015-04-02 15:41:26','professor',1),(947,1,'products___users',179,'none','2015-04-21 18:35:05','professor',1),(942,186,'users___users',167,'none','2015-04-21 18:24:21','',1),(944,1,'products___users',167,'none','2015-04-21 18:32:37','turma',1),(949,187,'users___groups',7,'none','2015-04-21 19:25:55','',1),(950,187,'users___users',178,'none','2015-04-21 19:26:19','',1);

/*Table structure for table `resources` */

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `desc` text,
  `group` varchar(20) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `abrev` varchar(6) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `resources` */

insert  into `resources`(`id`,`name`,`desc`,`group`,`date_created`,`active`,`abrev`,`idorg`) values (2,'Recurso 1','<p>Isto &eacute; um teste.</p>\r\n','Testes','2013-11-24 12:13:48',1,NULL,1);

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idstatus` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL COMMENT 'Paciente',
  `iduserpro` int(11) DEFAULT NULL COMMENT 'Profissional',
  `idproduct` int(11) DEFAULT NULL COMMENT 'Tratamento',
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `data_array` longtext,
  `id_createdby` int(11) DEFAULT NULL,
  `idservice_rel` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `services` */

insert  into `services`(`id`,`idstatus`,`iduser`,`iduserpro`,`idproduct`,`date_start`,`date_end`,`date_created`,`date_updated`,`data_array`,`id_createdby`,`idservice_rel`,`active`,`idorg`) values (1,1,181,169,1,'2014-01-31 18:20:00','2014-01-31 19:20:00','2014-01-30 15:41:16','2014-01-30 18:18:39','a:17:{s:9:\"pacientes\";s:3:\"181\";s:9:\"convenios\";s:3:\"167\";s:11:\"tratamentos\";s:1:\"1\";s:13:\"profissionais\";s:3:\"169\";s:10:\"date_start\";s:10:\"31/01/2014\";s:12:\"date_start_h\";s:2:\"18\";s:12:\"date_start_m\";s:2:\"20\";s:8:\"date_end\";s:10:\"31/01/2014\";s:10:\"date_end_h\";s:2:\"19\";s:10:\"date_end_m\";s:2:\"20\";s:9:\"obs_geral\";s:21:\"Nenhuma observação.\";s:8:\"recursos\";a:1:{i:0;s:1:\"2\";}s:9:\"auxiliar2\";s:1:\"0\";s:4:\"obs2\";s:0:\"\";s:11:\"aval_desenv\";s:3:\"foi\";s:9:\"aval_diag\";s:3:\"bom\";s:10:\"aval_indic\";s:3:\"né\";}',1,NULL,1,1),(2,4,181,164,1,'2014-01-31 09:30:00','2014-01-31 10:00:00','2014-01-30 18:20:29','2014-01-30 18:20:44','a:14:{s:9:\"pacientes\";s:3:\"181\";s:9:\"convenios\";s:3:\"167\";s:11:\"tratamentos\";s:1:\"1\";s:13:\"profissionais\";s:3:\"164\";s:10:\"date_start\";s:10:\"31/01/2014\";s:12:\"date_start_h\";s:2:\"09\";s:12:\"date_start_m\";s:2:\"30\";s:8:\"date_end\";s:10:\"31/01/2014\";s:10:\"date_end_h\";s:2:\"10\";s:10:\"date_end_m\";s:2:\"00\";s:9:\"obs_geral\";s:5:\"teste\";s:8:\"recursos\";a:1:{i:0;s:1:\"2\";}s:9:\"auxiliar2\";s:1:\"0\";s:4:\"obs2\";s:8:\"Não sei\";}',1,NULL,1,1);

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `level` int(4) DEFAULT '1',
  `color` varchar(15) DEFAULT 'black',
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Etapas globais de preenchimento dos formulários';

/*Data for the table `status` */

insert  into `status`(`id`,`name`,`level`,`color`,`idorg`) values (1,'Agendamento',1,'#3a87ad',1),(2,'Reagendamento',1,'blue',1),(3,'FNH Falta na Hora',2,'#2f96b4',1),(4,'FCA Falta com Aviso',1,'#faa732',1),(5,'FSA Falta sem Aviso',2,'#bd362f',1),(6,'OK Presente',2,'#51a351',1),(7,'OK Avaliado',2,'green',1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `salt` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `uf` varchar(3) DEFAULT NULL,
  `isadmin` enum('yes','no','group') DEFAULT 'no',
  `last_login` datetime DEFAULT NULL,
  `last_hist_action` datetime DEFAULT NULL,
  `idhist` int(11) DEFAULT NULL,
  `islocked` enum('yes','no') DEFAULT 'no',
  `date_created` datetime DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=188 DEFAULT CHARSET=latin1 COMMENT='Tabela básica de usuários do sistema';

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`salt`,`name`,`email`,`phone`,`city`,`uf`,`isadmin`,`last_login`,`last_hist_action`,`idhist`,`islocked`,`date_created`,`idorg`) values (99,'clinica',NULL,NULL,'Clinica',NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,'no',NULL,1),(1,'admin','9191281b8647d2bb1792954f8c54ae48','rdg7kxs2','Administrador','luizzz@me.com','','Belo Horizonte','mg','yes','2015-04-22 14:51:42','2015-04-22 15:56:48',NULL,'no','2015-04-21 18:05:02',1),(187,'usu7699798','3e106ad312c6d2cd9e59e0cbd020dad0','2c7asbpm','Ronan','luizzz@me.com','','','','no','2015-04-21 19:27:46','2015-04-21 19:28:21',1,'no','2015-04-21 19:25:20',1),(167,'1serie','366c19c10442b38bac69c1bf253b2653','h9nkczbs','1ª Série','Sala 20','','Prédio 1','MG','no',NULL,NULL,1,'no','2014-01-22 20:17:05',1),(186,'usu791164','0d66adca55c202677f0a7fb2d450ce2e','168bktfm','Luiz Tinoco','luizzz@me.com','(31) 8787-4787','','','no','2015-04-21 19:27:26','2015-04-21 19:27:33',1,'no','2015-04-21 18:07:16',1),(179,'Fulano','6a1183db7121c1e1368b8babc4e828f8','00zrn2jp','Fernandino Tinoco','luizzz@me.com','(31) 3444-9930','BELO HORIZONTE','Mi','no','2015-04-02 15:15:26','2015-04-02 15:15:43',1,'no','2015-04-21 18:06:27',1),(178,'2serie','1583f51db1f9ecc850a6fc6e31096a88','1vzf7bx4','2ª Série','Sala 21','','Prédio 1','MG','no',NULL,NULL,1,'no','2014-01-23 15:27:00',1);

/*Table structure for table `usersdata` */

DROP TABLE IF EXISTS `usersdata`;

CREATE TABLE `usersdata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iduser` bigint(20) DEFAULT NULL,
  `address` text,
  `address_num` varchar(10) DEFAULT NULL,
  `address_cpl` varchar(20) DEFAULT NULL,
  `address_ref` varchar(250) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `mobile` varchar(40) DEFAULT NULL,
  `website` varchar(40) DEFAULT NULL,
  `cnpj` varchar(40) DEFAULT NULL,
  `contact_name1` varchar(40) DEFAULT NULL,
  `contact_cargo1` varchar(40) DEFAULT NULL,
  `contact_email1` varchar(40) DEFAULT NULL,
  `contact_mobile1` varchar(40) DEFAULT NULL,
  `contact_phone1` varchar(40) DEFAULT NULL,
  `contact_fax` varchar(40) DEFAULT NULL,
  `contact_cpf` varchar(40) DEFAULT NULL,
  `logo` varchar(40) DEFAULT NULL,
  `contact_name2` varchar(40) DEFAULT NULL,
  `contact_cargo2` varchar(40) DEFAULT NULL,
  `contact_email2` varchar(40) DEFAULT NULL,
  `contact_mobile2` varchar(40) DEFAULT NULL,
  `contact_phone2` varchar(40) DEFAULT NULL,
  `contact_name3` varchar(40) DEFAULT NULL,
  `contact_cargo3` varchar(40) DEFAULT NULL,
  `contact_email3` varchar(40) DEFAULT NULL,
  `contact_mobile3` varchar(40) DEFAULT NULL,
  `contact_phone3` varchar(40) DEFAULT NULL,
  `contact_name4` varchar(40) DEFAULT NULL,
  `contact_cargo4` varchar(40) DEFAULT NULL,
  `contact_email4` varchar(40) DEFAULT NULL,
  `contact_mobile4` varchar(40) DEFAULT NULL,
  `contact_phone4` varchar(40) DEFAULT NULL,
  `contact_name5` varchar(40) DEFAULT NULL,
  `contact_cargo5` varchar(40) DEFAULT NULL,
  `contact_email5` varchar(40) DEFAULT NULL,
  `contact_mobile5` varchar(40) DEFAULT NULL,
  `contact_phone5` varchar(40) DEFAULT NULL,
  `outros` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `usersdata` */

insert  into `usersdata`(`id`,`iduser`,`address`,`address_num`,`address_cpl`,`address_ref`,`country`,`fax`,`mobile`,`website`,`cnpj`,`contact_name1`,`contact_cargo1`,`contact_email1`,`contact_mobile1`,`contact_phone1`,`contact_fax`,`contact_cpf`,`logo`,`contact_name2`,`contact_cargo2`,`contact_email2`,`contact_mobile2`,`contact_phone2`,`contact_name3`,`contact_cargo3`,`contact_email3`,`contact_mobile3`,`contact_phone3`,`contact_name4`,`contact_cargo4`,`contact_email4`,`contact_mobile4`,`contact_phone4`,`contact_name5`,`contact_cargo5`,`contact_email5`,`contact_mobile5`,`contact_phone5`,`outros`) values (1,1,'','','','','','','','01/03/2010','','','','','','','','','ec04c05cef3b2ce1301b77de28678d45.jpg','','','','','','','','','','','','','','','','','','','','',''),(5,167,'','','','',NULL,'','','22/10/1990','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','',''),(16,NULL,'AVENIDA CORONEL ALTINO FRANÇA','','','','','','3137732234','01/03/2009','','','','','','','(31) 3773-2234','','','','','','','','','','','','','','','','','','','','','','',''),(17,178,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,179,'RUA ITABIRA','','','','','','31344-499','06/01/2010','','','','','','','(31) 3444-9930','','ab5a38c32a4b88fe493f99930beef21e.jpg','','','','','','','','','','','','','','','','','','','','',''),(21,186,'Teste','77','','','','','','11/05/1999','mg10090909','','','','','','(31) 3888-3338','012.984.938-49','f0e6098317b79d90832ac8ee6b90f973.jpg','','','','','','','','','','','','','','','','','','','','',''),(22,187,'','','','','','','','08/02/2011','','','','','','','','','279a5b9ec72c1a4dd2216e17fa678e42.JPG','','','','','','','','','','','','','','','','','','','','','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
