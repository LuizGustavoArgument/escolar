/*
SQLyog Ultimate v11.5 (64 bit)
MySQL - 5.5.16 : Database - clinica
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` longtext NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('aa0318221a548b8612f5a7c6eb647e58','::1','Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',1427982134,'a:3:{s:9:\"user_data\";s:0:\"\";s:12:\"pagina_atual\";s:4:\"home\";s:16:\"flash:old:notice\";s:19:\"Sessão finalizada.\";}');

/*Table structure for table `costs` */

DROP TABLE IF EXISTS `costs`;

CREATE TABLE `costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('user','object','input') DEFAULT 'user',
  `name` varchar(60) DEFAULT NULL,
  `value` varchar(40) DEFAULT NULL,
  `stdrvalue` varchar(40) DEFAULT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `iduserfrom` int(11) DEFAULT NULL,
  `iduserto` int(11) DEFAULT NULL,
  `active` enum('yes','no') DEFAULT 'yes',
  `idorg` smallint(6) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `costs` */

insert  into `costs`(`id`,`type`,`name`,`value`,`stdrvalue`,`unit`,`iduserfrom`,`iduserto`,`active`,`idorg`) values (1,'object','0','12','14','time',1,169,'yes',1),(2,'object','1','8','18','time',1,169,'yes',1),(3,'object','2','9','18','time',1,169,'yes',1),(4,'object','3','9','12','time',1,169,'yes',1),(5,'object','4','12','18','time',1,169,'yes',1),(6,'object','5','8','18','time',1,169,'yes',1),(7,'object','6','9','10','time',1,169,'yes',1),(8,'user','Mult',NULL,'8','integer',1,169,'yes',1),(9,'object','0','','','time',1,164,'yes',1),(10,'object','1','12','14','time',1,164,'yes',1),(11,'object','2','13','16','time',1,164,'yes',1),(12,'object','3','12','16','time',1,164,'yes',1),(13,'object','4','','','time',1,164,'yes',1),(14,'object','5','','','time',1,164,'yes',1),(15,'object','6','','','time',1,164,'yes',1),(16,'user','Mult',NULL,'6','integer',1,164,'yes',1);

/*Table structure for table `entities` */

DROP TABLE IF EXISTS `entities`;

CREATE TABLE `entities` (
  `guid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') NOT NULL DEFAULT 'object',
  `subtype` int(11) DEFAULT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `site_guid` bigint(20) unsigned NOT NULL DEFAULT '1',
  `container_guid` bigint(20) unsigned NOT NULL,
  `access_id` int(11) NOT NULL,
  `time_created` int(11) NOT NULL,
  `time_updated` int(11) NOT NULL,
  `last_action` int(11) NOT NULL DEFAULT '0',
  `enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`guid`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `owner_guid` (`owner_guid`),
  KEY `site_guid` (`site_guid`),
  KEY `container_guid` (`container_guid`),
  KEY `access_id` (`access_id`),
  KEY `time_created` (`time_created`),
  KEY `time_updated` (`time_updated`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `entities` */

/*Table structure for table `entity_subtypes` */

DROP TABLE IF EXISTS `entity_subtypes`;

CREATE TABLE `entity_subtypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('object','user','group','site') NOT NULL DEFAULT 'object',
  `subtype` varchar(50) NOT NULL,
  `class` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`subtype`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `entity_subtypes` */

/*Table structure for table `financial` */

DROP TABLE IF EXISTS `financial`;

CREATE TABLE `financial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `obs` varchar(255) DEFAULT NULL,
  `status` enum('pending','paid','released') DEFAULT 'pending',
  `percent` varchar(10) DEFAULT NULL,
  `commission` varchar(40) DEFAULT '0',
  `idservicefrom` int(11) DEFAULT NULL,
  `iduserfrom` int(11) DEFAULT NULL,
  `idserviceto` int(11) DEFAULT NULL,
  `iduserto` int(11) DEFAULT NULL,
  `debit` varchar(40) DEFAULT NULL,
  `credit` varchar(40) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `date_pgto` date DEFAULT NULL,
  `date_rcb` datetime DEFAULT NULL,
  `num_rcb` varchar(10) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `financial` */

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `desc` varchar(160) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `is_user` enum('yes','no') DEFAULT 'yes',
  `kind` tinyint(4) DEFAULT '1',
  `roles` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='Armazena os tipos ou papéis dos usuários no sistema';

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`desc`,`idorg`,`date_created`,`code`,`is_user`,`kind`,`roles`) values (3,'Usuário do sistema','',1,'2013-06-14 10:27:27','OPER','yes',3,''),(5,'Outros profissionais','',1,'2013-06-14 10:27:44','PROF','no',4,''),(6,'Administrador','',1,'2013-06-14 10:27:52','ADM','yes',5,''),(23,'Psicologia',NULL,1,'2013-11-23 15:07:38','PSIC','no',4,NULL),(24,'Pilates',NULL,1,'2013-11-23 15:07:40','PIL','no',4,NULL),(25,'Thera',NULL,1,'2013-11-23 15:07:42','THERA','no',4,NULL),(7,'Paciente','',1,'2013-06-14 10:28:03','PAC','no',2,''),(19,'Fono',NULL,1,'2013-11-23 15:07:28','FONO','no',4,NULL),(20,'Fisio',NULL,1,'2013-11-23 15:07:31','FISIO','no',4,NULL),(21,'TO',NULL,1,'2013-11-23 15:07:34','TO','no',4,NULL),(22,'Pedagogia',NULL,1,'2013-11-23 15:07:36','PEDA','no',4,NULL),(9,'Insumo (Cavalo, etc)',NULL,1,'2012-09-25 10:00:11','INS','no',1,NULL),(16,'Auxiliar',NULL,1,'2013-11-22 09:14:58','AUX','no',6,NULL),(17,'Convênio',NULL,1,'2013-11-22 09:16:04','CONV','no',7,NULL);

/*Table structure for table `historic` */

DROP TABLE IF EXISTS `historic`;

CREATE TABLE `historic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusu` int(4) DEFAULT NULL,
  `module` varchar(60) DEFAULT NULL,
  `action` varchar(160) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `info` varchar(60) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1 COMMENT='Histórico de ações dos usuários no sistema';

/*Data for the table `historic` */

insert  into `historic`(`id`,`idusu`,`module`,`action`,`date`,`info`,`idorg`) values (1,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-23 12:29:01','::1',NULL),(2,1,'users','alterar_admin (16)','2014-01-23 14:10:12','::1',1),(3,1,'usersdata','cadastrar (17)','2014-01-23 14:11:40','::1',1),(4,1,'usersdata','cadastrar (18)','2014-01-23 14:22:16','::1',1),(5,1,'users','alterar_admin (179)','2014-01-23 14:23:03','::1',1),(6,1,'usersdata','cadastrar_p (19)','2014-01-23 15:20:56','::1',1),(7,1,'users','alterar_admin (180)','2014-01-23 15:21:17','::1',1),(8,1,'users','alterar_admin (180)','2014-01-23 15:21:33','::1',1),(9,1,'usersdata','cadastrar_p (20)','2014-01-23 15:22:03','::1',1),(10,1,'usersdata','cadastrar_p (21)','2014-01-23 15:22:44','::1',1),(11,1,'users','alterar_admin (181)','2014-01-23 15:26:35','::1',1),(12,1,'users','alterar_admin (179)','2014-01-23 15:26:53','::1',1),(13,1,'users','alterar_admin (178)','2014-01-23 15:27:12','::1',1),(14,1,'users','alterar_admin (169)','2014-01-23 15:27:27','::1',1),(15,1,'users','alterar_admin (169)','2014-01-23 15:27:40','::1',1),(16,1,'login','saiu do sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-23 15:29:06','::1',1),(17,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-23 15:42:41','::1',NULL),(18,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-24 07:54:49','::1',NULL),(19,1,'usersdata','cadastrar_p (21)','2014-01-24 07:55:06','::1',1),(20,1,'usersdata','cadastrar (22)','2014-01-24 09:15:29','::1',1),(21,1,'usersdata','cadastrar_p (23)','2014-01-24 09:15:44','::1',1),(22,1,'login','saiu do sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-24 09:17:46','::1',1),(23,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36','2014-01-24 09:40:01','::1',NULL),(24,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0','2014-01-29 16:21:22','::1',NULL),(25,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36','2014-01-30 09:18:31','::1',NULL),(26,1,'products','alterar (1)','2014-01-30 16:05:44','::1',1),(27,1,'products','alterar (1)','2014-01-30 16:06:40','::1',1),(28,1,'upload','C:/private_html/htdocs/clinica/uploads/2e8bf6d4487d024ffed7c20ea1e1422f.jpg','2014-01-30 16:22:53','::1',1),(29,1,'users','alterar_admin (179)','2014-01-30 16:22:53','::1',1),(30,1,'upload','C:/private_html/htdocs/clinica/uploads/eb265ebc9d0016a3e470b77802ff42dc.jpg','2014-01-30 16:23:12','::1',1),(31,1,'users','alterar_admin (179)','2014-01-30 16:23:12','::1',1),(32,1,'users','alterar_admin (181)','2014-01-30 16:27:14','::1',1),(33,1,'upload','C:/private_html/htdocs/clinica/uploads/b0b7994ffd46ea8722ce1bbd166408ed.jpg','2014-01-30 16:27:38','::1',1),(34,1,'users','alterar_admin (181)','2014-01-30 16:27:38','::1',1),(35,1,'upload','C:/private_html/htdocs/clinica/uploads/e02c16eea00ab06fe06b83b7f628276d.jpg','2014-01-30 16:28:25','::1',1),(36,1,'users','alterar_admin (169)','2014-01-30 16:28:25','::1',1),(37,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36','2014-02-14 19:06:07','::1',NULL),(38,1,'login','entrou no sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36','2015-04-02 10:37:08','::1',NULL),(39,1,'login','saiu do sistema utilizando Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36','2015-04-02 10:42:28','::1',1);

/*Table structure for table `metadata` */

DROP TABLE IF EXISTS `metadata`;

CREATE TABLE `metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `entity_guid` bigint(20) unsigned NOT NULL,
  `name_id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  `value_type` enum('integer','text','money') NOT NULL,
  `owner_guid` bigint(20) unsigned NOT NULL,
  `access_id` int(11) NOT NULL,
  `time_created` int(11) NOT NULL,
  `enabled` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`),
  KEY `entity_guid` (`entity_guid`),
  KEY `name_id` (`name_id`),
  KEY `value_id` (`value_id`),
  KEY `owner_guid` (`owner_guid`),
  KEY `access_id` (`access_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `metadata` */

/*Table structure for table `metastrings` */

DROP TABLE IF EXISTS `metastrings`;

CREATE TABLE `metastrings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `string` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `string` (`string`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `metastrings` */

/*Table structure for table `organization` */

DROP TABLE IF EXISTS `organization`;

CREATE TABLE `organization` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `desc` text,
  `folder` varchar(50) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `logo` varchar(80) DEFAULT NULL,
  `options` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Tabela-base para que o sistema execute em várias instancias';

/*Data for the table `organization` */

insert  into `organization`(`id`,`name`,`desc`,`folder`,`email`,`logo`,`options`) values (1,'Clínica','<div style=\"color: rgb(34, 34, 34); font-family: Calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">\r\n  <p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\"><strong><font face=\"Arial\" style=\"font-size: 10pt;\">%%%NOME%%%</font></strong></p>\r\n</div>\r\n<div style=\"color: rgb(34, 34, 34); font-family: Calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">\r\n  <p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\"><font face=\"Arial\" style=\"font-size: 10pt;\">%%%CARGO%%%</font></p>\r\n</div>\r\n<div style=\"color: rgb(34, 34, 34); font-family: Calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">\r\n  <p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\"><font face=\"Arial\" style=\"font-size: 10pt;\">%%%TELS%%%</font></p>\r\n</div>\r\n<div style=\"color: rgb(34, 34, 34); font-family: Calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">\r\n  <p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\"><a href=\"mailto:%%%EMAIL%%%\" target=\"_blank\" style=\"color: rgb(17, 85, 204);\"><strong><font face=\"Arial\" color=\"#0000ff\" style=\"font-size: 10pt;\">%%%EMAIL%%%</font></strong></a></p>\r\n</div>\r\n\r\n<div style=\"color: rgb(34, 34, 34); font-family: Calibri; font-size: 16px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255);\">\r\n  <p class=\"MsoNormal\" style=\"margin: 0cm 0cm 0pt;\"><strong><font face=\"Arial\" color=\"#008080\" style=\"font-size: 10pt;\">Gentileza confirmar o recebimento deste e-mail.</font></strong></p>\r\n</div>','uploads',NULL,'logo.jpg',NULL);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `desc` text,
  `group` varchar(20) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `abrev` varchar(6) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`desc`,`group`,`date_created`,`active`,`abrev`,`idorg`) values (1,'Tratamento 1','<p>Isto &eacute; apenas um teste.</p>\r\n','Testes','2014-01-30 16:05:48',1,'TRAT',1);

/*Table structure for table `relationship` */

DROP TABLE IF EXISTS `relationship`;

CREATE TABLE `relationship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idobj1` int(11) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `idobj2` int(11) DEFAULT NULL,
  `permission` varchar(60) DEFAULT 'none',
  `date_created` datetime DEFAULT NULL,
  `content` text,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=925 DEFAULT CHARSET=latin1 COMMENT='Entidade principal para relação entre tabelas';

/*Data for the table `relationship` */

insert  into `relationship`(`id`,`idobj1`,`type`,`idobj2`,`permission`,`date_created`,`content`,`idorg`) values (370,1,'users___groups',6,'none','2012-11-26 20:38:10','',1),(891,164,'users___groups',5,'none','2014-01-22 21:49:40','',1),(921,181,'users___groups',7,'none','2014-01-30 16:27:38','',1),(890,167,'users___groups',17,'none','2014-01-22 20:17:22','',1),(922,169,'users___groups',19,'none','2014-01-30 16:28:25','',1),(923,167,'users___services',2,'none','2014-01-30 18:20:29','',1),(873,2,'resources___products',1,'none','2013-11-24 12:13:55','',1),(924,2,'resources___services',2,'0','2014-01-30 18:20:29','Não sei',1),(875,1,'products___users',169,'none','2013-11-24 12:15:15','',1),(919,179,'users___groups',3,'none','2014-01-30 16:23:12','',1),(915,181,'users___users',167,'none','2014-01-24 09:40:13','',1),(909,178,'users___groups',17,'none','2014-01-23 15:27:12','',1),(916,167,'users___services',1,'none','2014-01-30 15:41:16','',1),(917,2,'resources___services',1,'0','2014-01-30 15:41:16','',1);

/*Table structure for table `resources` */

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) DEFAULT NULL,
  `desc` text,
  `group` varchar(20) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `abrev` varchar(6) DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `resources` */

insert  into `resources`(`id`,`name`,`desc`,`group`,`date_created`,`active`,`abrev`,`idorg`) values (2,'Recurso 1','<p>Isto &eacute; um teste.</p>\r\n','Testes','2013-11-24 12:13:48',1,NULL,1);

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idstatus` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL COMMENT 'Paciente',
  `iduserpro` int(11) DEFAULT NULL COMMENT 'Profissional',
  `idproduct` int(11) DEFAULT NULL COMMENT 'Tratamento',
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `data_array` longtext,
  `id_createdby` int(11) DEFAULT NULL,
  `idservice_rel` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `services` */

insert  into `services`(`id`,`idstatus`,`iduser`,`iduserpro`,`idproduct`,`date_start`,`date_end`,`date_created`,`date_updated`,`data_array`,`id_createdby`,`idservice_rel`,`active`,`idorg`) values (1,1,181,169,1,'2014-01-31 18:20:00','2014-01-31 19:20:00','2014-01-30 15:41:16','2014-01-30 18:18:39','a:17:{s:9:\"pacientes\";s:3:\"181\";s:9:\"convenios\";s:3:\"167\";s:11:\"tratamentos\";s:1:\"1\";s:13:\"profissionais\";s:3:\"169\";s:10:\"date_start\";s:10:\"31/01/2014\";s:12:\"date_start_h\";s:2:\"18\";s:12:\"date_start_m\";s:2:\"20\";s:8:\"date_end\";s:10:\"31/01/2014\";s:10:\"date_end_h\";s:2:\"19\";s:10:\"date_end_m\";s:2:\"20\";s:9:\"obs_geral\";s:21:\"Nenhuma observação.\";s:8:\"recursos\";a:1:{i:0;s:1:\"2\";}s:9:\"auxiliar2\";s:1:\"0\";s:4:\"obs2\";s:0:\"\";s:11:\"aval_desenv\";s:3:\"foi\";s:9:\"aval_diag\";s:3:\"bom\";s:10:\"aval_indic\";s:3:\"né\";}',1,NULL,1,1),(2,4,181,164,1,'2014-01-31 09:30:00','2014-01-31 10:00:00','2014-01-30 18:20:29','2014-01-30 18:20:44','a:14:{s:9:\"pacientes\";s:3:\"181\";s:9:\"convenios\";s:3:\"167\";s:11:\"tratamentos\";s:1:\"1\";s:13:\"profissionais\";s:3:\"164\";s:10:\"date_start\";s:10:\"31/01/2014\";s:12:\"date_start_h\";s:2:\"09\";s:12:\"date_start_m\";s:2:\"30\";s:8:\"date_end\";s:10:\"31/01/2014\";s:10:\"date_end_h\";s:2:\"10\";s:10:\"date_end_m\";s:2:\"00\";s:9:\"obs_geral\";s:5:\"teste\";s:8:\"recursos\";a:1:{i:0;s:1:\"2\";}s:9:\"auxiliar2\";s:1:\"0\";s:4:\"obs2\";s:8:\"Não sei\";}',1,NULL,1,1);

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `level` int(4) DEFAULT '1',
  `color` varchar(15) DEFAULT 'black',
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Etapas globais de preenchimento dos formulários';

/*Data for the table `status` */

insert  into `status`(`id`,`name`,`level`,`color`,`idorg`) values (1,'Agendamento',1,'#3a87ad',1),(2,'Reagendamento',1,'blue',1),(3,'FNH Falta na Hora',2,'#2f96b4',1),(4,'FCA Falta com Aviso',1,'#faa732',1),(5,'FSA Falta sem Aviso',2,'#bd362f',1),(6,'OK Presente',2,'#51a351',1),(7,'OK Avaliado',2,'green',1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `salt` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `uf` varchar(3) DEFAULT NULL,
  `isadmin` enum('yes','no','group') DEFAULT 'no',
  `last_login` datetime DEFAULT NULL,
  `last_hist_action` datetime DEFAULT NULL,
  `idhist` int(11) DEFAULT NULL,
  `islocked` enum('yes','no') DEFAULT 'no',
  `date_created` datetime DEFAULT NULL,
  `idorg` int(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=186 DEFAULT CHARSET=latin1 COMMENT='Tabela básica de usuários do sistema';

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`salt`,`name`,`email`,`phone`,`city`,`uf`,`isadmin`,`last_login`,`last_hist_action`,`idhist`,`islocked`,`date_created`,`idorg`) values (99,'clinica',NULL,NULL,'Clinica',NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,'no',NULL,1),(1,'admin','9191281b8647d2bb1792954f8c54ae48','rdg7kxs2','Admin','luizzz@me.com','','Belo Horizonte','mg','yes','2015-04-02 10:37:08','2015-04-02 10:42:28',NULL,'no','2013-05-20 14:21:11',1),(169,'fono1','1583f51db1f9ecc850a6fc6e31096a88','1vzf7bx4','Fonoaudióloga de Testes','fono1@inbloom.com.br','(31) 3444-9930','','','no',NULL,NULL,1,'no','2014-01-30 16:27:44',1),(167,'unimed','366c19c10442b38bac69c1bf253b2653','h9nkczbs','UNIMED BH','http://unimed.com.br','(31) 3444-9930','BELO HORIZONTE','MG','no',NULL,NULL,1,'no','2014-01-22 20:17:05',1),(164,'Teste','0f1f8251bc91811138dfe21d6560af24','00zb92ca','Testador da Silva Pires','teste@teste.com','','','','no',NULL,NULL,1,'no','2014-01-22 21:49:13',1),(181,'usu717923','1583f51db1f9ecc850a6fc6e31096a88','1vzf7bx4','Luiz Otavio Tinoco','luizzz@me.com','(31) 3773-2234','SETE LAGOAS','Mi','no',NULL,NULL,1,'no','2014-01-30 16:27:25',1),(179,'Fulavno','6a1183db7121c1e1368b8babc4e828f8','00zrn2jp','FFFFernandino Tinoco','luizzz@me.com','(31) 3444-9930','BELO HORIZONTE','Mi','no',NULL,NULL,1,'no','2014-01-30 16:23:03',1),(178,'PROMED','1583f51db1f9ecc850a6fc6e31096a88','1vzf7bx4','Promed','http://promed.com.br','','','MG','no',NULL,NULL,1,'no','2014-01-23 15:27:00',1);

/*Table structure for table `usersdata` */

DROP TABLE IF EXISTS `usersdata`;

CREATE TABLE `usersdata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iduser` bigint(20) DEFAULT NULL,
  `address` text,
  `address_num` varchar(10) DEFAULT NULL,
  `address_cpl` varchar(20) DEFAULT NULL,
  `address_ref` varchar(250) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `fax` varchar(40) DEFAULT NULL,
  `mobile` varchar(40) DEFAULT NULL,
  `website` varchar(40) DEFAULT NULL,
  `cnpj` varchar(40) DEFAULT NULL,
  `contact_name1` varchar(40) DEFAULT NULL,
  `contact_cargo1` varchar(40) DEFAULT NULL,
  `contact_email1` varchar(40) DEFAULT NULL,
  `contact_mobile1` varchar(40) DEFAULT NULL,
  `contact_phone1` varchar(40) DEFAULT NULL,
  `contact_fax` varchar(40) DEFAULT NULL,
  `contact_cpf` varchar(40) DEFAULT NULL,
  `logo` varchar(40) DEFAULT NULL,
  `contact_name2` varchar(40) DEFAULT NULL,
  `contact_cargo2` varchar(40) DEFAULT NULL,
  `contact_email2` varchar(40) DEFAULT NULL,
  `contact_mobile2` varchar(40) DEFAULT NULL,
  `contact_phone2` varchar(40) DEFAULT NULL,
  `contact_name3` varchar(40) DEFAULT NULL,
  `contact_cargo3` varchar(40) DEFAULT NULL,
  `contact_email3` varchar(40) DEFAULT NULL,
  `contact_mobile3` varchar(40) DEFAULT NULL,
  `contact_phone3` varchar(40) DEFAULT NULL,
  `contact_name4` varchar(40) DEFAULT NULL,
  `contact_cargo4` varchar(40) DEFAULT NULL,
  `contact_email4` varchar(40) DEFAULT NULL,
  `contact_mobile4` varchar(40) DEFAULT NULL,
  `contact_phone4` varchar(40) DEFAULT NULL,
  `contact_name5` varchar(40) DEFAULT NULL,
  `contact_cargo5` varchar(40) DEFAULT NULL,
  `contact_email5` varchar(40) DEFAULT NULL,
  `contact_mobile5` varchar(40) DEFAULT NULL,
  `contact_phone5` varchar(40) DEFAULT NULL,
  `outros` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `usersdata` */

insert  into `usersdata`(`id`,`iduser`,`address`,`address_num`,`address_cpl`,`address_ref`,`country`,`fax`,`mobile`,`website`,`cnpj`,`contact_name1`,`contact_cargo1`,`contact_email1`,`contact_mobile1`,`contact_phone1`,`contact_fax`,`contact_cpf`,`logo`,`contact_name2`,`contact_cargo2`,`contact_email2`,`contact_mobile2`,`contact_phone2`,`contact_name3`,`contact_cargo3`,`contact_email3`,`contact_mobile3`,`contact_phone3`,`contact_name4`,`contact_cargo4`,`contact_email4`,`contact_mobile4`,`contact_phone4`,`contact_name5`,`contact_cargo5`,`contact_email5`,`contact_mobile5`,`contact_phone5`,`outros`) values (1,1,'',NULL,NULL,NULL,'','','','','','',NULL,'','','','','012.345.678-90','99f27bc123eab46c93e4bef1b0ec27d6.JPG',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,164,'','','','','','','','01/10/1980','mg020930293','','','','','','(31) 29283-9843','','','','','','','','','','','','','','','','','','','','','','',''),(5,167,'','','','',NULL,'','','22/10/1990','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','',''),(7,169,'','','','','','','','22/10/1991','','','','','','','(31) 3773-2234','','e02c16eea00ab06fe06b83b7f628276d.jpg','','','','','','','','','','','','','','','','','','','','',''),(16,NULL,'AVENIDA CORONEL ALTINO FRANÇA','','','','','','3137732234','01/03/2009','','','','','','','(31) 3773-2234','','','','','','','','','','','','','','','','','','','','','','',''),(17,178,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,179,'RUA ITABIRA','','','','','','31344-499','06/01/2010','','','','','','','(31) 3444-9930','','eb265ebc9d0016a3e470b77802ff42dc.jpg','','','','','','','','','','','','','','','','','','','','',''),(20,181,'AVENIDA CORONEL ALTINO FRANÇA','','','','','','31377-322','01/01/1969','13.260.573/0001-04','','','','','','(31) 3773-2234','','b0b7994ffd46ea8722ce1bbd166408ed.jpg','','','','','','','','','','','','','','','','','','','','','<strong>Desenvolvimento:</strong> foi <hr>Diagnóstico: bom <hr>Indicação: né');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
