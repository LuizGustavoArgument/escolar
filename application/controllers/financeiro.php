<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financeiro extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('financeiro');
        $this->load->model('sistema','banco',TRUE);

        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        $this->load->library('form');
        
        // Configurando os demais menus suspensos
        $this->ocultarMenus();
        
        $this->session->set_userdata('pagina_atual', 'financeiro');
    }
    
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_financeiro',$c);

        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                $ret = false;
                break;
        // 4 = somente os que o usuário participa e dos consultores que ele coordena
            case 4:
                $proibido = array('cliente','funcionario','salva_atual','salva_atual_fun', 'relatorios');
                $ret[0] = array($this->session->userdata('esta_logado')); //id do usuario
                $ret[1] = $this->banco->relacao('users',$this->session->userdata('esta_logado'),'users'); //id dos usuarios que ele coordena
                $ret[2] = $this->banco->idservicos(array_merge($ret[1],$ret[0])); // ids dos servicos que ele pode ver
                break;
        // 3 = somente listagem (dele)
            case 3:
                $proibido = array('cliente','funcionario','salva_atual','salva_atual_fun', 'relatorios');
                $ret[0] = array($this->session->userdata('esta_logado'));
                $ret[1] = array();
                $ret[2] = $this->banco->idservicos(array_merge($ret[1],$ret[0])); // ids dos servicos que ele pode ver
                break;
        // outros = proibido
            default:
                $proibido = array('index','cliente','funcionario','geral','salva_atual','salva_atual_fun', 'relatorios');
                $ret = false;
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não possui permissão para acessar a área FINANCEIRA.');
            redirect('home');
        }
        
        return $ret;
    }

    public function index()
    {
        $this->perm(__FUNCTION__);
        
        $this->dados['central'] = '<section><p>Selecione uma das opções do menu.</p></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function cliente($opt="")
    {
        $cond = $this->perm(__FUNCTION__);
        
        if($cond)
        {
            $this->session->set_flashdata('notice','Você não possui permissão para acessar esta área.');
            redirect('home');
        }
            
        $this->cabecalho['titulo'] = 'Financeiro :: Por Cliente';
        $this->cabecalho['subtitulo'] = 'Planilha financeira filtrada por cliente e serviço contratado.';
        
        $this->dados['tabela'] = '<script>
        $(document).ready(function() {
        
        $.extend( $.fn.dataTableExt.oSort, {
            "alt-string-pre": function ( a ) {
                var m = a.match(/<strong>(.*?)<\/strong>/)[1].toLowerCase();
                var ukDatea = m.split(\'/\');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },
             
            "alt-string-asc": function( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
         
            "alt-string-desc": function(a,b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
            
        $("#financeiro").dataTable({
                 "iDisplayLength": 50,
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "sType": "alt-string" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" }
                    ]
        });
        
        
        
        });
        
        function salvacli(linha)
        {
            
            $.post("'.site_url('financeiro/salva_atual').'/"+linha, $("#financeirocli").serialize(), function(data) { if(data == "ok"){ $("#salvou").fadeIn().delay(800).fadeOut(); } } );
        }
        
        function changefilt()
        {
            var v1 = $("#pro:checked").val();
            var v2 = $("#pag:checked").val();
            var ret = "";
            
            if((typeof v1 == "undefined") && (typeof v2 == "undefined"))
            {
                ret = "0";
            }
            if((typeof v1 == "undefined") && v2 == 1)
            {
                ret = "1";
            }
            if(v1 == 1 && (typeof v2 == "undefined"))
            {
                ret = "3";
            }
            if(v1 == 1 && v2 == 1)
            {
                ret = "2";
            }
            
            document.location.href="'.site_url('financeiro/cliente').'/"+ret;
            return;
        }
        
        function hoje()
        {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){dd=\'0\'+dd} if(mm<10){mm=\'0\'+mm} today = dd+\'/\'+mm+\'/\'+yyyy;
            return today;
        }
        </script>';
        
        $this->dados['tabela'] .= '<p align="right"><label class="checkbox inline" for="pro"><input '.($opt == 3 || $opt == 2 ? 'checked' : '').' type="checkbox" onchange="changefilt()" name="pro" id="pro" value="1"> Mostrar <em>ProLucro</em></label>';
        $this->dados['tabela'] .= '<label class="checkbox inline" for="pag"><input '.($opt == 2 || $opt == 1 ? 'checked' : '').' type="checkbox" onchange="changefilt()" name="pag" id="pag" value="1"> Mostrar o que foi pago</label></p>';
        $this->dados['tabela'] .= '<form name="financeirocli" id="financeirocli" action="'. base_url() . 'financeiro/salva_atual" method="post">';
        $this->dados['tabela'] .= '<table id="financeiro"><thead><tr>';
        $this->dados['tabela'] .= '<th>Dia</th>
                                   <th>Cliente</th>
                                   <th>Serviço</th>
                                   <th>Parcela</th>
                                   <th>Obs</th>
                                   <th>Débito</th>
                                   <th>Crédito</th>
                                   <th>Pagto</th>
                                   <th>Dia Pagto</th>
                                   ';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $filt = '((financial.iduserfrom > 0 and financial.idserviceto > 0 and financial.iduserto = 0 and financial.idservicefrom = 0) or ';
        $filt .= '(financial.iduserfrom > 0 and financial.idserviceto > 0 and financial.iduserto is null and financial.idservicefrom is null))';
        if($opt == 0 || empty($opt))
        {
            // Default: não mostrar os pagos e ocultar "prolucro"
            $filt .= " and financial.status != 'paid'";
            $filt .= ' and financial.iduserfrom != 99 and financial.iduserto != 99';
        }
        if($opt == 1) // mostrar os pagos e ocultar proluco
        {
            $filt .= ' and financial.iduserfrom != 99 and financial.iduserto != 99';
        }
        if($opt == 2) // mostrar os pagos e mostra proluco
        {
            $filt .= '';
        }
        if($opt == 3) // oculta os pagos e mostra proluco
        {
             $filt .= " and financial.status != 'paid'";
        }
        $listagem = $this->banco->listagem_financeiro('cliente',$filt,'financial.date_pgto');
        foreach($listagem as $item)
        {
            $select_status = '
            <select name="status_'.$item->id.'" id="status_'.$item->id.'" class="input-small" style="height:20px;line-height:20px; padding:0px;margin:0px; background-color:#FFFFCC;" onchange="if(this.value == \'paid\' && (document.getElementById(\'date_updated_'.$item->id.'\').value == \'\' || document.getElementById(\'date_updated_'.$item->id.'\').value == \'-\')) { document.getElementById(\'date_updated_'.$item->id.'\').value = hoje(); } salvacli('.$item->id.');">
            <option value="pending" '.($item->status == "pending" ? 'selected' : '').'>Em aberto</option>
            <option value="paid" '.($item->status == "paid" ? 'selected' : '').'>Pago</option>
            <option value="release" '.($item->status == "release" ? 'selected' : '').'>Liberado</option>
            </select>
            ';
            $this->dados['tabela'] .= '<tr>
                                   <td><span style="text-align:center"><strong>'.date('d/m/Y',strtotime($item->date_pgto)).'</strong></span></td>
                                   <td><abbr title="'.$item->desc.'">'.$item->usersname.'</abbr></td>
                                   <td><abbr title="'.$item->statusname.'">'.$item->servicesname.'</abbr></td>
                                   <td>'.character_limiter($item->name,3,'.').'</td>
                                   <td><input type="text" onblur="salvacli('.$item->id.');" class="input-small" style="padding:0px;margin:0px;" name="obs_'.$item->id.'" id="obs_'.$item->id.'" value="'.$item->obs.'"></td>
                                   <td><div class="input-prepend input-append">
                                        <span class="add-on">R$</span>
                                        <input type="text" onblur="salvacli('.$item->id.');" style="background-color:red;color:white;" class="input-small" 
                                        name="debit_'.$item->id.'" id="debit_'.$item->id.'" alt="decimal" value="'.$this->preco($item->debit,false).'">
                                       </div>
                                   </td>
                                   <td><div class="input-prepend input-append">
                                       <span class="add-on">R$</span><input type="text" onblur="salvacli('.$item->id.');" style="background-color:blue;color:white;" class="input-small" 
                                       name="credit_'.$item->id.'" id="credit_'.$item->id.'" alt="decimal" value="'.$this->preco($item->credit,false).'">
                                       </div>
                                   </td>
                                   <td>'.$select_status.'</td>
                                   <td><input type="text" style="padding:0px;margin:0px;" alt="date" onblur="salvacli('.$item->id.');" class="input-small" name="date_updated_'.$item->id.'" id="date_updated_'.$item->id.'" value="'.((empty($item->date_updated) || ($item->date_updated == '0000-00-00 00:00:00')) ? '-' : date('d/m/Y',strtotime($item->date_updated))).'"></td>
                                   
                                   </tr>';
        }
        $this->dados['tabela'] .= '</tbody></table></form>';
        $this->dados['central'] = '<section style="margin-top:-55px"><p>'.$this->dados['tabela'].'</p><div id="salvou" style="display:none;"><small style="color:green"><i class="fas fa-thumbs-up"></i> Salvo!</small></div></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function funcionario($opt="")
    {
        $cond = $this->perm(__FUNCTION__);
        
        if($cond)
        {
            $this->session->set_flashdata('notice','Você não possui permissão para acessar esta área.');
            redirect('home');
        }
            
        $this->cabecalho['titulo'] = 'Financeiro :: Por Funcionário';
        $this->cabecalho['subtitulo'] = 'Planilha financeira filtrada por funcionário/consultor.';
        
        $this->dados['tabela'] = '<script>
        $(document).ready(function() {
            
            $.extend( $.fn.dataTableExt.oSort, {
            "alt-string-pre": function ( a ) {
                var m = a.match(/<strong>(.*?)<\/strong>/)[1].toLowerCase();
                var ukDatea = m.split(\'/\');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },
             
            "alt-string-asc": function( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
         
            "alt-string-desc": function(a,b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
            
             $("#financeiro").dataTable({
                 "iDisplayLength": 50,
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "sType": "alt-string" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" }
                    ]
        }); 
        
        });
        
        function salvafun(linha)
        {
            $.post("'.site_url('financeiro/salva_atual_fun').'/"+linha, $("#financeirofun").serialize(), function(data) { if(data == "ok"){ $("#salvou").fadeIn().delay(800).fadeOut(); } } );
        }

        function changefilt()
        {
            var v1 = $("#pro:checked").val();
            var v2 = $("#pag:checked").val();
            var ret = "";
            
            if((typeof v1 == "undefined") && (typeof v2 == "undefined"))
            {
                ret = "0";
            }
            if((typeof v1 == "undefined") && v2 == 1)
            {
                ret = "1";
            }
            if(v1 == 1 && (typeof v2 == "undefined"))
            {
                ret = "3";
            }
            if(v1 == 1 && v2 == 1)
            {
                ret = "2";
            }
            
            document.location.href="'.site_url('financeiro/funcionario').'/"+ret;
            return;
        }

        function hoje()
        {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            if(dd<10){dd=\'0\'+dd} if(mm<10){mm=\'0\'+mm} today = dd+\'/\'+mm+\'/\'+yyyy;
            return today;
        }
        </script>';
        
        $this->dados['tabela'] .= '<p align="right"><label class="checkbox inline" for="pro"><input '.($opt == 3 || $opt == 2 ? 'checked' : '').' type="checkbox" onchange="changefilt()" name="pro" id="pro" value="1"> Mostrar <em>ProLucro</em></label>';
        $this->dados['tabela'] .= '<label class="checkbox inline" for="pag"><input '.($opt == 2 || $opt == 1 ? 'checked' : '').' type="checkbox" onchange="changefilt()" name="pag" id="pag" value="1"> Mostrar o que foi pago</label></p>';
        $this->dados['tabela'] .= '<form name="financeirofun" id="financeirofun" action="'. base_url() . 'financeiro/salva_atual" method="post">';
        $this->dados['tabela'] .= '<table id="financeiro"><thead><tr>';
        $this->dados['tabela'] .= '<th>Dia</th>
                                   <th>Consultor</th>
                                   <th>Serviço</th>
                                   <th>Parcela</th>
                                   <th>Obs</th>
                                   <th>%</th>
                                   <th>Débito</th>
                                   <th>Crédito</th>
                                   <th>Pagto</th>
                                   <th>Dia Pagto</th>
                                   ';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $filt = '((financial.iduserfrom = 0 and financial.idserviceto = 0 and financial.iduserto > 0 and financial.idservicefrom > 0) or ';
        $filt .= '(financial.iduserfrom is null and financial.idserviceto is null and financial.iduserto > 0 and financial.idservicefrom > 0))';
        if($opt == 0 || empty($opt))
        {
            // Default: não mostrar os pagos e ocultar "prolucro"
            $filt .= " and financial.status != 'paid'";
            $filt .= ' and financial.iduserfrom != 99 and financial.iduserto != 99';
        }
        if($opt == 1) // mostrar os pagos e ocultar proluco
        {
            $filt .= ' and financial.iduserfrom != 99 and financial.iduserto != 99';
        }
        if($opt == 2) // mostrar os pagos e mostra proluco
        {
            $filt .= '';
        }
        if($opt == 3) // oculta os pagos e mostra proluco
        {
             $filt .= " and financial.status != 'paid'";
        }
        
        
        $listagem = $this->banco->listagem_financeiro('funcionario',$filt,'financial.date_pgto');
        foreach($listagem as $item)
        {
            $select_status = '
            <select name="status_'.$item->id.'" id="status_'.$item->id.'" class="input-small" style="height:20px;line-height:20px;padding:0px;margin:0px;background-color:#FFFFCC;" onchange="if(this.value == \'paid\' && (document.getElementById(\'date_updated_'.$item->id.'\').value == \'\' || document.getElementById(\'date_updated_'.$item->id.'\').value == \'-\')) { document.getElementById(\'date_updated_'.$item->id.'\').value = hoje(); } salvafun('.$item->id.');">
            <option value="pending" '.($item->status == "pending" ? 'selected' : '').'>Em aberto</option>
            <option value="paid" '.($item->status == "paid" ? 'selected' : '').'>Pago</option>
            <option value="release" '.($item->status == "release" ? 'selected' : '').'>Liberado</option>
            </select>
            ';
            $this->dados['tabela'] .= '<tr>
                                   <td><span style="text-align:center"><strong>'.date('d/m/Y',strtotime($item->date_pgto)).'</strong></span></td>
                                   <td><abbr title="'.$item->usersname.'">'.$item->usersusername.'</abbr></td>
                                   <td><abbr title="'.$item->desc.'">'.$item->servicesname.'</abbr></td>
                                   <td>'.character_limiter($item->name,3,'.').'</td>
                                   <td><input type="text" onblur="salvafun('.$item->id.');" style="padding:0px;margin:0px;" class="input-small" name="obs_'.$item->id.'" id="obs_'.$item->id.'" value="'.$item->obs.'"></td>
                                   <td>'.(!empty($item->percent) ? $this->preco(((float) $item->percent+ (float) $item->commission),false).'%' : '').'</td>
                                   <td><div class="input-prepend input-append">
                                        <span class="add-on">R$</span>
                                        <input type="text" readonly="readonly" style="background-color:red;color:white;" class="input-small" 
                                        name="debit_'.$item->id.'" id="debit_'.$item->id.'" alt="decimal" value="'.$this->preco($item->debit,false).'">
                                       '.(!empty($item->commission) ? '<span class="help-block"><small>Incluso '.$this->preco($item->commission,false).'% de comissão.</small></span>' : '').'
                                       </div>
                                   </td>
                                   <td><div class="input-prepend input-append">
                                       <span class="add-on">R$</span><input type="text" onblur="salvafun('.$item->id.');" style="background-color:blue;color:white;" class="input-small" 
                                       name="credit_'.$item->id.'" id="credit_'.$item->id.'" alt="decimal" value="'.$this->preco($item->credit,false).'">
                                       </div>
                                   </td>
                                   <td>'.$select_status.'</td>
                                   <td><input type="text" style="padding:0px;margin:0px;" alt="date" onblur="salvafun('.$item->id.');" class="input-small" name="date_updated_'.$item->id.'" 
                                   id="date_updated_'.$item->id.'" value="'.((empty($item->date_updated) || ($item->date_updated == '0000-00-00 00:00:00')) ? '-' : date('d/m/Y',strtotime($item->date_updated))).'"></td>
                                   </tr>';
        }
        $this->dados['tabela'] .= '</tbody></table></form>';
        $this->dados['central'] = '<section style="margin-top:-55px"><p>'.$this->dados['tabela'].'</p><div id="salvou" style="display:none;"><small style="color:green"><i class="fas fa-thumbs-up"></i> Salvo!</small></div></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
   }
   
    public function geral()
    {
        $cond = $this->perm(__FUNCTION__);
        $filt = "";
        
        if($cond)
        {
            if(!empty($cond[2])) {
                
                // Se for cliente ou consulta, mostra apenas dele (idserviceto = servico contatado)
                $tmpcheck = $this->banco->relacao('users',$cond[0][0],'groups',7); // 7 = clientes
                if(!empty($tmpcheck))
                    $filt .= 'idserviceto in ('.implode(',',$cond[2]).')';
                
                $tmpcheck = $this->banco->relacao('users',$cond[0][0],'groups',8); // 8 = consulta
                if(!empty($tmpcheck))
                    $filt .= 'idserviceto in ('.implode(',',$cond[2]).')';
                    
                // Se for consultor OU coordenador, mostra os que estiverem envolvido em algum serviço
                if(empty($filt))
                {
                    $filt .= 'idservicefrom in ('.implode(',',$cond[2]).')'; // and iduserto in ('.implode(',',$cond[1]).')';
                    
                    if(!empty($cond[1]))
                    $filt .= ' and iduserto in ('.implode(',',$cond[1]).')';
                    else
                        $filt .= ' and iduserto = '. $cond[0][0];
                }
                
            } else { $filt .= 'idserviceto = 0 and (iduserfrom = '.$this->session->userdata('esta_logado').' or iduserto = '.$this->session->userdata('esta_logado').')'; }
        }
        $this->cabecalho['titulo'] = 'Financeiro :: Controle Geral';
        $this->cabecalho['subtitulo'] = 'Visualize todos os dados financeiros.';
        
        $this->dados['tabela'] = '<script>
        $(document).ready(function() {
            
            $.extend( $.fn.dataTableExt.oSort, {
            "alt-string-pre": function ( a ) {
                var m = a.match(/<strong>(.*?)<\/strong>/)[1].toLowerCase();
                var ukDatea = m.split(\'/\');
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            },
             
            "alt-string-asc": function( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },
         
            "alt-string-desc": function(a,b) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        } );
            
             $("#financeiro").dataTable({
                 "iDisplayLength": 50,
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "aoColumns": [
                        { "sType": "alt-string" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" },
                        { "sSortDataType": "dom-text" }
                    ]
        }); 
        
        });
        
        
        </script>';
        
        $this->dados['tabela'] .= '<form name="financeiroger" id="financeiroger" method="post">';
        $this->dados['tabela'] .= '<table id="financeiro"><thead><tr>';
        $this->dados['tabela'] .= '<th>Dia</th>
                                   <th>Nome</th>
                                   <th>Serviço</th>
                                   <th>Parcela</th>
                                   <th>Obs</th>
                                   <th>%</th>
                                   <th>Débito</th>
                                   <th>Crédito</th>
                                   <th>Pagto</th>
                                   <th>Dia Pagto</th>
                                   ';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        
        $listagem = $this->banco->listagem('financial','financial.date_pgto',$filt);
        foreach($listagem as $item)
        {
            $select_status = '
            <select name="status_'.$item->id.'" readonly="readonly" id="status_'.$item->id.'" class="input-small" style="height:20px;line-height:20px;padding:0px;margin:0px;background-color:#FFFFCC;" onchange="salvafun('.$item->id.');">
            <option value="pending" '.($item->status == "pending" ? 'selected' : '').'>Em aberto</option>
            <option value="paid" '.($item->status == "paid" ? 'selected' : '').'>Pago</option>
            <option value="release" '.($item->status == "release" ? 'selected' : '').'>Liberado</option>
            </select>
            ';
            
            if($item->status == "pending")
                $select_status = 'Em aberto';
            elseif($item->status == "paid")
                $select_status = '<span style="color:green">Pago</span>';
            elseif($item->status == "release")
                $select_status = '<span style="color:orange">Liberado</span>';
            
            if(!empty($item->iduserfrom))
                $usu = '<i class="fas fa-user"></i> '.$this->banco->campo('users','name','id = ' . $item->iduserfrom);
            elseif(!empty($item->iduserto))
                $usu = '<i class="fas fa-thumbs-up"></i> '.$this->banco->campo('users','name','id = ' . $item->iduserto);
            
            if(!empty($item->idservicefrom))
                $ser = $this->banco->campo('services','name', 'id = ' . $item->idservicefrom);
            elseif(!empty($item->idserviceto))
                $ser = $this->banco->campo('services','name', 'id = ' . $item->idserviceto);
            
            $this->dados['tabela'] .= '<tr>
                                   <td><span style="text-align:center"><strong>'.date('d/m/Y',strtotime($item->date_pgto)).'</strong></span></td>
                                   <td>'.$usu.'</td>
                                   <td>'.$ser.'</td>
                                   <td>'.character_limiter($item->name,3,'.').'</td>
                                   <td>'.$item->obs.'</td>
                                   <td>'.(!empty($item->percent) ? $this->preco($item->percent,false).'%' : '').'
                                   '.(!empty($item->commission) ? ' + '.$this->preco($item->commission,false).'%' : '').'
                                   </td>
                                   <td><div class="input-prepend input-append">
                                        <span class="add-on">R$</span>
                                        <input type="text" readonly="readonly" style="background-color:red;color:white;" class="input-small" 
                                        name="debit_'.$item->id.'" id="debit_'.$item->id.'" value="'.$this->preco($item->debit,false).'">
                                       
                                       </div>
                                   </td>
                                   <td><div class="input-prepend input-append">
                                       <span class="add-on">R$</span><input type="text" readonly="readonly" style="background-color:blue;color:white;" class="input-small" 
                                       name="credit_'.$item->id.'" id="credit_'.$item->id.'" value="'.$this->preco($item->credit,false).'">
                                       </div>
                                   </td>
                                   <td>'.$select_status.'</td>
                                   <td>'.((empty($item->date_updated) || ($item->date_updated == '0000-00-00 00:00:00')) ? '-' : date('d/m/Y',strtotime($item->date_updated))).'</td>
                                   </tr>';
        }
        $this->dados['tabela'] .= '</tbody></table></form>';
        $this->dados['central'] = '<section style="margin-top:-55px"><p>'.$this->dados['tabela'].'</p><div id="salvou" style="display:none;"><small style="color:green"><i class="fas fa-thumbs-up"></i> Salvo!</small></div></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
   }
   
   public function salva_atual($linha)
   {
       $cond = $this->perm(__FUNCTION__);
       
       if($cond)
       {
            echo 'fail';
            return false;
       }
       
       // Campos alteráveis
       $campos = array('status_'.$linha,'obs_'.$linha,'debit_'.$linha,'credit_'.$linha,'date_updated_'.$linha);
       
       $dados = array(
            'id' => $linha,
            'status' => strip_tags($_POST[$campos[0]]),
            'obs' => strip_tags($_POST[$campos[1]]),
            'debit' => $this->limpapreco($_POST[$campos[2]]),
            'credit' => $this->limpapreco($_POST[$campos[3]]),
            'date_updated' => (($_POST[$campos[4]] == "" || $_POST[$campos[4]] == "-") ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $_POST[$campos[4]]))))
       );
       
       $idtmp = $this->banco->atualiza('financial',$dados);
       
       echo 'ok';
       return false;
   }

   
   public function salva_atual_fun($linha)
   {
       $cond = $this->perm(__FUNCTION__);
       
       if($cond)
       {
            echo 'fail';
            return false;
       }
           
       // Campos alteráveis
       $campos = array('status_'.$linha,'obs_'.$linha,'credit_'.$linha,'date_updated_'.$linha);
       
       $dados = array(
            'id' => $linha,
            'status' => strip_tags($_POST[$campos[0]]),
            'obs' => strip_tags($_POST[$campos[1]]),
            'credit' => str_replace(array(","),array("."),$_POST[$campos[2]]),
            'date_updated' => (($_POST[$campos[3]] == "" || $_POST[$campos[3]] == "-") ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(str_replace('/', '-', $_POST[$campos[3]]))))
       );
       
       $idtmp = $this->banco->atualiza('financial',$dados);
       
       echo 'ok';
       return false;
   }
   

}
?>