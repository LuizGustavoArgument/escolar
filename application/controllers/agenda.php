<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agenda extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('agenda');
        $this->load->model('sistema','banco',TRUE);
        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        $this->load->library('form');
        
        $this->session->set_userdata('pagina_atual', 'agenda');
    }
    
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_cadastros',$c);

        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                break;
        // Por enquanto, o sistema está sem controle de permissões
        // outros = proibido
            default:
                $proibido = array('alterarativo', 'remover', 'insumo', 'index', 'coord', 'inserir','listar','eu','alterar','listargrupo', 'alterargrupo', 'inserirgrupo');
                
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não tem permissão para acessar esta página. Contate o administrador.');
            redirect('home');
        }
    }
    
    public function index($result='')
    {
        $this->perm(__FUNCTION__);
            
        $html = '<div class="row">';
        $html .= '<p>Em construção.</p>';
        $html .= '</div>';
        
        $this->dados['central'] = '<section>'.$html.'</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function verifica_datas($tipo,$id,$ajax=true)
    {
        if(!is_numeric($id) || empty($id)) return false;
        if(empty($tipo)) return false;
        
        $dias = array('domingo','segunda','terça','quarta','quinta','sexta','sábado');
        $resultado = '';
        $areas = '';
        switch($tipo)
        {
            case 'paciente' :
                // A partir do paciente selecionado, verifica as melhores datas para ele (se configurado)
                $lis = $this->banco->listagem('costs','name','iduserto = '. (int) $id . ' and active = "yes" and type = "object"');
                if(!empty($lis)) :
                $resultado .= '<strong>Atenção!</strong> Horários preferenciais deste paciente:<br><ul>';
                foreach($lis as $item)
                {
                    if(!empty($item->value) && !empty($item->stdrvalue))
                    $resultado .= '<li>'.$dias[$item->name].': de '.$item->value.'h a '.$item->stdrvalue.'h.</li>';
                }
                $resultado .= '</ul>';
                endif;
                // Busca os convênios
                $areas = '<h5>Convênio:</h5>';
                $res = $this->banco->relacao('users',$id,'users');
                
                if(empty($res))
                {
                    $areas .= '<p><em>Não possui convênio.</em><input type="hidden" name="convenios" id="convenios" value="0" /></p>';
                }
                else 
                {
                    $areas .= '<label class="radio"><input type="radio" id="convenios0" name="convenios" value="0"> Nenhum convênio (particular)</label>';
                    $lis2 = $this->banco->listagem('users','name','id in ('.implode(',',$res).') and islocked = "no"');
                    foreach($lis2 as $item2)
                    {
                        $areas .= '<label class="radio"><input type="radio" name="convenios" id="convenios'.$item2->id.'" value="'.$item2->id.'"> '.$item2->name.'</label>';
                    }
                }
                $grpk = 2;
            break;
                
            case 'profissional' :
                // A partir do paciente selecionado, verifica as melhores datas para ele (se configurado)
                $lis = $this->banco->listagem('costs','name','iduserto = '. (int) $id . ' and active = "yes" and type = "object"');
                if(!empty($lis)) :
                $resultado = '<strong>Atenção!</strong> Horários preferenciais deste profissional:<br><ul>';
                foreach($lis as $item)
                {
                    if(!empty($item->value) && !empty($item->stdrvalue))
                    $resultado .= '<li>'.$dias[$item->name].': de '.$item->value.'h a '.$item->stdrvalue.'h.</li>';
                }
                $resultado .= '</ul>';
                endif;
                $grpk = 4;
            break;
        }
        
        $ret = '<p></p><div class="alert alert-success" id="alertdatas" style="margin-bottom:0px">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            '.$this->verifica_profile($id,$grpk,false).'
            '.$areas.'
            </div>';
        
        if($resultado)
        {    
            $ret = '<p></p><div class="alert alert-danger" id="alertdatas" style="margin-bottom:0px">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            '.$this->verifica_profile($id,$grpk,false).'
            '.$resultado.'
            '.$areas.'
            </div>';
        }
        
        if($ajax)
            echo $ret;
        else
            return $ret;
    }

    public function verifica_profile($id,$grp="",$ajax=true)
    {
        if(!is_numeric($id) || empty($id)) return false;
            
        $lis = $this->banco->listagem_users('users.id = '.$id);    
        
        foreach($lis as $item) :
            // Pacientes
            if($grp == 2)
            {
                $nome = $item->name;
                $info = '<address>
                  <strong>'.$item->email.'</strong><br>
                  '.$item->address.'<br>
                  '.$item->city.', '.$item->uf.' '.$item->mobile.'<br>
                  <abbr title="Telefone">Tel:</abbr> '.$item->phone.' '.$item->contact_fax.'
                </address>';
                $foto = (!empty($item->logo) ? 'src="'.site_url('uploads/'.$item->logo).'"' : 'data-src="holder.js/64x64"');
            }
            
            // Profissionais
            
            $areas = '';
            $res = $this->banco->relacao('products','','users',$id);
            
            if(empty($res))
            {
                $areas = '<em>Não atende nenhuma área em específico.</em><br>';
            }
            else 
            {
                $lis2 = $this->banco->listagem('products','name','id in ('.implode(',',$res).') and active = 1');
                foreach($lis2 as $item2)
                {
                    $areas .= '<strong>'.$item2->name.'</strong><br>';
                }
            }
            
            if($grp == 4)
            {
                $nome = $item->name;
                $info = '<address>
                  '.$areas.'
                  '.$item->email.'<br>
                  <abbr title="Telefone">Tel:</abbr> '.$item->phone.' '.$item->contact_fax.'
                </address>';
                $foto = (!empty($item->logo) ? 'src="'.site_url('uploads/'.$item->logo).'"' : 'data-src="holder.js/64x64"');
            }
        endforeach;
            
        $ret = '<div class="media">
              <a class="pull-left" href="#">
                <img class="media-object" style="width:64px;" '.$foto.'>
              </a>
              <div class="media-body">
                <h4 class="media-heading">'.$nome.'</h4>
                <p>'.$info.'</p>
              </div>
            </div>';
            
        if($ajax)
            echo $ret;
        else
            return $ret;
    }

    public function verifica_profissionais($id,$ajax=true)
    {
        if(!is_numeric($id) || empty($id)) return false;
            
        $pacs = '<select name="profissionais" id="profissionais" class="col-4">';
        $pacs .= '<option value="0">Escolha um profissional...</option>';
        // kind = 4 são profissionais
        $usus = $this->geraUsersByGroupKind(4);
        if(!empty($usus)) :
        $lis = $this->banco->listagem('users','name','id in ('.implode(",",$usus).')');
        foreach($lis as $i)
        {
            $pacs .= '<option value="'.$i->id.'">'.$i->name.'</option>';
        }
        endif;
        $pacs .= '</select>';
        
        if($ajax)
            echo $pacs;
        else {
            return $pacs;
        }
    }
    
    public function verifica_tratamento($id,$ajax=true)
    {
        if(!is_numeric($id) || empty($id)) return false;
            
        $trats = $this->banco->listagem('products','id','id = '.(int) $id);
        $ret = '<p><em>Este tratamento não possui recursos.</p></em>';
        foreach($trats as $t)
        {
            // Monta form de resources
            $resources = $this->banco->relacao('resources','','products',$t->id);
            $ret = '';
            if(!empty($resources))
            {
                $ret .= '<table class="table table-striped">';
                $resrc = $this->banco->listagem('resources','name','active = "1" and id in ('.implode(',',$resources).')');
                $auxusers = $this->geraUsersByGroupKind(6);
                $auxes = array();
                if(!empty($auxusers))
                {
                    $auxes = $this->banco->listagem('users','name','id in ('.implode(',',$auxusers).')');
                    $aux = '';
                    foreach($auxes as $a)
                    {
                        $aux .= '<option value="'.$a->id.'">'.$a->name.'</option>'; 
                    }
                }
                
                foreach($resrc as $r)
                {
                    $ret .= '<tr>
                             <td><label for="recursos'.$r->id.'" class="checkbox">
                              <input type="checkbox" name="recursos[]" value="'.$r->id.'" id="recursos'.$r->id.'" />
                              <abbr title="'.strip_tags($r->desc).'">'.$r->name.'</abbr></label>
                             </td>
                             <td>
                             <label for="auxiliar'.$r->id.'">Auxiliar</label>
                             <select name="auxiliar'.$r->id.'" id="auxiliar'.$r->id.'">
                             <option value="0">Sem auxiliar</option>
                             '.(!empty($auxes) ? $aux : '').'
                             </select>
                             </td>
                             <td><label for="obs'.$r->id.'">Atributos</label>
                             <input type="text" name="obs'.$r->id.'" id="obs'.$r->id.'" />
                             </td>
                             </tr>';
                }
                $ret .= '</table>';
            }
            
            
            if($t->group == 'Avaliação')
            {
                //Monta form de avaliacao
                $ret .= '
                <table class="table table-striped" width="400" border="0">
                  <tr>
                    <td width="122">Desenvolvimento:</td>
                    <td width="262"><input type="text" name="aval_desenv" id="aval_desenv" value="" /></td>
                  </tr>
                  <tr>
                    <td>Diagnóstico:</td>
                    <td><input type="text" name="aval_diag" id="aval_diag" value="" /></td>
                  </tr>
                  <tr>
                    <td>Indicação:</td>
                    <td><input type="text" name="aval_indic" id="aval_indic" value="" /></td>
                  </tr>
                </table>
                ';
            }
        }
        
        if($ajax)
            echo $ret;
        else
            return $ret;
    }
    
    public function novo($ini="",$fim="")
    {
        $this->perm(__FUNCTION__);
        
        $js = '
        <script>
        $(function() {
            
            var checkin = $(\'#dp3\').datepicker({
            onRender: function(date) {
            return date.valueOf() <= now.valueOf() ? \'disabled\' : \'\';
            }
            }).on(\'changeDate\', function(ev) {
            if (ev.date.valueOf() >= checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
            }
            checkin.hide();
            $(\'#dp4\')[0].focus();
            }).data(\'datepicker\');
            var checkout = $(\'#dp4\').datepicker({
            onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? \'disabled\' : \'\';
            }
            }).on(\'changeDate\', function(ev) {
            checkout.hide();
            }).data(\'datepicker\');
            
            $("select#pacientes").change(function(){
                var pac = $(this).find("option:selected").val();
                if(pac > 0)
                {
                    $.get("'.site_url('agenda/verifica_datas/paciente').'/"+pac,function(data){
                        $("#result_paciente").html(data).fadeIn();
                    });
                }
                else 
                {
                	$("#result_paciente").html("").fadeOut(); 
                }
            });
            
            $("select#tratamentos").change(function(){
                var pac = $(this).find("option:selected").val();
                if(pac > 0)
                {
                    $.get("'.site_url('agenda/verifica_profissionais/').'/"+pac,function(data){
                        $("#result_tratamentos").html(data).fadeIn();
                        $("select#profissionais").change(function(){
                            var pac = $(this).find("option:selected").val();
                            if(pac > 0)
                            {
                                $.get("'.site_url('agenda/verifica_datas/profissional').'/"+pac,function(datap){
                                    $("#result_profissionais").html(datap).fadeIn();
                                    var valtrat = $("select#tratamentos").find("option:selected").val();
                                    $.get("'.site_url('agenda/verifica_tratamento/').'/"+valtrat,function(datar){
                                        $("#result_recursos").html(datar).fadeIn();
                                        $("#tudook").fadeIn();
                                    });
                                });
                            }
                            else 
                            {
                                $("#result_profissionais").html("").fadeOut(); 
                            }
                        });
                    });
                    
                }
                else 
                {
                    $("#result_tratamentos").html("<em>Escolha um tratamento antes...</em>");
                    $("#result_recursos").html("<em>Escolha um profissional antes...</em>");
                }
            });
        });
        
        function enviaformulario(formulario,recursos)
        {
            if(recursos != "")
                var dados = formulario + "&" + recursos;
            else
                var dados = formulario;
            $.post("'.site_url('agenda/salvar').'",dados,function(data){
                $(\'div.modal-body#ag_resumo\').html(data);
                $(\'#myModal\').modal(\'show\');
            });
        }
        
        </script>
        ';
        
        $pacs = '<select name="pacientes" id="pacientes" class="col-4">';
        $pacs .= '<option value="0">Escolha um paciente...</option>';
        $clis = $this->banco->relacao('users','','groups',7); // 7 = paciente
        if(empty($clis)) { die("<script>alert('Não há pacientes cadastrados');history.back();</script>"); }
        $lis = $this->banco->listagem('users','name','id in ('.implode(",",$clis).')');
        foreach($lis as $i)
        {
            $pacs .= '<option value="'.$i->id.'">'.$i->name.'</option>';
        }
        $pacs .= '</select>';
        
        
        $trat = '<select name="tratamentos" id="tratamentos" class="col-4">';
        $trat .= '<option value="0">Escolha um tratamento...</option>';
        $lis = $this->banco->listagem('products','name','active = "1"');
        foreach($lis as $i)
        {
            $trat .= '<option value="'.$i->id.'">'.$i->name.'</option>';
        }
        $trat .= '</select>';
        
        $html = '<form name="agendando" id="agendando" class="form-horizontal" method="post" enctype="multipart/form-data" action="'.site_url('agenda/salvar').'">';
        
        $html .= '<div class="form-group">';
        $html .= '<label for="paciente">Paciente</label>';
        $html .= '<div class="inline">';
        $html .= $pacs;
        $html .= '&nbsp;&nbsp;&nbsp;';
        $html .= '</div>';
        $html .= '<div id="result_paciente" class="col-5 hide"></div>';
        $html .= '</div>'; // form-group
        
        $html .= '<div class="form-group">';
        $html .= '<label for="tratamentos">Tratamento</label>';
        $html .= '<div class="inline">';
        $html .= $trat;
        $html .= '&nbsp;&nbsp;&nbsp;';
        $html .= '</div>';
        $html .= '</div>'; // form-group
        
        $html .= '<div class="form-group">';
        $html .= '<label for="profissionais">Profissional</label>';
        $html .= '<div class="inline">';
        $html .= '<div id="result_tratamentos"><em>Escolha um tratamento antes...</em></div>';
        $html .= '</div>';
        $html .= '<div id="result_profissionais" class="col-5 hide"></div>';
        $html .= '</div>'; // form-group
        
        
        $html .= '<div class="form-group">';
        $html .= '<label for="date_start">Início</label>';
        $html .= '<div class="inline"><div class="input-append date" id="dp3" data-date="'.date('d/m/Y',(!empty($ini) ? $ini : strtotime('now'))).'" data-date-format="dd/mm/yyyy">';
        $html .= '<input name="date_start" id="date_start" value="'.date('d/m/Y',(!empty($ini) ? $ini : strtotime('now'))).'" type="text" size="16" class="form-control"><span class="add-on"><i class="far fa-calendar-alt"></i></span>';
        $html .= '</div>';
        $html .= '&nbsp;&nbsp;&nbsp;<input name="date_start_h" id="date_start_h" value="'.date('H',(!empty($ini) ? $ini : strtotime('now'))).'" type="number" min="0" max="23" size="2" class="col-1"> : ';
        $html .= '<input name="date_start_m" id="date_start_m" value="'.date('i',(!empty($ini) ? $ini : strtotime('now'))).'" type="number" min="0" max="59" step="10" size="2" class="col-1"></div>';
        $html .= '</div>';
        $html .= '</div>'; // form-group
        
        $html .= '<div class="form-group">';
        $html .= '<label for="date_start">Fim</label>';
        $html .= '<div class="inline"><div class="input-append date" id="dp4" data-date="'.date('d/m/Y',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" data-date-format="dd/mm/yyyy">';
        $html .= '<input name="date_end" id="date_end" value="'.date('d/m/Y',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" type="text" size="16" class="form-control"><span class="add-on"><i class="far fa-calendar-alt"></i></span>';
        $html .= '</div>';
        $html .= '&nbsp;&nbsp;&nbsp;<input name="date_end_h" id="date_end_h" value="'.date('H',(!empty($fim) ? $fim : strtotime('next hour'))).'" type="number" min="0" max="23" size="2" class="col-1"> : ';
        $html .= '<input name="date_end_m" id="date_end_m" value="'.date('i',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" type="number" min="0" max="59" size="2" class="col-1"></div>';
        $html .= '';
        $html .= '</div>'; // form-group
        
        $html .= '<div class="form-group">';
        $html .= '<label>Recursos do tratamento selecionado</label>';
        $html .= '<div class="inline">';
        $html .= '<div id="result_recursos"><em>Escolha um profissional antes...</em></div>';
        $html .= '</div>';
        $html .= '<div id="result_auxiliares" class="col-5 hide"></div>';
        $html .= '</div>'; // form-group
        
        $html .= '<div class="form-group">';
        $html .= '<p>Outras observações:</p>
                <p>
                  <label for="obs_geral"></label>
                  <textarea name="obs_geral" id="obs_geral" rows="5"></textarea>
                </p>';
        $html .= '</div>'; // form-group
        
        $html .= '<p id="tudook" class="hide">
                  <button class="btn btn-lg btn-success" type="button" onclick="var recursos = $(\'table :input\').serialize(); var formulario = $(\'form#agendando\').serialize(); enviaformulario(formulario,recursos);">Salvar consulta</button>
                  <button class="btn btn-lg" type="button" onclick="document.location.href=\''.site_url('home').'\'">Cancelar</button>
                </p>';
        
        $html .= '</form>'; // form
        
        // Modal dialog
        $html .= '<!-- Modal -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Agendamento de consulta</h3>
          </div>
          <div class="modal-body" id="ag_resumo">
            
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button>
            <button class="btn btn-primary" onclick="document.location.href=\''.site_url('home').'\'">Ir para a agenda</button>
          </div>
        </div>';
        
        $this->dados['central'] = '<section>'.$html.'</section>'.$js;
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function salvar()
    {
        $this->perm(__FUNCTION__);
        
        if(empty($_POST)) die("<div class='alert alert-danger'>Erro ao enviar formulário.</div>");
        if(empty($_POST['pacientes']) || empty($_POST['tratamentos']) || empty($_POST['profissionais']))
        {
            die("<div class='alert alert-danger'>Campos obrigatórios não foram preenchidos.</div>");
        }
        
        $datetmp = explode("/",$_POST['date_start']);
        $ini = $datetmp[2] . '-' . $datetmp[1] . '-' . $datetmp[0] . ' ' . $_POST['date_start_h'] . ':' . $_POST['date_start_m'] . ':00';
        unset($datetmp);
        $datetmp = explode("/",$_POST['date_end']);
        $fim = $datetmp[2] . '-' . $datetmp[1] . '-' . $datetmp[0] . ' ' . $_POST['date_end_h'] . ':' . $_POST['date_end_m'] . ':00';
        
        if(strtotime($ini) >= strtotime($fim))
        {
            die("<div class='alert alert-danger'>A data final precisa ser maior do que a data inicial.</div>");
        }
        
        $dados = array(
            'idstatus' => 1,
            'iduser' => $_POST['pacientes'],
            'iduserpro' => $_POST['profissionais'],
            'idproduct' => $_POST['tratamentos'],
            'date_start' => $ini,
            'date_end' => $fim,
            'date_created' => date('Y-m-d H:i:s'),
            'data_array' => serialize($_POST),
            'id_createdby' => $this->session->userdata('esta_logado'),
            'active' => 1,
            'idorg' => 1
        );
        
        $s = $this->banco->insere('services',$dados);
        $ret = '';
        if($s > 0)
        {
            // Relacao de convenios (se existir)
            if(!empty($_POST['convenios']))
            {
                $this->banco->relacao_criar('users',$_POST['convenios'],'services',$s);
            }
            
            // Relacao de recursos (se existir)
            if(!empty($_POST['recursos']))
            {
                foreach($_POST['recursos'] as $r)
                    $this->banco->relacao_criar('resources',$r,'services',$s,$_POST['auxiliar'.$r],$_POST['obs'.$r]);
            }
            
            // Sucesso
            $ret = "<div class='alert alert-success'>Agendamento efetuado!</div>";
            echo $ret.$this->resumo($s,false);
        }
        return false;
    }
    
    public function resumo($id,$ajax=true)
    {
        $this->perm(__FUNCTION__);
        
        $dias = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
        
        $lis = $this->banco->listagem('services','id','id = '. (int) $id);
        $ret = '';
        foreach($lis as $i) :
        
        $ret .= '<h4 style="color:'.$this->banco->campo('status','color','id = '.$i->idstatus).'">'.$this->banco->campo('status','name','id = '.$i->idstatus).'</h4>';
        
        //$ret .= '<h5>Início/Término</h5>';
        $ret .= '<p><strong>'. $dias[date('w',strtotime($i->date_start))] . ' - ' . date('d/m/Y \d\a\s H:i',strtotime($i->date_start)) 
        . ' até as ' . date('H:i',strtotime($i->date_end)) . '</strong></p>';
        
        $ret .= '<h5>Tratamento:</h5>';
        $ret .= '<p>'.$this->banco->campo('products','name','id = '.$i->idproduct).'</p>';
        
        $ret .= '<h5>Paciente:</h5>';
        $ret .= $this->verifica_profile($i->iduser,2,false);
        
        $ret .= '<h5>Profissional:</h5>';
        $ret .= $this->verifica_profile($i->iduserpro,4,false);
        
        $ret .= '<h5>Outras informações:</h5>';
        $super = unserialize($i->data_array);
        // Se tem convenio, mostra
        if(!empty($super['convenios']))
        {
            $ret .= '<p><strong>Convênio: </strong>'.$this->banco->campo('users','name','id = '.$super['convenios']).'</p>';
        }
        
        $r_num = 1;
        foreach($super['recursos'] as $r)
        {
            $ret .= '<p><strong>Recurso '.$r_num.': </strong><br>'.$this->banco->campo('resources','name','id = '.$r).'<br>';
            if($super['auxiliar'.$r] > 0)
                $ret .= 'Auxiliado por '.$this->banco->campo('users','name','id = '.$super['auxiliar'.$r]).'<br>';
            if(!empty($super['obs'.$r]))
                $ret .= '<em>'.$super['obs'.$r].'</em><br>';
            $r_num++;
        }
        
        if(!empty($super['obs_geral']))
        {
            $ret .= '<p><strong>Obs: </strong><br>'.$super['obs_geral'].'</p>';
        }
        
        endforeach;
        
        // Resumo do agendamento
        if($ajax)
            echo $ret;
        else
            return $ret;
        
    }
    
    public function listar()
    {
        $this->perm(__FUNCTION__);
            
        $this->dados['central'] = '<section><p>Em construção.</p></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function status()
    {
        $this->perm(__FUNCTION__);
            
        $this->dados['central'] = '<section><p>Em construção.</p></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
}