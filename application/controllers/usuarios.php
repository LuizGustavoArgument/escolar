<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends MY_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->geraMenu(anchor('home/index','home'). ' / usu&aacute;rios');
		$this->load->model('sistema','banco',TRUE);
		if ($this->session->userdata('esta_logado') == 0)
		{
			$this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');		
			redirect('login');
		}
		// Somente admins acessam esta area!
		if ($this->checaPermissao() != 1)
		{
			$this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');		
			redirect('home');
		}
		
		// verifica quais grupos o usuario logado pode gerenciar
		if($this->session->userdata('grpadmin') != 0)
		{
			$this->grp_filter = $this->session->userdata('grpadmin');
		}
		else
		{
			$lstmp = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
			foreach($lstmp as $ls)
			{
				$this->grp_filter[] = $ls->id;
			}
		}
		
    }

	public function index($pesq="")
	{
		$condicao = ' and isadmin = "no" ';	
		if(!empty($pesq))
		{
			// pega apenas os usuarios que pertencem ao pesquisador
			$pesq = (int) $pesq;
			if (($this->session->userdata('nivel2') == 2 && $this->session->userdata('esta_logado') == $pesq) ||
				 $this->session->userdata('nivel2') == 0 || $this->session->userdata('nivel2') == 1)
				 {
				 	$ids = $this->banco->campo('relationship','idobj1','type = "users___groups" and content like "'.$pesq.'"');
					if (is_array($ids))
				 		$condicao .= ' and id in ('.implode(',',$ids).')';
					elseif ($ids > 0)
						$condicao .= ' and id = '.$ids;
					else die('Nenhum usuário encontrado.');
				 }
		}
		$res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
		foreach($res as $item)
			$this->dados['grupos'][$item->id] = $item->name;
		
		$this->dados['resultado_query'] = $rq = $this->banco->listagem('users','name','idorg = '. $this->org['id'] . $condicao);
		
		foreach($rq as $item)
		{
			$this->dados['usu_progress'][$item->id] = $this->progresso(1,false,$item->id);
		}
		
		$this->load->view('_cabecalho',$this->cabecalho);
		$this->load->view('usuarios/listar',$this->dados);
		$this->load->view('_rodape',$this->rodape);
	}
	
	public function listar($pesq="")
	{
		$this->index($pesq);
	}
	
	public function liberar($id)
	{
		$id = (int) $id;
		if (empty($id)) { echo 'fail'; return false; }
		$dados = array('id' => $id, 'islocked' => 'no');
		$res = $this->banco->atualiza('users',$dados);
		if ($res)
			echo 'ok';
		else
			echo 'fail';
		return false;
	}
	
	public function criar($grp="")
	{
		$this->form_validation->set_rules('username','Login','trim|required|min_length[14]');
		$this->form_validation->set_rules('password','Senha','trim|required');
		$this->form_validation->set_rules('password_again','Confirmação de senha','trim|required|matches[password]');
		$this->form_validation->set_rules('name','Nome','required');
		$this->form_validation->set_rules('email','E-mail','required|valid_email');
		
		$this->geraJavaScript('usercreate');
		
		if(!empty($grp))
			$this->dados['grupo_marcado'] = $grp;
		else {
			$this->dados['grupo_marcado'] = '';
		}
		
		$this->dados['grupos'] = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
		
		$this->dados['enquetes'] = $this->banco->listagem('polls','title','idorg = '. $this->org['id']);
		
		// Para a geracao de cadastros automaticos
		$this->dados['lastid'] = '';
		
		if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('usuarios/criar',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{
			$salt = $this->generatePassword();
			$password = md5($this->input->post('password') . $salt);
			
			// verifica se o username ja nao existe
			$username = $this->input->post('username');
			$res = $this->banco->campo('users','username','username like "'.$username.'"');
			if (!empty($res))
			{
				$this->session->set_flashdata('error','Login informado ('.$username.') já existe. Tente outro.');
				redirect('usuarios/criar');
			}
			
			// verifica se o email ja nao existe
			$email = $this->input->post('email');
			$res = $this->banco->campo('users','email','email like "'.$email.'"');
			if (!empty($res))
			{
				$this->session->set_flashdata('error','O e-mail informado ('.$email.') já existe.');
				redirect('usuarios/criar');
			}

			// Associando grupos
			$grupos = $this->input->post('grupos');
			$enquetes = $this->input->post('enquetes');
			if (empty($grupos))
			{
				$this->session->set_flashdata('error','O usuário precisa fazer parte de algum grupo.');
				redirect('usuarios/criar');
			}
			
			
			$idhist = $this->input->post('idhist');
			$isadmin = $this->input->post('isadmin');
			
			
			$dados = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'username' => $username,
				'password' => $password,
				'salt' => $salt,
				'phone' => $this->input->post('phone'),
				'isadmin' => $isadmin,
				'islocked' => $this->input->post('islocked'),
				'city' => $this->input->post('city'),
				'uf' => $this->input->post('uf'),
				'date_created' => date('Y-m-d H:i:s'),
				'idhist' => $idhist,
				'idorg' => $this->input->post('idorg')
			);
			$iduser = $this->banco->insere('users',$dados);
			if ($iduser > 0)
			{
				foreach($grupos as $item)
				{
					$this->banco->relacao_criar('users',$iduser,'groups',$item);
				}

				if (in_array(5,$grupos) && !empty($enquetes))
				foreach($enquetes as $item2)
				{
					$this->banco->relacao_criar('users',$iduser,'polls',$item2);
				}

				
				// Registra no historico
				$this->historico('user','criou um novo usuário ('.$iduser.') de login <b>'. $dados['username'] .'</b>');
				
				$this->session->set_flashdata('success','Usuário '.$dados['username'].' criado.');
				redirect('grupos/index');
			}
			else
			{
				$this->session->set_flashdata('error','Uusário '.$dados['name'].' não foi criado devido a um erro no sistema.');
				redirect('usuarios/criar');
			}
		}
	}

	public function editar($uid)
	{
		if (empty($uid) || (!is_numeric($uid)))
		{
			$this->session->set_flashdata('error','Chamada inválida na URL');
			redirect('usuarios/index');
		}
		
		$this->form_validation->set_rules('name','Nome','required');
		$this->form_validation->set_rules('email','E-mail','required|valid_email');
		
		$listagem = $this->banco->listagem('users','name','id = '. $uid);
		$this->dados['resultado_query'] = $listagem;
		
		$this->dados['grupos'] = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
		
		$this->dados['grupos_usuario'] = $this->banco->relacao('users',$uid,'groups');
		
		$this->dados['enquetes'] = $this->banco->listagem('polls','title','idorg = '. $this->org['id']);
		
		$this->dados['enquetes_usuario'] = $this->banco->relacao('users',$uid,'polls');
		
		$this->dados['dados_cadastrais'] = $this->banco->query('SELECT polls_itens.title, relationship.content FROM polls_itens, relationship WHERE polls_itens.idpollgroup = 1 AND polls_itens.id = relationship.idobj2 AND relationship.idobj1 = '.$uid.' AND relationship.type = "users___polls_itens";');
		
		if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('usuarios/editar',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{

			// Associando grupos
			$grupos = $this->input->post('grupos');
			$enquetes = $this->input->post('enquetes');
			if (empty($grupos))
			{
				$this->session->set_flashdata('error','O usuário precisa fazer parte de algum grupo.');
				redirect('usuarios/editar/'.$uid);
			}
			
			if ($this->input->post('change'))
				$change = $this->input->post('change');
			else 
				$change = 0;
			
			$idhist = $this->input->post('idhist');
			$isadmin = $this->input->post('isadmin');
			
			// mudando o password
			if ($change == 1)
			{
				if ($this->input->post('password') != $this->input->post('password_again'))
				{
					$this->session->set_flashdata('error','A senha redigitada não confere com a senha informada.');
					redirect('usuarios/editar/'.$uid);
				}
				$salt = $this->generatePassword();
				$password = md5($this->input->post('password') . $salt);
				
				$dados = array(
					'id' => $uid,
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'password' => $password,
					'salt' => $salt,
					'phone' => $this->input->post('phone'),
					'isadmin' => $isadmin,
					'idhist' => $idhist,
					'city' => $this->input->post('city'),
					'uf' => $this->input->post('uf'),
					'islocked' => $this->input->post('islocked'),
					'date_created' => date('Y-m-d H:i:s'),
					'idorg' => $this->input->post('idorg')
				);
			}
			else
			{
				$dados = array(
					'id' => $uid,
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'phone' => $this->input->post('phone'),
					'isadmin' => $isadmin,
					'islocked' => $this->input->post('islocked'),
					'idhist' => $idhist,
					'city' => $this->input->post('city'),
					'uf' => $this->input->post('uf'),
					'date_created' => date('Y-m-d H:i:s'),
					'idorg' => $this->input->post('idorg')
				);
			}
			
			$respuser = $this->banco->atualiza('users',$dados);
			if ($respuser > 0)
			{
				// Deleta o usuario pra sempre caso esteje marcada a opção
				$removeu = $this->input->post('remover');
				if ($removeu > 0)
				{
					// Registra no historico
					$this->historico('user','removeu o usuário ('.$uid.')');
					$del = $this->banco->remover('users',$removeu);
				}
				else $removeu = 0;
				
				// Grava set de grupos
				$mexeu = $this->input->post('mexeu');
				
				if ($mexeu > 0 && $removeu == 0)
				{
					$res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
					$grp = $this->input->post('grupos');
					foreach($res as $item)
					{
						if (in_array($item->id,$grp))
							$g[] = $item->id;
					}
					
					if (!empty($g))
					{
						foreach($g as $valor)
						{
							$u[] = $uid;
							$v[] = 'none';
						}
					}
					else
						{
							$g = array('');
							$u = array($uid);
							$v = array('none');
						}
					
					$restmp = $this->banco->relacao_alterar('users',$u,'groups',$g,$v);
					
					// Registra no historico
					$this->historico('user','alterou o usuário ('.$uid.') nas configurações de grupo');
					
					unset($u,$res,$item,$g,$v);
					
					$res = $this->banco->listagem('polls','title','idorg = '. $this->org['id']);
					$enq = $this->input->post('enquetes');
					foreach($res as $item)
					{
						if (in_array($item->id,$enq))
							$g[] = $item->id;
					}
					
					if (!empty($g))
					{
						foreach($g as $valor)
						{
							$u[] = $uid;
							$v[] = 'none';
						}
					}
					else
						{
							$g = array('');
							$u = array($uid);
							$v = array('none');
						}
					
					$restmp = $this->banco->relacao_alterar('users',$u,'polls',$g,$v);
					
					// Registra no historico
					$this->historico('user','alterou o usuário ('.$uid.') nas configurações de enquete');
				}
				
				// Registra no historico
				$this->historico('user','alterou o usuário ('.$uid.')');
					
				$this->session->set_flashdata('success','Usuário '.$dados['name'].' alterado.');
				redirect('grupos/index');
			}
			else
			{
				$this->session->set_flashdata('error','Usuário '.$dados['name'].' não foi alterado devido a um erro no sistema.');
				redirect('usuarios/editar/'.$uid);
			}
		}
	}

	public function check_username()
	{
		$username = $_POST['username'];
		$check = false;
		
		if($check == false)
			$check = $this->validaCPF($username);
		
		if($check == false)
			$check = $this->validaCNPJ($username);
		
		if(!$check)
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/cross.png" alt="erro" align="left"/> &nbsp;&nbsp;CPF ou CNPJ inválido!</small>';
			return false;
		}
		
		if (empty($username)) return false;
		$res = $this->banco->campo('users','username','username like "'.$username.'"');
		if (empty($res))
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/tick.png" alt="ok" align="left"/>&nbsp;&nbsp;Disponível.</small>';
		}
		else
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/cross.png" alt="erro" align="left"/> &nbsp;&nbsp;Este CPF/CNPJ já existe! Digite outro.</small>';
		}
		return false;
	}
	
	public function check_email()
	{
		$this->load->helper('email');
		if(!valid_email($_POST['email']))
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/cross.png" alt="erro" align="left"/> &nbsp;&nbsp;E-mail inválido! Digite outro.</small>';
			return false;
		}
		
		$username = $_POST['email'];
		if (empty($username)) return false;
		$res = $this->banco->campo('users','email','email like "'.$username.'"');
		if (empty($res))
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/tick.png" alt="ok" align="left"/>&nbsp;&nbsp;Disponível.</small>';
		}
		else
		{
			echo '<small><img src="'. base_url() .'css/blueprint/plugins/buttons/icons/cross.png" alt="erro" align="left"/> &nbsp;&nbsp;Este e-mail já existe! Digite outro.</small>';
		}
		return false;
	}

	private function generatePassword($length=8,$level=1)
	{

	   list($usec, $sec) = explode(' ', microtime());
	   srand((float) $sec + ((float) $usec * 100000));
	
	   $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
	   $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   $validchars[3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";
	
	   $password  = "";
	   $counter   = 0;
	
	   while ($counter < $length) {
		 $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level])-1), 1);
	
		 if (!strstr($password, $actChar)) {
			$password .= $actChar;
			$counter++;
		 }
	  }

     return $password;
	}

	private function validaCPF($cpf) 
	{
      $soma = 0;
      
	  $cpf = str_replace("/","", str_replace("-","",str_replace(".","",$cpf)));
	   
      if (strlen($cpf) != 11)
         return false;
       
      // Verifica 1º digito      
      for ($i = 0; $i < 9; $i++) {         
         $soma += (($i+1) * $cpf[$i]);
      }
 
      $d1 = ($soma % 11);
       
      if ($d1 == 10) {
         $d1 = 0;
      }
       
      $soma = 0;
       
      // Verifica 2º digito
      for ($i = 9, $j = 0; $i > 0; $i--, $j++) {
         $soma += ($i * $cpf[$j]);
      }
       
      $d2 = ($soma % 11);
       
      if ($d2 == 10) {
         $d2 = 0;
      }      
       
      if ($d1 == $cpf[9] && $d2 == $cpf[10]) {
         return true;
      }
      else {
         return false;
      }
   }

   // VERFICA CNPJ
   private function validaCNPJ($cnpj) 
   {
      	
	  $cnpj = str_replace("/","", str_replace("-","",str_replace(".","",$cnpj)));
		
      if (strlen($cnpj) != 14)
         return false;
 
      $soma = 0;
       
      $soma += ($cnpj[0] * 5);
      $soma += ($cnpj[1] * 4);
      $soma += ($cnpj[2] * 3);
      $soma += ($cnpj[3] * 2);
      $soma += ($cnpj[4] * 9);
      $soma += ($cnpj[5] * 8);
      $soma += ($cnpj[6] * 7);
      $soma += ($cnpj[7] * 6);
      $soma += ($cnpj[8] * 5);
      $soma += ($cnpj[9] * 4);
      $soma += ($cnpj[10] * 3);
      $soma += ($cnpj[11] * 2);
 
      $d1 = $soma % 11;
      $d1 = $d1 < 2 ? 0 : 11 - $d1;
 
      $soma = 0;
      $soma += ($cnpj[0] * 6);
      $soma += ($cnpj[1] * 5);
      $soma += ($cnpj[2] * 4);
      $soma += ($cnpj[3] * 3);
      $soma += ($cnpj[4] * 2);
      $soma += ($cnpj[5] * 9);
      $soma += ($cnpj[6] * 8);
      $soma += ($cnpj[7] * 7);
      $soma += ($cnpj[8] * 6);
      $soma += ($cnpj[9] * 5);
      $soma += ($cnpj[10] * 4);
      $soma += ($cnpj[11] * 3);
      $soma += ($cnpj[12] * 2);
       
       
      $d2 = $soma % 11;
      $d2 = $d2 < 2 ? 0 : 11 - $d2;
       
      if ($cnpj[12] == $d1 && $cnpj[13] == $d2) {
         return true;
      }
      else {
         return false;
      }
   }

}

/* End of file usuarios.php */
/* Location: ./application/controllers/usuarios.php */