<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->geraMenu('home');
        $this->load->model('entidades');
		$this->load->model('sistema','banco',TRUE);
		//$this->load->model('formularios','formularios',TRUE);
		if ($this->session->userdata('esta_logado') == 0)
		{
			$this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');		
			redirect('login');
		}
        
        // Configurando os demais menus suspensos
        $this->ocultarMenus();
        
        $this->session->set_userdata('pagina_atual', 'home');
		$this->load->library('form');
    }
    
    public function dashboard($result='')
    {
        $filt = '';
        if(is_numeric($result))
        {
            if($this->banco->isUser('cliente',$this->session->userdata('esta_logado')))
            {
                redirect("login");
                return;
            }
            // Filtra pelo código do cliente informado
            $s = $this->banco->idservicos(array($result));
            if(!empty($s))
                $filt = $s;
        }
            
        $this->checaPermissao();
        
        $this->dados['tabela'] = '<div id="calendar" style="margin:0 auto;"></div>';
        
        $js = "<script>
        $(document).ready(function() {
    
        
        });
        </script>";
        
        $this->dados['tabela'] = '';
        
        // Lista a série que o usuário está
        $lis = $this->banco->relacao('users',$this->session->userdata('esta_logado'),'users');
        if(empty($lis)) $serie = '';
        else foreach($lis as $item)
        {
            $serie = $this->banco->campo('users','name','id = '.$item) . '&nbsp;&nbsp;&nbsp;<small>'.$this->banco->campo('users','email','id = '.$item).'</small>';
        }
        
        // Se for aluno...
        $cpl = '';
        $s = array();
        if($this->isAluno() && !empty($serie)) {
            foreach($lis as $item) {
                $s = array_merge($s,$this->banco->relacao('products','','users',$item));
            }
            $cpl = " and id in (".implode(',',$s).")";
        }
        
        // Lista de disciplinas
        $lis = $this->banco->listagem('products','name','active = 1'.$cpl);
        $html = '<h3 style="color:black;">'.$serie.'</h3>';
        $html .= '<ul class="thumbnails">';
        $x = 0;
        foreach($lis as $item)
        {
            $html .= '
              <li class="col-4">
                <div class="thumbnail">
                  <div class="caption">
                    <h3>'.$item->name.'</h3>
                    <p>'.$item->desc.'</p>
                    <p><a href="'.site_url('produtos/aulas/'.$item->abrev).'" class="btn btn-primary">Acessar</a> '
                    . ($this->isAdmin() ? '<a href="'.site_url('produtos/alterar/'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time())).'" class="btn">Mudar recado</a>' : '').'</p>
                  </div>
                </div>
              </li>
          ';
            $x++;
            if($x % 3 == 0) { $html .= '</ul><ul class="thumbnails">'; }
        }
        $html .= '</ul>';
        $this->dados['tabela'] .= $html;
        
        $this->dados['central'] = $js.'<section>'.  $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function eventos($filt="")
    {
        $ret = array();
        
        // TODO: criar filtro de acordo com o range de data, para não ficar pesado!
        $lis = $this->banco->listagem('services','id','active = 1');
        
        if(!empty($lis))
        foreach($lis as $item) :
            
            unset($temp);
            $temp = explode(" ",$item->date_start);
            list($y1,$m1,$d1) = explode("-",$temp[0]);
            list($h1,$i1,$s1) = explode(":",$temp[1]);
            $m1--;
            
            unset($temp);
            $temp = explode(" ",$item->date_end);
            list($y2,$m2,$d2) = explode("-",$temp[0]);
            list($h2,$i2,$s2) = explode(":",$temp[1]);
            $m2--;
            
            $ret[] = '
                     {
                        id: '.$item->id.',
                        title: \'['.$this->banco->campo('products','abrev','id = '.$item->idproduct).'] '.word_limiter($this->banco->campo('users','name','id = '.$item->iduser),2,'').' ('.word_limiter($this->banco->campo('users','name','id = '.$item->iduserpro),2,'').')\',
                        start: new Date('.$y1.', '.$m1.', '.$d1.', '.$h1.', '.$i1.'),
                        end: new Date('.$y2.', '.$m2.', '.$d2.', '.$h2.', '.$i2.'),
                        allDay: false,
                        color: \''.$this->banco->campo('status','color','id = '.$item->idstatus).'\',
                        url: \''.site_url('home/editaragenda/'.$item->id).'\'
                     }';
        
        endforeach;
        
        return implode(','."\n",$ret);
    }
    
    public function editaragenda($id)
    {
        if(empty($id))
        {
            redirect('home');
            return false;
        }
        else 
        {
            $js = '<script>
            $(document).ready(function(){
                $("button#mudastatus").click(function(){
                    var tipo = $(this).data("codigo");
                    var id = $(this).data("id");
                    $.get("'.site_url('home/alterastatus').'/"+tipo+"/"+id,function(data){
                        alert(data);
                        document.location.reload();
                    });
                });
                $("button#mudaagenda").click(function(){
                    var id = $(this).data("id");
                    var stat = $(this).data("status");
                    var dados = $("form#agendando").serialize();
                    $.post("'.site_url('home/alteraagenda').'/"+id+"/"+stat,dados,function(data){
                        alert(data);
                        document.location.reload();
                    });
                });
                var checkin = $(\'#dp3\').datepicker({
                onRender: function(date) {
                return date.valueOf() <= now.valueOf() ? \'disabled\' : \'\';
                }
                }).on(\'changeDate\', function(ev) {
                if (ev.date.valueOf() >= checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
                }
                checkin.hide();
                $(\'#dp4\')[0].focus();
                }).data(\'datepicker\');
                var checkout = $(\'#dp4\').datepicker({
                onRender: function(date) {
                return date.valueOf() <= checkin.date.valueOf() ? \'disabled\' : \'\';
                }
                }).on(\'changeDate\', function(ev) {
                checkout.hide();
                }).data(\'datepicker\');
            });
            </script>';
            $tabela = '<form name="agendando" id="agendando" method="post">';
            $html = '';
            $lis = $this->banco->listagem('services','id','id = '.(int) $id);
            foreach($lis as $item)
            {
                if($item->idstatus == 2) // reagendar
                {
                    $html .= '<h4>Selecione uma nova data abaixo:</h4>';
                    $html .= '<div class="form-group">';
                    $html .= '<label for="date_start">Início</label>';
                    $html .= '<div class="inline"><div class="input-append date" id="dp3" data-date="'.date('d/m/Y',(!empty($ini) ? $ini : strtotime('now'))).'" data-date-format="dd/mm/yyyy">';
                    $html .= '<input name="date_start" id="date_start" value="'.date('d/m/Y',(!empty($ini) ? $ini : strtotime('now'))).'" type="text" size="16" class="form-control"><span class="add-on"><i class="far fa-calendar-alt"></i></span>';
                    $html .= '</div>';
                    $html .= '&nbsp;&nbsp;&nbsp;<input name="date_start_h" id="date_start_h" value="'.date('H',(!empty($ini) ? $ini : strtotime('now'))).'" type="number" min="0" max="23" size="2" class="col-1"> : ';
                    $html .= '<input name="date_start_m" id="date_start_m" value="'.date('i',(!empty($ini) ? $ini : strtotime('now'))).'" type="number" min="0" max="59" step="10" size="2" class="col-1"></div>';
                    $html .= '';
                    $html .= '</div>'; // form-group
                    
                    $html .= '<div class="form-group">';
                    $html .= '<label for="date_end">Fim</label>';
                    $html .= '<div class="inline"><div class="input-append date" id="dp4" data-date="'.date('d/m/Y',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" data-date-format="dd/mm/yyyy">';
                    $html .= '<input name="date_end" id="date_end" value="'.date('d/m/Y',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" type="text" size="16" class="form-control"><span class="add-on"><i class="far fa-calendar-alt"></i></span>';
                    $html .= '</div>';
                    $html .= '&nbsp;&nbsp;&nbsp;<input name="date_end_h" id="date_end_h" value="'.date('H',(!empty($fim) ? $fim : strtotime('next hour'))).'" type="number" min="0" max="23" size="2" class="col-1"> : ';
                    $html .= '<input name="date_end_m" id="date_end_m" value="'.date('i',(!empty($fim) ? $fim : strtotime('+1 hour'))).'" type="number" min="0" max="59" size="2" class="col-1"></div>';
                    $html .= '';
                    $html .= '</div>'; // form-group
                    $tabela .= '<blockquote>'.$html.'</blockquote><button type="button" id="mudaagenda" data-status="'.$item->idstatus.'" data-id="'.$item->id.'">Salvar</button><hr>';
                }

                if($item->idstatus == 7) // avaliado
                {
                    $dataarray = $this->banco->campo('services','data_array','id = '.$id);
                    $dataarray = unserialize($dataarray);
                    
                    $a1 = (!empty($dataarray['aval_desenv']) ? $dataarray['aval_desenv'] : '');
                    $a2 = (!empty($dataarray['aval_diag']) ? $dataarray['aval_diag'] : '');
                    $a3 = (!empty($dataarray['aval_indic']) ? $dataarray['aval_indic'] : '');
                        
                    $html .= '<h4>Descreva sobre a avaliação abaixo:</h4>';
                    $html .= '
                    <table class="table table-striped" width="400" border="0">
                      <tr>
                        <td width="122">Desenvolvimento:</td>
                        <td width="262"><input type="text" class="col-6" name="aval_desenv" id="aval_desenv" value="'.$a1.'" /></td>
                      </tr>
                      <tr>
                        <td>Diagnóstico:</td>
                        <td><input type="text" name="aval_diag" class="col-6" id="aval_diag" value="'.$a2.'" /></td>
                      </tr>
                      <tr>
                        <td>Indicação:</td>
                        <td><input type="text" name="aval_indic" class="col-6" id="aval_indic" value="'.$a3.'" /></td>
                      </tr>
                    </table>
                    ';
                    $tabela .= '<blockquote>'.$html.'</blockquote><button type="button" id="mudaagenda" data-status="'.$item->idstatus.'" data-id="'.$item->id.'">Salvar</button><hr>';
                }
                
            }
            $tabela .= $this->resumo($id,false);
            $tabela .= '</form>';
        }
        
        $this->dados['central'] = $js.'<section>'.  $tabela . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function verifica_profile($id,$grp="",$ajax=true)
    {
        if(!is_numeric($id) || empty($id)) return false;
            
        $lis = $this->banco->listagem_users('users.id = '.$id);    
        
        foreach($lis as $item) :
            // Pacientes
            if($grp == 2)
            {
                $nome = $item->name;
                $info = '<address>
                  <strong>'.$item->email.'</strong><br>
                  '.$item->address.'<br>
                  '.$item->city.', '.$item->uf.' '.$item->mobile.'<br>
                  <abbr title="Telefone">Tel:</abbr> '.$item->phone.' '.$item->contact_fax.'
                </address>';
                $foto = (!empty($item->logo) ? 'src="'.site_url('uploads/'.$item->logo).'"' : 'data-src="holder.js/64x64"');
            }
            
            // Profissionais
            
            $areas = '';
            $res = $this->banco->relacao('products','','users',$id);
            
            if(empty($res))
            {
                $areas = '<em>Não atende nenhuma área em específico.</em><br>';
            }
            else 
            {
                $lis2 = $this->banco->listagem('products','name','id in ('.implode(',',$res).') and active = 1');
                foreach($lis2 as $item2)
                {
                    $areas .= '<strong>'.$item2->name.'</strong><br>';
                }
            }
            
            if($grp == 4)
            {
                $nome = $item->name;
                $info = '<address>
                  '.$areas.'
                  '.$item->email.'<br>
                  <abbr title="Telefone">Tel:</abbr> '.$item->phone.' '.$item->contact_fax.'
                </address>';
                $foto = (!empty($item->logo) ? 'src="'.site_url('uploads/'.$item->logo).'"' : 'data-src="holder.js/64x64"');
            }
        endforeach;
            
        $ret = '<div class="media">
              <a class="pull-left" href="#">
                <img class="media-object" style="width:64px;" '.$foto.'>
              </a>
              <div class="media-body">
                <h4 class="media-heading">'.$nome.'</h4>
                <p>'.$info.'</p>
              </div>
            </div>';
            
        if($ajax)
            echo $ret;
        else
            return $ret;
    }
    
    public function agendar($ini,$fim,$tudo=false)
    {
        $inicio = preg_replace('/\(.*\)/', '', utf8_encode(base64_decode($ini)));
        $final  = preg_replace('/\(.*\)/', '', utf8_encode(base64_decode($fim)));
        
        $id = '';
        
        if(empty($id))
        {
            redirect('agenda/novo/'.strtotime($inicio).'/'.strtotime($final));
            return false;
        }
        else 
        {
            $js = '<script>
            $(document).ready(function(){
                $("button#mudastatus").click(function(){
                    var tipo = $(this).data("codigo");
                    var id = $(this).data("id");
                    $.get("'.site_url('home/alterastatus').'/"+tipo+"/"+id,function(data){
                        alert(data);
                        document.location.reload();
                    });
                });
            });
            </script>';
            $tabela = $this->resumo($id,false);
        }
        
        $this->dados['central'] = $js.'<section>'.  $tabela . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function alterastatus($tipo,$id)
    {
        if(empty($tipo) || !is_numeric($tipo)) die('Falha ao alterar status.');
        if(empty($id) || !is_numeric($id)) die('Falha ao alterar status.');
        
        $dados = array('id' => $id, 'idstatus' => $tipo, 'date_updated' => date('Y-m-d H:i:s'));
        
        $opa = $this->banco->atualiza('services',$dados);
        if($opa)
        {
            die('Status alterado com sucesso.');
        }
        else 
        {
            die('Falha ao alterar status.');
        }
    }

    public function alteraagenda($id,$tipo)
    {
        if(empty($tipo) || !is_numeric($tipo)) die('Falha ao alterar.');
        if(empty($id) || !is_numeric($id)) die('Falha ao alterar.');
        if(empty($_POST)) die('Falha ao alterar.');
        
        //$dados = array('id' => $id, 'idstatus' => $tipo, 'date_updated' => date('Y-m-d H:i:s'));
        
        if($tipo == 2) // reagendamento
        {
            $datetmp = explode("/",$_POST['date_start']);
            $ini = $datetmp[2] . '-' . $datetmp[1] . '-' . $datetmp[0] . ' ' . $_POST['date_start_h'] . ':' . $_POST['date_start_m'] . ':00';
            unset($datetmp);
            $datetmp = explode("/",$_POST['date_end']);
            $fim = $datetmp[2] . '-' . $datetmp[1] . '-' . $datetmp[0] . ' ' . $_POST['date_end_h'] . ':' . $_POST['date_end_m'] . ':00';
            
            if(strtotime($ini) >= strtotime($fim))
            {
                die("A data final precisa ser maior do que a data inicial.");
            }
            
            $dataarray = $this->banco->campo('services','data_array','id = '.$id);
            $dataarray = unserialize($dataarray);
            $dataarray['date_start'] = $_POST['date_start'];
            $dataarray['date_start_h'] = $_POST['date_start_h'];
            $dataarray['date_start_m'] = $_POST['date_start_m'];
            $dataarray['date_end'] = $_POST['date_end'];
            $dataarray['date_end_h'] = $_POST['date_end_h'];
            $dataarray['date_end_m'] = $_POST['date_end_m'];
            
            $dados = array(
                'id' => $id, 'idstatus' => 1, 'date_updated' => date('Y-m-d H:i:s'),
                'date_start' => $ini,
                'date_end' => $fim,
                'data_array' => serialize($dataarray)
            );
            $opa = $this->banco->atualiza('services',$dados);
        }

        if($tipo == 7) // Avaliacao
        {
            $dados['id'] = $this->banco->campo('usersdata','id','iduser = '.$this->banco->campo('services','iduser','id = '.$id));
            $dados['outros'] = '<strong>Desenvolvimento:</strong> '.$_POST['aval_desenv'].' <hr><strong>Diagnóstico:</strong> '.$_POST['aval_diag'].' <hr><strong>Indicação:</strong> '.$_POST['aval_indic'];
            
            $opa = $this->banco->atualiza('usersdata',$dados);
            
            
            $dataarray = $this->banco->campo('services','data_array','id = '.$id);
            $dataarray = unserialize($dataarray);
            $dataarray['aval_desenv'] = $_POST['aval_desenv'];
            $dataarray['aval_diag'] = $_POST['aval_diag'];
            $dataarray['aval_indic'] = $_POST['aval_indic'];
            
            $dados = array(
                'id' => $id, 'date_updated' => date('Y-m-d H:i:s'),
                'data_array' => serialize($dataarray)
            );
            $opa = $this->banco->atualiza('services',$dados);
        }
        
        
        if($opa)
        {
            die('Agendamento alterado com sucesso.');
        }
        else 
        {
            die('Falha ao alterar.');
        }
    }
    
    public function resumo($id,$ajax=true)
    {
        $dias = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
        
        $lis = $this->banco->listagem('services','id','id = '. (int) $id);
        $ret = '';
        foreach($lis as $i) :
        
        $ret .= '<h4 style="color:'.$this->banco->campo('status','color','id = '.$i->idstatus).'">'.$this->banco->campo('status','name','id = '.$i->idstatus).'</h4>';
        
        $ret .= '<p>Alterar o status para:</p>';
        
        $ret .= '<div class="btn-toolbar"><div class="btn-group">';
        $ret .= '<button type="button" class="btn btn-sm btn-info" id="mudastatus" data-codigo="3" data-id="'.$id.'">FNH</button>';
        $ret .= '<button type="button" class="btn btn-sm btn-warning" id="mudastatus" data-codigo="4" data-id="'.$id.'">FCA</button>';
        $ret .= '<button type="button" class="btn btn-sm btn-danger" id="mudastatus" data-codigo="5" data-id="'.$id.'">FSA</button></div><div class="btn-group">';
        $ret .= '<button type="button" class="btn btn-sm btn-success" id="mudastatus" data-codigo="6" data-id="'.$id.'">Presente</button>';
        $ret .= '<button type="button" class="btn btn-sm" id="mudastatus" data-codigo="7" data-id="'.$id.'">Avaliado</button></div><div class="btn-group">';
        $ret .= '<button type="button" class="btn btn-sm btn-link" id="mudastatus" data-codigo="2" data-id="'.$id.'">Reagendar...</button>';
        $ret .= '</div></div><p></p>';
        
        $ret .= '<p><strong>'. $dias[date('w',strtotime($i->date_start))] . ' - ' . date('d/m/Y \d\a\s H:i',strtotime($i->date_start)) 
        . ' até as ' . date('H:i',strtotime($i->date_end)) . '</strong></p>';
        
        $ret .= '<h5>Tratamento:</h5>';
        $ret .= '<p>'.$this->banco->campo('products','name','id = '.$i->idproduct).'</p>';
        
        $ret .= '<h5>Paciente:</h5>';
        $ret .= $this->verifica_profile($i->iduser,2,false);
        
        $ret .= '<h5>Profissional:</h5>';
        $ret .= $this->verifica_profile($i->iduserpro,4,false);
        
        $ret .= '<h5>Outras informações:</h5>';
        $super = unserialize($i->data_array);
        // Se tem convenio, mostra
        if(!empty($super['convenios']))
        {
            $ret .= '<p><strong>Convênio: </strong>'.$this->banco->campo('users','name','id = '.$super['convenios']).'</p>';
        }
        
        $r_num = 1;
        foreach($super['recursos'] as $r)
        {
            $ret .= '<p><strong>Recurso '.$r_num.': </strong><br>'.$this->banco->campo('resources','name','id = '.$r).'<br>';
            if($super['auxiliar'.$r] > 0)
                $ret .= 'Auxiliado por '.$this->banco->campo('users','name','id = '.$super['auxiliar'.$r]).'<br>';
            if(!empty($super['obs'.$r]))
                $ret .= '<em>'.$super['obs'.$r].'</em><br>';
            $r_num++;
        }
        
        if(!empty($super['obs_geral']))
        {
            $ret .= '<p><strong>Obs: </strong><br>'.$super['obs_geral'].'</p>';
        }
        
        endforeach;
        
        // Resumo do agendamento
        if($ajax)
            echo $ret;
        else
            return $ret;
        
    }
    
	public function index($result='')
	{
		$this->dashboard($result);
        return;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */