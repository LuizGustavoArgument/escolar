<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custos extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('custos');
        $this->load->model('sistema','banco',TRUE);
        
        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        
        $this->session->set_userdata('pagina_atual', 'custos');
        $this->load->library('form');
    }
    
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_cadastros',$c);

        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                break;
        // Por enquanto, o sistema está sem controle de permissões
        // outros = proibido
            default:
                $proibido = array('alterarativo', 'remover', 'insumo', 'index', 'coord', 'inserir','listar','eu','alterar','listargrupo', 'alterargrupo', 'inserirgrupo');
                
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não tem permissão para acessar esta página. Contate o administrador.');
            redirect('home');
        }
    }
    
    public function index($result='')
    {
        $this->perm(__FUNCTION__);
            
        $this->dados['central'] = '<section><p>Escolha uma opção ao lado.</p></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function atribuir()
    {
        $ret = $this->perm(__FUNCTION__);
            
        $lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id']);
        $groups[0] = 'Nenhum';
        foreach($lis as $item)
        {
            $groups[$item->id] = $item->name;
        }
            
          
        $filt = ""; 
        //if($ret) $filt = "idhist = ".$ret;
        $lisusu1 = $this->geraUsersByGroupKind(4);
        $lisusu2 = $this->geraUsersByGroupKind(3);
        $lisusu = array_merge($lisusu1,$lisusu2);
        $filt = 'id in ('.implode(',',$lisusu).')';
        $listagem = $this->banco->listagem('users','name',$filt);
        foreach($listagem as $item)
        {
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp[0] = 0;
            $usuarios[base64_encode('e_'.$item->username.'_'.$item->id.'_'.time())] = '['. $groups[$grp[0]] . '] ' . $item->name;
        }
        
        $this->dados['central'] = '<section><p>Selecione um usuário</p>';
        
        $this->dados['central'] .= '
        <div class="input-append">
        <select class="col-4" id="appendedInputButton" name="usu" id="usu">';
        
        if(empty($usuarios))
        {
            $this->session->set_flashdata('notice','Você não criou nenhum profissional. Nada a ser feito aqui.');
            redirect('custos/index');
        }
        
        foreach($usuarios as $c => $i)
        {
            $this->dados['central'] .= '<option value="'.$c.'">'.$i.'</option>';
        }
        
        $this->dados['central'] .= '</select>
        <button class="btn" id="alteraCadastro" type="button">Ver planilha</button>
        </div>
        <script>$(document).ready(function() {
            
            $("button#alteraCadastro").click(function(){
                var code = $("select[name=\'usu\'] option:selected").val();
                var url = \''.site_url('custos/alterar').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>
        ';
        
        $this->dados['central'] .= '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
            
    public function alterar($cod,$opcao=0)
    {
        $ret = $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Horários & Custos :: Planilha de custo';
        $this->cabecalho['subtitulo'] = 'Planilha de custo/hora do profissional';
          
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('custos/index');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameusu = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idusu = (int) $code[2];
            $link_date = $code[3];
        }
        
        if($idusu == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('custos/index');
        }

        // Verifica se é insumo
        /*
        $grpchk = $this->banco->relacao('users',$idusu,'groups');
        if($grpchk[0] == 9)
        {
            if($this->uri->segment(4) === FALSE)
                $opcao = 2;        
        }
        // Verifica se é consultor ou coordenador
        elseif($grpchk[0] == 3 || $grpchk[0] == 5)
        {
            if($this->uri->segment(4) === FALSE)
                $opcao = 1;
        }
        */
        // Lista de grupos
        $lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id']);
        $groups[0] = 'Nenhum';
        foreach($lis as $item)
        {
            $groups[$item->id] = $item->name;
        }
        
        $grp = $this->banco->relacao('users',$idusu,'groups');
        if(empty($grp)) $grp[0] = 0;
        $grpid = $grp[0];
        
        $this->dados['central'] = '
        <form name="frmcustos" id="frmcustos" method="POST">
        <section>
        <p><strong>Nome:</strong> '.$this->banco->campo('users','name','id = ' . $idusu).'</p>
        <p><strong>Username:</strong> '.$this->banco->campo('users','username','id = ' . $idusu).'</p>
        <p><strong>Grupo:</strong> '.$groups[$grpid].'</p>
        <p><a href="#">Ver dados cadastrais</a></p>
        ';
        
        // Quando perfil consultor está logado, permite alteração somente dos que ele criou
        if($ret) {
            $idhist = $this->banco->campo('users','idhist','id = ' . $idusu);
            if($idhist != $ret) {
                $this->session->set_flashdata('notice','Não é permitido alterar usuários/insumos que você não criou.');
                redirect('custos/index');
            }
        }
        
        // Opcao de especificar 
        $this->dados['central'] .= '<h5>Escolha a opção de acordo com o grupo:</h5>';
        $this->dados['central'] .= '<ul class="navbar nav-tabs">
        <li '.($opcao == 0 ? 'class="active"' : '').'>
        <a href="'.site_url('custos/alterar/'.$cod.'/0').'"><i class="fas fa-user"></i> Planilha de horários</a>
        </li>
        <li '.($opcao == 1 ? 'class="active"' : '').'><a href="'.site_url('custos/alterar/'.$cod.'/1').'"><i class="fas fa-briefcase"></i> Custo</a></li>
        </ul>';
        
        // Tabela de horarios
        $tab = array();
        
        // Busca o que esta salvo
        $lis = $this->banco->listagem('costs','name','iduserto = '.$idusu.' and unit = "time" and type like "object"');
        if(!empty($lis))
        foreach($lis as $item)
        {
            $tab[0][$item->name] = $item->value;
            $tab[1][$item->name] = $item->stdrvalue;
        }
        
        if ($opcao == 0) :

            // Monta a planilha
            $js = '
            <script type="text/javascript">
            $(document).ready(function() {
                            
                        var container = $("#folhaTable");
                          
                        var pln = $(\'input[name="folha"]\').val();
                        if(pln === undefined || pln == 0 || pln == "")
                        {
                            var conteudo = 
                                '.json_encode($tab).'
                              ;
                            
                        }
                        else
                            {
                              
                              if(pln !== 0)
                                var conteudo = JSON.parse($(\'input[name="folha"]\').val());
                              
                            }
                            
                       var html2Renderer = function (instance, td, row, col, prop, value, cellProperties) {
                              var escaped = Handsontable.helper.stringify(value);
                              escaped = strip_tags(escaped, \'<em><b><a>\');
                              $(td).css({
                                background: \'rgb(255, 255, 204)\'
                              });
                              td.innerHTML = escaped;
                              return td;
                       };
                       
                       var amareloRenderer = function (instance, td, row, col, prop, value, cellProperties) {
                              Handsontable.TextCell.renderer.apply(this, arguments);
                              $(td).css({
                                background: \'rgb(255,255,0)\'
                              });
                          };
                       
                       function grana(amount) {
                            var i = parseFloat(amount);
                            if(isNaN(i)) { i = 0.00; }
                            var minus = \'\';
                            if(i < 0) { minus = \'-\'; }
                            i = Math.abs(i);
                            i = parseInt((i + .005) * 100);
                            i = i / 100;
                            s = new String(i);
                            if(s.indexOf(\'.\') < 0) { s += \'.00\'; }
                            if(s.indexOf(\'.\') == (s.length - 2)) { s += \'0\'; }
                            s = minus + s;
                            return s.replace(".",",");
                         }
                       
                       
                       container.handsontable({
                            data: conteudo,
                            colHeaders:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],
                            rowHeaders:["Início","Final"],
                            minRows: 2,
                            minCols: 7,
                            cells: function (row, col, prop) {
                                
                                if(row === 0)
                                {
                                    //container.handsontable(\'setCellReadOnly\', row, col);
                                    return {type: {renderer: html2Renderer}}
                                }
                                
                                
                            }, onChange: function(data,source) {
                                if (source === \'loadData\') {
                                  return; 
                                }
                                
                            }
                          });
                      
            });
            
            // Salva a planilha
            function salvaTudo()
            {
                var container = $("#folhaTable");
                var dados = container.handsontable(\'getData\');
                $(\'input[name="folha"]\').val(JSON.stringify(dados));
                $.post("'.site_url('custos/salva_atual').'", $("#frmcustos").serialize(), function(data) { if(data == "ok"){ $("#salvou").fadeIn().delay(800).fadeOut(); } } );
            }
            
            </script>
            ';
        endif;

        // Se quiser especificar tudo:
        if($opcao == 0)
        {
            $html = '';
            $html.='<input type="hidden" name="option" id="option" value="'.$opcao.'">';
            $html.='<input type="hidden" name="folha" id="folha" value="0">';
            $html.='<input type="hidden" name="idusu" id="idusu" value="'.$idusu.'">';
                
            $this->dados['central'] .= '
            <div id="folhaTable"></div>
            ' . $html;
        }
        elseif ($opcao == 1)
        {
            // Verifica se há algum dado salvo para este usuário
            $content1 = $this->banco->campo('costs','stdrvalue','iduserto = '.$idusu.' and type like "user"');
            if(empty($content1)) $content1 = '';
                
            // Se quiser especificar apenas o valor hora:
            $js = '
            <script>
            function salvaTudo() {$.post("'.site_url('custos/salva_atual').'", $("#frmcustos").serialize(), function(data) { if(data == "ok"){ $("#salvou").fadeIn().delay(800).fadeOut(); } else { alert("Falha na permissão de salvar planilha."); } } );}
            </script>
            ';
            $html = '';
            $html.='<label>Fator multiplicador</label><div class="input-prepend input-append">
                               <span class="add-on">x</span>';
            $html.='<input name="custoporunid" class="col-2" id="appendedPrependedInput" type="text" alt="integer" placeholder="0" value="'.$content1.'">';
            $html.='<span class="add-on">/Atendimento.</span>';
            $html.='</div>';
            
            $html.='<input type="hidden" name="option" id="option" value="'.$opcao.'">';
            $html.='<input type="hidden" name="idusu" id="idusu" value="'.$idusu.'">';
            
            $this->dados['central'] .= '<p>
            '.$html.'
            </p>';
        }
        
        $this->dados['central'] .= '<hr><div id="salvou" style="display:none;"><small style="color:green"><i class="fas fa-thumbs-up"></i> Salvo!</small></div>
        <p><button class="btn btn-success" type="button" onclick="salvaTudo();">Salvar</button></p>
        ';
        $this->dados['central'] .= '</section></form>'.$js;
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function salva_atual()
    {
        $ret = $this->perm(__FUNCTION__);
            
        $data = $_POST;
        
        // Quando perfil consultor está logado, permite alteração somente dos que ele criou
        if($ret) {
            $idhist = $this->banco->campo('users','idhist','id = ' . (int) $data['idusu']);
            if($idhist != $ret) {
                echo 'fail';
                return false;
            }
        }
        
        if($data['option'] == 0)
        {
            $planilha = json_decode($data['folha']); // planilha[linha][coluna]
            for($linha=0;$linha<=1;$linha++)
            {
                foreach($planilha[$linha] as $coluna => $val)
                {
                    $dados = array(
                        'name' => $coluna, // 0 for sunday through 6 for saturday (aka date(w))
                        'iduserfrom' => $this->session->userdata('esta_logado'),
                        'iduserto' => $data['idusu'],
                        ($linha == 0 ? 'value' : 'stdrvalue') => trim($val),
                        'unit' => 'time',
                        'type' => 'object'
                    );
                    $onde = array('name' => $coluna, 'iduserto' => $data['idusu'], 'unit' => 'time', 'type' => 'object');
                    $r = $this->banco->atualiza2('costs',$dados,$onde);
                }
            }
        }
        elseif ($data['option'] == 1)
        {
            $val = $data["custoporunid"];
            $dados = array(
                'name' => 'Mult',
                'iduserfrom' => $this->session->userdata('esta_logado'),
                'iduserto' => $data['idusu'],
                'stdrvalue' => trim($val),
                'unit' => 'integer',
                'type' => 'user'
            );
            $onde = array('name' => 'Mult', 'iduserto' => $data['idusu'], 'unit' => 'integer', 'type' => 'user');
            $r = $this->banco->atualiza2('costs',$dados,$onde);
        }

        echo 'ok';
        return false;
    }
    
    public function custohora()
    {
        $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Horários & Custos :: Tabela de custo e horários';
        $this->cabecalho['subtitulo'] = 'Tabela de custo/hora da equipe e horários preferenciais';
        
        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
            "iDisplayLength": 50,
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }
        }); 
        
            $("a#alteraCadastro").click(function(){
                var code = $(this).attr("rel");
                var url = \''.site_url('custos/alterar').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>';
        
        $this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
        $this->dados['tabela'] .= '<th>Nome</th>
                                   <th>E-mail</th>
                                   <th>Grupo</th>
                                   <th>Custo por Unid.</th>
                                   <th>Horários</th>';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id']);
        $groups[0] = 'Nenhum';
        foreach($lis as $item)
        {
            $groups[$item->id] = $item->name;
        }
        
        $lisusu1 = $this->geraUsersByGroupKind(4);
        $lisusu2 = $this->geraUsersByGroupKind(3);
        $lisusu = array_merge($lisusu1,$lisusu2);
        $filt = 'id in ('.implode(',',$lisusu).')';
        $listagem = $this->banco->listagem('users','name',$filt);
        foreach($listagem as $item)
        {
            
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp[0] = 0;
            //$sal = $this->banco->custo_user($item->id,true);
            $cus = $this->banco->custo_user($item->id);
            
            $this->dados['tabela'] .= '<tr><td>'.$item->name.'</td>
                                   <td>'.$item->email.'</td>
                                   <td>'.$groups[$grp[0]].'</td>
                                   <td align="center">'.(!empty($cus) ? $cus : 'n/d').'</td>
                                   <td>';
            if($item->id != 99)
                $this->dados['tabela'] .= '<a class="btn" href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->username.'_'.$item->id.'_'.time()) .'">Ver planilha</a></li>';
            $this->dados['tabela'] .= '</td></tr>';
                                   
        }
        $this->dados['tabela'] .= '</tbody></table>';
        
        $this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function encargos()
    {
        $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Horários & Custos :: Encargos';
        $this->cabecalho['subtitulo'] = 'Tabela de horários';
        
        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
            "iDisplayLength": 50,
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }
        }); 
        
            $("a#alteraCadastro").click(function(){
                var code = $(this).attr("rel");
                var url = \''.site_url('custos/alterarencargo').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>';
        
        $this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
        $this->dados['tabela'] .= '<th>Cód.</th>
                                   <th>Nome</th>
                                   <th>Descrição</th>
                                   <th>Grupo</th>
                                   <th>%</th>
                                   <th>Ordem</th>
                                   <th>Opções</th>';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        //$filt = 'charges.kind not in (0) and id != 99';
        $filt = '';
        $listagem = $this->banco->listagem('charges','name',$filt);
        foreach($listagem as $item)
        {
            $grp = array('1' => 'Encargos', '2' => 'Taxas', '3' => 'Benefícios', '0' => 'FIXO', '4' => 'Outros');
           
            $this->dados['tabela'] .= '
            <tr><td>'.$item->id.'</td>
            <td><span style="text-align:center"><strong>'.$item->name.'</strong></span></td>
                                   <td>'.$item->desc.'</td>
                                   <td>'.$grp[$item->kind].'</td>
                                   <td>'.(!empty($item->percentage) ? $this->preco($item->percentage,false) : 'n/a').'</td>
                                   <td>'. $item->ord .'</td>
                                   <td>
                                   <div class="btn-group btn-xs">
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Ação
                                        <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Alterar</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Excluir</a></li>
                                        </ul>
                                   </div>
                                   </td></tr>';
                                   
        }
        $this->dados['tabela'] .= '</tbody></table>';
        
        $this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function novoencargo()
    {
        $this->perm(__FUNCTION__);
        $this->inserirencargo();
    }
    
    public function alterarencargo($cod)
    {
        $this->perm(__FUNCTION__);
            
        // Verifica o padrao enviado pelo post
        
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('custos/encargos');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameprod = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idprod = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idprod == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('custos/encargos');
        }
        
        // Restaura os dados
        $dados = array(
            'url' => 'custos/alterarencargo/'.$cod,
            'tipo' => 'alterar',
            'id' => $idprod,
            'titulo' => 'Custos :: Alterar encargo',
            'subtitulo' => 'Alteração de um encargo'
        );
        
        $this->inserirenc($dados);
    }

    public function inserirenc($result=array())
    {
        $this->perm(__FUNCTION__);
        if(empty($result))
        {
            $result = array(
                'url' => 'custos/inserirenc',
                'tipo' => 'cadastrar',
                'id' => '',
                'titulo' => 'Horários & Custos :: Inserir novo encargo',
                'subtitulo' => 'Criar novo encargo'
            );
        }
            
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('name'=>'textlarge', 'kind' => 'select_required', 'desc' => 'textarea',
         'percentage' => 'textxsmall', 'ord'=>'textxsmall', 'formula' => 'textlarge');
                
        // Lista de grupos
        $lis = array('1' => 'Encargos', '2' => 'Taxas', '3' => 'Benefícios', '4' => 'Outros');
        
        // Tipos de campos
        $nomes_users = array(
            'Título'=>'max_length[120]|required',
            'Tipo' => $lis,
            'Descrição' => '',
            'Porcentagem <small>(para ENCARGOS e TAXAS)</small>' => '',
            'Ordem' => 'numeric|required',
            'Fórmula' => ''
        );
        
        // Monta o form
        $this->form->open($result['url'],'custos');    
        
        // Valores padrão
        if(empty($result['id']))
        {
            $f = '';
            $attr_users['formula'] = array('value' => $f);
        }
        else
            {   // Para quando for alteração (recupera dados)
                $f = $this->banco->relacao('charges',$result['id'],'charges',$result['id'],'',true);
                $attr_users['formula'] = array('value' => (empty($f[0]) ? '' : $f[0]));
                
                $listagem = $this->banco->listagem('charges','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
                foreach($listagem[0] as $nomeitem => $item)
                {
                    $attr_users[$nomeitem] = array('value' => ($nomeitem == "percentage" ? $this->preco($item,false) : $item));
                }
                
                $campos_users['id'] = 'hidden';
                $nomes_users['id'] = $result['id'];
            }
        
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
         
        $this->geraForm($campos_users,$nomes_users,$attr_users);
        
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'encargos_criar');
        $this->form->onsuccess('redirect','custos/encargos');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

}