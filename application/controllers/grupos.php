<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grupos extends MY_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->geraMenu(anchor('home/index','home'). ' / grupos');
		if ($this->session->userdata('esta_logado') == 0)
		{
			$this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');		
			redirect('login');
		}
		$this->load->model('sistema','banco',TRUE);
		// Somente admins acessam esta area!
		if ($this->checaPermissao() != 1)
		{
			$this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');		
			redirect('home');
		}
		
		// verifica quais grupos o usuario logado pode gerenciar
		if($this->session->userdata('grpadmin') != 0)
		{
			$this->grp_filter = $this->session->userdata('grpadmin');
		}
		else
		{
			$lstmp = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
			foreach($lstmp as $ls)
			{
				$this->grp_filter[] = $ls->id;
			}
		}

        $this->session->set_userdata('pagina_atual', 'grupos');
    }

	private function contagem($filtro)
	{
		$this->dados['num_aplicadas'][$filtro] = 0;
		$this->dados['num_pendentes'][$filtro] = 0;
		$this->dados['num_finalizadas'][$filtro] = 0;
		if (!empty($filtro)) :
			$listagem = $this->banco->listagem('groups','id','id = '. (int) $filtro .' and idorg = '. $this->org['id']);
			foreach($listagem as $item)
			{
				$listagem2 = $this->banco->relacao('users','','groups',$item->id);
				foreach($listagem2 as $itemp)
				{
					$tmp = $this->banco->listagem('users','name','id = '. $itemp . ' and isadmin = "no"');
					foreach($tmp as $item2) 
					{
						$idenquete = $this->banco->relacao('users',$item2->id,'polls');
						foreach($idenquete as $idenq)
						{
							if (!empty($idenq))
							{
								$finalizou = $this->banco->campo('relationship','permission',
								'idobj1 = '.$item2->id.' and type = "users___polls" and idobj2 = '.$idenq.'');
								
								$this->dados['num_aplicadas'][$item->id]++;
								
								if ($finalizou == 'none' || empty($finalizou))
								{
									$this->dados['num_pendentes'][$item->id]++;
								}
								else 
								{
									$this->dados['num_finalizadas'][$item->id]++;
								}
							}
						}
					}
				}
			}
		endif;
	}

	public function index($filtro="")
	{
		
		
		if (!empty($filtro))
		{
			// Mostra por grupo
			$listagem = $this->banco->listagem('groups','code','id = '.(int) $filtro .' and idorg = '. $this->org['id']);	
			$condicao_extra = '';
		}
		else
		{ 
			// Mostra tudo
			$listagem = $this->banco->listagem('groups','code','idorg = '. $this->org['id']);
			$condicao_extra = '';
		}
		$this->dados['resultado_query'] = $listagem;
		
		foreach($listagem as $item)
		{
			$listagem2 = $this->banco->relacao('users','','groups',$item->id);
			//$this->contagem($item->id);
			if(in_array($item->id,$this->grp_filter)) :
			if(!empty($listagem2))
			foreach($listagem2 as $itemp)
			{
				$tmp = $this->banco->listagem('users','name','id = '. $itemp . $condicao_extra);
				foreach($tmp as $item2) {
						
						$this->dados['usu_name'][$item->id][] = $item2->name;
						$this->dados['usu_login'][$item->id][] = $item2->username;
						$this->dados['usu_email'][$item->id][] = $item2->email;
						$this->dados['usu_phone'][$item->id][] = $item2->phone;
						$this->dados['usu_id'][$item->id][] = $itemp;
						$this->dados['usu_status'][$item->id][] = ($item2->islocked == 'yes' ? '<span style="color:red">Inapto</span><br/><input type="button" id="liberando" name="'.$item2->id.'" value="Liberar" />' : '<span style="color:green">Liberado</span>');
						//$this->dados['usu_cpf'][$item->id][] = '';
						if ($item2->idhist == '1')
						{
								$this->dados['usu_admin'][$item->id][] = '<span style="color:green"><small>Concluído</small></span>';
								$this->dados['usu_type'][$item->id][] = '';
						}
						else 
						{
								$this->dados['usu_admin'][$item->id][] = '<span style="color:red"><small>Em Aberto</small></span>';
								$this->dados['usu_type'][$item->id][] = '';
						}
						$this->dados['dados_cadastrais'][$item->id][] = $this->banco->query('SELECT polls_itens.title, relationship.content FROM polls_itens, relationship WHERE polls_itens.idpollgroup = 1 AND polls_itens.id = relationship.idobj2 AND relationship.idobj1 = '.$item2->id.' AND relationship.type = "users___polls_itens";');
				}
			}
			endif;
		}
		$this->load->view('_cabecalho',$this->cabecalho);
		$this->load->view('grupos/listar',$this->dados);
		$this->load->view('_rodape',$this->rodape);
	}

	public function criar()
	{
		if($this->session->userdata('grpadmin') != 0)
		{
			$this->session->set_flashdata('notice','Somente administradores (de nível máximo) possuem permissão de acesso a esta página');		
			redirect('home');
		}
		
		$this->form_validation->set_rules('name','Nome do grupo','required');
		
		$this->dados['grupos'] = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
		
		if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('grupos/criar',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{
			
			// verifica se o nome ja nao existe
			$gname = $this->input->post('name');
			$res = $this->banco->campo('groups','name','name like "'.$gname.'"');
			if (!empty($res))
			{
				$this->session->set_flashdata('error','Grupo informado ('.$gname.') já existe. Tente outro.');
				redirect('grupos/criar');
			}
			
			$dados = array(
				'name' => $gname,
				'desc' => $this->input->post('desc'),
				'code' => $this->input->post('code'),
				'date_created' => date('Y-m-d H:i:s'),
				'idorg' => $this->input->post('idorg')
			);
			$idg = $this->banco->insere('groups',$dados);
			if ($idg > 0)
			{
				// Registra no historico
				$this->historico('group','criou um novo grupo ('.$idg.') de nome <b>'.$gname.'</b>');
				
				
				$this->session->set_flashdata('success','Grupo '.$dados['name'].' criado.');
				redirect('grupos/index');
			}
			else
			{
				$this->session->set_flashdata('error','Grupo '.$dados['name'].' não foi criado devido a um erro no sistema.');
				redirect('grupos/criar');
			}
		}
	}

	public function editar($idg)
	{
		if($this->session->userdata('grpadmin') != 0)
		{
			$this->session->set_flashdata('notice','Somente administradores (de nível máximo) possuem permissão de acesso a esta página');		
			redirect('home');
		}
			
		if (empty($idg) || (!is_numeric($idg)))
		{
			$this->session->set_flashdata('error','Chamada inválida na URL');
			redirect('grupos/index');
		}
		
		
		$listagem = $this->banco->listagem('groups','name','id = '. $idg);
		$this->dados['resultado_query'] = $listagem;
		
		$this->form_validation->set_rules('name','Nome do centro','required');
		
		$this->dados['grupos'] = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
				
		if ($this->form_validation->run() == FALSE)
		{	
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('grupos/editar',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{
			$gname = $this->input->post('name');
			
			$dados = array(
				'id' => $idg,
				'name' => $gname,
				'code' => $this->input->post('code'),
				'desc' => $this->input->post('desc'),
				'idorg' => $this->input->post('idorg')
			);
			$resposta = $this->banco->atualiza('groups',$dados);
			if ($resposta > 0)
			{
				// Deleta o grupo pra sempre caso esteje marcada a opção
				$removeu = $this->input->post('remover');
				if ($removeu > 0)
				{
					$del = $this->banco->remover('groups',$removeu);
				} else $removeu = 0;
				
				
				// Registra no historico
				$this->historico('group','alterou o grupo ('.$idg.') de nome <b>'.$gname.'</b>');
				
				$this->session->set_flashdata('success','Grupo '.$dados['name'].' alterado. Se pertence a este grupo, faça o login novamente para ver as alterações.');
				redirect('grupos/index');
			}
			else
			{
				$this->session->set_flashdata('error','Grupo '.$dados['name'].' não foi alterado devido a um erro no sistema.');
				redirect('grupos/editar/'.$idg);
			}
		}
	}


}

/* End of file grupos.php */
/* Location: ./application/controllers/grupos.php */