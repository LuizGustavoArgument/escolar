<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('produtos');
        $this->load->model('sistema','banco',TRUE);
        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        $this->load->library('form');
        
        $this->session->set_userdata('pagina_atual', 'produtos');
    }
    
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_cadastros',$c);

        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                break;
        // Por enquanto, o sistema está sem controle de permissões
        // outros = proibido
            default:
                $proibido = array('alterarativo', 'remover', 'insumo', 'index', 'coord', 'inserir','listar','eu','alterar','listargrupo', 'alterargrupo', 'inserirgrupo');
                
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não tem permissão para acessar esta página. Contate o administrador.');
            redirect('home');
        }
    }
    
    public function index($result='')
    {
        $this->perm(__FUNCTION__);
            
        $this->dados['central'] = '<section><p>Escolha uma opção ao lado.</p></section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function listar()
    {
        $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Disciplinas :: Listar';
        $this->cabecalho['subtitulo'] = 'Disciplinas oferecidas pela escola';
        
        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }
        }); 
        
            $("a#alteraCadastro").click(function(){
                var code = $(this).attr("rel");
                var url = \''.site_url('produtos/alterar').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>';
        
        $this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
        $this->dados['tabela'] .= '<th>Cód.</th>
                                   <th>Nome</th>
                                   <th>Grupo</th>
                                   <th>Data de criação</th>
                                   <th>Ativado</th>
                                   <th>Opções</th>';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $filt = '';
        $listagem = $this->banco->listagem('products','name',$filt);
        foreach($listagem as $item)
        {
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp = 9; else { if(is_array($grp)) $grp = $grp[0]; }
            
            $btn = '';
            if(!empty($item->abrev) && $this->session->userdata('nivel') == 'yes')
            {
                $btn = '<button class="btn btn-xs btn-info" type="button" onclick="document.location.href=\''.site_url('produtos/aulas/'.$item->abrev).'\'">Executar</button>&nbsp;';
            }
            
            $this->dados['tabela'] .= '<tr><td><span style="text-align:center"><strong>'.$item->id.'</strong></span></td>
                                   <td>'.$btn.$item->name.'</td>
                                   <td>'.$item->group.'</td>
                                   <td>'.date('d/m/Y',strtotime($item->date_created)).'</td>
                                   <td>'.($item->active == 1 ? 'Sim' : 'Não').'</td>
                                   <td>
                                   <div class="btn-group btn-xs">
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Ação
                                        <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Alterar</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Excluir</a></li>
                                        </ul>
                                   </div>
                                   </td></tr>';
                                   
        }
        $this->dados['tabela'] .= '</tbody></table>';
        
        $this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function listarrecurso()
    {
        $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Disciplinas :: Listar turmas';
        $this->cabecalho['subtitulo'] = 'Turmas de uma determinada disciplina';
        
        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }
        }); 
        
            $("a#alteraCadastro").click(function(){
                var code = $(this).attr("rel");
                var url = \''.site_url('produtos/alterarrecurso').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>';
        
        $this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
        $this->dados['tabela'] .= '<th>Cód.</th>
                                   <th>Nome</th>
                                   <th>Grupo</th>
                                   <th>Disciplina</th>
                                   <th>Data de criação</th>
                                   <th>Ativado</th>
                                   <th>Opções</th>';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $filt = 'active = 1';
        $listagem = $this->banco->listagem('resources','name',$filt);
        
        // Gera produtos por recurso
        $lis = $this->banco->listagem('products','name','active = 1');
        foreach($lis as $l)
        {
            $rlis = $this->banco->relacao('resources','','products',$l->id);
            if(!empty($rlis))
            {
                foreach($rlis as $r)    
                    $produtos[$r] = $l->name;
            }
        }
        
        foreach($listagem as $item)
        {
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp = 9; else { if(is_array($grp)) $grp = $grp[0]; }
            
            $btn = '';
            
            $this->dados['tabela'] .= '<tr><td><span style="text-align:center"><strong>'.$item->id.'</strong></span></td>
                                   <td>'.$btn.$item->name.'</td>
                                   <td>'.$item->group.'</td>
                                   <td>'.$produtos[$item->id].'</td>
                                   <td>'.date('d/m/Y',strtotime($item->date_created)).'</td>
                                   <td>'.($item->active == 1 ? 'Sim' : 'Não').'</td>
                                   <td>
                                   <div class="btn-group btn-xs">
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Ação
                                        <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Alterar</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Excluir</a></li>
                                        </ul>
                                   </div>
                                   </td></tr>';
                                   
        }
        $this->dados['tabela'] .= '</tbody></table>';
        
        $this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function novo()
    {
        $this->perm(__FUNCTION__);
        $this->inserir();
    }
    
    public function novorecurso()
    {
        $this->perm(__FUNCTION__);
        $this->inserirrecurso();
    }
    
    private function envia_arquivo($dados,$conteudo='',$attr=array(),$update="")
    {
        if(empty($update))
        {
                $lista = array(
                'name' => $attr['nome'],
                'size' => $attr['tamanho'],
                'type' => $attr['tipo']
                );
                
                $lista2 = array(
                    'title' => $dados['title'],
                    'desc' => $dados['desc'],
                    'active' => $dados['active'],
                    'permission' => $dados['permission'],
                    'file1_info' => json_encode($lista),
                    'file1' => $conteudo,
                    'idproduct' => $dados['coord'],
                    //'idtarget' => $dados['idtarget'],
                    'category' => $dados['category']
                );
                
                $retorno = $this->banco->insere('docs',$lista2);
                if(!empty($dados['assocturma']))
                {
                    foreach($dados['assocturma'] as $item)
                    {
                        $this->banco->relacao_criar('users',$item,'docs',$retorno);
                    }
                }
        }
        else
        {
            if($dados['remover'] == 1)
            {
                // Remove
                $this->banco->remover('docs',$dados['iddoc']);
                return $update;
            }
            $lista2 = array(
                    'id' => $update,
                    'title' => $dados['title'],
                    'desc' => $dados['desc'],
                    'active' => $dados['active'],
                    'permission' => $dados['permission'],
                    'idproduct' => $dados['coord'],
                    //'idtarget' => $dados['idtarget'],
                    'category' => $dados['category']
                );
                
                if(!empty($attr['nome']) && !empty($conteudo))
                {
                    $lista = array(
                    'name' => $attr['nome'],
                    'size' => $attr['tamanho'],
                    'type' => $attr['tipo']
                    );
                    $lista2['file1_info'] = json_encode($lista);
                    $lista2['file1'] = $conteudo;
                }
                
                $retorno = $this->banco->atualiza('docs',$lista2);
                $this->banco->relacao_remover('users','','docs',$retorno);
                if(!empty($dados['assocturma']))
                {
                    foreach($dados['assocturma'] as $item)
                    {
                        $this->banco->relacao_criar('users',$item,'docs',$retorno);
                    }
                }
        }
        return $retorno;
    }
    
    private function listinhaAssoc($id,$titulo,$lbl,$kind)
    {
        $html = '<p><a href="javascript:void(0);" title="Clique para ver..." onclick="$(\'#mostra'.$lbl.'\').toggle();">'.$titulo.'</a></p>';
        $html .= '<blockquote id="mostra'.$lbl.'" style="display:none">';
        
        // Relacao users para esta disciplina
        $osgrupos = $this->banco->campo('groups','id','kind = '.$kind);
        if(!is_array($osgrupos)) $osgrupos = array($osgrupos);
      
        $relusu = array();
        if(!empty($id)) // $id é o cod da disciplina
        {
            $relusu = $this->banco->relacao('products',$id,'users');
        }
        
        // lista os associaveis
        $lis = array();
        foreach($osgrupos as $grp) {
            $lis = array_merge($lis,$this->banco->relacao('users','','groups',$grp));
        }
        $lis = array_unique($lis);

        // Busca o que já estava gravado
        //$gravado = $this->banco->relacao('docs',$id,'users');

        if(!empty($lis))
        foreach($lis as $i)
        {
            /*if(in_array($i,$gravado)) $chk = ' checked '; else*/ $chk = '';
            if(empty($relusu) || (!empty($relusu) && in_array($i,$relusu)))
            {
                $html .= '<label class="checkbox"><input type="checkbox" '.$chk.' '
                    . 'id="'.$this->banco->campo('users','username','id = '.$i).'" name="'.$lbl.'[]" value="'.$i.'">'.
                    $this->banco->campo('users','name','id = '.$i).'</label>';
            }
        }
        $html .= '</blockquote>';
        return $html;
    }
    
    public function download($id)
    {
        $ok = false;
        $lista = $this->banco->listagem('docs','id','id = '.$id);
        if(empty($lista)) {$this->output->set_output('ERRO01: Falha ao baixar arquivo.'); return false;}
        else
        {
            foreach($lista as $item)
            {
                $tipos = json_decode($item->file1_info);
                $name = $item->title;
                $ext = $tipos->type;
                $content = $item->file1;
            }
        }
        
        // Permissoes
        //TODO: definir o resto!
        if($this->session->userdata('nivel') == 'yes')
        {
            $ok = true;
        }
        
        if($ok)
        {
            $name = urlencode($name);
            $conteudo = $this->decompress($content);
            if (!$conteudo) {
               $this->output->set_output('ERRO05: Falha ao baixar arquivo. Arquivo danificado! Envie o arquivo novamente.'); return false;
            }
            @ob_end_clean();
            $this->load->helper('download');
            force_download($name.'.'.$ext,$conteudo);
        }
        else {
            $this->output->set_output('ERRO02: Falha de permissão.'); return false;
        }
        return false;
    }
    
    public function aulas_upload_result()
    {
        $this->cabecalho['titulo'] = 'Anexar arquivos';
        $this->cabecalho['subtitulo'] = '';
        $this->dados['central'] = '';
        $this->dados['central'] .= '<section><p><button type="button" onclick="document.location.href=\''.site_url('produtos/aulas_upload').'\'" class="btn btn-primary">Adicionar outro...</button>'
                . '&nbsp;&nbsp;<button type="button" onclick="window.opener.location.reload();window.close();" class="btn">Fechar</button></p></section>';
        
        $this->load->view('_cabecalho_print',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape_print',$this->rodape);
    }
    
    public function aulas_upload($id="")
    {
        // Para inserir um novo arquivo dentro de uma disciplina
        $this->perm(__FUNCTION__);
        
        $this->cabecalho['titulo'] = 'Anexar arquivos';
        $this->cabecalho['subtitulo'] = 'Utilize esta área para associar arquivos às disciplinas e séries disponíveis. Todos os campos são obrigatórios.';
        $this->dados['central'] = '';
        
        if($id > 0)
        {
            if($this->input->post('coord'))
            {
                $conteudo = '';
                if (!empty($_FILES))
                {
                    $tamanho = $_FILES['filepath']['size'];
                    $tipo = $_FILES['filepath']['type'];
                    $ext = pathinfo($_FILES["filepath"]["name"], PATHINFO_EXTENSION);
                    $nome = $_FILES['filepath']['name'];
                    $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                    if($tamanho >= 15900000)
                    {
                        $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                        redirect('produtos/aulas_upload_result');
                        exit;
                    }
					
                    $conteudo = @gzdeflate($conteudo);
                    if(!$conteudo)
                    {
                        $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                        redirect('produtos/aulas_upload_result');
                        exit;
                    }
                    $dados_arquivo = $this->input->post();
                    $ok = $this->envia_arquivo($dados_arquivo,$conteudo,array('tamanho'=>$tamanho,'tipo'=>$ext,'nome' =>$nome));
                    if ($ok > 0)
                    {
                        $this->session->set_flashdata('success','O arquivo '.strtoupper($ext).' foi gravado com sucesso.');
                        
                    }
                    else
                    $this->session->set_flashdata('error','Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.');
                    redirect('produtos/aulas_upload_result');
                    exit;
                }
                else
                {
                    $this->session->set_flashdata('error','Informar um arquivo é obrigatório.');
                    redirect('produtos/aulas_upload_result');
                }
            }
                
            // Ajax
            $html = '<form name="consults" action="'.site_url('produtos/aulas_upload/'.$id).'" id="consults" method="post" enctype="multipart/form-data">';
            $html .= $this->listinhaAssoc($id,'<abbr title="Ou seja, todos desta disciplina nas turmas selecionadas irão receber este arquivo.">Associar a turma(s)?</abbr>','assocturma',7);
            //$html .= $this->listinhaAssoc($id,'<abbr title="Ou seja, apenas os alunos selecionados desta disciplina irão rebeber este arquivo.">Associar a aluno(s)?</abbr>','assocaluno',2);
            
            $html .= '<br><p>Nome do arquivo:</p><input type="text" required="required" class="form-input" name="title" value="">';
            $html .= '<p>Comentários:</p><textarea required="required" class="form-input" name="desc"></textarea>';
            $html .= '<p>Arquivo:</p><input type="file" required="required" class="form-input" name="filepath" id="filepath">';
            $html .= '<p><br>Pasta:</p><input type="text" required="required" class="form-input" value="Outros" name="category">';
            $html .= '<p>Ativo?</p><select class="form-input" name="active"><option value="0">Não</option><option value="1">Sim</option></select>';
            $html .= '<p>Permissão:</p><select class="form-input" name="permission"><option value="READ">Apenas baixar</option><option value="WRTE">Total</option></select>';
            //$html .= '<p>Destacar no mural?</p><select class="form-input" name="idtarget"><option value="1">Não</option><option value="2">Sim, durante 1 dia</option><option value="3">Sim, durante 5 dias</option><option value="4">Sim, durante 10 dias</option><option value="5">Sim, eternamente</option></select>';
            
            $html .= '<input type="hidden" name="coord" id="coord" value="'.$id.'"><p>&nbsp;</p>';
            $html .= '<p><input type="submit" name="salvar" value="Salvar" class="btn">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="btn" name="limpar" value="Limpar tudo"></p>';
            $html .= '</form>';
            echo $html;
            return;
        }

        $this->dados['central'] .= '
        <script>
            function qualcoord(valor)
            {
                if(valor > 0)
                {
                    $.get("'.site_url('produtos/aulas_upload').'/"+valor,function(data){
                        $("#resultado").html(data);
                    });
                }
                else
                    {
                        $("#resultado").html("Selecione um cadastro acima para ver a relação de cadastros.");
                    }
                return false;
            }
        </script>
        ';
            
        $this->dados['central'] .= '<section><p>Escolha a disciplina</p>';
        
        $this->dados['central'] .= '<select class="col-4" name="cons" id="cons" onchange="qualcoord(this.value);">';
        $this->dados['central'] .= '<option value="0">-- selecione --</option>';
        
        // lista de associaveis apenas
        $lis = $this->banco->listagem('products','id','active = 1');
        if(!empty($lis))
        foreach($lis as $i)
        {
            $this->dados['central'] .= '<option value="'.$i->id.'">'.$i->name.' ('.$i->group.')</option>';
        }
        $this->dados['central'] .= '</select>';
        $this->dados['central'] .= '<p>&nbsp;</p><div id="resultado">Selecione uma disciplina acima para ver a relação de cadastros associados.</div>';
        
        $this->dados['central'] .= '</section>';

        $this->load->view('_cabecalho_print',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape_print',$this->rodape);

    }
    
    public function aulas_upload_edit($id="")
    {
        // Para inserir um novo arquivo dentro de uma disciplina
        $this->perm(__FUNCTION__);
        
        $this->cabecalho['titulo'] = 'Editar';
        $this->cabecalho['subtitulo'] = 'Utilize esta área para associar arquivos às disciplinas e séries disponíveis. Todos os campos são obrigatórios.';
        $this->dados['central'] = '';
        
        if($id > 0)
        {
            if($this->input->post('coord'))
            {
                $conteudo = '';
                $tamanho = '';
                $ext = '';
                $nome = '';
                if (!empty($_FILES))
                {
                    $tamanho = $_FILES['filepath']['size'];
                    $tipo = $_FILES['filepath']['type'];
                    $ext = pathinfo($_FILES["filepath"]["name"], PATHINFO_EXTENSION);
                    $nome = $_FILES['filepath']['name'];
                    $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                    if($tamanho >= 15900000)
                    {
                        $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                        redirect('produtos/aulas_upload_result');
                        exit;
                    }
					
                    $conteudo = @gzdeflate($conteudo);
                    if(!$conteudo)
                    {
                        $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                        redirect('produtos/aulas_upload_result');
                        exit;
                    }
                }
                $dados_arquivo = $this->input->post();
                $ok = $this->envia_arquivo($dados_arquivo,$conteudo,array('tamanho'=>$tamanho,'tipo'=>$ext,'nome' =>$nome),$id);
                if ($ok > 0)
                {
                    if($dados_arquivo['remover'] == 0) 
                        $this->session->set_flashdata('success','O arquivo '.strtoupper($ext).' foi alterado.');
                    else
                        $this->session->set_flashdata('success','O arquivo foi removido do sistema.');
                    redirect('produtos/aulas_upload_result');
                }
                else
                {
                    $this->session->set_flashdata('error','Falha ao gravar.');
                    redirect('produtos/aulas_upload_result');
                    exit;
                }
            }
                
            // Ajax
            $html = '<form name="consults" action="'.site_url('produtos/aulas_upload_edit/'.$id).'" id="consults" method="post" enctype="multipart/form-data">';
            //$html .= $this->listinhaAssoc($id,'<abbr title="Ou seja, todos desta disciplina nas turmas selecionadas irão receber este arquivo.">Associar a turma(s)?</abbr>','assocturma',7);
            //$html .= $this->listinhaAssoc($id,'<abbr title="Ou seja, apenas os alunos selecionados desta disciplina irão rebeber este arquivo.">Associar a aluno(s)?</abbr>','assocaluno',2);
            $s = $this->banco->relacao('users','','docs',$id);
            if(!empty($s)) :
                $html .= '<p>Este arquivo está associado às seguintes séries:</p><ul>';
                foreach($s as $i) {
                    $html .= '<li>'.$this->banco->campo('users','name','id = '.$i).'<input type="hidden" name="assocturma[]" id="assocturma'.$i.'" value="'.$i.'" /></li>';
                }
                $html .= '</ul>';
            endif;
            $l = $this->banco->listagem('docs','id','id = '.$id);
            foreach($l as $item) :
                $tipo = json_decode($item->file1_info);
                $html .= '<br><p>Nome do arquivo:</p><input type="text" required="required" class="form-input" name="title" value="'.$item->title.'">';
                $html .= '<p>Comentários:</p><textarea required="required" class="form-input" name="desc">'.$item->desc.'</textarea>';
                $html .= '<p>Arquivo: <strong>'.$tipo->name.'</strong> - <em><small>'.$tipo->size.' bytes</small></em> [ <a href="javascript:void(0)" onclick="$(\'#subst\').show();$(this).hide();">Substituir...</a> ]</p>';
                $html .= '<div id="subst" style="display:none"><input type="file" class="form-input" name="filepath" id="filepath"></div>';
                $html .= '<p><br>Pasta:</p><input type="text" required="required" class="form-input" value="'.$item->category.'" name="category">';
                $html .= '<p>Ativo?</p><select class="form-input" name="active"><option value="0" '.($item->active == 0 ? 'selected' : '').'>Não</option><option value="1" '.($item->active == 1 ? 'selected' : '').'>Sim</option></select>';
                $html .= '<p>Permissão:</p><select class="form-input" name="permission"><option value="READ" '.($item->permission == 'READ' ? 'selected' : '').'>Apenas baixar</option><option value="WRTE" '.($item->permission == 'WRTE' ? 'selected' : '').'>Total</option></select>';
                //$html .= '<p>Destacar no mural?</p><select class="form-input" name="idtarget"><option '.($item->idtarget == 1 ? 'selected' : '').' value="1">Não</option><option value="2" '.($item->idtarget == 2 ? 'selected' : '').'>Sim, durante 1 dia</option><option value="3" '.($item->idtarget == 3 ? 'selected' : '').'>Sim, durante 5 dias</option><option value="4" '.($item->idtarget == 4 ? 'selected' : '').'>Sim, durante 10 dias</option><option value="5" '.($item->idtarget == 5 ? 'selected' : '').'>Sim, eternamente</option></select>';
                $html .= '<p><strong>REMOVER?</strong></p><select class="form-input" name="remover"><option value="0">Não</option><option value="1">SIM</option></select>';
                $html .= '<input type="hidden" name="coord" id="coord" value="'.$item->idproduct.'"><p><input type="hidden" name="iddoc" value="'.$item->id.'" />&nbsp;</p>';
            endforeach;
            $html .= '<p><input type="submit" name="salvar" value="Salvar alterações" class="btn">
            &nbsp;&nbsp;&nbsp;&nbsp;</p>';
            $html .= '</form>';
            $this->dados['central'] = $html;
        }
        else
        {
            $this->dados['central'] = '<p>Chamada inválida!</p>';
        }
        $this->dados['central'] .= '
        <script>
            function qualcoord(valor)
            {
                if(valor > 0)
                {
                    $.get("'.site_url('produtos/aulas_upload').'/"+valor,function(data){
                        $("#resultado").html(data);
                    });
                }
                else
                    {
                        $("#resultado").html("Selecione um cadastro acima para ver a relação de cadastros.");
                    }
                return false;
            }
        </script>
        ';
            
        $this->load->view('_cabecalho_print',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape_print',$this->rodape);

    }
    
    public function aulas($cod)
    {
        $this->perm(__FUNCTION__);
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('home');
        }
        
        // Se for aluno, verifica se pode acessar mesmo
        $series = array();
        $cpl = '';
        if($this->isAluno()) :
            $lis = $this->banco->relacao('users',$this->session->userdata('esta_logado'),'users');
            if(empty($lis)) $serie = '';
            else foreach($lis as $item)
            {
                $serie = $this->banco->campo('users','name','id = '.$item) . '&nbsp;&nbsp;&nbsp;<small>'.$this->banco->campo('users','email','id = '.$item).'</small>';
                $series[] = $item;
            }

            // Se for aluno...
            $s = array();
            if(!empty($serie)) {
                foreach($lis as $item) {
                    $s = array_merge($s,$this->banco->relacao('products','','users',$item));
                }
                $cpl = " and id in (".implode(',',$s).")";
            }
        endif;
        
        $id = $this->banco->campo('products','id','abrev = "'.$cod.'" AND active = 1'.$cpl);
        if(empty($id) || is_array($id))
        {
            $this->session->set_flashdata('error','Disciplina não encontrada ou desativada.');
            redirect('home');
        }
        else
        {
            $nome = $this->banco->campo('products','name','id = '.$id);
            $recado = $this->banco->campo('products','desc','id = '.$id);
            $result = array(
                'url' => 'produtos/aulas/'.$cod,
                'tipo' => '',
                'id' => '',
                'titulo' => ''.$nome,
                'subtitulo' => 'Bem vindo à área da disciplina '.$nome
            );
        
            $this->cabecalho['titulo'] = $result['titulo'];
            $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
            $this->dados['central'] = ''
                    . '<div class="jumbotron" style="padding:20px">
                        <h4>Recados:</h4>
                        <p>'.$recado.'</p>
                      </div>'
                    . '<hr>'
                    . ($this->isAdmin() ? '<p align="right"><a href="javascript:void(0);" onclick="return windowpop(\''.site_url('produtos/aulas_upload').'\', 600, 433)" class="btn btn-sm btn-warning">Enviar novo arquivo...</a></p>' : '');
            
            $this->dados['central'] .= $this->aulasArquivos($id,$series);
            
            if($this->isAdmin()) 
            {
                
                // Mostra lista de alunos matriculados em todas as series disponiveis
                $series = array();
                $alunos = array();
                $alunostip = array();
                $professores = array();
                $prods = $this->banco->relacao('products',$id,'users');
                $turmas = '<h4>Turmas:</h4><p><ul>';
                if(!empty($prods))
                {
                    foreach($prods as $p)
                    {
                        if($this->isProfessor($p))
                        {
                            $professores[] = $this->banco->campo('users','name','id = '.$p);
                        }
                        else
                        {
                            $turmas .= '<li>'.$this->banco->campo('users','name','id = '.$p).'</li>';
                            $alunostip = $this->banco->relacao('users','','users',$p);
                            if(!empty($alunostip))
                            {
                                $alunos['0'] = ' -- Selecione um aluno e clique no botão abaixo -- ';
                                foreach($alunostip as $item)
                                {
                                    $params = array($item,$this->banco->campo('usersdata','id','iduser = '.$item));
                                    if(!empty($params[1])) {
                                        $codusu = base64_encode('e_'.$params[1].'_'.$params[0].'_'.time());
                                        $alunos[$codusu] = '['.$this->banco->campo('users','name','id = '.$p).'] ' . $this->banco->campo('users','name','id = '.$item);
                                    }
                                }
                            }
                        }
                    }
                }
                $turmas .= '</ul></p>';
                asort($alunos);
                
                if(!empty($professores))
                {
                    $this->cabecalho['subtitulo'] = '<strong>Professor:</strong> ' . implode(', ',$professores);
                }
                
                if(!empty($alunos)) 
                {
                    $this->dados['central'] .= ''
                        . '<div class="jumbotron" style="padding:20px">
                            '.$turmas.'<h4>Alunos:</h4>
                            <p>'.  form_dropdown('osalunos',$alunos,'0','id="osalunos"').'  </p>
                                <p><button class="btn btn-success" type="button" onclick="if(document.getElementById(\'osalunos\').value != \'0\')document.location.href=\''.site_url('cadastro/alterar').'/\'+document.getElementById(\'osalunos\').value;">Acessar cadastro</button></p>
                          </div>';
                }
            }
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('padrao',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
    }
    
    private function aulasArquivos($id,$regra=array())
    {
        // Busca lista de categorias!
        $lista = $this->banco->queryfull('select category from docs where idproduct = '.$id.' group by category');
        $categorias = array();
        $ativo = 'active';
        $html = '<div class="row"><div class="col-10"><ul class="navbar nav-tabs" id="myTab">';
        $i = 0;
        if(empty($lista)) $categorias['cat0'] = '';
        else foreach($lista as $item) {
            $html .= '<li class="'.$ativo.'"><a href="#cat'.$i.'">'.$item->category.'</a></li>';
            $categorias['cat'.$i] = $item->category;
            $ativo = '';
            $i++;
        }
        // Busca lista de arquivos!
        $html .= '</ul><div class="tab-content">';
        $ativo = 'active';
        
        // ANTES... filtre os arquivos (caso definido)
        $r = array();
        $cpl = '';
        if(!empty($regra))
        {
            foreach($regra as $a)
            {
                $d = $this->banco->relacao('users',$a,'docs');
                if(!is_array($d)) $d = array($d);
                $r = array_merge($r,$d);
            }
            if(!empty($r))
                $cpl = ' AND active = 1 AND id in ('.implode(',',$r).')';
        }
        
        foreach($categorias as $kk => $cat)
        {
            $lista2 = $this->banco->queryfull('select title, docs.desc, category, active, permission, idtarget, file1_info, id from docs where category like "'.$cat.'" and idproduct = '.$id.' '.$cpl.' order by title');
            $html .= '<div class="tab-pane '.$ativo.'" id="'.$kk.'">';
            $html .= '<table class="table"><thead><tr>'
                    . '<th>Nome</th><th>Descrição</th><th>Tipo</th><th>Ativo?</th>'
                    . '<th>Ação</th>'
                    . '</tr></thead><tbody>';
            $ativo = '';
            foreach($lista2 as $item) 
            {
                $tipos = json_decode($item->file1_info);
                $html .= '<tr><td><a href="'.site_url('produtos/download/'.$item->id).'" title="Clique para baixar">'.$item->title.'</a></td>'
                        . '<td>'.word_limiter(strip_tags($item->desc),50).'</td>'
                        . '<td>'.$tipos->type.'</td>'
                        . '<td>'.($item->active == 1 ? 'Sim' : 'Não').'</td>'
                        . '<td>'.($item->permission == 'READ' ? ($this->isAluno() ? '<a class="btn btn-sm" href="'.site_url('produtos/download/'.$item->id).'" title="Clique para baixar">Clique para baixar</a>' : '<a href="javascript:void(0);" onclick="return windowpop(\''.site_url('produtos/aulas_upload_edit/'.$item->id).'\', 600, 433)" class="btn btn-sm btn-warning">Editar</a>') : '<a href="javascript:void(0);" onclick="return windowpop(\''.site_url('produtos/aulas_upload_edit/'.$item->id).'\', 600, 433)" class="btn btn-sm btn-warning">Clique para editar</a>').'</td>'
                        . '</tr>'
                    ;
            }
            $html .= '</tbody></table>';
            $html .= '</div>';
        }
        $html .= '</div></div></div>';
 
        $html .= '<script>
          $(function () {
            $(\'#myTab a\').click(function (e) {
                e.preventDefault();
                $(this).tab(\'show\');
            })
          })
        </script>';
        
        return $html;
    }
    
    private function decompress( $compressed, $length = null ) {
	
	if ( false !== ($decompressed = @gzinflate( $compressed ) ) )
		return $decompressed;
	if ( false !== ( $decompressed = $this->compatible_gzinflate( $compressed ) ) )
		return $decompressed;
	if ( false !== ( $decompressed = @gzuncompress( $compressed ) ) )
		return $decompressed;
	if ( function_exists('gzdecode') ) {
		$decompressed = @gzdecode( $compressed );
		if ( false !== $decompressed )
			return $decompressed;
	}
	return $compressed;
    }
    
    private function compatible_gzinflate($gzData) {
	
	if ( substr($gzData, 0, 3) == "\x1f\x8b\x08" ) {
		$i = 10;
		$flg = ord( substr($gzData, 3, 1) );
		if ( $flg > 0 ) {
			if ( $flg & 4 ) {
				list($xlen) = unpack('v', substr($gzData, $i, 2) );
				$i = $i + 2 + $xlen;
			}
			if ( $flg & 8 )
				$i = strpos($gzData, "\0", $i) + 1;
			if ( $flg & 16 )
				$i = strpos($gzData, "\0", $i) + 1;
			if ( $flg & 2 )
				$i = $i + 2;
		}
		return @gzinflate( substr($gzData, $i, -8) );
	} else {
		return false;
	}
    }
    
    public function alterar($cod)
    {
        $this->perm(__FUNCTION__);
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('produtos/listar');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameprod = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idprod = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idprod == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('produtos/listar');
        }
        
        // Restaura os dados
        $dados = array(
            'url' => 'produtos/alterar/'.$cod,
            'tipo' => 'alterar',
            'id' => $idprod,
            'titulo' => 'Disciplinas :: Alterar',
            'subtitulo' => 'Alteração de uma disciplina'
        );
        
        $this->inserir($dados);
    }

    public function alterarrecurso($cod)
    {
        $this->perm(__FUNCTION__);
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('produtos/listarrecurso');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameprod = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idprod = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idprod == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('produtos/listarrecurso');
        }
        
        // Restaura os dados
        $dados = array(
            'url' => 'produtos/alterarrecurso/'.$cod,
            'tipo' => 'alterar',
            'id' => $idprod,
            'titulo' => 'Disciplinas :: Alterar Turma',
            'subtitulo' => 'Alteração de uma turma associada a uma disciplina'
        );
        
        $this->inserirrecurso($dados);
    }


    public function inserir($result=array())
    {
        $this->perm(__FUNCTION__);
        if(empty($result))
        {
            $result = array(
                'url' => 'produtos/inserir',
                'tipo' => 'cadastrar',
                'id' => '',
                'titulo' => 'Disciplinas :: Inserir',
                'subtitulo' => 'Criar nova disciplina'
            );
        }
            
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('name'=>'textlarge', 'group' => 'text_autocomplete', 'abrev' => 'textxsmall', 'active' => 'checkbox', 'desc' => 'textarea_advanced', 'date_created'=>'hidden');
                
        // Lista de grupos
        $lis = $this->campos->lista_simples('group_products');
        
        foreach($lis as $it)
            $grps[] = $it['group']; 
        
        if(empty($grps)) $grps_autocomplete = '[]';
        else 
        {
            $grps_autocomplete = implode('","',$grps);
            $grps_autocomplete = '["'.$grps_autocomplete.'"]'; // monta a array            
        }
        
        // Tipos de campos
        $nomes_users = array(
            'Nome da disciplina'=>'max_length[120]|required',
            'Grupo' => $grps_autocomplete,
            'Abreviação' => '',
            'Ativo?' => 1,
            'Descrição completa' => '',
            'date_created' => date('Y-m-d H:i:s'));
        
        // Monta o form
        $this->form->open($result['url'],'produtos');    
        
        // Valores padrão
        if(empty($result['id']))
        {
            $attr_users = array();
        }
        else
            {   // Para quando for alteração (recupera dados)
                $attr_users = array();
                $listagem = $this->banco->listagem('products','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
                foreach($listagem[0] as $nomeitem => $item)
                {
                    $attr_users[$nomeitem] = array('value' => $item);
                }
                
                $campos_users['id'] = 'hidden';
                $nomes_users['id'] = $result['id'];
            }
        
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
        //$attr_users['active'] = array('value' => true);
         
        $this->geraForm($campos_users,$nomes_users,$attr_users);
        
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'produtos_criar');
        $this->form->onsuccess('redirect','produtos/listar');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function inserirrecurso($result=array())
    {
        $this->perm(__FUNCTION__);
        if(empty($result))
        {
            $result = array(
                'url' => 'produtos/inserirrecurso',
                'tipo' => 'cadastrar',
                'id' => '',
                'titulo' => 'Disciplinas :: Inserir Turma',
                'subtitulo' => 'Criar nova turma associada a disciplina'
            );
        }
            
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('name'=>'textlarge', 'product' => 'select', 'group' => 'text_autocomplete', 'active' => 'checkbox', 'desc' => 'textarea_advanced', 'date_created'=>'hidden');
                
        // Lista de grupos
        $lis = $this->campos->lista_simples('group_resources');
        
        foreach($lis as $it)
            $grps[] = $it['group']; 
        
        if(empty($grps)) $grps_autocomplete = '[]';
        else 
        {
            $grps_autocomplete = implode('","',$grps);
            $grps_autocomplete = '["'.$grps_autocomplete.'"]'; // monta a array            
        }
        
        // Lista de produtos
        $lis = $this->banco->listagem("products",'name','active = 1');
        foreach($lis as $it)
            $prds[$it->id] = $it->name;
        
        // Tipos de campos
        $nomes_users = array(
            'Nome da turma'=>'max_length[120]|required',
            'Disciplina associada' => $prds,
            'Grupo' => $grps_autocomplete,
            'Ativo?' => 1,
            'Descrição completa' => '',
            'date_created' => date('Y-m-d H:i:s'));
        
        // Monta o form
        $this->form->open($result['url'],'recursos');    
        
        // Valores padrão
        if(empty($result['id']))
        {
            $attr_users = array();
        }
        else
            {   // Para quando for alteração (recupera dados)
                $attr_users = array();
                $listagem = $this->banco->listagem('resources','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
                foreach($listagem[0] as $nomeitem => $item)
                {
                    $attr_users[$nomeitem] = array('value' => $item);
                }
                
                $campos_users['id'] = 'hidden';
                $nomes_users['id'] = $result['id'];
            }
        
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
        //$attr_users['active'] = array('value' => true);
         
        $this->geraForm($campos_users,$nomes_users,$attr_users);
        
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'recursos_criar');
        $this->form->onsuccess('redirect','produtos/listarrecurso');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }


    public function coord_pro($id="")
    {

        // Para definir quais usuários o coordenador coordena
        $this->perm(__FUNCTION__);
        
        $this->cabecalho['titulo'] = 'Disciplinas :: Associar professores';
        $this->cabecalho['subtitulo'] = 'Utilize esta área para associar profissionais às disciplinas disponíveis.';
        $this->dados['central'] = '';
        
        if($id > 0)
        {
            if($_POST)
            {
                $a = false;
                // Salva as informações
                if(!empty($_POST['cons']))
                {
                    $this->banco->relacao_remover('products',$_POST['coord'],'users','','none','professor');
                    foreach($_POST['cons'] as $c)
                    {
                        if($c > 0)
                            $a = $this->banco->relacao_criar('products',$_POST['coord'],'users',$c,'none','professor');
                    }
                    if($a)
                        echo '<p style="color:green">As informações foram salvas!</p>';
                    else
                        echo '<p style="color:red">Salvo.</p>';
                }
            }
                
            // Ajax
            $html = '<p>Cadastros associados:</p>';
            $html .= '<form id="consults">';
            // lista os associaveis
            $grupos = $this->banco->listagem('groups','id','kind = 3');
            $lis = array();
            foreach($grupos as $grp)
                $lis = array_merge($lis,$this->banco->relacao('users','','groups',$grp->id));
            $lis = array_unique($lis);
            
            // Busca o que já estava gravado
            $gravado = $this->banco->relacao('products',$id,'users');
            
            if(!empty($lis))
            foreach($lis as $i)
            {
                if(in_array($i,$gravado)) $chk = ' checked '; else $chk = ''; 
                $html .= '<input type="hidden" value="-'.rand(1,99).'" name="cons[]" />';
                $html .= '<label class="checkbox"><input type="checkbox" '.$chk.' id="'.$this->banco->campo('users','username','id = '.$i).'" name="cons[]" value="'.$i.'">'.$this->banco->campo('users','name','id = '.$i).'</label>';
            }
            $html .= '<input type="hidden" name="coord" id="coord" value="'.$id.'"><p>&nbsp;</p>';
            $html .= '<p><input type="button" name="salvar" value="Salvar" class="btn" onclick="$.post(\''.site_url('produtos/coord_pro/'.$id).'\',$(\'#consults\').serialize(), function(data){ $(\'#resultado\').html(data).fadeIn(); });">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="btn" name="limpar" value="Limpar tudo"></p>';
            $html .= '</form>';
            echo $html;
            return;
        }

        $this->dados['central'] .= '
        <script>
            function qualcoord(valor)
            {
                if(valor > 0)
                {
                    $.get("'.site_url('produtos/coord_pro').'/"+valor,function(data){
                        $("#resultado").html(data);
                    });
                }
                else
                    {
                        $("#resultado").html("Selecione um cadastro acima para ver a relação de cadastros.");
                    }
                return false;
            }
        </script>
        ';
            
        $this->dados['central'] .= '<section><p>Escolha a disciplina</p>';
        
        $this->dados['central'] .= '<select class="col-4" name="cons" id="cons" onchange="qualcoord(this.value);">';
        $this->dados['central'] .= '<option value="0">-- selecione --</option>';
        
        // lista de associaveis apenas
        $lis = $this->banco->listagem('products','id','active = 1');
        if(!empty($lis))
        foreach($lis as $i)
        {
            $this->dados['central'] .= '<option value="'.$i->id.'">'.$i->name.' ('.$i->group.')</option>';
        }
        $this->dados['central'] .= '</select>';
        $this->dados['central'] .= '<p>&nbsp;</p><div id="resultado">Selecione uma disciplina acima para ver a relação de cadastros associados.</div>';
        
        $this->dados['central'] .= '</section>';

        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function coord_aux($id="")
    {

        // Para definir quais usuários o coordenador coordena
        $this->perm(__FUNCTION__);
        
        $this->cabecalho['titulo'] = 'Disciplinas :: Associar turmas';
        $this->cabecalho['subtitulo'] = 'Utilize esta área para associar disciplinas às turmas disponíveis.';
        $this->dados['central'] = '';
        
        if($id > 0)
        {
            if($_POST)
            {
                $a = false;
                // Salva as informações
                if(!empty($_POST['cons']))
                {
                    $this->banco->relacao_remover('products',$_POST['coord'],'users','','none','turma');
                    foreach($_POST['cons'] as $c)
                    {
                        if($c > 0)
                            $a = $this->banco->relacao_criar('products',$_POST['coord'],'users',$c,'none','turma');
                    }
                    if($a)
                        echo '<p style="color:green">As informações foram salvas!</p>';
                    else
                        echo '<p style="color:red">Salvo.</p>';
                }
            }
                
            // Ajax
            $html = '<p>Cadastros associados:</p>';
            $html .= '<form id="consults">';
            // lista os associaveis
            $grupos = $this->banco->listagem('groups','id','kind = 7');
            $lis = array();
            foreach($grupos as $grp)
                $lis = array_merge($lis,$this->banco->relacao('users','','groups',$grp->id));
            $lis = array_unique($lis);
            
            // Busca o que já estava gravado
            $gravado = $this->banco->relacao('products',$id,'users');
            
            if(!empty($lis))
            foreach($lis as $i)
            {
                if(in_array($i,$gravado)) $chk = ' checked '; else $chk = '';
                $html .= '<input type="hidden" value="-'.rand(1,99).'" name="cons[]" />';
                $html .= '<label class="checkbox"><input type="checkbox" '.$chk.' id="'.$this->banco->campo('users','username','id = '.$i).'" name="cons[]" value="'.$i.'">'.$this->banco->campo('users','name','id = '.$i).'</label>';
            }
            $html .= '<input type="hidden" name="coord" id="coord" value="'.$id.'"><p>&nbsp;</p>';
            $html .= '<p><input type="button" name="salvar" value="Salvar" class="btn" onclick="$.post(\''.site_url('produtos/coord_aux/'.$id).'\',$(\'#consults\').serialize(), function(data){ $(\'#resultado\').html(data).fadeIn(); });">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="btn" name="limpar" value="Limpar tudo"></p>';
            $html .= '</form>';
            echo $html;
            return;
        }

        $this->dados['central'] .= '
        <script>
            function qualcoord(valor)
            {
                if(valor > 0)
                {
                    $.get("'.site_url('produtos/coord_aux').'/"+valor,function(data){
                        $("#resultado").html(data);
                    });
                }
                else
                    {
                        $("#resultado").html("Selecione uma disciplina acima para ver a relação de turmas associadas.");
                    }
                return false;
            }
        </script>
        ';
            
        $this->dados['central'] .= '<section><p>Escolha a disciplina</p>';
        
        $this->dados['central'] .= '<select name="cons" id="cons" onchange="qualcoord(this.value);">';
        $this->dados['central'] .= '<option value="0">-- selecione --</option>';
        
        // lista de associaveis apenas
        $lis = $this->banco->listagem('products','id','active = 1');
        if(!empty($lis))
        foreach($lis as $i)
        {
            $this->dados['central'] .= '<option value="'.$i->id.'">'.$i->name.' ('.$i->abrev.')</option>';
        }
        $this->dados['central'] .= '</select>';
        $this->dados['central'] .= '<p>&nbsp;</p><div id="resultado">Selecione uma disciplina acima para ver a relação de turmas associadas.</div>';
        
        $this->dados['central'] .= '</section>';

        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

}  
?>