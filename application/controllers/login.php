<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
	
	function __construct()
    {
		parent::__construct();
		$this->geraMenu('login');
		$this->load->model('autenticacao','autentica',TRUE);
    }
	
	function index()
	{
		
		if ($this->session->userdata('esta_logado') && $this->session->userdata('nivel'))
		{
			$this->redireciona();
		}
		else
		{	
			//$this->session->sess_destroy();
			
			$this->form_validation->set_rules('usuario','Login','required');
			$this->form_validation->set_rules('senha','Senha','required');
			
			if ($this->form_validation->run() == FALSE)
			{
				//$dados['unidades'] = $this->autentica->listagem('groups','name','',true);
				$this->load->view('_cabecalho',$this->cabecalho);
				$this->load->view('login');
				$this->load->view('_rodape',$this->rodape);
			}
			else
			{
				$resultado = $this->autentica->dados_usuario($this->autenticar());
				if ($resultado)
				{
					foreach($resultado as $row) {
						$nome_usuario = $row->name;
						$email_usuario = $row->email;
						$login_usuario = $row->username;
						$codigo = $row->id;
						$nivel = $row->isadmin;
						$nivel2 = $row->idhist;
						$grpadmin = ($row->idhist == 1) ? $this->autentica->relacao('users',$row->id,'groups') : 0;
						$grupos = $this->autentica->relacao('users',$row->id,'groups');
                        $permissoes = $this->autentica->dados_permissao($grupos[0]);
						$organizacao = $this->autentica->dados_organizacao($row->idorg);
						$travado = $row->islocked;
					}
                    
					$novosdados = array(
					   'nome_usuario'  => $nome_usuario,
					   'e-mail'     => $email_usuario,
					   'login_usuario' => $login_usuario,
					   'esta_logado' => $codigo,
					   'nivel' => $nivel,
					   'nivel2' => $nivel2,
					   'grpadmin' => $grpadmin,
					   'grupos' => $grupos,
					   'permissoes' => $permissoes,
					   'orgdata' => $organizacao
					);
					
					if($travado == "yes")
					{
						redirect('login/encerra_sessao/proibido');
					}
					
					$this->session->set_userdata($novosdados);
					$this->historico('login','');
					// Faz manutencao no historico (limpa entradas antigas)
					//$this->limpa_historico($organizacao[0]['options']);
					$this->redireciona();
				}
				else
				{
					$this->session->set_flashdata('error','Usuário ou Senha inválida.');	
					redirect('login');
				}
			}
			
		}
	}
	
	
	function autenticar()
	{
		$login = $this->input->post('usuario');
		$senha = $this->input->post('senha');
		$condicao = '';
		//$unidade = $this->input->post('unidade');
		/*if(strlen($login) == 11 && is_numeric($login)) // detecta se é CPF
		{
			$resp = $this->autentica->autentica($login, $senha, 'cpf');
		}
		else*/
		if (!empty($login) && !empty($senha))
		{
			$resp = $this->autentica->autentica($login, $senha, '');
		}
		else
			$resp = 0;
			
		return $resp;
	}
	
	function encerra_sessao($msg="")
	{
		$this->historico('logout','');
		$novosdados = array(
		           'nome_usuario'  => '',
		           'login_usuario' => '',
		           'e-mail'     => '',
		           'esta_logado' => 0,
				   'nivel' => '',
				   'nivel2' => '',
				   'grpadmin' => array(),
				   'grupos' => array(),
				   'permissoes' => array(),
				   'orgdata' => array()
		);
		$this->session->unset_userdata($novosdados);
                
		$notice = 'Sessão finalizada.';
		if($msg == 'obrigado') $notice = 'Obrigado por participar!';
		if($msg == 'proibido') $notice = '<strong>Você ainda não confirmou o seu cadastro!</strong><br>Verifique a caixa postal do seu e-mail (inclusive a pasta spam).';
		$this->session->set_flashdata('notice',$notice);
                redirect('login/saindo');
	}
    
    function saindo()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
        
    function check_session()
    {
        if($this->session->userdata('esta_logado'))
            echo 'ok';
        else 
            echo 'fail';
    }
    
    function redireciona()
    {
        $grupos = $this->session->userdata('grupos');
        $ok = false;
        
        // Verifica se o grupo permite login
        foreach($grupos as $i)
        {
            $cond = $this->banco->campo('groups','is_user','id = '.$i);
            if(!empty($cond) && $cond == "yes")
            {
                $ok = true;
                break;
            }
        }
        
        if(!$ok)
        {
            redirect('login/encerra_sessao');
        }
        else 
        {
            redirect('home');
        }
        return;
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */