<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arquivos extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('arquivos');
        $this->load->model('sistema','banco',TRUE);

        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        $this->load->library('form');
        
        // Configurando os demais menus suspensos
        $this->ocultarMenus();

        $this->session->set_userdata('pagina_atual', 'arquivos');
    }
    
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_arquivos',$c);
        
        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                $ret = false;
                break;
        // 4 = somente os que o usuário participa e dos consultores que ele coordena (para consultores/coordenadores)
            case 4:
                $proibido = array();
                $ret[0] = array($this->session->userdata('esta_logado')); //id do usuario
                $ret[1] = $this->banco->relacao('users',$this->session->userdata('esta_logado'),'users'); //id dos usuarios que ele coordena
                $ret[2] = $this->banco->idservicos(array_merge($ret[1],$ret[0])); // ids dos servicos que ele pode ver
                $ret[3] = $this->banco->idclientes($ret[0]); // ids dos clientes que ele pode ver
                break;
        // 3 = somente a pasta pessoal dele - nao ve a pasta oficial do servico (para clientes)
            case 3:
                $proibido = array('pastadocliente','listarserv','listaAjaxArquivos','listarcli');
                $ret[0] = array($this->session->userdata('esta_logado'));
                $ret[1] = array();
                $ret[2] = $this->banco->idservicos(array_merge($ret[1],$ret[0])); // ids dos servicos que ele pode ver
                $ret[3] = array();
                break;
        // outros = proibido
            default:
                $proibido = array('index','pastadocliente','minhapasta','listarserv','listaAjaxArquivos','listarcli', 'criar', 'editar');
                $ret = false;
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não possui permissão para acessar a área ARQUIVOS.');
            redirect('home');
        }
        
        return $ret;
    }
    
    public function index($result='')
    {
        $this->perm(__FUNCTION__);
        
        $this->dados['central'] = '
        <script>
        
        var currentAnchor = null;
                
        function checkAnchor()
        {
            if(currentAnchor != document.location.hash)
            {
                currentAnchor = document.location.hash;
                if(currentAnchor)
                {
                    var local = currentAnchor.substring(1);
                    if(local.length == 6)
                    return mostraArquivos(local);
                }
            }
            return false;
        }
        
        function mostraArquivos(local)
        {
            /*document.location.href=\''.($this->session->userdata('ultima_pagina') ? $this->session->userdata('ultima_pagina') : site_url('arquivos/minhapasta')).'\';*/
            window.opener.location.reload();
            window.close();
            return;
        }
        setInterval("checkAnchor()", 1500);
        </script>';
        $this->cabecalho['titulo'] = '';
        $this->cabecalho['subtitulo'] = '';
        $this->dados['central'] .= '<section><p>'.($this->session->userdata('ultima_pagina') ? 'Retornando...' : '...').'</p></section>';
        $this->session->unset_userdata('ultima_pagina');
        $this->load->view('_cabecalho_print',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape_print',$this->rodape);
    }
    
    public function pastadocliente($cod,$ajaxed=false,$lastpage="")
    {
        $cond = $this->perm(__FUNCTION__);
        
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            if($ajaxed) { $this->output->set_output("Chamada inválida!"); return false; }
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('arquivos/index');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameusu = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idusu = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idusu == 0)
        {
            if($ajaxed) { $this->output->set_output("Chamada inválida! Código não encontrado na etapa inicial."); return false; }
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado na etapa inicial.');
            redirect('arquivos/index');
        }

        // verifica se o cara pode ver/alterar esses arquivos... (admin pode)
        if($this->session->userdata('nivel') == 'no' && (empty($cond[3]) || (!in_array($idusu,$cond[3]))))
        {
            if($ajaxed) { $this->output->set_output("Sem permissão para ver os arquivos deste cliente."); return false; }
            $this->session->set_flashdata('error','Sem permissão para ver os arquivos deste cliente.');
            redirect('arquivos/index');
        }
        $this->session->set_userdata('ultima_pagina',(($ajaxed ? base64_decode($lastpage) : current_url())));
        $this->minhapasta($cod,$ajaxed);
    }
    
    public function minhapasta($cod="",$ajaxed=false,$lastpage="")
    {
        $cond = $this->perm(__FUNCTION__);
        
        if(!empty($cod))
        {
            // Verifica o padrao enviado pelo post
            $code = explode('_',base64_decode($cod));
            $nameusu = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idusu = (int) $code[2];
            $link_date = $code[3]; 
            
        
            if($idusu == 0)
            {
                if($ajaxed) { $this->output->set_output("Chamada inválida na re-checagem de segurança!"); return false; }
                $this->session->set_flashdata('error','Chamada inválida! Código não encontrado na re-checagem.');
                redirect('arquivos/index');
            }
            
            $nomeusu = $this->banco->campo('users','name','id =' . $idusu);
            if(!empty($lastpage)) $this->session->set_userdata('ultima_pagina',(($ajaxed ? base64_decode($lastpage) : current_url())));
        }
        else 
        {
            $this->session->set_userdata('ultima_pagina',(($ajaxed ? base64_decode($lastpage) : current_url())));
        }
        
        $this->cabecalho['titulo'] = 'Arquivos :: '. (empty($cod) ? 'Minha pasta' : 'Pasta do aluno '.$nomeusu);
        $this->cabecalho['subtitulo'] = 
        (empty($code) ?
        'Na pasta GERAL você pode enviar qualquer tipo de arquivo.' : 
        'Anexe documentos particulares a este aluno na pasta ESCOLA. O aluno não poderá ver o conteúdo desta pasta e apenas você visualiza os seus documentos.');
        
        $this->dados['central'] = '';
        
        $this->dados['central'] .= '
        <script language="javascript">
         $(function () {
            
         });
        </script>';
        
        $this->dados['central'] .= '<ul class="navbar nav-tabs" id="myTab">
        <li class="active"><a href="#geral" data-toggle="tab">Geral</a></li>';
        // A aba contratos só aparece para clientes
        //if($this->banco->isUser('cliente',$this->session->userdata('esta_logado')) || (isset($idusu) && $this->banco->isUser('cliente',$idusu)))
        //    $this->dados['central'] .= '<li><a href="#docs" data-toggle="tab">Contratos</a></li>';
        // A aba consultor só aparece quando não é um cliente que está logado e o perfil visualidado for de um cliente
        if(!$this->banco->isUser('cliente',$this->session->userdata('esta_logado')))
            if(isset($idusu) && $this->banco->isUser('cliente',$idusu))
                $this->dados['central'] .= '<li><a href="#consultor" data-toggle="tab">Escola</a></li>';
        $this->dados['central'] .= '</ul>';
        
        // Lista de pastas do usuário
        $lisPastas = $this->banco->listagem('folders','id','owner = ' . (!empty($idusu) ? $idusu : $this->session->userdata('esta_logado')));
        $opts = array('<h3>Pasta não encontrada</h3>','<h3>Pasta não encontrada</h3>');
        $fldr = array('0','0');
        foreach($lisPastas as $p)
        {
            // Verifica a permissao para acesso a pasta
            $folderid = $p->nameid;
            $perm = $this->checaPermissao($folderid);

            if ($perm > 0)
            {
                $dados['perm'] = $perm;
                switch($perm)
                {
                    case 1: // Admin
                    $dados['perm_text'] = '';//'Você possui privilégio total nesta pasta.';
                    //$dados['perm_link'] = anchor('#','Enviar novo arquivo',array('class' => 'btn btn-xs btn-success', 'onclick' => 'return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)'));
                    $dados['perm_link'] = '<a href="javascript:void(0)" onclick="return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)" class="btn btn-xs btn-success">Enviar novo arquivo</a>';
                    $dados['perm_edit'] = '';
                    break;
                    
                    case 2: // User com perm. escrita
                    $dados['perm_text'] = '';//'Você possui privilégios de criação (upload) e editação de documentos nesta pasta.';
                    //$dados['perm_link'] = anchor('#','Enviar novo arquivo',array('class' => 'btn btn-xs btn-success', 'onclick' => 'return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)'));
                    $dados['perm_link'] = '<a href="javascript:void(0)" onclick="return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)" class="btn btn-xs btn-success">Enviar novo arquivo</a>';
                    $dados['perm_edit'] = '';
                    break;
                    
                    case 3: // User com perm. leitura
                    $dados['perm_text'] = '';//'Você apenas pode ver os arquivos desta pasta.';
                    $dados['perm_link'] = '';
                    $dados['perm_edit'] = '';
                    break;
                    
                    case 4: // User com perm. solicitacao
                    $dados['perm_text'] = 'Sem acesso a esta pasta.';
                    $dados['perm_link'] = '';
                    $dados['perm_edit'] = '';
                    break;
                
                    case 5: // Admin
                    $dados['perm_text'] = '';//'Você possui privilégio total nesta pasta.';
                    //$dados['perm_link'] = anchor('#','Enviar novo arquivo',array('class' => 'btn btn-xs btn-success', 'onclick' => 'return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)'));
                    $dados['perm_link'] = '<a href="javascript:void(0)" onclick="return windowpop(\''.site_url('arquivos/criar/'.$folderid).'\', 600, 433)" class="btn btn-xs btn-success">Enviar novo arquivo</a>';
                    $dados['perm_edit'] = '';
                    break;
                }
            }
            else
                {
                    $dados['perm_text'] = '<h6>Você não possui privilégios de acesso a esta página.</h6>';
                    $dados['perm_text'] .= '<p>Caso necessite de acesso, contate o administrador do sistema.</p>';
                    $dados['perm_link'] = '';
                    $dados['perm_edit'] = '';
                }

            // Definindo as opções de cada pasta
            if($p->name == (empty($nameusu) ? $this->session->userdata('login_usuario') : $nameusu) && $p->folder_level == 2)
            {
                $opts[0] = '<div align="left">'.$dados['perm_text'].''.$dados['perm_link'].'</div>';
                $fldr[0] = $folderid;
            }
            /*elseif($p->name == 'docs' && $p->folder_level == 3)
            {
                $opts[1] = '<div align="left">'.$dados['perm_text'].''.$dados['perm_link'].'</div>';
                $fldr[1] = $folderid;
            }*/
            elseif($p->name == 'escola' && $p->folder_level == 3)
            {
                $opts[1] = '<div align="left">'.$dados['perm_text'].''.$dados['perm_link'].'</div>';
                $fldr[1] = $folderid;
            }
        }
        
        
        
        
        $this->dados['central'] .= '
        <div class="tab-content">
        <div class="tab-pane active" id="geral">'.$opts[0].$this->listaArquivos($fldr[0]).'</div>';
        //<div class="tab-pane" id="docs">'.$opts[1].$this->listaArquivos($fldr[1]).'</div>';
        
        if(!$this->banco->isUser('cliente',$this->session->userdata('esta_logado')))
            $this->dados['central'] .= '<div class="tab-pane" id="consultor">'.$opts[1].$this->listaArquivos($fldr[1]).'</div>';
        
        $this->dados['central'] .= '</div>
        ';
        
        if($ajaxed) { $this->output->set_output($this->dados['central']); return false; }
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function listarserv($qualserv="")
    {
        // Lista os serviços disponíveis e seus arquivos
        $cond = $this->perm(__FUNCTION__);
        $filt = "";
        
        if($cond)
        {
            if(!empty($cond[2])) {
                // Só os serviços que ele pode ver
                $filt .= 'id in ('.implode(',',$cond[2]).')';
                
            } else { $filt .= 'id = 0'; }
        }
        
        $this->cabecalho['titulo'] = 'Arquivos :: Contratos de Serviços';
        $this->cabecalho['subtitulo'] = 'Contratos gerados para cada serviço.';
        
        $this->dados['central'] = '';
        
        $this->dados['tabela'] = '<script>
        function fnFormatDetails ( oTable, nTr, code )
        {
            $.get("'.site_url('arquivos/listaAjaxArquivos').'/"+code,function(data){
                $("td.listinha"+code).css("padding","5px").html("Carregando...").html(data);
            })
            return;
        }
        
        
        $(document).ready(function() {
            
        var oTable = $("#listarserv").dataTable({
                 "iDisplayLength": 50,
                 "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" }
                    
        });
        
        $("a#alteraDownload").click(function(){
                var code = $(this).attr("rel");
                if(code == "") return false;
                var url = \''.site_url('documentos/download').'/\'+code+\'\';
                document.location.href=url;
                return false;
            });
        
        $("button#enviarArqs").click(function(){
                var code = $(this).attr("rel");
                if(code == "") return false;
                var url = \''.site_url('arquivos/criar/999999').'/\'+code+\'\';
                document.location.href=url;
                return false;
            });
        
        $("button#listarArqs").live(\'click\',function(){
                var code = $(this).attr("rel");
                var nTr = $(this).parents(\'tr\')[0];
                if(code == "") return false;
                if ( oTable.fnIsOpen(nTr) )
                {
                    $(this).html("Listar arquivos");
                    oTable.fnClose( nTr );
                }
                else
                {
                    /* Open this row */
                    $(this).html("Ocultar...");
                    oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr, code), \'listinha\'+code );
                }
         });
         
         '.($qualserv>0 ? '$("#listarArqs[rel='.$qualserv.']").trigger("click");' : '').'
        
        });
        
        </script>';
        
        if($qualserv>0)
        {
            if(empty($filt))
            {
                $filt = "id = ".$qualserv;
            }
            else 
            {
                if($filt != "id = 0" && in_array($qualserv,$cond[2]))
                    $filt = "id = ".$qualserv;
            }
        } 
        
        $this->dados['tabela'] .= '<form name="listarservfrom" id="listarservform" method="post">';
        $this->dados['tabela'] .= '<table id="listarserv"><thead><tr>';
        $this->dados['tabela'] .= '<th>Cód.</th>
                                   <th>Nome</th>
                                   <th>Cliente</th>
                                   <th>Status</th>
                                   <th>Arquivos</th>
                                   <th>Ações</th>
                                   ';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $lis = $this->banco->listagem('status','id','idorg = '.$this->org['id']);
        $status[0] = '???';
        foreach($lis as $item)
        {
            $status[$item->id] = $item->name;
            $statuscolor[$item->id] = $item->color;
        }
        
        $listagem = $this->banco->listagem('services','name',$filt);
        $baixar = "";
        foreach($listagem as $item)
        {
            // Buscando o ID e o nome do cliente
            if(empty($item->data_array)) 
                $cli = 0;
            else {
                $masterArray = unserialize($item->data_array);
                if(empty($masterArray["clients"][0]))
                    $cli = 0;
                else
                    $cli = $masterArray["clients"][0];
                // Busca o local de download do pdf (se existir)
                if(!empty($masterArray["caminho_doc"]))
                    $baixar = $masterArray["caminho_doc"];
                else $baixar = '';
                // Busca a chave para acesso a pasta do cliente
                $usuariocod="";
                if($cli > 0) :
                    $listagemu = $this->banco->listagem('users','name','id = '.$cli);
                    foreach($listagemu as $itemu)
                    {
                        $usuariocod = base64_encode('e_'.$itemu->username.'_'.$itemu->id.'_'.time());
                    }
                endif;
            }
            
            $this->dados['tabela'] .= '<tr><td><span style="text-align:center"><strong>'.$item->id.'</strong></span></td>
                                   <td>'.$item->name.'</td>
                                   <td>'.(empty($cli) ? '???' : 
                                   '<a title="Clique para ver os arquivos deste cliente" href="'.site_url('arquivos/pastadocliente/'.$usuariocod).'">
                                   '.$this->banco->campo('users','name','id = ' . $cli).'</a>').
                                   '</td>
                                   <td><span style="color:'.$statuscolor[$item->idstatus].'">'.$status[$item->idstatus].'</span></td>
                                   <td>'.$this->listaAjaxArquivos($item->id,true).'</td>
                                   <td>
                                    <button class="btn btn-xs btn-primary" type="button" id="listarArqs" rel="'.$item->id.'">Listar arquivos</button>
                                    <button class="btn btn-xs" type="button" id="enviarArqs" rel="'.$item->id.'">Enviar arquivo</button></td>
                                   </tr>';
        }
        
        $this->dados['tabela'] .= '</tbody></table></form>';
        $this->dados['central'] = '<section>'.$this->dados['tabela'].'<div id="salvou" style="display:none;"><small style="color:green"><i class="fas fa-thumbs-up"></i> Salvo!</small></div></section>';
        
        $this->session->set_userdata('ultima_pagina',current_url());
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function listaAjaxArquivos($idserv,$justqtde=false)
    {
        // Lista os serviços disponíveis e seus arquivos
        $cond = $this->perm(__FUNCTION__);
        $filt = "";
        
        /*
        if($cond)
        {
            if(!empty($cond[2])) {
                // Só os serviços que ele pode ver
                if(!in_array($idserv,$cond[2]))
                {
                    echo '<p>Sem permissão para ver estes arquivos!</p>';
                    return false;
                }
                
            } else {
                echo '<p>Sem permissão para ver estes arquivos!</p>';
                return false;
            }
        }
        */ 
            
        $lista = $this->banco->listagem('docsuser','name','idserv = '. (int) $idserv);
        if(empty($lista))
        {
            if($justqtde) return 0;
            echo '<p>Nenhum arquivo encontrado.</p>';
            return false;
        }

        $tbl = '<table width="100%" border="0" style="font-size:11px">
          <thead class="ui-state-default">
          <tr>
            <td>Nome</td>
            <td>Descrição</td>
            <td>Criado por</td>
            <td>Data</td>
            <td>&nbsp;</td>
          </tr>
          </thead><tbody>';
        $c = 0;
        
        // Lista de icons
        $icons = $this->geraFileTypes(true);
        
        // Lista de usuarios
        $res = $this->banco->listagem('users','name','idorg = '. $this->org['id']);
        foreach($res as $row)
            $usuarios[$row->id] = $row->name;
        
        // Lista de grupos
        $res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
        foreach($res as $row)
            $grupos[$row->id] = $row->name;
        
        foreach($lista as $item)
        {
            //$icon = '<img src="'.$icons[$this->verifica_mime_type($item->type)].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />';
            if(empty($icons[$this->verifica_mime_type($item->type)])) $ico = $icons['txt']; else $ico = $icons[$this->verifica_mime_type($item->type)];
            $icon = '<img src="'.$ico.'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
            
            $baixar = base64_encode($item->id . '|||' . $item->date_created);
            $folderid_real = $this->banco->relacao('docsuser',$item->id,'folders');
            $folderid = $this->banco->campo('folders','nameid','id in ('.(is_array($folderid_real) ? implode(',',$folderid_real) : $folderid_real).')');
            if(is_array($folderid)) $folderid = $folderid[0];
            $strRemove = base64_encode($item->id . '|||' . $item->date_created. '|||'.$folderid.'|||'.$item->idusr);
            $hasDoc = $this->banco->campo('services','document','id = ' . (int) $idserv);
            
            $acoes = '
                '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar,'<i class="fas fa-download"></i>')).'&nbsp;|&nbsp;
                '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? anchor('arquivos/editar/'.$item->id,'<i class="fas fa-pencil-alt"></i>') . '&nbsp;|&nbsp;' : '') .'
                '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? '<a href="javascript:void(0)" onclick="if(confirm(\'Tem certeza?\')){removeArq(\''.$strRemove.'\');} else return false;"><i class="fas fa-times"></i></i></a>' : '') .'
            ';
            
            if(($item->idtpl == 1) || ($item->idtpl == 0 && $baixar == $hasDoc))
            {
                // Só admin altera ou remove
                if($this->session->userdata('nivel') == 'yes')
                {
                    $acoes = '
                    '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar,'<i class="fas fa-download"></i>')).'&nbsp;|&nbsp;
                    '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? anchor('arquivos/editar/'.$item->id,'<i class="fas fa-pencil-alt"></i>') . '&nbsp;|&nbsp;' : '') .'
                    '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? '<a href="javascript:void(0)" onclick="if(confirm(\'Tem certeza?\')){removeArq(\''.$strRemove.'\');} else return false;"><i class="fas fa-times"></i></i></a>' : '') .'
                    ';
                }
                else 
                {
                    $acoes = '
                    '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar,'<i class="fas fa-download"></i>')).'
                    ';
                }
                
                if($item->idtpl == 1)
                {
                    $acoes = '
                    '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar,'<i class="fas fa-download"></i>')).'
                    ';
                }
            }
            
            $tbl .= '
            <tr><td>
                '. (empty($item->content) ? $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>" : $icon).'&nbsp;
                <strong>'.anchor('documentos/download/'.$baixar,$item->name,array("title" => "Arquivo ". strtoupper(basename($ico,".png")))).'</strong>
                </td>
                <td>'. (strstr($item->desc,'1') ? '...' : word_limiter($item->desc,15)).'</td>
                <td><abbr title="'.$grupos[$item->idgrp] .'">'. $usuarios[$item->idusr] .'</abbr></td>
                <td>'. date('d/m/Y H:i',strtotime($item->date_created)) .'</td>
                <td>'.$acoes.'</td>
            </tr>
            ';
            $c++;
        }
        $tbl .= "</tbody></table>";
        $tbl .= '<script>function removeArq(str)
        {
            $.get("'.site_url('arquivos/remover').'/"+str,function(data){
                if(data) {alert(data);} else {
                    document.location.reload();
                }
            });
        }</script>
        ';
        
        if($justqtde) return $c;
        echo $tbl;
        return false;
        
    }

    public function listaAjaxArquivosServico($idserv,$justqtde=false)
    {
        // Lista os serviços disponíveis e seus arquivos
        $cond = $this->perm(__FUNCTION__);
        $filt = "";
        
        if($cond)
        {
            if(!empty($cond[2])) {
                // Só os serviços que ele pode ver
                if(!in_array($idserv,$cond[2]))
                {
                    echo '<p>Sem permissão para ver estes arquivos!</p>';
                    return false;
                }
                
            } else {
                echo '<p>Sem permissão para ver estes arquivos!</p>';
                return false;
            }
        }
            
            
        $lista = $this->banco->listagem('docsuser','date_created','idserv = '. (int) $idserv);
        if(empty($lista))
        {
            if($justqtde) return 0;
            echo '<p>Nenhum arquivo encontrado.</p><!--<p><button class="btn btn-xs" onclick="anexaLocal()" type="button"><i class="fas fa-upload"></i>&nbsp;Enviar arquivo</button></p>-->';
            return false;
        }

        $tbl = '<table width="100%" border="0" style="font-size:11px">
          <thead class="ui-state-default">
          <tr>
            <td>Nome</td>
            <td>Descrição</td>
            <td>Criado por</td>
            <td>Data</td>
            <td>&nbsp;</td>
          </tr>
          </thead><tbody>';
        $c = 0;
        
        // Lista de icons
        $icons = $this->geraFileTypes(true);
        
        // Lista de usuarios
        $res = $this->banco->listagem('users','name','idorg = '. $this->org['id']);
        foreach($res as $row)
            $usuarios[$row->id] = $row->name;
        
        // Lista de grupos
        $res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
        foreach($res as $row)
            $grupos[$row->id] = $row->name;
        
        foreach($lista as $item)
        {
            //$icon = '<img src="'.$icons[$this->verifica_mime_type($item->type)].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />';
            if(empty($icons[$this->verifica_mime_type($item->type)])) $ico = $icons['txt']; else $ico = $icons[$this->verifica_mime_type($item->type)];
            $icon = '<img src="'.$ico.'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
            $folderid = 999999;
            $baixar = base64_encode($item->id . '|||' . $item->date_created);
            $strRemove = base64_encode($item->id . '|||' . $item->date_created. '|||'.$folderid.'|||'.$item->idusr);
            $hasDoc = $this->banco->campo('services','document','id = ' . (int) $idserv);
            
            if($item->idtpl == 0 && $baixar != $hasDoc) :
                
                $tbl .= '
                <tr>
                    <td>
                    '. (empty($item->content) ? $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>" : $icon).'&nbsp;<strong>'.anchor('documentos/download/'.$baixar,$item->name).'</strong>
                    </td>
                    <td>'. (strstr($item->desc,'1') ? '...' : word_limiter($item->desc,15)).'</td>
                    <td><abbr title="'.$grupos[$item->idgrp] .'">'. $usuarios[$item->idusr] .'</abbr></td>
                    <td>'. date('d/m/Y H:i',strtotime($item->date_created)) .'</td>
                    <td>
                    '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? '<a href="javascript:void(0)" onclick="if(confirm(\'Tem certeza?\')){removeArq(\''.$strRemove.'\');} else return false;"><i class="fas fa-times"></i></i></a>' . '&nbsp;|&nbsp;' : '') .'
                    '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar,'<i class="fas fa-download"></i>')).'
                    </td>
                </tr>
                ';
                $c++;
            
            endif;
        }
        if($c == 0) 
        {
            $tbl .= '<tr><td colspan=5>Nenhum arquivo encontrado.';
            $tbl .= '<!--<p><button class="btn btn-xs" onclick="anexaLocal()" type="button"><i class="fas fa-upload"></i>&nbsp;Enviar arquivo</button></p>-->';
            $tbl .= '</td></tr>';
            $tbl .= "</tbody></table>";
        }
        $tbl .= '
        <script>
        function anexaLocal()
        {
            var caminho = "'.site_url('arquivos/anexar/'.$idserv).'";
            window.open(caminho,\'Prvwindow\',\'status=0, toolbar=0, location=0, menubar=0, directories=0, \' +
            \'resizable=0, scrollbars=1, width=800, height=700\');
            
            return false;
        }
        
        function removeArq(str)
        {
            $.get("'.site_url('arquivos/remover').'/"+str,function(data){
                if(data) {alert(data);} else {
                    window.location.hash = \'arquivos\';
                    document.location.reload();
                }
            });
        }
        </script>
        ';
        
        $tbl .= '<p><button class="btn btn-xs" onclick="anexaLocal()" type="button"><i class="fas fa-upload"></i>&nbsp;Enviar arquivo</button></p>';
        
        if($justqtde) return $c;
        echo $tbl;
        return false;
        
    }

    public function listarcli()
    {
        $cond = $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Arquivos :: Ver arquivos de clientes';
        $this->cabecalho['subtitulo'] = 'Veja os arquivos, compartilhe e anexe documentos a este cliente.';
            
        $usrs = $this->banco->relacao('users','','groups',7); // 7 = clientes 
        $filt = "id in (".implode(',',$usrs).")"; // Somente clientes
        
        if($cond)
        {
            if(!empty($cond[3])) :
                
                $filt .= ' and id in ('.implode(',',$cond[3]).')';
                
            endif;
        }
        
        //if($ret) $filt = "idhist = ".$ret;
        $listagem = $this->banco->listagem('users','name',$filt);
        foreach($listagem as $item)
        {
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp[0] = 0;
            $usuarios[base64_encode('e_'.$item->username.'_'.$item->id.'_'.time())] = '['. $item->username . '] ' . $item->name;
        }
        
        $this->dados['central'] = '<section><p>Selecione um cliente</p>';
        
        $this->dados['central'] .= '
        <div class="input-append">
        <select class="col-4" id="appendedInputButton" name="usu" id="usu">';
        
        if(empty($usuarios))
        {
            $this->session->set_flashdata('notice','Não há clientes para você.');
            redirect('arquivos/index');
        }
        
        foreach($usuarios as $c => $i)
        {
            $this->dados['central'] .= '<option value="'.$c.'">'.$i.'</option>';
        }
        
        $this->dados['central'] .= '</select>
        <button class="btn btn-primary" id="alteraArquivos" type="button">Ver arquivos</button>
        </div>
        <script>$(document).ready(function() {
            
            $("button#alteraArquivos").click(function(){
                var code = $("select[name=\'usu\'] option:selected").val();
                var url = \''.site_url('arquivos/pastadocliente').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>
        ';
        
        $this->dados['central'] .= '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    private function listaArquivos($folderid)
    {
        if(empty($folderid) || $folderid == 0)
        {
            return 'Pasta não encontrada no banco de dados.';
        }
            
        $perm = $this->checaPermissao($folderid);
        $dados['perm'] = $perm;
        
            $res = $this->banco->listagem('folders','name','nameid = "'.$folderid.'"');
            if (!$res)
            {
                return 'Pasta não encontrada no banco de dados.';
            }
            
            foreach($res as $row) {
                $folder_id = $row->id;
                $dados['name'] = $row->name;
                $dados['desc'] = $row->desc;
                $folder_parent = $row->folder_parent;
                $folder_level = $row->folder_level;
            }
            
            if ($folder_parent > 0) // possui pasta-pai
            {
                $tmp = $dados['name'];
                $dados['name'] = '';
                for($i=1;$i<=$folder_level-1;$i++)
                {
                    if ($i > 1)
                    $dados['name'] = $this->banco->campo('folders','name','nameid = "'.$folder_parent.'"') . ' / ' . $dados['name'];                    
                    else
                    $dados['name'] .= $this->banco->campo('folders','name','nameid = "'.$folder_parent.'"') . ' / ';
                    $folder_parent = $this->banco->campo('folders','folder_parent','nameid = "'.$folder_parent.'"');
                }
                $dados['name'] .= $tmp;
            }
            
            // Lista de usuarios
            $res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id']);
            foreach($res as $row)
                $dados['usuarios'][$row->id] = $row->name;
            
            // Lista de grupos
            $res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
            foreach($res as $row)
                $dados['grupos'][$row->id] = $row->name;
            
            $dados['folderid'] = $folderid;
            $dados['idf'] = $folder_id;
            
            // Checa se a pasta é exclusiva para templates
            if ($this->verificaPastaTemplate($folder_id))
            {
                $dados['resultado_query'] = $this->banco->listagem_docsuser($folder_id,'idorg = '. $this->org['id']);
                // Busca o dado do primeiro documento (template pai) onde temos o nome dos campos
                foreach($dados['resultado_query'] as $item) {
                    $dados['name_tpl'] = $item->name;
                    $dados['desc_tpl'] = $item->desc;
                    $dados['date_tpl'] = date('d/m/Y H:i',strtotime($item->date_created));
                    $dados['tpl'] = $this->banco->dados('template',$item->idtpl);
                    break;
                }
                // Busca o nome dos campos
                foreach($dados['tpl'] as $item)
                {
                    if ($item->template_type == 2) // template de campo unico
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        $dados['cols_tpl'][1] = 'Título';
                        $dados['cols_tpl'][2] = 'Descrição';
                        $dados['cols_tpl'][3] = 'Criado por';
                        $dados['cols_tpl'][4] = 'Data';
                        $dados['cols_tpl'][5] = 'Opções';
                    }
                    elseif ($item->template_type == 3) // template de varios campos sem def. de areas
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        $dados['cols_tpl_tmp'][0] = 'Cód.';
                        $dados['cols_asterisk'] = array();
                        $dados['cols_asterisk_tmp'] = array();
                        $tmp_counter = 1;
                        $counter = 1;
                        $tmp_tpl = $item->template_value;
                        // Extrai os campos definidos no documento
                        preg_match_all("/(?<=(\[\[))(.*?)(?=\])/",$tmp_tpl,$campos_tpl);
                        //print_r($campos_tpl[0]); exit;
                        if(!empty($campos_tpl[0]))
                        foreach($campos_tpl[0] as $campo)
                        {
                            // Adiciona somente os que possuem asterisco no final
                            if (strstr($campo,'*'))
                            {
                                $dados['cols_tpl'][$counter] = str_replace('*','',$campo);
                                $dados['cols_asterisk'][$tmp_counter] = $tmp_counter;
                                $counter++;
                            }
                            else
                            {
                                $dados['cols_tpl_tmp'][$tmp_counter] = $campo;
                                $dados['cols_asterisk_tmp'][$tmp_counter] = $tmp_counter;
                            }
                            $tmp_counter++;
                        }
                        // Se nao achou nenhum campo com asterisco, coloca todos.
                        if ($counter == 1)
                        {
                            $dados['cols_tpl'] = $dados['cols_tpl_tmp'];
                            $counter = $tmp_counter;
                            $dados['cols_asterisk'] = $dados['cols_asterisk_tmp'];
                        }
                        // Insere os campos finais
                        $dados['cols_tpl'][$counter++] = 'Data';
                        $dados['cols_tpl'][$counter++] = 'Opções';
                        
                    }
                    else // template de vários campos com def. de areas
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        
                        $tmp_counter = 1;
                        $titulo = md5($item->title);
                        $tmp_tpl = $item->template_value;
                        $tmp_srl = explode("||",$tmp_tpl);
                        foreach($tmp_srl as $newitem) 
                        {
                            if (strstr($newitem,$titulo))
                            {
                                $newitem = unserialize($newitem);
                                if (!empty($newitem))
                                foreach($newitem[$titulo] as $brandnewitem)
                                {
                                    $dados['cols_tpl'][$tmp_counter] = $brandnewitem['fname'];
                                    $tmp_counter++;
                                }
                            }
                        }
                        $dados['cols_tpl'][$tmp_counter++] = 'Data';
                        $dados['cols_tpl'][$tmp_counter++] = 'Opções';
                    }
                }
                
                //if ($dados['perm_link'] == anchor('arquivos/criar/'.$folderid,'Enviar novo documento'))
                    //$dados['perm_link'] = anchor('documentos/criar_registro/'.$folderid,'Criar novo registro');
                
                return $this->load->view('arquivos/listar_tpl',$dados,true);
            }
            else
            {
                $dados['icons'] = $this->geraFileTypes(true);
                $dados['resultado_query'] = $this->banco->listagem_docsuser($folder_id,'idorg = '. $this->org['id']);
                return $this->load->view('arquivos/listar',$dados,true);
            }
    }
    
    private function verificaPastaTemplate($id)
    {
        $check = $this->banco->relacao('docsuser','','folders',$id);
        $tpl = 0;
        if (count($check) == 0)
            return false;
        else
        {
            foreach($check as $iddoc)
            {
                $tpl = $this->banco->campo('docsuser','idtpl','id = '. $iddoc);
            }       
        }
        //if ($tpl > 0)
            //return true;
        //else
            return false;
    }
    
    public function verifica_mime_type($mime,$inverse=false)
    {
        if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT))
        {
            include(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT);
        }
        elseif (is_file(APPPATH.'config/mimes'.EXT))
        {
            include(APPPATH.'config/mimes'.EXT);
        }
        
        if ($inverse)
        {
            if(empty($mimes[$mime])) return "txt";
            $val = $mimes[$mime];
            if (is_array($val))
                return $val[0];
            else 
              return $val;
        }
        // Verificando os tipos mais comuns primeiro
        if (stristr($mime,'excel')) return "xls";
        if (stristr($mime,'jpeg')) return "jpg";
        if (stristr($mime,'jpg')) return "jpg";
        if (stristr($mime,'spreadsheetml')) return "xlsx";
        
        $extensao = $this->array_search_key2($mime,$mimes);
        if (is_array($extensao)) $extensao = $extensao[0]; // TODO: não está funcionando bem...
        if (strlen($extensao) > 1)
        {
            return $extensao;
        }
        else
        {
            return "txt";
        }
    }
    
    private function array_search_key2($needle, $haystack,$strict=false,$path=array())
    {
        if( !is_array($haystack) ) {
        return false;
        }
     
        foreach( $haystack as $key => $val ) {
            if( is_array($val) && $subPath = $this->array_search_key2($needle, $val, $strict, $path) ) {
                $path = array_merge($path, array($key), $subPath);
                return $path;
            } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
                $path[] = $key;
                return $path;
            }
        }
        return false;
    }
    
    public function format_filesize($size) 
    {
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
      if ($size == 0) 
      {
         return('n/a'); 
      } 
      else 
      {
         return (round($size/pow(1024, ($i = floor(log($size, 1024)))), $i > 1 ? 2 : 0) . $sizes[$i]); 
      }
    }
    
    public function gera_numeracao($iddoc, $datadoc)
    {
        // Busca dados do documento e pasta
        $rel = $this->banco->relacao('docsuser',$iddoc,'folders');
        if (empty($rel)) return false;
        foreach($rel as $val)
        {
            if (empty($val)) return false;
            $lista = $this->banco->dados('folders',$val);
        }
        // Verifica se é uma pasta com template
        $tpl = $this->verificaPastaTemplate($val);
        foreach($lista as $item) :
            if (empty($item->num) || $item->num == 0)
            {
                return $iddoc .'-'. date('nY',$datadoc);
            }
            elseif ($item->num == -1) // Numeração anual
            {
                // pega a lista de documentos na pasta
                $onde = 'type = "docsuser___folders" and idobj2 = '. $val;
                $d = $this->banco->listagem('relationship','date_created',$onde);
                $ano_atual = '1990';
                foreach($d as $ditem) :
                    // define o ano
                    $ano = date('Y',strtotime($ditem->date_created));
                    if ($ano != $ano_atual)
                    {
                        $contador = 1;
                        $ano_atual = $ano;
                    }
                    // define qual o numero adequado
                    if($ditem->idobj1 == $iddoc)
                    {
                        if ($tpl) $contador--;
                        return $contador.'/'.$ano;
                        break;
                    }
                    $contador++;
                endforeach;
            }
            elseif ($item->num > 0) // Numeracao personalizada
            {
                // pega a lista de documentos na pasta
                $onde = 'type = "docsuser___folders" and idobj2 = '. $val;
                $d = $this->banco->listagem('relationship','date_created',$onde);
                if ($tpl)
                    $contador = $item->num - 1;
                else 
                    $contador = $item->num;
                foreach($d as $ditem) :
                    // define o ano
                    $ano = date('Y',strtotime($ditem->date_created));
                    // define qual o numero adequado
                    if($ditem->idobj1 == $iddoc)
                    {
                        return $contador.'/'.$ano;
                        break;
                    }
                    $contador++;
                endforeach;
            }
        endforeach;
    }

    public function criar($folderid,$idserv="",$redireciona="")
    {
        // Verifica permissões de acesso
        /*$perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Você não possui permissão de acesso a esta página');        
            redirect('arquivos');
        }*/
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
            
        $this->form_validation->set_rules('folder_list','Pasta','required');
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        $tmpdocs = $this->geraFileTypes();
        
        $this->dados['tipos_documentos'] = '<option value="pdf" selected="selected">Padrão (automático)</option>';
        foreach($tmpdocs as $item)
        {
            if ($item != '..png' || $item != '.image.png')
            $this->dados['tipos_documentos'] .= '<option value="'.basename($item,'.png').'">'.strtoupper(basename($item,'.png')).'</option>';
        }
        $this->dados['tipos_documentos'] .= '<option value="avi">Outros vídeos</option>';
        $this->dados['tipos_documentos'] .= '<option value="gif">Outras imagens</option>';
        $this->dados['tipos_documentos'] .= '<option value="doc">Outros documentos</option>';
        $this->dados['tipos_documentos'] .= '<option value="ini">Outros arquivos</option>';
        
        $this->dados['folderid'] = $folderid;
        $this->dados['idserv'] = $idserv;
        
        $this->dados['folder_list'] = '<input name="folder_list" id="folder_list" type="hidden" value="'.$folderid.'" />';
        $this->dados['folder_list'] .= '<strong>Pasta de destino:</strong> ' . $this->banco->campo('folders','name','nameid = '. $folderid) . '';
        
        
        $dados = array();   
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho_print',$this->cabecalho);
            $this->load->view('arquivos/criar',$this->dados);
            $this->load->view('_rodape_print',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $nameid_destino = $this->input->post('folder_list');
            $pasta_destino = $nameid_destino;
            $pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','id = "'.$pasta_destino.'"');
            $dados_arquivo = array('name' => $this->input->post('name'),'folder' => $nome_pasta);
            // verifica se é upload ou template
            if ($this->input->post('document_option') == 'opcao1')
            // UPLOAD DE ARQUIVO
            {
                // Verifica se a pasta é apta para receber um arquivo
                if($this->verificaPastaTemplate($pasta_destino))
                {
                    $this->session->set_flashdata('error','A pasta especificada não é apta para receber arquivos binários.');
                    if(!empty($redireciona)) {
                            echo "<script>alert('A pasta especificada não é apta para receber arquivos binários.');window.close();</script>";
                        } else
                    redirect('arquivos');
                    exit;
                }
                    
                if (!empty($_FILES))
                {
                    $tamanho = $_FILES['filepath']['size'];
                    $tipo = $_FILES['filepath']['type'];
                    $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                    //$conteudo = mysql_real_escape_string($conteudo);
                    //if (!stristr($tipo,'pdf'))
                    // Verificando o tamanho do arquivo
                    if($tamanho >= 15900000)
                    {
                        $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                        if(!empty($redireciona)) {
                            echo "<script>alert('O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');window.close();</script>";
                        } else
                        redirect('arquivos/index#'. $nameid_destino);
                        exit;
                    }
                    
                    $conteudo = @gzdeflate($conteudo);
                    if(!$conteudo)
                    {
                        $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                        if(!empty($redireciona))  {
                            echo "<script>alert('Falha ao compactar arquivo.');window.close();</script>";
                        }  else
                        redirect('arquivos/index#'. $nameid_destino);
                        exit;
                    }
                    $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo);
                    if ($ok > 0)
                    {
                        $this->session->set_flashdata('success','O arquivo foi gravado com sucesso.');
                        $dados['id'] = $ok;
                    }
                    else
                    $this->session->set_flashdata('error','Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.');
                }
                else
                {
                    $this->session->set_flashdata('notice','O arquivo enviado está vazio ou não foi especificado! Edite o registro criado e envie o arquivo novamente.');
                    $conteudo = '';
                }
                $template_id = '';
            }
            $g = $this->session->userdata('grupos');
            $dados = array(
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'date_created' => date('Y-m-d H:i:s'),
                'readonly' => $this->input->post('readonly'),
                'idgrp' => $g[0],
                'idusr' => $this->session->userdata('esta_logado'),
                'idorg' => $this->org['id'],
                'idserv' => $this->dados['idserv'],
                'idtpl' => $template_id,
                'locked' => 'no'
            );
            $retorno = 0;
            
            if (empty($ok))
            {
                $retorno = $this->banco->insere('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$retorno,'folders',$pasta_destino);
            }
            else
            {
                $dados['id'] = $ok;
                $retorno = $this->banco->atualiza('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$ok,'folders',$pasta_destino);
            }
                
            if ($retorno > 0)
            {
                if(!empty($redireciona))  {
                            echo "<script>alert('Arquivo enviado.');window.close();</script>";
                        }  else
                redirect('arquivos/index#'. $folderid);
            }
            else
            {
                $this->session->set_flashdata('error','O arquivo '.$dados['name'].' não foi criada devido a um erro no sistema.');
                if(!empty($redireciona))  {
                            echo "<script>alert('O arquivo não foi enviado devido a um erro no sistema.');window.close();</script>";
                        }  else
                redirect('arquivos/criar/'. $folderid);
            }
        }
    }

    public function anexar($idservice,$tipo=2)
    {
        if(empty($idservice) || empty($tipo))
        {
            echo 'Chamada inválida!';
            return false;
        }
        
        if($tipo == 1)
        {
            $prod_titulo = '[Proposta '.$idservice.'] Contrato ProLucro';
        }
            
        $dadospage['central'] = '
        <script language="JavaScript" type="text/javascript">
         function CloseAndRefresh() 
          {
             opener.location.reload(true);
             //self.close();
          }
          
          $(document).ready(function(){
              $("ul.nav.nav").hide();
              $("div.well.sidebar").hide();
          });
          
          $(window).unload(function() {
              opener.location.reload(true);
          });
          
        </script>
        <section>
        <div class="form-actions">
        <form action="'.site_url('arquivos/anexar/'.$idservice).'" method="post" accept-charset="utf-8" name="form2" id="form2" enctype="multipart/form-data">        
                        <input type="hidden" name="permission" id="permission" value="0" />
                        <input type="hidden" name="document_option" value="opcao1" />
                        <input type="hidden" name="readonly" value="no" />
                        <label>Arquivo</label>
                        <p><input name="filepath" type="file" required="required" /></p>
                        <label>Nome do documento</label> 
                        <input type="text" name="name" id="name" required="required" value="'.(empty($prod_titulo) ? '' : $prod_titulo).'" />
                        <label>Descrição</label> 
                        <textarea name="desc" id="desc" cols="45" rows="5" required="required" placeholder="Digite um breve resumo da proposta aqui."></textarea>
                        <input name="type" id="type" type="hidden" value="pdf" />
                        <p>
                        <button type="submit" class="btn btn-warning">Anexar este arquivo</button><br><small>Todos os campos acima são obrigatórios.</small>
                        </p>
        </form></div></section>';
        
        //$this->form_validation->set_rules('filepath','Arquivo','required');
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('padrao',$dadospage);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $nameid_destino = 999999;
            $pasta_destino = $nameid_destino;
            $pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','id = "'.$pasta_destino.'"');
            $dados_arquivo = array('name' => $this->input->post('name'),'folder' => $nome_pasta);
            // verifica se é upload ou template
            $success = "Erro";
            if ($this->input->post('document_option') == 'opcao1')
            // UPLOAD DE ARQUIVO
            {
                // Verifica se a pasta é apta para receber um arquivo
                if($this->verificaPastaTemplate($pasta_destino))
                {
                    echo "<script>alert('A pasta especificada não é apta para receber arquivos binários.');window.close();</script>";
                    exit;
                }
                    
                if (!empty($_FILES))
                {
                    $tamanho = $_FILES['filepath']['size'];
                    $tipo = $_FILES['filepath']['type'];
                    $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                    
                    // Nova maneira de detectar o MIME type etc
                    $ext = pathinfo($_FILES["filepath"]["name"], PATHINFO_EXTENSION);
                    
                    $ext_tmp = $this->verifica_mime_type($tipo);
                    if($ext_tmp == 'txt' && $ext != 'txt')
                        $tipo = $this->verifica_mime_type($ext,true);
                    
                    //$conteudo = mysql_real_escape_string($conteudo);
                    //if (!stristr($tipo,'pdf'))
                    // Verificando o tamanho do arquivo
                    if($tamanho >= 15900000)
                    {
                        echo "<script>alert('O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');window.close();</script>";
                        exit;
                    }
                    
                    $conteudo = @gzdeflate($conteudo);
                    if(!$conteudo)
                    {
                        echo "<script>alert('Falha ao compactar arquivo.');window.close();</script>";
                        exit;
                    }
                    $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo);
                    if ($ok > 0)
                    {
                        $success = 'O arquivo foi anexado.';
                        $dados['id'] = $ok;
                    }
                    else
                    $success = 'Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.';
                }
                else
                {
                    echo "<script>alert('O arquivo não foi especificado ou é inválido.');window.close();</script>";
                    exit;
                    $conteudo = '';
                }
                $template_id = '';
            }
            $g = $this->session->userdata('grupos');
            $dados = array(
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'date_created' => date('Y-m-d H:i:s'),
                'readonly' => $this->input->post('readonly'),
                'idgrp' => $g[0],
                'idusr' => $this->session->userdata('esta_logado'),
                'idorg' => $this->org['id'],
                'idserv' => $idservice,
                'idtpl' => $template_id,
                'locked' => 'no'
            );
            $retorno = 0;
            
            if (empty($ok))
            {
                $retorno = $this->banco->insere('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$retorno,'folders',$pasta_destino);
            }
            else
            {
                $dados['id'] = $ok;
                $retorno = $this->banco->atualiza('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$ok,'folders',$pasta_destino);
            }
                
            echo "<script>window.close();</script>";
            return false;
        }
    }

    public function editar($idfile)
    {
        
        if (empty($idfile) || !is_numeric($idfile))
        {
            echo 'Falha na chamada da URL';
            return false;
        }
        
        // Busca o nameid da pasta na qual o documento pertence (considera-se pasta unica)
        $fid = $this->banco->relacao('docsuser',$idfile,'folders');
        $folderid = $this->banco->campo('folders','nameid','id = '. $fid[0]);
        
        // Verifica permissao
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');        
            redirect('arquivos/index#'. $folderid);
        }

        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
            
        // Regra de validacao do from
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        // Busca todos os campos do documento
        $listagem = $this->banco->listagem('docsuser','name','id = '. (int) $idfile);
        $this->dados['resultado_query'] = $listagem;
        foreach($listagem as $f)
        {
            $ext = $this->verifica_mime_type($f->type);
            if ($f->readonly == 'yes' && $perm != 1)
            {
                $this->session->set_flashdata('notice','Documento configurado como somente leitura');       
                redirect('arquivos/index#'. $folderid);
            }
        }
        
        // Lista de filetypes do form (talvez ficará obsoleto isto pois o automático funciona bem)
        $tmpdocs = $this->geraFileTypes();
        $this->dados['tipos_documentos'] = '<option value="pdf">Padrão (automático)</option>';
        foreach($tmpdocs as $item)
        {
            if ($item != '..png' || $item != '.image.png')
            {
                $this->dados['tipos_documentos'] .= '<option value="'.basename($item,'.png').'"';
                $this->dados['tipos_documentos'] .= (strtolower(basename($item,'.png')) == $ext ? ' selected="selected" >' : '>');
                $this->dados['tipos_documentos'] .= strtoupper(basename($item,'.png')).'</option>';
            }
        }
        $this->dados['tipos_documentos'] .= '<option value="avi">Outros vídeos</option>';
        $this->dados['tipos_documentos'] .= '<option value="gif">Outras imagens</option>';
        $this->dados['tipos_documentos'] .= '<option value="doc">Outros documentos</option>';
        $this->dados['tipos_documentos'] .= '<option value="ini">Outros arquivos</option>';
        
        $this->dados['folderid'] = $folderid;
        
        $this->dados['folder_list'] = '<input name="folder_list" id="folder_list" type="hidden" value="'.$folderid.'" />';
        $this->dados['folder_list'] .= '<strong>Pasta de origem:</strong> ' . $this->banco->campo('folders','name','nameid = '. $folderid) . '';
        
        // Lista de icones
        $this->dados['icons'] = $this->geraFileTypes(true);
        
        
        
        $dados = array();   
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('arquivos/editar',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $pasta_destino = $this->input->post('folder_list');
            $id_pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','nameid = "'.$pasta_destino.'"');
            $dados_arquivo = array('name' => $this->input->post('name'),'folder' => $nome_pasta);
            
            // Remocao do arquivo anexo
            if ($this->input->post("remove_arquivo") == 1)
            {
                $conteudo = '';
                $tamanho = '';
                $tipo = '';
                
                $dados = array(
                'id' => $idfile,
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'content' => $conteudo,
                'size' => $tamanho,
                'type' => $tipo
                );
            }
            else
                {
                    $dados = array(
                            'id' => $idfile,
                            'name' => $this->input->post('name'),
                            'desc' => $this->input->post('desc'),
                            'type' => $this->input->post('type')
                        );
                }
            
            // Tratamento do UPLOAD DE ARQUIVO
            if (!empty($_FILES['filepath']['tmp_name']))
            {
                $tamanho = $_FILES['filepath']['size'];
                $tipo = $_FILES['filepath']['type'];
                $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                //$conteudo = mysql_real_escape_string($conteudo);
                if($tamanho >= 15900000)
                {
                    $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                    redirect('arquivos/index#'. $nameid_destino);
                    exit;
                }
                $conteudo = @gzdeflate($conteudo);
                if(!$conteudo)
                {
                    $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                    redirect('arquivos/index#'. $folderid);
                    exit;
                }
                $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo,$idfile);
                if ($ok > 0)
                {
                    $this->session->set_flashdata('success','O arquivo foi gravado com sucesso.');
                    $dados['id'] = $idfile;
                }
                else
                $this->session->set_flashdata('error','Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.');
            }
            $template_id = '';
            
            $retorno = 0;
            
            // Faz a atualizacao do registro no banco de dados
            $retorno = $this->banco->atualiza('docsuser',$dados);
            if ($retorno > 0)
            {
                // Deleta o arquivo pra sempre caso esteje marcada a opção
                $removeu = $this->input->post('remover');
                if ($removeu > 0)
                {
                    $del = $this->banco->remover('docsuser',$removeu,$id_pasta_destino);
                    // Registra no historico
                    $this->historico('document','removeu o arquivo ('.$removeu.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                } else $removeu = 0;
                
                // Regrava relacoes, caso houve alteracao da pasta de destino
                $mexeu = $this->input->post('mexeu');
                if ($mexeu > 0 && $removeu == 0)
                {
                    $r = array($retorno);
                    $p = array($id_pasta_destino);
                    $this->banco->relacao_alterar('docsuser',$r,'folders',$p);
                    // Registra no historico
                    $this->historico('document','moveu o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> para a pasta <i>'.$nome_pasta.'</i>');
                }
                // Registra no historico
                $this->historico('document','editou o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
            }
            
                
            if ($retorno > 0)
            {
                $this->session->set_flashdata('success','O arquivo '.$dados['name'].' foi alterado com sucesso.');
                redirect('arquivos/index#'.$pasta_destino);
            }
            else
            {
                $this->session->set_flashdata('error','O arquivo '.$dados['name'].' não foi alterado devido a um erro no sistema.');
                redirect('arquivos/editar/'.$idfile);
            }
        }
    }

    private function envia_arquivo($dados,$conteudo,$tamanho,$tipo,$update="")
    {
        if(empty($update))
        {
            $lista = array(
            'size' => $tamanho,
            'content' => $conteudo,
            'type' => $tipo
            );
            $retorno = $this->banco->insere('docsuser',$lista);
            $this->historico('document','enviou um novo arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$dados['folder'].'</i>');
        }
        else
        {
            $lista = array(
            'id' => $update,
            'size' => $tamanho,
            'content' => $conteudo,
            'type' => $tipo
            );
            $retorno = $this->banco->atualiza('docsuser',$lista);
            $this->historico('document','alterou o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$dados['folder'].'</i>');
        }
        
        return $retorno;
    }
    
    public function listarservAjax($idserv)
    {
        echo $this->listaArqServico($idserv);
        return false;
    }

    public function remover($str,$tempo="")
    {
        if(empty($str))
        {
            echo 'Erro01 - falha na string de segurança';
            return false;
        }
        else
        {
            $code = explode('|||',base64_decode($str));
            $removeu = (int) $code[0];
            $nome = $code[1];
            if(empty($code[2]) && empty($code[3]))
            {
                // Remoção rápida para anexos de documentos em novos orçamentos
                $fld = $this->banco->relacao('docsuser',$removeu,'folders');
                $fld_nameid = $this->banco->campo('folders','nameid','id = '.$fld[0]);
                if($fld_nameid != "999999")
                {
                    echo 'Erro04 - o arquivo pertence a uma pasta inválida';
                    return false;
                }
                $del = $this->banco->remover('docsuser',$removeu);
                return false;
            }
            $folderid = $code[2];
            $userid = (int) $code[3];
        }
        
        $perm = $this->checaPermissao($folderid);
        /*if ($perm != 1 && $perm != 2)
        {
            echo 'Erro02 - permissão inválida';
            return false;
        }*/
        
        if($userid != $this->session->userdata('esta_logado') && !$this->banco->isUser('administrador',$this->session->userdata('esta_logado')))
        {
            echo 'Erro03 - usuário não permitido';
            return false;
        }
        
        $nome_pasta = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
        if(empty($nome_pasta))
        {
            echo 'Erro06 - falha ao buscar info da pasta';
            return false;
        }
        
        $del = $this->banco->remover('docsuser',$removeu);
        $this->historico('document','removeu o arquivo ('.$removeu.') de nome <b>'.$nome.'</b> na pasta <i>'.$nome_pasta.'</i>');
        
        return false;
    }
}
?>
