<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends MY_Controller {
	
	public function __construct()
    {
		parent::__construct();
		// Gera o menu
		$this->geraMenu('cadastro');
		// Carrega o modelo de dados
		$this->load->model('sistema','banco',TRUE);
		if ($this->session->userdata('esta_logado') == 0)
		{
			$this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');		
			redirect('login');
		}
        // Configura como pagina atual (usado para confiugurar permissões)
        $this->session->set_userdata('pagina_atual', 'cadastros');
        // Carrega lib que monta formulários
		$this->load->library('form');
        
    }
	
    private function perm($fn,$opt="")
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_cadastros',$c);

        switch($c) :
        // 5 = permissão total
            case 5:
                $proibido = array();
                break;
        // Por enquanto, o sistema está sem controle de permissões
        // outros = proibido
            default:
                $proibido = array('alterarativo', 'remover', 'insumo', 'index', 'coord', 'inserir','listar','eu','alterar','listargrupo', 'alterargrupo', 'inserirgrupo');
                
                break;
        endswitch;
        
        if(in_array($fn,$proibido))
        {
            $this->session->set_flashdata('notice','Você não tem permissão para acessar esta página. Contate o administrador.');
            redirect('home');
        }
    }
    
	public function index($result='')
	{
        $this->perm(__FUNCTION__);
            
		$this->dados['central'] = '<section><p>Escolha uma opção ao lado.</p></section>';
		
		$this->load->view('_cabecalho',$this->cabecalho);
		$this->load->view('padrao',$this->dados);
		$this->load->view('_rodape',$this->rodape);
	}
    
    public function coord($id="")
    {

        // Para definir quais usuários o coordenador coordena
        $this->perm(__FUNCTION__);
        
        $this->cabecalho['titulo'] = 'Cadastros :: Associar turmas';
        $this->cabecalho['subtitulo'] = 'Utilize esta área para associar turmas com os alunos cadastrados.';
        $this->dados['central'] = '';
        
        if($id > 0)
        {
            if($_POST)
            {
                $a = false;
                // Salva as informações
                if(!empty($_POST['cons'])) 
                {
                    $this->banco->relacao_remover('users',$_POST['coord'],'users');
                    foreach($_POST['cons'] as $c)
                    {
                        if($c > 0)
                            $a = $this->banco->relacao_criar('users',$_POST['coord'],'users',$c);
                    }
                    if($a)
                        echo '<p style="color:green">As informações foram salvas!</p>';
                    else
                        echo '<p style="color:red">Salvo.</p>';
                }
            }
                
            // Ajax
            $html = '<p>Turmas:</p>';
            $html .= '<form id="consults">';
            // lista os associaveis
            // Se for paciente, lista apenas os convênios
            $lekind = 'kind = 4 or kind = 6 or kind = 2 or kind = 7 or kind = 1';
            if(in_array(7,$this->banco->relacao('users',$id,'groups')))
            {
                $lekind = 'kind = 7';
            }
            $grupos = $this->banco->listagem('groups','id',$lekind);
            $lis = array();
            foreach($grupos as $grp)
                $lis = array_merge($lis,$this->banco->relacao('users','','groups',$grp->id));
            $lis = array_unique($lis);
            
            // Busca o que já estava gravado
            $gravado = $this->banco->relacao('users',$id,'users');
            
            if(!empty($lis))
            foreach($lis as $i)
            {
                if(in_array($i,$gravado)) $chk = ' checked '; else $chk = ''; 
                $html .= '<input type="hidden" value="-'.rand(1,99).'" name="cons[]" />';
                $html .= '<label class="checkbox"><input type="checkbox" '.$chk.' id="'.$this->banco->campo('users','username','id = '.$i).'" name="cons[]" value="'.$i.'">'.$this->banco->campo('users','name','id = '.$i).'</label>';
            }
            $html .= '<input type="hidden" name="coord" id="coord" value="'.$id.'"><p>&nbsp;</p>';
            $html .= '<p><input type="button" name="salvar" value="Salvar" class="btn" onclick="$.post(\''.site_url('cadastro/coord/'.$id).'\',$(\'#consults\').serialize(), function(data){ $(\'#resultado\').html(data).fadeIn(); });">
            &nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="btn" name="limpar" value="Limpar tudo"></p>';
            $html .= '</form>';
            echo $html;
            return;
        }

        $this->dados['central'] .= '
        <script>
            function qualcoord(valor)
            {
                if(valor > 0)
                {
                    $.get("'.site_url('cadastro/coord').'/"+valor,function(data){
                        $("#resultado").html(data);
                    });
                }
                else
                    {
                        $("#resultado").html("Selecione um cadastro acima para ver a relação de cadastros.");
                    }
                return false;
            }
        </script>
        ';
            
        $this->dados['central'] .= '<section><p>Escolha o aluno</p>';
        
        $this->dados['central'] .= '<select name="cons" id="cons" onchange="qualcoord(this.value);">';
        $this->dados['central'] .= '<option value="0">-- selecione --</option>';
        
        // lista de associaveis apenas
        //$lis1 = $this->geraUsersByGroupKind(4); //$this->banco->relacao('users','','groups',5);
        $lis = $this->geraUsersByGroupKind(2);
        //$lis3 = $this->geraUsersByGroupKind(6);
        //$lis = array_merge($lis1,$lis2,$lis3);
        if(!empty($lis))
        foreach($lis as $i)
        {
            $this->dados['central'] .= '<option value="'.$i.'">'.$this->banco->campo('users','name','id = '.$i).'</option>';
        }
        $this->dados['central'] .= '</select>';
        $this->dados['central'] .= '<p>&nbsp;</p><div id="resultado">Selecione um aluno acima para ver a relação de turmas.</div>';
        
        $this->dados['central'] .= '</section>';

        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function inserindo()
    {
        $html = '<ul class="media-list">
          
          
          <li class="media">
            <a class="pull-left" href="#" onclick="document.location.href=\''.site_url('cadastro/pessoa').'\'">
              <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
            </a>
            <div class="media-body">
              <h4 class="media-heading">Aluno</h4>
              <a href="#" class="btn btn-success" onclick="document.location.href=\''.site_url('cadastro/pessoa').'\'">Cadastrar ></a>
         
             
            </div>
          </li>
          
          <li class="media">
            <a class="pull-left" href="#" onclick="document.location.href=\''.site_url('cadastro/convenio').'\'">
              <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
            </a>
            <div class="media-body">
              <h4 class="media-heading">Turma</h4>
              <a href="#" class="btn btn-success" onclick="document.location.href=\''.site_url('cadastro/convenio').'\'">Cadastrar ></a>
         
             
            </div>
          </li>
         
          <!--<li class="media">
            <a class="pull-left" href="#" onclick="document.location.href=\''.site_url('cadastro/insumo').'\'">
              <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
            </a>
            <div class="media-body">
              <h4 class="media-heading">Insumo</h4>
              <a href="#" class="btn btn-success" onclick="document.location.href=\''.site_url('cadastro/insumo').'\'">Cadastrar ></a>
         </div>
          </li>-->
         <li class="media">
            <a class="pull-left" href="#" onclick="document.location.href=\''.site_url('cadastro/inserir').'/3\'">
              <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
            </a>
            <div class="media-body">
              <h4 class="media-heading">Professor</h4>
              <a href="#" class="btn btn-success" onclick="document.location.href=\''.site_url('cadastro/inserir').'/3\'">Cadastrar ></a>
         
              
            </div>
          </li>
              
            </div>
          </li>
        </ul>';
        $html .= '<p>Outros cadastros:</p>';
        $html .= '<form id="consults">';
        // lista os grupos
        $grupos = $this->banco->listagem('groups','name','kind != 1 and kind != 2 and kind != 3 and kind != 7');
        $lis = array();
        foreach($grupos as $grp)
            $lis[$grp->id] = $grp->name;
        
        $html .= '<select class="col-4" name="groupset" id="groupset" onchange="document.location.href=\''.site_url('cadastro/inserir').'/\'+$(\'#groupset\').val()+\'\'">';
        if(!empty($lis))
        foreach($lis as $k => $i)
        {
            $html .= '<option value="'.$k.'">'.$i.'</option>';
        }
        $html .= '</select><p>
        <a href="#" class="btn btn-primary" onclick="document.location.href=\''.site_url('cadastro/inserir').'/\'+$(\'#groupset\').val()+\'\'">Acessar ></a></p>';
        $html .= '</form>';
        
        $this->dados['central'] = '<section>';
        $this->dados['central'] .= $html;
        $this->dados['central'] .= '</section>';

        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
      
	public function inserir($groupset,$result=array())
	{
		$this->perm(__FUNCTION__);
        
        if(empty($groupset))
        {
            echo '<script>document.location.href="'.site_url('cadastro/inserindo').'";</script>';
            return;
        }
        else
        {
            if($groupset == 9)
            {
                // Insumo
                $this->insumo(9,$result);
            }
            
            if($groupset == 17)
            {
                // Convenio (serie)
                $this->convenio(17,$result);
            }
        }
        
        if(empty($result))
        {
            $result = array(
                'url' => 'cadastro/inserir/'.$groupset,
                'id' => '',
                'idusudata' => '',
                'tipo' => (($groupset != 3 && $groupset != 6) ? 'cadastrar_p' : 'cadastrar'),
                'titulo' => 'Cadastros :: INSERIR '.strtoupper($this->banco->campo('groups','name','id = '.$groupset)),
                'subtitulo' => 'Preencha os dados abaixo.'
            );
        }
        
        // COndições de erro para insumos
        if(!empty($_POST['groups']) && ($_POST['groups'] == 9 || $_POST['groups'] == 17))
        {
            if(empty($_POST['username']) || empty($_POST['name']))
            {
                echo '<script>alert("Todos os campos são obrigatórios!");history.back();</script>';
                return;
            }
            
            if(!empty($_POST['username']) && strlen($_POST['username']) <= 2)
            {
                echo '<script>alert("O campo NOME ABREVIADO precisa ter no mínimo 3 caracteres.");history.back();</script>';
                return;
            }
            
            if(!empty($_POST['username']))
            {
                // Verifica se o username já existe
                $testerel = $this->banco->campo('users','username','username like "'.strtolower($_POST['username']).'"');
                if(!empty($testerel))
                {
                    echo '<script>alert("O NOME ABREVIADO informado ('.$_POST['username'].') já existe. Informe outro.");history.back();</script>';
                    return;
                }
            }
            
        }
            
		$this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
		
		// Relacao de campos
		$campos_users = array('groups'=>'hidden',
		'username' => ($groupset == 3 || $groupset == 6 ? 'text' : 'hidden'),
		'password' => (($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6)) ? 'hidden' : 'pass'),
		'password_again' => (($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6))  ? 'hidden' : 'pass'),
		'name' => 'textlarge','email' => 'textmail','phone' => 'textphone','contact_cpf' => 'textcpf','city','uf' => 'textxsmall',
		'country','date_created'=>'hidden');
		$campos_usersdata = array('address' => 'textlarge','address_num', 'address_cpl', 'address_ref' => 'textlarge', 'fax','mobile'=> 'textcep',
		                          'website' => 'text','cnpj' => 'text',
								  'contact_name1' => 'textlarge', 'contact_cargo1' => 'textlarge',
								  'contact_email1' => 'textmail','contact_mobile1' => 'textphone','contact_phone1' => 'textphone', 'contact_fax' => 'textphone',
								  'logo'=>'iupload',
                                  'contact_name2' => 'textlarge', 'contact_cargo2' => 'textlarge',
                                  'contact_email2' => 'textmail','contact_mobile2' => 'textphone','contact_phone2' => 'textphone',
                                  'contact_name3' => 'textlarge', 'contact_cargo3' => 'textlarge',
                                  'contact_email3' => 'textmail','contact_mobile3' => 'textphone','contact_phone3' => 'textphone',
                                  'contact_name4' => 'textlarge', 'contact_cargo4' => 'textlarge',
                                  'contact_email4' => 'textmail','contact_mobile4' => 'textphone','contact_phone4' => 'textphone',
                                  'contact_name5' => 'textlarge', 'contact_cargo5' => 'textlarge',
                                  'contact_email5' => 'textmail','contact_mobile5' => 'textphone','contact_phone5' => 'textphone',
                                  'outros' => 'textarea_advanced');
		
        // Lista de grupos
		// Filtra de acordo com o grupo do usuário logado
		$c = $this->checaPermissao();
        //$cond = ' and id != 6';
        //if($c < 5)
        //    $cond = ' and id not in (6,9,5)'; // somente adminsitradores criam insumos, novos administradores e consultores
		//$lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id'] . $cond);
		//$groups[0] = '-- selecione --';
		//foreach($lis as $item)
	    //{
		//	$groups[$item->id] = $item->name;
		//}
		
		// Tipos de campos
		$nomes_users = array(
			'Grupo' => $groupset,
			'Nome de usuário (único, sem acentos ou espaços)'=>($groupset == 3 || $groupset == 6 ? 'trim|alpha_numeric|max_length[30]|required' : 'usu'.$groupset.rand(100000,999999)),
			($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6) ? 'password' : 'Senha') => ($result['tipo'] == 'cadastrar' ? 'trim|alpha_numeric|max_length[20]|xss_clean|required' : ($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6) ? '123' : 'trim|alpha_numeric|max_length[20]|xss_clean')),
			($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6) ? 'password_again' : 'Repita a senha') => ($result['tipo'] == 'cadastrar' ? 'trim|alpha_numeric|max_length[20]|xss_clean|required|matches[password]' : ($result['tipo'] == 'cadastrar_p' || ($groupset != 3 && $groupset != 6) ? '123' : 'trim|alpha_numeric|max_length[20]|xss_clean|matches[password]')),
			'Nome' => 'max_length[100]|required',
			'E-mail' => 'max_length[80]|valid_email|required',
			'Telefone celular' => 'max_length[40]',
			'CPF' => 'max_length[40]', // usersdata
			'Cidade' => 'max_length[40]',
			'Estado' => 'max_length[2]|min_length[2]',
			'País' => 'max_length[40]', //usersdata
			'date_created' => date('Y-m-d H:i:s'));
	    $nomes_usersdata = array(
			'Endereço' => '',
			'Nº' => '',
			'Complemento' => '',
			'Como chegar ou Ponto de Referência' => '',
			'Bairro' => 'max_length[40]',
			'CEP' => 'max_length[10]',
			'Data de nascimento' => 'max_length[10]',
			'Identidade' => 'max_length[40]',
			'Nome do contato 1'=> 'max_length[40]',
			'Tipo do contato 1'=> 'max_length[40]',
			'Email do contato 1' => 'max_length[40]',
			'Celular do contato 1' => 'max_length[40]',
			'Telefone do contato 1' => 'max_length[40]',
			'Telefone fixo' => 'max_length[40]',
			'Logotipo' => '',
            
            'Nome do contato 2'=> 'max_length[40]',
            'Tipo do contato 2'=> 'max_length[40]',
            'Email do contato 2' => 'max_length[40]',
            'Celular do contato 2' => 'max_length[40]',
            'Telefone do contato 2' => 'max_length[40]',
            
            'Nome do contato 3'=> 'max_length[40]',
            'Tipo do contato 3'=> 'max_length[40]',
            'Email do contato 3' => 'max_length[40]',
            'Celular do contato 3' => 'max_length[40]',
            'Telefone do contato 3' => 'max_length[40]',
            
            'Nome do contato 4'=> 'max_length[40]',
            'Tipo do contato 4'=> 'max_length[40]',
            'Email do contato 4' => 'max_length[40]',
            'Celular do contato 4' => 'max_length[40]',
            'Telefone do contato 4' => 'max_length[40]',
            
            'Nome do contato 5'=> 'max_length[40]',
            'Tipo do contato 5'=> 'max_length[40]',
            'Email do contato 5' => 'max_length[40]',
            'Celular do contato 5' => 'max_length[40]',
            'Telefone do contato 5' => 'max_length[40]',
            
            'Anotações' => '');
		
        // Se for alteração, mostra campo de alteração de senha extra
        if($result['tipo'] == 'alterar' || $result['tipo'] == 'alterar_admin')
        {
            $campos_users['password_atual'] = (($groupset != 3 && $groupset != 6) ? 'hidden' : 'pass');
            $nomes_users['Senha atual'] = (($groupset != 3 && $groupset != 6) ? '123' : 'trim|alpha_numeric|max_length[20]|xss_clean');
        }
        
        $attr_users = array();
        $attr_usersdata = array();
        $extrahtml = '';
        
        // Valores padrão
        if(empty($result['id']))
        {
           // Valores e atributos?
           //$attr_users['groups'] = array('value' => $groupset);
        
           $attr_usersdata['country'] = array('value' => 'Brasil');
        }
        else
            {   // Para quando for alteração (recupera dados)
                $attr_users = array();
                $listagem = $this->banco->listagem('users','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
                foreach($listagem[0] as $nomeitem => $item)
                {
                    $attr_users[$nomeitem] = array('value' => $item);
                }
                
                $attr_usersdata = array();
                $listagem = $this->banco->listagem('usersdata','id','iduser = '. $result['id'],true);
                if(!empty($listagem)) :
                    
                    foreach($listagem[0] as $nomeitem => $item)
                    {
                        $attr_usersdata[$nomeitem] = array('value' => $item);
                        // Recupera o CPF e Páis
                        if($nomeitem == 'contact_cpf' || $nomeitem == 'country')
                        {
                            $attr_users[$nomeitem] = array('value' => $item);
                        }
                    }
                    unset($attr_usersdata['iduser']);
                    
                endif;
                
                unset($attr_users['id']);
                unset($attr_users['password']);
                $sal = $attr_users['salt']['value'];
                unset($attr_users['salt']);
                
                // Busca o grupo
                //$g = $this->banco->relacao('users',$result['id'],'groups');
                //if (empty($g)) $g[0] = 0;
                //$attr_users['groups'] = $g[0];
                $grpkind = $this->banco->campo('groups','kind','id = '.$groupset);
                if(empty($grpkind)) $grpkind = 0;
                
                if($grpkind == 2) // Paciente possui convenios
                {
                    $extrahtml .= '
                    <fieldset>
                    <legend>Séries</legend>
                    '.$this->listarelacoes(2,$result['id']).'
                    </fieldset>
                    ';
                    
                }
                
                if($grpkind == 4) // Profissionais possuem tratamentos asosciados
                {
                    $extrahtml .= '
                    <fieldset>
                    <legend>Outros</legend>
                    '.$this->listarelacoes(4,$result['id']).'
                    </fieldset>
                    ';
                }
                
                if(($groupset != 3 && $groupset != 6))
                {
                    $nomes_users['Nome de usuário (único, sem acentos ou espaços)'] = $this->banco->campo('users','username','id = '.$result['id']);
                }
                
                $campos_users['id'] = 'hidden';
                $nomes_users['id'] = $result['id'];
            }
		$campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
        
        /*if(($groupset != 3 && $groupset != 6))
        {
            if($result['tipo'] == 'cadastro' || $result['tipo'] == 'cadastro_p')
            {
                $extrahtml .= '<input type="hidden" name="password" value="123" />';
                $extrahtml .= '<input type="hidden" name="password_again" value="123" />';
            }
        }*/
        
		// Monta o form
		$this->form->open($result['url'],'produtos');
		
        // Verifica se é somente leitura ou não
        $ro = (($result['id'] != $this->session->userdata('esta_logado') && $this->session->userdata('nivel') != 'yes') ? true : false);
        $formsalvar = '';
        if(($result['id'] == $this->session->userdata('esta_logado') || $this->session->userdata('nivel') == 'yes'))
          $formsalvar = '<button type="submit" class="btn btn-success"> Salvar formulário </button>';//$this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        //$this->form->fieldset('Dados cadastrais');
		$this->geraForm($campos_users,$nomes_users,$attr_users,$ro);
		//$this->form->fieldset('Dados complementares','id=cplr');
		$this->geraForm($campos_usersdata,$nomes_usersdata,$attr_usersdata,$ro);
		
        // Mostra a logo, se possuir
        if(!empty($attr_usersdata['logo']['value']))
            $foto = '<p><img src="'.site_url('uploads').'/'.$attr_usersdata['logo']['value'].'" class="img-rounded" alt="Logo atual" style="width:100%"></p>';
        else
            $foto = '<p><img src="'.site_url('img').'/semfoto.jpg" class="img-rounded" alt="Sem foto" style="width:100%"></p>';
        
        // Tudo OK
		$this->form->model('formularios', 'usuarios_criar');
		$this->form->onsuccess('redirect','cadastro/listar'.((!empty($_POST['groups']) && $_POST['groups'] == 9) ? '/insumos' : ''));
	
        // Javascript auxiliar
        $data['js'] = '';
	
        // String de seguranca do usuario
        if(!empty($result['id']))
        {
            $str_usu = base64_encode('e_'.$this->banco->campo('users','username','id =' . $result['id']).'_'.$result['id'].'_'.time());
            
            $data['js'] .= '<script>$(document).ready(function(){
            $.get(\''.site_url('arquivos/minhapasta/'.$str_usu.'/1/'.base64_encode(current_url())).'\',function(data){
                        $("#listinha2").html(data);
            });});</script>';
        }
                
        // Se deu erro
        $data['errors'] = $this->form->errors;
		
		$form = $this->form->get_array();
        
        //print_r($form); exit;
		//echo $mostrasenhas;
		// Aplica template
        $this->smarty->assign('header',$this->load->view('_cabecalho',$this->cabecalho,true));
        $this->smarty->assign('footer',$this->load->view('_rodape',$this->rodape,true));
        $this->smarty->assign('foto',$foto);
        //$this->smarty->assign('mostrasenhas','');
        $this->smarty->assign('extrahtml',$extrahtml);
        $this->smarty->assign('groupset',$groupset);
        $this->smarty->assign('ehadmin',$this->session->userdata('nivel'));
        $this->smarty->assign('ologin',(empty($result['id']) ? '' : $attr_users['username']['value']));
        $this->smarty->assign('asenha',(empty($result['id']) ? '' : $sal));
        if(empty($result['id'])) $form['id'] = '';
        $form['baseurl'] = site_url();
        $this->smarty->assign('form',$form);

        $this->smarty->assign('formsalvar',$formsalvar);
        $this->smarty->view('profile.tpl',$data);
        
	}
    
    public function listarelacoes($grupokind,$id)
    {
        $ret = '<ul class="media-list">';
        
        if($grupokind == 2)
        {
            $res = $this->banco->relacao('users',$id,'users');
            
            if(empty($res))
            {
                $ret = '<p>Não possui.</p>';
                return $ret;
            }
            
            $lis = $this->banco->listagem('users','name','id in ('.implode(',',$res).')');
            foreach($lis as $item) :
                $ret .= '  <li class="media">
                    <a class="pull-left" href="'.$item->email.'" title="'.$item->email.'" target="_blank">
                      <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
                    </a>
                    <div class="media-body">
                      <h4 class="media-heading">'.$item->name.'</h4>
                      <p>'.$item->phone.'<br>
                      '.$item->city.'</p>
                    </div>
                  </li>';
            endforeach;
        }
        elseif($grupokind == 4)
        {
            $res = $this->banco->relacao('products','','users',$id);
            
            if(empty($res))
            {
                $ret = '<p>Não possui.</p>';
                return $ret;
            }
            
            $lis = $this->banco->listagem('products','name','id in ('.implode(',',$res).') and active = 1');
            foreach($lis as $item) :
                $ret .= '  <li class="media">
                    <a class="pull-left" title="'.$item->name.'">
                      <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACDUlEQVR4Xu2Yz6/BQBDHpxoEcfTjVBVx4yjEv+/EQdwa14pTE04OBO+92WSavqoXOuFp+u1JY3d29rvfmQ9r7Xa7L8rxY0EAOAAlgB6Q4x5IaIKgACgACoACoECOFQAGgUFgEBgEBnMMAfwZAgaBQWAQGAQGgcEcK6DG4Pl8ptlsRpfLxcjYarVoOBz+knSz2dB6vU78Lkn7V8S8d8YqAa7XK83ncyoUCjQej2m5XNIPVmkwGFC73TZrypjD4fCQAK+I+ZfBVQLwZlerFXU6Her1eonreJ5HQRAQn2qj0TDukHm1Ws0Ix2O2260RrlQqpYqZtopVAoi1y+UyHY9Hk0O32w3FkI06jkO+74cC8Dh2y36/p8lkQovFgqrVqhFDEzONCCoB5OSk7qMl0Gw2w/Lo9/vmVMUBnGi0zi3Loul0SpVKJXRDmphvF0BOS049+n46nW5sHRVAXMAuiTZObcxnRVA5IN4DJHnXdU3dc+OLP/V63Vhd5haLRVM+0jg1MZ/dPI9XCZDUsbmuxc6SkGxKHCDzGJ2j0cj0A/7Mwti2fUOWR2Km2bxagHgt83sUgfcEkN4RLx0phfjvgEdi/psAaRf+lHmqEviUTWjygAC4EcKNEG6EcCOk6aJZnwsKgAKgACgACmS9k2vyBwVAAVAAFAAFNF0063NBAVAAFAAFQIGsd3JN/qBA3inwDTUHcp+19ttaAAAAAElFTkSuQmCC">
                    </a>
                    <div class="media-body">
                      <h4 class="media-heading">'.$item->name.'</h4>
                      <p><strong>'.$item->group.'</strong></p>
                      <p>'.$item->desc.'</p>
                    </div>
                  </li>';
            endforeach;
        }
        
        $ret .= '</ul>';
        
        return $ret;
    }
    
    public function insumo($groupset=9,$result=array())
    {
        $this->perm(__FUNCTION__);
            
        if(empty($result))
        {
            $result = array(
                'url' => 'cadastro/inserir/9',
                'tipo' => 'cadastrar',
                'id' => '',
                'idusudata' => '',
                'titulo' => 'Cadastros :: Inserir insumo',
                'subtitulo' => 'Preencha os dados abaixo de acordo com o insumo.'
            );
        }
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('groups'=>'hidden','username','password' => 'hidden','password_atual' => 'hidden', 'password_again' => 'hidden','name' => 'textlarge',
                              'email' => 'hidden','phone' => 'hidden','city' => 'hidden','uf' => 'hidden','date_created'=>'hidden');
        $campos_usersdata = array();
        
        // Define o grupo
        $groups = 9; // Insumo!
        $c = $this->checaPermissao();
        
        // Tipos de campos
        $nomes_users = array(
            'groups' => $groups,
            'Nome abreviado (único, sem acentos ou espaços)'=>'trim|alpha_numeric|max_length[30]|required',
            'password' => '123',
            'password_atual' => '123',
            'password_again' => '123',
            'Nome do insumo' => 'max_length[100]|required',
            'email' => 'insumo'.rand(0,9999).'@inbloom.com.br',
            'phone' => '(31) 0000-0000',
            'city' => 'Belo Horizonte',
            'uf' => 'MG',
            'date_created' => date('Y-m-d H:i:s'));
            
        $nomes_usersdata = array();
        $attr_users = array();
        
        // Valores padrão
        if(!empty($result['id']))
        {   // Para quando for alteração (recupera dados)
            $attr_users = array();
            $listagem = $this->banco->listagem('users','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
            foreach($listagem[0] as $nomeitem => $item)
            {
                if($nomeitem == 'username' || $nomeitem == 'name')
                $attr_users[$nomeitem] = array('value' => $item);
            }
            
            $attr_usersdata = array();
            
            // Só admin altera insumos!
            if($c < 5)
            {
                $this->session->set_flashdata('notice','Somente administradores alteram insumos!');     
                redirect('cadastro/listar');
            }
            $campos_users['id'] = 'hidden';
            $nomes_users['id'] = $result['id'];
        }
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
        
        
        // Monta o form
        $this->form->open($result['url'],'produtos');
        
        $this->form->fieldset('Dados cadastrais');
        $this->geraForm($campos_users,$nomes_users,$attr_users);
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'usuarios_criar');
        $this->form->onsuccess('redirect','cadastro/listar/insumos');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function convenio($gset=17,$result=array())
    {
        $this->turma($gset,$result);
    }

    public function turma($groupset=17,$result=array())
    {
        $this->perm(__FUNCTION__);
            
        if(empty($result))
        {
            $result = array(
                'url' => 'cadastro/inserir/17',
                'tipo' => 'cadastrar',
                'id' => '',
                'idusudata' => '',
                'titulo' => 'Cadastros :: Inserir turma',
                'subtitulo' => 'Preencha os dados abaixo para cadastrar uma turma.'
            );
        }
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('groups'=>'hidden','username','password' => 'hidden','password_atual' => 'hidden', 'password_again' => 'hidden','name' => 'textlarge',
                              'email' => 'text','phone' => 'textphone','city' => 'text','uf' => 'hidden','date_created'=>'hidden');
        $campos_usersdata = array();
        
        // Define o grupo
        $groups = 17; // Convenio
        $c = $this->checaPermissao();
        
        // Tipos de campos
        $nomes_users = array(
            'groups' => $groups,
            'Nome abreviado (único, sem acentos ou espaços)'=>'trim|alpha_numeric|max_length[30]|required',
            'password' => '123',
            'password_atual' => '123',
            'password_again' => '123',
            'Nome da turma' => 'max_length[100]|required',
            'Local' => 'max_length[80]',
            'Telefone' => 'max_length[20]',
            'Sede' => 'max_length[150]',
            'uf' => 'MG',
            'date_created' => date('Y-m-d H:i:s'));
            
        $nomes_usersdata = array();
        $attr_users = array();
        
        // Valores padrão
        if(!empty($result['id']))
        {   // Para quando for alteração (recupera dados)
            $attr_users = array();
            $listagem = $this->banco->listagem('users','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
            foreach($listagem[0] as $nomeitem => $item)
            {
                if($nomeitem == 'username' || $nomeitem == 'name' || $nomeitem == 'email' || $nomeitem == 'phone' || $nomeitem == 'city')
                $attr_users[$nomeitem] = array('value' => $item);
            }
            
            $attr_usersdata = array();
            
            $campos_users['id'] = 'hidden';
            $nomes_users['id'] = $result['id'];
        }
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];
        
        
        // Monta o form
        $this->form->open($result['url'],'produtos');
        
        $this->form->fieldset('Dados cadastrais');
        $this->geraForm($campos_users,$nomes_users,$attr_users);
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'usuarios_criar');
        $this->form->onsuccess('redirect','cadastro/listar');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function pessoa($grp=7,$result=array())
    {
        $this->perm(__FUNCTION__);
        
        if(empty($grp)) $grp = 7;
        if(empty($result))
        {
            // Restaura os dados
            $dados = array(
                'url' => 'cadastro/inserir/7',
                'tipo' => 'cadastrar_p',
                'id' => '',
                'idusudata' => '',
                'titulo' => 'Cadastro :: NOVO ALUNO',
                'subtitulo' => 'Criação de cadastros de alunos.'
            );
            $this->inserir($grp,$dados);
        }
    }
    
    public function listarajax($filtro="")
    {
        // Lista de grupos
        $lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id']);
        $groups[0] = '-- selecione --';
        $sWhereid = '';
        foreach($lis as $item)
        {
            $groups[$item->id] = $item->name;
            if(!empty($filtro) && strtolower($item->name) == $filtro)
            {
                $sWhereid = $item->id;
            }
        }
        
        // Relação de colunas "buscáveis" no banco de dados 
        $aColumns = array( 'username', 'name', 'email' );
        
        /* 
         * Paginação
         */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            /*$sLimit = intval( $_GET['iDisplayStart'] ).", ".
                intval( $_GET['iDisplayLength'] );*/
              $sLimit = intval( $_GET['iDisplayLength'] ).", ".
                intval( $_GET['iDisplayStart'] );  
        }
    
    
        /*
         * Ordenação
         */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "" )
            {
                $sOrder = "";
            }
        }
        
        /* 
         * Filtragem
         * 
         */
        
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "(";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        
        /* Filtragem individual por coluna */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = " ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
            }
        }
        
        /* Filtragem extra via parâmetro da função */
        if(!empty($sWhereid))
        {
            $relUsers = $this->banco->relacao('users','','groups',$sWhereid);
            if(!empty($sWhere))
                $sWhere .= " and users.id in (".implode(',',$relUsers).")";
            else
                $sWhere = "users.id in (".implode(',',$relUsers).")";
        }
        

        $iTotal = $this->banco->listagem_users('','name',true);
        
        $iFilteredTotal = $this->banco->listagem_users($sWhere,$sOrder,true);
        
        $listagem = $this->banco->listagem_users($sWhere,$sOrder,false,false,$sLimit);
        
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        foreach($listagem as $item)
        {
            $row = array();
            
            $grp = $this->banco->relacao('users',$item->id,'groups');
            if(empty($grp)) $grp = 9; else { if(is_array($grp)) $grp = $grp[0]; }
            
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $groups[$grp];
            $row[] = $item->username;
            $row[] = ($item->islocked == 'yes' ? '<u>Não</u>' : 'Sim');
            $row[] = '<abbr title="Última ação no sistema: '.( !empty($item->last_hist_action) ? date('d/m/Y H:i', strtotime($item->last_hist_action)) : 'n/d' ).'">'.( !empty($item->last_login) ? date('d/m/Y H:i', strtotime($item->last_login)) : 'n/d' ).'</abbr>';
            $row[] = '<div class="btn-group btn-xs">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                    Ação
                    <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                    <li><a href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->idusersdata.'_'.$item->id.'_'.time()) .'">Ver/Alterar</a></li>
                    <li><a href="javascript:void(0);" id="alteraCusto" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Associar custo</a></li>
                    <li class="divider"></li>
                    <li><a href="javascript:void(0);" id="alteraAtivo" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Ativar/Desativar</a></li>
                    </ul>
                    </div>';
            
            
            $output['aaData'][] = $row;
        }
        echo json_encode( $output );
        return;
    }
    
	public function listar($filtro="")
	{
		$this->perm(__FUNCTION__);
        
        // Verifica a permissão novamente (clientes não acessam aqui)
        $c = $this->checaPermissao();
        if($c == 3) { redirect("home"); }
            
		$this->cabecalho['titulo'] = 'Cadastros :: Listar'.(($filtro == 'cliente') ? ' clientes' : '');
		$this->cabecalho['subtitulo'] = 'Lista de usuários e insumos do sistema.';

        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
                "iDisplayLength": 50,
                  
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    },
                    "fnInitComplete": function () {
                        
                        $("button#alteraCadastro").click(function(){
                            var code = $(this).attr("rel");
                            var url = \''.site_url('cadastro/alterar').'/\'+code;
                            document.location.href=url;
                            return false;
                        });
                        
                        $("button#alteraCusto").click(function(){
                            var code = $(this).attr("rel");
                            var url = \''.site_url('custos/alterar').'/\'+code;
                            document.location.href=url;
                            return false;
                        });
                        
                        $("button#alteraAtivo").click(function(){
                            var code = $(this).attr("rel");
                            var url = \''.site_url('cadastro/alterarativo').'/\'+code;
                            document.location.href=url;
                            return false;
                        });
                        
                    }
        }); 
        });</script>';
		
		// Lista de grupos
		$lis = $this->banco->listagem('groups','id','idorg = '.$this->org['id']);
		$groups[0] = '-- selecione --';
		foreach($lis as $item)
		{
			$groups[$item->id] = $item->name;
		}
		
		$this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
		$this->dados['tabela'] .= '<th>Cód.</th>
								   <th>Nome</th>
								   <th>Grupo</th>
								   <th>E-mail</th>
								   <th>Ativo</th>
								   <th>Telefone</th>
								   <th>Opções</th>';
		$this->dados['tabela'] .= '</tr></thead><tbody>';
		
        $filt = '';
		$listagem = $this->banco->listagem_users($filt,'name');
		foreach($listagem as $item)
		{
			$grp = $this->banco->relacao('users',$item->id,'groups');
			if(empty($grp)) $grp = 9; else { if(is_array($grp)) $grp = $grp[0]; }
			
            if(($filtro == 'cliente' && $grp == 7) || ($filtro == 'insumos' && $grp == 9 && $item->id != 99) || ($filtro == '' && $grp != 9))
            {
    			$this->dados['tabela'] .= '<tr><td><span style="text-align:center"><strong>'.$item->id.'</strong></span></td>
    								   <td>'.$item->name.'</td>
    								   <td>'.($item->id == 99 ? 'Especial' : $groups[$grp]).'</td>
    								   <td>'.$item->email.'</td>
    								   <td>'.($item->islocked == 'yes' ? '<u>Não</u>' : 'Sim').'</abbr></td>
    								   <td>'.$item->phone.'</td>
    								   <td>'.$this->geraBtnListar(array($item->id,$item->idusersdata,$item->name)).'
    								       
    								   </td>
    								   </tr>';
            }
		}
        
		$this->dados['tabela'] .= '</tbody></table>';
		
		$this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
		
		$this->load->view('_cabecalho',$this->cabecalho);
		$this->load->view('padrao',$this->dados);
		$this->load->view('_rodape',$this->rodape);
	}
    
    private function geraBtnListar($params)
    {
        $c = $this->checaPermissao();
        $tit = "Alterar";
        $tit2 = "";//$tit2 = "Associar custo";
        $tit3 = "Remover";
        if($c == 4)
        {
            // Pode criar e alterar aqueles que foram criados por ele
           if(($this->banco->campo('users','idhist','id = '.$params[0]) == $this->session->userdata('esta_logado')) ||
               ($params[0] == $this->session->userdata('esta_logado')))
           {
               $tit = "Alterar";
               $tit2 = "";//$tit2 = "Associar custo";
           }
            else
           {
               $tit = "&nbsp;&nbsp;&nbsp;Ver&nbsp;&nbsp;&nbsp;";
               $tit2 = "";
           }
           $tit3 = "";
        }
        if($c == 3)
        {
            // Apenas ele
           if($params[0] == $this->session->userdata('esta_logado'))
           {
               $tit = "Alterar";
               $tit2 = "";
           }
           else
           {
               $tit = "&nbsp;&nbsp;&nbsp;Ver&nbsp;&nbsp;&nbsp;";
               $tit2 = "";//$tit2 = "";
           }
           $tit3 = "";
        }
        
        if($this->banco->isUser('cliente',$params[0])) // Se for cliente, não tem associar custo
            $tit2 = "";
        
        $ret =  '<div class="btn-group btn-sm">';
        $ret .= '<button class="btn" type="button" id="alteraCadastro" rel="'. base64_encode('e_'.$params[1].'_'.$params[0].'_'.time()) .'">'.$tit.'</button>';
        if(!empty($tit2))
        $ret .= '<button class="btn btn-warning" type="button" id="alteraCusto" rel="'. base64_encode('e_'.$params[2].'_'.$params[0].'_'.time()) .'" title="'.$tit2.'"><i class="fas fa-check"></i></button>';
        if(!empty($tit3))
        $ret .= '<button class="btn btn-danger" type="button" onclick="document.location.href=\''.site_url('cadastro/remover/'.$params[0]).'\'" title="'.$tit3.'"><i class="fas fa-times"></i></i></button>';
        $ret .= '</div>';
        return $ret;
    }
    
    public function eu()
    {
        $this->perm(__FUNCTION__);
            
        $meuid = $this->session->userdata('esta_logado');
        $meuiduserdata = $this->banco->campo('usersdata','id','iduser = '.$meuid);
        $eu = base64_encode('e_'.$meuiduserdata.'_'.$meuid.'_'.time());
        $this->alterar($eu);
    }

    public function remover($cod,$confirma="")
    {
        $this->perm(__FUNCTION__);
            
        if(empty($cod) || !is_numeric($cod) || $this->session->userdata('nivel') == 'no')
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('cadastro/listar');
            return;
        }
        
        $this->cabecalho['titulo'] = 'Cadastros :: Remover usuário/insumo';
        $this->cabecalho['subtitulo'] = 'Remoção de usuários e insumos do sistema.';
        
        if(empty($confirma) || $confirma != "sim")
        {
            $this->dados['central'] = '<section><p><strong>CUIDADO: </strong> a remoção de <strong>'.$this->banco->campo('users','username','id = '.$cod).'</strong> afetará nos seguintes serviços:</p><ul>';
            
            // Lista de serviços que o usuário/insumo faz parte
            $lis1 = $this->banco->idservicos(array($cod));
            if(empty($lis1))
                $this->dados['central'] .= '<li>Nenhum serviço foi encontrado.</li>';
            else
                {
                    foreach($lis1 as $item)
                    {
                        $this->dados['central'] .= '<li>'.$this->banco->campo('services','name','id = '.$item).'</li>';
                    }
                }
            
            $this->dados['central'] .= '</ul>
            <p>A remoção de usuários e insumos envolvidos em serviços em execução podem gerar erros, sendo necessária a recriação destes serviços!</p>
            <p>Além do cadastro, será removido todos os dados relacionados tais como: custos associados, históricos de conversa e todos os arquivos que foram gerados a partir deste usuário.</p>
            <p><button type="button" class="btn btn-warning" onclick="document.location.href=\''.site_url('cadastro/remover/'.$cod.'/sim').'\'">CONFIRMAR REMOÇÃO</button></p>';
            
            $this->dados['central'] .= '</section>';
        }
        else
            {
                $res = $this->banco->remover('users',$cod);
                $this->dados['central'] = '<section>
                <p>O processo de remoção foi executado.</p>';
                $this->dados['central'] .= '<u>Status:</u> '. ($res ? 'REMOVIDO!' : 'Falha ao remover...');
                $this->dados['central'] .= '</section>';
            }
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }
    
    public function alterar($cod)
    {
        $this->perm(__FUNCTION__);
        
        // Quem modifica o cadastro:
        // -> O dono do cadastro
        // -> Administrador
            
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('cadastro/listar');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $idusudata = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idusu = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idusu == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('cadastro/listar');
        }
        
        // Verifica se é insumo
        $grpchk = $this->banco->relacao('users',$idusu,'groups');
        if($grpchk[0] == 9)
        {
            // Restaura os dados
            $dados = array(
                'url' => 'cadastro/alterar/'.$cod,
                'tipo' => ($this->session->userdata('nivel') == 'yes' ? 'alterar_admin' : 'alterar'),
                'id' => $idusu,
                'idusudata' => $idusudata,
                'titulo' => 'Cadastro :: Alterar Insumo',
                'subtitulo' => 'Alteração de um insumo'
            );
            $this->insumo(9,$dados);
        }
        elseif($grpchk[0] == 17)
        {
            // Restaura os dados
            $dados = array(
                'url' => 'cadastro/alterar/'.$cod,
                'tipo' => ($this->session->userdata('nivel') == 'yes' ? 'alterar_admin' : 'alterar'),
                'id' => $idusu,
                'idusudata' => $idusudata,
                'titulo' => 'Cadastro :: Alterar Série',
                'subtitulo' => 'Alteração de uma série'
            );
            $this->convenio(17,$dados);
        }
        else 
        {
            // Restaura os dados
            $dados = array(
                'url' => 'cadastro/alterar/'.$cod,
                'tipo' => ($this->session->userdata('nivel') == 'yes' ? 'alterar_admin' : 'alterar'),
                'id' => $idusu,
                'idusudata' => $idusudata,
                'titulo' => 
                (($idusu == $this->session->userdata('esta_logado') || $this->session->userdata('nivel') == 'yes') ? 'Cadastro :: Alterar' : 'Cadastro :: Visualizar'),
                'subtitulo' => 
                (($idusu == $this->session->userdata('esta_logado') || $this->session->userdata('nivel') == 'yes') ? 'Alteração dos dados cadastrais de um usuário' : 'Visualização do cadastro')
            );
            $this->inserir($grpchk[0],$dados);
        }
    }

    public function alterarativo($cod)
    {
        $this->perm(__FUNCTION__);
            
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('cadastro/listar');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $idusudata = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idusu = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idusu == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('cadastro/listar');
        }
        
        $status_atual = $this->banco->campo('users','islocked','id = '.$idusu);
        
        // Restaura os dados
        $dados = array(
            'id' => $idusu,
            'islocked' => ($status_atual == 'yes' ? 'no' : 'yes')
        );
        
        $r = $this->banco->atualiza('users',$dados);
        if($r)
        {
            $this->session->set_flashdata(($status_atual == 'yes' ? 'success' : 'notice'),'Usuário '.($status_atual == 'yes' ? 'desbloqueado!' : 'bloqueado!'));
            redirect('cadastro/listar');
        }
        
    }

    public function listargrupo()
    {
        $this->perm(__FUNCTION__);
            
        $this->cabecalho['titulo'] = 'Grupos :: Listar';
        $this->cabecalho['subtitulo'] = 'Definição do tipo de usuário e suas permissões no sistema';
        
        $this->dados['tabela'] = '<script>$(document).ready(function() { $("#listageral").dataTable({
            "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" },
                    "sDom": \'T<"clear">lfrtip\',
                    "oTableTools": {
                        "sSwfPath": "'. base_url() .'js/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
                    }
        }); 
        
            $("a#alteraCadastro").click(function(){
                var code = $(this).attr("rel");
                var url = \''.site_url('cadastro/alterargrupo').'/\'+code;
                document.location.href=url;
                return false;
            });
        
        });</script>';
        
        $this->dados['tabela'] .= '<table id="listageral"><thead><tr>';
        $this->dados['tabela'] .= '<th>Cód.</th>
                                   <th>Nome</th>
                                   <th>Abrev.</th>
                                   <th>Acessa o sistema?</th>
                                   <th>Opções</th>';
        $this->dados['tabela'] .= '</tr></thead><tbody>';
        
        $filt = '';
        $listagem = $this->banco->listagem('groups','name',$filt);
        foreach($listagem as $item)
        {
            $simnao = array('yes' => 'sim', 'no' => 'não');
            
            $this->dados['tabela'] .= '<tr><td><span style="text-align:center"><strong>'.$item->id.'</strong></span></td>
                                   <td>'.$item->name.'</td>
                                   <td>'.$item->code.'</td>
                                   <td>'.$simnao[$item->is_user].'</td>
                                   <td>'.
                                    ($item->is_user == 'yes' ? '&nbsp;<button class="btn" disabled>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>' : '<div class="btn-group btn-xs">
                                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                        Ação
                                        <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0);" id="alteraCadastro" rel="'. base64_encode('e_'.$item->name.'_'.$item->id.'_'.time()) .'">Alterar</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Excluir</a></li>
                                        </ul>
                                   </div>').'
                                   </td></tr>';
                                   
        }
        $this->dados['tabela'] .= '</tbody></table>';
        
        $this->dados['central'] = '<section style="margin-top:-55px">'. $this->dados['tabela'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function alterargrupo($cod)
    {
        $this->perm(__FUNCTION__);
            
        // Verifica o padrao enviado pelo post
        if(empty($cod))
        {
            $this->session->set_flashdata('error','Chamada inválida!');
            redirect('cadastro/listargrupo');
        }
        else
        {
            $code = explode('_',base64_decode($cod));
            $nameprod = $code[1];
            /*if (!in_array($code[2],$check2))
            {
                $this->session->set_flashdata('error','Chamada inválida! Usuário não permitido ou falha na string de segurança. Por favor, autentique-se novamente.');      
                redirect('login/encerra_sessao');
            }*/
            $idprod = (int) $code[2];
            $link_date = $code[3]; 
        }
        
        if($idprod == 0)
        {
            $this->session->set_flashdata('error','Chamada inválida! Código não encontrado.');
            redirect('cadastro/listargrupo');
        }
        
        // Restaura os dados
        $dados = array(
            'url' => 'cadastro/alterargrupo/'.$cod,
            'tipo' => 'alterar',
            'id' => $idprod,
            'titulo' => 'Grupos :: Alterar',
            'subtitulo' => 'Alteração de grupo'
        );
        
        $this->inserirgrupo($dados);
    }

    public function inserirgrupo($result=array())
    {
        //$this->perm(__FUNCTION__);
        if(empty($result))
        {
            $result = array(
                'url' => 'cadastro/inserirgrupo',
                'tipo' => 'cadastrar',
                'id' => '',
                'titulo' => 'Grupos :: Inserir',
                'subtitulo' => 'Inserir novo grupo no sistema'
            );
        }
            
            
        $this->cabecalho['titulo'] = $result['titulo'];
        $this->cabecalho['subtitulo'] = $result['subtitulo'];
            
        $this->dados['central'] = '';
        
        // Relacao de campos
        $campos_users = array('name'=>'textlarge', 'desc'=>'textlarge', 'code' => 'textxsmall', 'is_user' => 'select', 'kind' => 'select', 'date_created'=>'hidden','roles' => 'hidden');
                
        // Lista de alternativas
        $lis = array('yes' => 'sim', 'no' => 'não');
        $kinds = array('1' => 'Insumo','2' => 'Cliente', '4' => 'Profissional', '6' => 'Auxiliar', '7' => 'Convênio');
        
        // Tipos de campos
        $nomes_users = array(
            'Nome do grupo'=>'max_length[120]|required',
            'Descrição'=>'max_length[250]',
            'Abreviação'=>'max_length[10]',
            'Acessa o sistema?' => $lis,
            'Tipo de registro' => $kinds,
            'date_created' => date('Y-m-d H:i:s'),
            'roles' => ''
       );
        
        // Monta o form
        $this->form->open($result['url'],'cadastro');    
        
        // Valores padrão
        if(empty($result['id']))
        {
            $attr_users = array();
        }
        else
            { // Para quando for alteração (recupera dados)
                $attr_users = array();
                $listagem = $this->banco->listagem('groups','id','id = '. $result['id'] .' and idorg = ' . $this->org['id'],true);
                foreach($listagem[0] as $nomeitem => $item)
                {
                    $attr_users[$nomeitem] = array('value' => $item);
                    if($nomeitem == 'roles')
                        $nomes_users['roles'] = $item;
                }
                
                //$attr_users['is_user'] = $this->banco->campo('groups','is_user','id = '. $result['id'] .' and idorg = ' . $this->org['id']);
                
                $campos_users['id'] = 'hidden';
                $nomes_users['id'] = $result['id'];

            }
        
        $campos_users['tipo'] = 'hidden';
        $nomes_users['tipo'] = $result['tipo'];

        $this->geraForm($campos_users,$nomes_users,$attr_users);
        
        //$this->roles($nomes_users['roles']);
        
        $this->form->html('<hr>');
        $this->form->submit('Salvar','salvar','class="btn btn-success"');
        
        // Tudo OK
        $this->form->model('formularios', 'grupos_criar');
        $this->form->onsuccess('redirect','cadastro/listargrupo');
        
        // Mostra na tela
        $data['form'] = $this->form->get(); // this returns the validated form as a string
        
        // Javascript auxiliar
        $data['js'] = '';
        
        // Se deu erro
        $data['errors'] = $this->form->errors;
        
        $this->dados['central'] = $data['js'] . $data['errors'] . '<section>'. $data['form'] . '</section>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('padrao',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    private function roles($dados="")
    {
        $this->form->html('<hr>');
        $html = '<table class="table table-condensed table-hover">
        <caption><strong>Regras de acesso</strong></caption>
        <tbody>
          <tr>
            <td valign="top">Cadastros</td>
            <td><p>
              <label>
                <input type="radio" name="cadastros" value="5" id="cadastros_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="4" id="cadastros_1" />
                Tudo exceto grupos e cadastro de insumos</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="3" id="cadastros_2" />
                Apenas seus dados cadastrais</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="1" id="cadastros_3" />
                Proibido</label><br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Custos</td>
            <td><p>
              <label>
                <input type="radio" name="custos" value="5" id="custos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="custos" value="4" id="custos_1" />
                Somente os que ele criou</label>
              <br />
              <label>
                <input type="radio" name="custos" value="3" id="custos_2" />
                Somente listagem</label>
              <br />
              <label>
                <input type="radio" name="custos" value="1" id="custos_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Serviços</td>
            <td><p>
              <label>
                <input type="radio" name="servicos" value="5" id="servicos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="4" id="servicos_1" />
                Somente os que ele participa/coordena</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="3" id="servicos_2" />
                Somente os que ele participa</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="2" id="servicos_3" />
                Apenas a listagem (ver status) dos que ele participa</label>
              <label>
                <input type="radio" name="servicos" value="1" id="servicos_4" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Produtos</td>
            <td><p>
              <label>
                <input type="radio" name="produtos" value="5" id="produtos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="produtos" value="4" id="produtos_1" />
                Somente consulta</label>
              <br />
              <label>
                <input type="radio" name="produtos" value="1" id="produtos_2" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Financeiro</td>
            <td><p>
              <label>
                <input type="radio" name="financeiro" value="5" id="financeiro_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="4" id="financeiro_1" />
                Somente os que ele faz parte</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="3" id="financeiro_2" />
                Somente listagem dele</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="1" id="financeiro_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Arquivos</td>
            <td><p>
              <label>
                <input type="radio" name="arquivos" value="5" id="arquivos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="arquivos" value="4" id="arquivos_1" />
                Somente a pasta pessoal e a dos clientes</label>
              <br />
              <label>
                <input type="radio" name="arquivos" value="3" id="arquivos_2" />
                Somente a pasta pessoal</label>
              <br />
              <label>
                <input type="radio" name="arquivos" value="1" id="arquivos_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Mensagens (chat)</td>
            <td><p>
              <label>
                <input type="radio" name="mensagens" value="5" id="mensagens_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="4" id="mensagens_1" />
                Todos exceto clientes que não pertencem a ele</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="3" id="mensagens_2" />
                Apenas o seu consultor</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="1" id="mensagens_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Entrevistas</td>
            <td><p>
              <label>
                <input type="radio" name="entrevistas" value="5" id="entrevistas_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="entrevistas" value="4" id="entrevistas_1" />
                Acesso mediante código</label>
              <br />
              <label>
                <input type="radio" name="entrevistas" value="1" id="entrevistas_2" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Produto Avulso</td>
            <td><p>
              <label>
                <input type="radio" name="avulso" value="5" id="avulso_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="avulso" value="4" id="avulso_1" />
                Apenas leitura</label>
              <br />
              <label>
                <input type="radio" name="avulso" value="1" id="avulso_2" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Outras regras</td>
            <td><p>
              <label>
                <input type="radio" name="outras" value="5" id="outras_0" />
                Permitir remoção de registros</label>
              <br />
              <label>
                <input type="radio" name="outras" value="4" id="outras_1" />
                Não permitir remoção de registros</label>
              <br />
            </p></td>
          </tr></tbody></table>';
          
          if(!empty($dados))
          {
          $js = '<script>
          $(document).ready(function() {
                var conteudo = JSON.parse($(\'input[name="roles"]\').val());
                $("input[type=radio]").each(function(i){
                    var mygroup = $(this).attr("name");
                    $("input[name="+mygroup+"][value="+conteudo[mygroup]+"]").attr(\'checked\', \'checked\');
                });
          });
          </script>';
          }
          else 
          {
	      $js = '<script>
          $(document).ready(function() {
                $("input[type=radio]").each(function(i){
                    var value = 1;
                    var mygroup = $(this).attr("name");
                    if(mygroup != "outras")
                        $("input[name="+mygroup+"][value="+value+"]").attr(\'checked\', \'checked\');
                    else
                        $("input[name="+mygroup+"][value=4]").attr(\'checked\', \'checked\');
                });
          });
          </script>';
          }
          
          //$this->form->html($html.$js);
          $this->form->html('');
    }

}

/* End of file cadastro.php */
/* Location: ./application/controllers/cadastro.php */