<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documentos extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->geraMenu('documentos');
        $this->load->model('sistema','banco',TRUE);
        if ($this->session->userdata('esta_logado') == 0)
        {
            $this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');     
            redirect('login');
        }
        $this->load->library('form');
        
        // Configurando os demais menus suspensos
        $this->ocultarMenus();
        
        $this->session->set_userdata('pagina_atual', 'documentos');
    }
    
    private function perm()
    {
        // Confere a permissão em cada tela
        $c = $this->checaPermissao();
        
        // Oculta as opções do menu
        $this->geraJavaScript('ocultar_documentos',$c);
        
    }
    
    public function index($result='')
    {
        $this->perm();
                    
        // Formata tela para comportar o algoritmo de listagem
        $this->geraJavaScript('listagem');
        
        // Mostra lista das pastas com links
        $this->dados['folder_list'] = 'Pastas: ';
        $this->dados['folder_list'] .= '<select name="folder_list" id="folder_list" onchange="mostraArquivos(this.value)">';
        $this->dados['folder_list'] .= '<option value="" selected="selected">-- selecione --</option>';
        $this->dados['folder_list'] .= '<optgroup label="'.$this->org['name'].'">';
        foreach($this->geraFolderList() as $key => $item)
        {
            if ($key > 0)
            {
                $this->dados['folder_list'] .= '<option value="'.$key.'">'.$item.'</option>';
            }
        }
        $this->dados['folder_list'] .= '</optgroup>';
        $this->dados['folder_list'] .= '';
        
        $this->geraDestaques();
        $this->dados['destaques'] = '<div></div>';
        
        $this->load->view('_cabecalho',$this->cabecalho);
        $this->load->view('documentos/home',$this->dados);
        $this->load->view('_rodape',$this->rodape);
    }

    public function gera_numeracao($iddoc, $datadoc)
    {
        // Busca dados do documento e pasta
        $rel = $this->banco->relacao('docsuser',$iddoc,'folders');
        if (empty($rel)) return false;
        foreach($rel as $val)
        {
            if (empty($val)) return false;
            $lista = $this->banco->dados('folders',$val);
        }
        // Verifica se é uma pasta com template
        $tpl = $this->verificaPastaTemplate($val);
        foreach($lista as $item) :
            if (empty($item->num) || $item->num == 0)
            {
                return $iddoc .'-'. date('nY',$datadoc);
            }
            elseif ($item->num == -1) // Numeração anual
            {
                // pega a lista de documentos na pasta
                $onde = 'type = "docsuser___folders" and idobj2 = '. $val;
                $d = $this->banco->listagem('relationship','date_created',$onde);
                $ano_atual = '1990';
                foreach($d as $ditem) :
                    // define o ano
                    $ano = date('Y',strtotime($ditem->date_created));
                    if ($ano != $ano_atual)
                    {
                        $contador = 1;
                        $ano_atual = $ano;
                    }
                    // define qual o numero adequado
                    if($ditem->idobj1 == $iddoc)
                    {
                        if ($tpl) $contador--;
                        return $contador.'/'.$ano;
                        break;
                    }
                    $contador++;
                endforeach;
            }
            elseif ($item->num > 0) // Numeracao personalizada
            {
                // pega a lista de documentos na pasta
                $onde = 'type = "docsuser___folders" and idobj2 = '. $val;
                $d = $this->banco->listagem('relationship','date_created',$onde);
                if ($tpl)
                    $contador = $item->num - 1;
                else 
                    $contador = $item->num;
                foreach($d as $ditem) :
                    // define o ano
                    $ano = date('Y',strtotime($ditem->date_created));
                    // define qual o numero adequado
                    if($ditem->idobj1 == $iddoc)
                    {
                        return $contador.'/'.$ano;
                        break;
                    }
                    $contador++;
                endforeach;
            }
        endforeach;
    }

    public function listar($folderid)
    {
        if (empty($folderid)) {
            echo 'Falha ao mostrar arquivos da pasta.';
        } else {
            
            // Verifica a permissao para acesso a pasta
            $perm = $this->checaPermissao($folderid);

            if ($perm > 0)
            {
                $dados['perm'] = $perm;

                switch($perm)
                {
                    case 1: // Admin
                    $dados['perm_text'] = 'Você possui privilégio total nesta pasta.';
                    $dados['perm_link'] = anchor('documentos/criar/'.$folderid,'Enviar novo documento');
                    $dados['perm_edit'] = anchor('documentos/editar_pasta/'.$folderid,'Propriedades da pasta');
                    break;
                    
                    case 2: // User com perm. escrita
                    $dados['perm_text'] = 'Você possui privilégios de criação e editação de documentos nesta pasta.';
                    $dados['perm_link'] = anchor('documentos/criar/'.$folderid,'Enviar novo documento');
                    $dados['perm_edit'] = '';
                    break;
                    
                    case 3: // User com perm. leitura
                    $dados['perm_text'] = 'Você possui privilégio de leitura nesta pasta.';
                    $dados['perm_link'] = anchor('documentos/solicitar/'.$folderid,'Solicitar documento');
                    $dados['perm_edit'] = '';
                    break;
                    
                    case 4: // User com perm. solicitacao
                    $dados['perm_text'] = 'Sem acesso à leitura. Você poderá apenas solicitar documentos nesta pasta.';
                    $dados['perm_link'] = anchor('documentos/solicitar/'.$folderid,'Solicitar documento');
                    $dados['perm_edit'] = '';
                    break;
                }
            }
            else
                {
                    echo '<h6>Você não possui privilégios de acesso a esta página.</h6>';
                    echo '<p>Caso necessite de acesso, contate o administrador do sistema.</p>';
                    return false;
                }
                
            $res = $this->banco->listagem('folders','name','nameid = "'.$folderid.'"');
            if (!$res)
            {
                echo 'Pasta não encontrada no banco de dados.';
                return false;
            }
            
            foreach($res as $row) {
                $folder_id = $row->id;
                $dados['name'] = $row->name;
                $dados['desc'] = $row->desc;
                $folder_parent = $row->folder_parent;
                $folder_level = $row->folder_level;
            }
            
            if ($folder_parent > 0) // possui pasta-pai
            {
                $tmp = $dados['name'];
                $dados['name'] = '';
                for($i=1;$i<=$folder_level-1;$i++)
                {
                    if ($i > 1)
                    $dados['name'] = $this->banco->campo('folders','name','nameid = "'.$folder_parent.'"') . ' / ' . $dados['name'];                    
                    else
                    $dados['name'] .= $this->banco->campo('folders','name','nameid = "'.$folder_parent.'"') . ' / ';
                    $folder_parent = $this->banco->campo('folders','folder_parent','nameid = "'.$folder_parent.'"');
                }
                $dados['name'] .= $tmp;
            }
            
            // Lista de usuarios
            $res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id']);
            foreach($res as $row)
                $dados['usuarios'][$row->id] = $row->name;
            
            // Lista de grupos
            $res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
            foreach($res as $row)
                $dados['grupos'][$row->id] = $row->name;
            
            $dados['folderid'] = $folderid;
            
            $dados['sol_list'] = '<br><br><strong>Solicitações e comentários feitos na pasta '.$dados['name'].': </strong>';
            
            // Checa se a pasta é exclusiva para templates
            if ($this->verificaPastaTemplate($folder_id))
            {
                $dados['resultado_query'] = $this->banco->listagem_docsuser($folder_id,'idorg = '. $this->org['id']);
                // Busca o dado do primeiro documento (template pai) onde temos o nome dos campos
                foreach($dados['resultado_query'] as $item) {
                    $dados['name_tpl'] = $item->name;
                    $dados['desc_tpl'] = $item->desc;
                    $dados['date_tpl'] = date('d/m/Y H:i',strtotime($item->date_created));
                    $dados['tpl'] = $this->banco->dados('template',$item->idtpl);
                    break;
                }
                // Busca o nome dos campos
                foreach($dados['tpl'] as $item)
                {
                    if ($item->template_type == 2) // template de campo unico
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        $dados['cols_tpl'][1] = 'Título';
                        $dados['cols_tpl'][2] = 'Descrição';
                        $dados['cols_tpl'][3] = 'Criado por';
                        $dados['cols_tpl'][4] = 'Data';
                        $dados['cols_tpl'][5] = 'Opções';
                    }
                    elseif ($item->template_type == 3) // template de varios campos sem def. de areas
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        $dados['cols_tpl_tmp'][0] = 'Cód.';
                        $dados['cols_asterisk'] = array();
                        $dados['cols_asterisk_tmp'] = array();
                        $tmp_counter = 1;
                        $counter = 1;
                        $tmp_tpl = $item->template_value;
                        // Extrai os campos definidos no documento
                        preg_match_all("/(?<=(\[\[))(.*?)(?=\])/",$tmp_tpl,$campos_tpl);
                        //print_r($campos_tpl[0]); exit;
                        if(!empty($campos_tpl[0]))
                        foreach($campos_tpl[0] as $campo)
                        {
                            // Adiciona somente os que possuem asterisco no final
                            if (strstr($campo,'*'))
                            {
                                $dados['cols_tpl'][$counter] = str_replace('*','',$campo);
                                $dados['cols_asterisk'][$tmp_counter] = $tmp_counter;
                                $counter++;
                            }
                            else
                            {
                                $dados['cols_tpl_tmp'][$tmp_counter] = $campo;
                                $dados['cols_asterisk_tmp'][$tmp_counter] = $tmp_counter;
                            }
                            $tmp_counter++;
                        }
                        // Se nao achou nenhum campo com asterisco, coloca todos.
                        if ($counter == 1)
                        {
                            $dados['cols_tpl'] = $dados['cols_tpl_tmp'];
                            $counter = $tmp_counter;
                            $dados['cols_asterisk'] = $dados['cols_asterisk_tmp'];
                        }
                        // Insere os campos finais
                        $dados['cols_tpl'][$counter++] = 'Data';
                        $dados['cols_tpl'][$counter++] = 'Opções';
                        
                    }
                    else // template de vários campos com def. de areas
                    {
                        $dados['cols_tpl'][0] = 'Cód.';
                        
                        $tmp_counter = 1;
                        $titulo = md5($item->title);
                        $tmp_tpl = $item->template_value;
                        $tmp_srl = explode("||",$tmp_tpl);
                        foreach($tmp_srl as $newitem) 
                        {
                            if (strstr($newitem,$titulo))
                            {
                                $newitem = unserialize($newitem);
                                if (!empty($newitem))
                                foreach($newitem[$titulo] as $brandnewitem)
                                {
                                    $dados['cols_tpl'][$tmp_counter] = $brandnewitem['fname'];
                                    $tmp_counter++;
                                }
                            }
                        }
                        $dados['cols_tpl'][$tmp_counter++] = 'Data';
                        $dados['cols_tpl'][$tmp_counter++] = 'Opções';
                    }
                }
                
                if ($dados['perm_link'] == anchor('documentos/criar/'.$folderid,'Enviar novo documento'))
                    $dados['perm_link'] = anchor('documentos/criar_registro/'.$folderid,'Criar novo registro');
                
                $this->load->view('documentos/listar_tpl',$dados);
            }
            else
            {
                $dados['icons'] = $this->geraFileTypes(true);
                $dados['resultado_query'] = $this->banco->listagem_docsuser($folder_id,'idorg = '. $this->org['id']);
                $this->load->view('documentos/listar',$dados);
            }
        }
    }

    public function listar_fullscreen($folderid)
    {
        $dados['full_content'] = $this->listar($folderid);
        $this->load->view('fullscreen',$dados);
    }

    public function solicitar($folder)
    {
        redirect('mensagens/criar_solicitacao/'.$folder);
        return;
    }
    
    public function criar_pasta()
    {
        // Somente admins acessam esta area!
        if ($this->checaPermissao() != 1)
        {
            $this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');        
            redirect('home');
        }
        
        $this->dados['usuarios'] = '<option value="">Todos os usuários</option>';
        $res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id']);
        foreach($res as $row)
            $this->dados['usuarios'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
        
        $this->dados['grupos'] = '<option value="">Todos os grupos</option>';
        $res = $this->banco->listagem('groups','name','idorg = '. $this->org['id']);
        foreach($res as $row)
            $this->dados['grupos'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
        
        $this->dados['folder_list'] = '<option value="0">'.$this->org['name'].'</option>';
        foreach($this->geraFolderList() as $key => $item)
        {
            $this->dados['folder_list'] .= '<option value="'.$key.'">'.$item.'</option>';
        }
            
        $this->form_validation->set_rules('name','Pasta','required');
        $this->form_validation->set_rules('desc','Descrição','required');
            
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('documentos/criar_pasta',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // busca dados da pasta pai selecionada
            $pai = $this->input->post('folder_list');
            if ($pai > 0)
            {
                // busca o nivel atual
                $folderinfo = $this->banco->listagem('folders','name','nameid = '.$pai);
                foreach($folderinfo as $info)
                {
                    $folder_level = ((int) $info->folder_level + 1);
                    $folder_parent = $info->nameid;
                }
            }
            else
            {
                $folder_level = 1;
                $folder_parent = 0;
            }
    

            $dados = array(
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'date_created' => date('Y-m-d H:i:s'),
                'nameid' => rand(111111,999999),
                'folder_level' => $folder_level,
                'folder_parent' => $folder_parent,
                'idorg' => $this->org['id'],
                'num' => 0
            );
            if ($this->banco->insere('folders',$dados) > 0)
            {
                // Registra no historico
                $this->historico('folder','criou a pasta <b>'.$dados['name'].'</b>');
                $this->session->set_flashdata('success','Pasta '.$dados['name'].' criada.');
                redirect('documentos/index#'. $dados['nameid']);
            }
            else
            {
                $this->session->set_flashdata('error','Pasta '.$dados['name'].' não foi criada devido a um erro no sistema.');
                redirect('documentos/criar_pasta');
            }
        }
    }

    public function editar_pasta($nomeid="")
    {
        //$this->firephp->log($nomeid);
        // Somente admins acessam esta area!
        if ($this->checaPermissao() != 1)
        {
            $this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');        
            redirect('home');
        }
        
        //$nomeid = basename($nomeid,".php");
        if (empty($nomeid))
        {
            echo 'Falha na chamada da URL';
            return;
        }
        
        
        
        // Nome da Pasta atual
        $this->dados['folder_list'] = $this->banco->campo('folders','name','nameid like "'.$nomeid.'"');
        
        // Busca todos os campos da pasta
        $listagem = $this->banco->listagem('folders','name','nameid like "'. $nomeid .'"');
        $this->dados['resultado_query'] = $listagem;
        
        // Lista de permissoes (mostra as permissoes de cada grupo com relacao a pasta atual)
        $this->dados['grupos'] = $this->geraListaPermissoesPasta($nomeid);
        //$this->dados['grupos'] .= '&nbsp;'. anchor('grupos/index','editar permissões',array('target' => '_blank'));
        
        // Opcoes (criado em 2/9/2011)
        // Pega o padrao de numeracao de documentos, para setar na view
        foreach($listagem as $item) :
            if ($item->num == 0)
                $this->dados['opcoes'] = array(' checked="checked" ','','','');
            elseif ($item->num == -1)
                $this->dados['opcoes'] = array('','',' checked="checked" ','');
            else 
                $this->dados['opcoes'] = array('',' checked="checked" ','',$item->num);
        endforeach;
        
        $this->form_validation->set_rules('name','Pasta','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('documentos/editar_pasta',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // pega o ID
            $id = $this->banco->campo('folders','id','nameid = "'.$nomeid.'"');
            
            // define ordenacao dos docs da pasta
            if ($this->input->post('num') == 99)
                $num = $this->input->post('num_val');
            else
                $num = $this->input->post('num');
            
            $dados = array(
                'id' => $id,
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'num' => $num
            );
            if ($this->banco->atualiza('folders',$dados) > 0)
            {
                $removeu = $this->input->post('remover');
                if ($removeu > 0)
                {
                    // Verifica primeiro se há subfolders!
                    $parente = $this->banco->contar('folders','folder_parent = ' . $nomeid .'');
                    if ($parente > 0)
                        $del = false;
                    else
                        $del = $this->banco->remover('folders',$removeu);
                    
                    if($del)
                    {
                        // Registra no historico
                        $this->historico('folder','removeu a pasta <b>'.$dados['name'].'</b>');
                        $this->session->set_flashdata('success','Pasta '.$dados['name'].' removida.');
                        redirect('documentos/index#'. $nomeid);
                    }
                    else 
                    {
                        $this->session->set_flashdata('error','Pasta '.$dados['name'].' não foi removida pois existem subpastas nela.');
                        redirect('documentos/editar_pasta/'.$nomeid);
                    }
                } else $removeu = 0;
                
                // Registra no historico
                $this->historico('folder','alterou a pasta <b>'.$dados['name'].'</b>');
                
                $this->session->set_flashdata('success','Pasta '.$dados['name'].' atualizada.');
                redirect('documentos/index#'. $nomeid);
            }
            else
            {
                $this->session->set_flashdata('error','Pasta '.$dados['name'].' não foi atualizada devido a um erro no sistema.');
                redirect('documentos/editar_pasta/'.$nomeid);
            }
        }
    }

    public function criar($folderid="")
    {
        // Verifica permissões de acesso
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Você não possui permissão de acesso a esta página');        
            redirect('home');
        }
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
            
        $this->form_validation->set_rules('folder_list','Pasta','required');
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        $tmpdocs = $this->geraFileTypes();
        
        $this->dados['tipos_documentos'] = '<option value="pdf" selected="selected">Padrão (automático)</option>';
        foreach($tmpdocs as $item)
        {
            if ($item != '..png' || $item != '.image.png')
            $this->dados['tipos_documentos'] .= '<option value="'.basename($item,'.png').'">'.strtoupper(basename($item,'.png')).'</option>';
        }
        $this->dados['tipos_documentos'] .= '<option value="avi">Outros vídeos</option>';
        $this->dados['tipos_documentos'] .= '<option value="gif">Outras imagens</option>';
        $this->dados['tipos_documentos'] .= '<option value="doc">Outros documentos</option>';
        $this->dados['tipos_documentos'] .= '<option value="ini">Outros arquivos</option>';
        
        $this->dados['folder_list'] = '<option value="">-- selecione --</option>';
        $this->dados['folder_list'] .= '<optgroup label="CISMEP">';
        foreach($this->geraFolderList() as $key => $item)
        {
            if ($key > 0)
            {
                if ($folderid > 0 && $key == $folderid)
                {
                    $this->dados['folder_list'] .= '<option value="'.$key.'" selected="selected">'.$item.'</option>';
                    $this->dados['folderid'] = $key;
                }
                else
                {
                    $this->dados['folder_list'] .= '<option value="'.$key.'">'.$item.'</option>';
                    if (empty($this->dados['folderid']))
                        $this->dados['folderid'] = $key;
                }
            }
        }
        $this->dados['folder_list'] .= '</optgroup>';
        
        $this->dados['templates'] = '<option value="">-- selecione --</option>';
        $res = $this->banco->listagem('template','title','idorg = '. $this->org['id']);
        foreach($res as $row)
            $this->dados['templates'] .= '<option value="'. $row->id .'">'. $row->title .'</option>';
        
        $dados = array();   
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('documentos/criar',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $nameid_destino = $this->input->post('folder_list');
            $pasta_destino = $nameid_destino;
            $pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','id = "'.$pasta_destino.'"');
            $dados_arquivo = array('name' => $this->input->post('name'),'folder' => $nome_pasta);
            // verifica se é upload ou template
            if ($this->input->post('document_option') == 'opcao1')
            // UPLOAD DE ARQUIVO
            {
                // Verifica se a pasta é apta para receber um arquivo
                if($this->verificaPastaTemplate($pasta_destino))
                {
                    $this->session->set_flashdata('error','A pasta especificada não é apta para receber arquivos binários.');
                    redirect('documentos/index#'. $nameid_destino);
                    exit;
                }
                    
                if (!empty($_FILES))
                {
                    $tamanho = $_FILES['filepath']['size'];
                    $tipo = $_FILES['filepath']['type'];
                    $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                    //$conteudo = mysql_real_escape_string($conteudo);
                    //if (!stristr($tipo,'pdf'))
                    // Verificando o tamanho do arquivo
                    if($tamanho >= 15900000)
                    {
                        $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                        redirect('documentos/index#'. $nameid_destino);
                        exit;
                    }
                    
                    $conteudo = @gzdeflate($conteudo);
                    if(!$conteudo)
                    {
                        $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                        redirect('documentos/index#'. $nameid_destino);
                        exit;
                    }
                    $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo);
                    if ($ok > 0)
                    {
                        $this->session->set_flashdata('success','O arquivo foi gravado com sucesso.');
                        $dados['id'] = $ok;
                    }
                    else
                    $this->session->set_flashdata('error','Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.');
                }
                else
                {
                    $this->session->set_flashdata('notice','O arquivo enviado está vazio ou não foi especificado! Edite o registro criado e envie o arquivo novamente.');
                    $conteudo = '';
                }
                $template_id = '';
            }
            else
            // TEMPLATE
            {
                // Verifica se a pasta é apta para receber um template
                if(!$this->verificaPasta($nameid_destino,true))
                {
                    $this->session->set_flashdata('error','A pasta especificada não é apta para receber novos templates.');
                    redirect('documentos/index#'. $nameid_destino);
                }
                    
                if ($this->input->post('template_list'))
                {
                    $template_id = $this->input->post('template_list');
                    // Registra no historico
                    $this->historico('document','definiu um template ('.$template_id.') na pasta <i>'.$nome_pasta.'</i>');
                            
                    $this->session->set_flashdata('success','Template definido na pasta.');
                }
                else
                {
                    $this->session->set_flashdata('notice','O template não foi especificado! Edite o registro criado e selecione um template na lista.');
                    $conteudo = '';
                    $template_id = '';
                }
            }
            $g = $this->session->userdata('grupos');
            $dados = array(
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'date_created' => date('Y-m-d H:i:s'),
                'readonly' => $this->input->post('readonly'),
                'idgrp' => $g[0],
                'idusr' => $this->session->userdata('esta_logado'),
                'idorg' => $this->org['id'],
                'idtpl' => $template_id,
                'locked' => 'no'
            );
            $retorno = 0;
            
            if (empty($ok))
            {
                $retorno = $this->banco->insere('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$retorno,'folders',$pasta_destino);
            }
            else
            {
                $dados['id'] = $ok;
                $retorno = $this->banco->atualiza('docsuser',$dados);
                if ($retorno)
                    $this->banco->relacao_criar('docsuser',$ok,'folders',$pasta_destino);
            }
                
            if ($retorno > 0)
            {
                redirect('documentos/index#'. $folderid);
            }
            else
            {
                $this->session->set_flashdata('error','O arquivo '.$dados['name'].' não foi criada devido a um erro no sistema.');
                redirect('documentos/criar');
            }
        }
    }

    public function criar_registro($folderid="",$is_contrato="")
    {
        // Somente admins acessam esta area!
        $this->perm();
        
        /*
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Você não possui permissão de acesso a esta página');        
            redirect('home');
        }
        */
        // Pega o ID
        $this->dados['folderid'] = $folderid;
        $folder_id = $this->banco->campo('folders','id','nameid = "'.$folderid.'"');
        $nome_pasta = $this->banco->campo('folders','name','nameid = "'.$folderid.'"');
        
        // Checa se a pasta é exclusiva para templates
        if (!$this->verificaPastaTemplate($folder_id))
        {
            $this->session->set_flashdata('notice','Chamada inválida a pasta que suporta apenas templates');        
            redirect('home');
        }

        $orgdata = $this->session->userdata('orgdata');
        $this->dados['pasta_org'] = $orgdata[0]['folder'];
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
        
        // Campos obrigatórios
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        if ($this->form_validation->run() == FALSE)
        {
            // Gera o form de acordo com o conteudo do template
            $this->dados['formulario'] = $this->geraFormTemplate($folder_id);
            if(empty($is_contrato))
            {
                $this->load->view('_cabecalho',$this->cabecalho);
                $this->load->view('documentos/criar_tpl',$this->dados);
                $this->load->view('_rodape',$this->rodape);
            }
            else
                {
                    $this->dados['nomes'] = base64_decode($is_contrato);
                    $this->load->view('_cabecalho_print',$this->cabecalho);
                    $this->load->view('documentos/criar_tpl_ctr',$this->dados);
                    $this->load->view('_rodape_print',$this->rodape);
                }
        }
        else
        {
            if ($_POST['preview'] == 0)
            {
                // gravar alteracoes
                if ($_POST['template_type'] == 2) // campo unico
                {
                    $conteudo = $_POST['editor'];
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no'
                    );
                    $retorno = $this->banco->insere('docsuser',$dados);
                    if ($retorno)
                    {
                        $this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        // Registra no historico
                        $this->historico('document','criou um registro ('.$retorno.') de campo único com o nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                        $this->session->set_flashdata('success','Documento gravado com sucesso.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao gravar documento.');
                        }
                    if(empty($is_contrato))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">window.close();</script>';
                    //redirect('documentos/index#'.$folderid);
                }
                elseif($_POST['template_type'] == 3) // multi campos sem desenho de areas
                {
                    $campos = array();
                    $valores = array();
                        
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    // coloca o valor de cada campo em uma array
                    foreach($_POST as $labeltmp => $valortmp) :
                        if(strstr($labeltmp,'campo_')) 
                        {
                            $campos[] = $labeltmp;
                            $valores[] = $valortmp;
                        }
                    endforeach;
                    
                    // TODO - corrigir bugs quando inserimos um campo do tipo (tabela) ou (texto)
                    $conteudo = serialize($valores);
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $nome,
                        'desc' => $desc,
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no'
                    );
                    $retorno = $this->banco->insere('docsuser',$dados);
                    if ($retorno)
                    {
                        
                        $this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        // Registra no historico
                        $this->historico('document','criou um registro ('.$retorno.') de campos múltiplos com o nome <b>'.$nome.'</b> na pasta <i>'.$nome_pasta.'</i>');
                        
                        $this->session->set_flashdata('success','Documento gravado com sucesso.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao gravar documento.');
                        }
                    
                   if(empty($is_contrato))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">window.close();</script>';
                }
                else // multi campos com desenho de areas
                {
                    $campos = array();
                    $valores = array();
                    
                    foreach($_POST as $etiqueta => $conteudo)
                    {
                        $etiq = strstr($etiqueta,'txt_'); // campo texto
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'img_'); // campo imagem
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'date_'); // campo data
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                    }

                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'contenttpl' => $conteudo_tpl,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no'
                    );
                    $retorno = $this->banco->insere('docsuser',$dados);
                    if ($retorno)
                    {
                        $this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        // Registra no historico
                        $this->historico('document','criou um registro ('.$retorno.') de campos múltiplos sob quadrantes com o nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                        
                        $this->session->set_flashdata('success','Documento gravado com sucesso.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao gravar documento.');
                        }
                    if(empty($is_contrato))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">window.close();</script>';
                    //redirect('documentos/index#'.$folderid);
                }
            }
            else
            {
                // preview do documento
                if ($_POST['template_type'] == 2) // campo unico
                {
                    $params['bg'] = $this->input->post('template_bg');
                    $conteudo = $_POST['editor'];
                    $this->pdfviewer($params, $conteudo);
                }
                elseif($_POST['template_type'] == 3) // multi campos sem desenho de regioes
                {
                    $campos = array();
                    $valores = array();
                        
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    $params['bg'] = $this->input->post('template_bg');
                    
                    // coloca o valor de cada campo em uma array
                    foreach($_POST as $labeltmp => $valortmp) :
                        if(strstr($labeltmp,'campo_')) 
                        {
                            $campos[] = $labeltmp;
                            $valores[] = $valortmp;
                        }
                    endforeach;
                    
                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    
                    $conteudo_full = $this->montarDivsDocumento($template_id,$conteudo,$conteudo_tpl);
                    $params['bg'] = $this->input->post('template_bg');
                    
                    $this->pdfviewer($params, $conteudo_full);
                    
                }
                else // multi-campos com desenho de regioes
                {
                    $campos = array();
                    $valores = array();
                    foreach($_POST as $etiqueta => $conteudo)
                    {
                        $etiq = strstr($etiqueta,'txt_'); // campo texto
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'img_'); // campo imagem
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'date_'); // campo data
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                    }
                    
                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    $template_id = $this->input->post('template_id');
                    
                    $conteudo_full = $this->montarDivsDocumento($template_id,$conteudo,$conteudo_tpl);
                    $params['bg'] = $this->input->post('template_bg');
                    
                    $this->pdfviewer($params, $conteudo_full);
                }
            }   
            //redirect('documentos/index');
        }
    }

    public function editar_registro($folderid="",$idfile,$is_orc="")
    {
        // Somente admins acessam esta area!
        $this->perm();
        
        /*
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Você não possui permissão de acesso a esta página');        
            redirect('home');
        }
        */
        if (empty($idfile) || !is_numeric($idfile))
        {
            echo 'Falha na chamada da URL';
            return false;
        }
        
        // Pega o ID
        $this->dados['folderid'] = $folderid;
        $folder_id = $this->banco->campo('folders','id','nameid = "'.$folderid.'"');
        $nome_pasta = $this->banco->campo('folders','name','nameid = "'.$folderid.'"');
        $this->dados['idfile'] = $idfile;
        
        // Checa se a pasta é exclusiva para templates
        if (!$this->verificaPastaTemplate($folder_id))
        {
            $this->session->set_flashdata('notice','Chamada inválida a pasta que suporta apenas templates');        
            redirect('home');
        }
        
        // Checa se o arquivo é somente leitura (nao pode alterar)
        $query = $this->banco->dados('docsuser',(int) $idfile);
        $this->dados['resultado_query'] = $query;
        foreach($query as $itemp)
        {
            if ($itemp->readonly == 'yes') $readonly = true; else $readonly = false;
            if ($readonly && $perm != 1)
            {
                $this->session->set_flashdata('notice','Arquivo configurado apenas para leitura: somente os administradores possuem permissão de acesso a esta página');        
                //redirect('documentos/index#'.$folderid);
                redirect('arquivos/index#'.$folderid);
            }
            $conteudo_form = $itemp->content;
            $labels_form = $itemp->contenttpl;
        }
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
        
        // Campos obrigatórios
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
            
        // Gera o form de acordo com o conteudo do template
        if(empty($is_orc))
            $this->dados['formulario'] = $this->geraFormTemplate($folder_id,$conteudo_form,$labels_form,$readonly);
        else {
            $fields = array('produtos(texto)','prod_precos(texto)'); // Mostra apenas estes campos
            $this->dados['formulario'] = $this->geraFormTemplate($folder_id,$conteudo_form,$labels_form,$readonly,$fields);
        }
        
        // Lista de permissoes (mostra as permissoes de cada grupo com relacao a pasta selecionada)
        $this->dados['grupos'] = $this->geraListaPermissoesPasta($folderid);
        $this->dados['grupos'] .= '&nbsp;'. anchor('grupos/index','editar permissões',array('target' => '_blank'));
        
        $orgdata = $this->session->userdata('orgdata');
        $this->dados['pasta_org'] = $orgdata[0]['folder'];
        
        if ($this->form_validation->run() == FALSE)
        {
            
            if(empty($is_orc))
            {
                $this->load->view('_cabecalho',$this->cabecalho);
                $this->load->view('documentos/editar_tpl',$this->dados);
                $this->load->view('_rodape',$this->rodape);
            }
            else
            {
                $this->load->view('_cabecalho_print',$this->cabecalho);
                $this->load->view('documentos/editar_tpl_orc',$this->dados);
                $this->load->view('_rodape_print',$this->rodape);
            }
            
        }
        else
        {
            if ($_POST['preview'] == 0)
            {
                // gravar alteracoes
                if ($_POST['template_type'] == 2) // campo unico
                {
                    $conteudo = $_POST['editor'];
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no',
                        'id' => $idfile
                    );
                    $retorno = $this->banco->atualiza('docsuser',$dados);
                    if ($retorno > 0)
                    {
                        // Deleta o arquivo pra sempre caso esteje marcada a opção
                        $removeu = $this->input->post('remover');
                        if ($removeu > 0)
                        {
                            $del = $this->banco->remover('docsuser',$removeu);
                            // Registra no historico
                            $this->historico('document','removeu o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                        
                        } else $removeu = 0;
                        //$this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        if ($removeu == 0)
                        {
                            // Registra no historico
                            $this->historico('document','alterou o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                            $this->session->set_flashdata('success','Documento atualizado com sucesso.');
                        }
                        else
                            $this->session->set_flashdata('success','Documento removido.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao gravar documento.');
                        }
                    
                    if(empty($is_orc))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">opener.location.reload(true);window.close();</script>';
                    //redirect('documentos/index#'.$folderid);
                }
                elseif($_POST['template_type'] == 3) // multi campos sem desenho de areas
                {
                    $campos = array();
                    $valores = array();
                        
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    // coloca o valor de cada campo em uma array
                    foreach($_POST as $labeltmp => $valortmp) :
                        if(strstr($labeltmp,'campo_')) 
                        {
                            $campos[] = $labeltmp;
                            $valores[] = $valortmp;
                        }
                    endforeach;
                    
                    $conteudo = serialize($valores);
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $nome,
                        'desc' => $desc,
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no',
                        'id' => $idfile
                    );
                    $retorno = $this->banco->atualiza('docsuser',$dados);
                    if ($retorno)
                    {
                        // Deleta o arquivo pra sempre caso esteje marcada a opção
                        $removeu = $this->input->post('remover');
                        if ($removeu > 0)
                        {
                            $del = $this->banco->remover('docsuser',$removeu);
                            // Registra no historico
                            $this->historico('document','removeu o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                        } else $removeu = 0;
                        //$this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        if ($removeu == 0)
                        {
                            // Registra no historico
                            $this->historico('document','alterou o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                            $this->session->set_flashdata('success','Documento atualizado com sucesso.');
                        }
                        else
                            $this->session->set_flashdata('success','Documento removido.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao atualizar documento.');
                        }
                    
                    
                    if(empty($is_orc))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">opener.location.reload(true);window.close();</script>';
                }
                else // multi campos
                {
                    $campos = array();
                    $valores = array();
                    foreach($_POST as $etiqueta => $conteudo)
                    {
                        $etiq = strstr($etiqueta,'txt_'); // campo texto
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'img_'); // campo imagem
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'date_'); // campo data
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                    }
                    
                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    
                    $g = $this->session->userdata('grupos');
                    $dados = array(
                        'name' => $this->input->post('name'),
                        'desc' => $this->input->post('desc'),
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'content' => $conteudo,
                        'contenttpl' => $conteudo_tpl,
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => $template_id,
                        'locked' => 'no',
                        'id' => $idfile 
                    );
                    $retorno = $this->banco->atualiza('docsuser',$dados);
                    if ($retorno)
                    {
                        // Deleta o arquivo pra sempre caso esteje marcada a opção
                        $removeu = $this->input->post('remover');
                        if ($removeu > 0)
                        {
                            // Registra no historico
                            $this->historico('document','removeu o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                            $del = $this->banco->remover('docsuser',$removeu);
                        } else $removeu = 0;
                        //$this->banco->relacao_criar('docsuser',$retorno,'folders',$folder_id);
                        if ($removeu == 0)
                        {
                            // Registra no historico
                            $this->historico('document','alterou o documento ('.$idfile.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                            $this->session->set_flashdata('success','Documento atualizado com sucesso.');
                        }
                        else
                            $this->session->set_flashdata('success','Documento removido.');
                    }
                    else
                        {
                            $this->session->set_flashdata('error','Falha ao gravar documento.');
                        }
                    if(empty($is_orc))
                        echo '<script language="javascript">window.opener.location.href="'.site_url('documentos/index#'.$folderid).'";window.close();</script>';
                    else
                        echo '<script language="javascript">opener.location.reload(true);window.close();</script>';
                    //redirect('documentos/index#'.$folderid);
                }
            }
            else
            {
                // preview do documento
                if ($_POST['template_type'] == 2) // campo unico
                {
                    $params['bg'] = $this->input->post('template_bg');
                    $conteudo = $_POST['editor'];
                    $this->pdfviewer($params, $conteudo);
                }
                elseif($_POST['template_type'] == 3) // multi campos sem desenho de regioes
                {
                    $campos = array();
                    $valores = array();
                        
                    $nome = $this->input->post('name');
                    $desc = $this->input->post('desc');
                    $template_id = $this->input->post('template_id');
                    $params['bg'] = $this->input->post('template_bg');
                    
                    // coloca o valor de cada campo em uma array
                    foreach($_POST as $labeltmp => $valortmp) :
                        if(strstr($labeltmp,'campo_')) 
                        {
                            $campos[] = $labeltmp;
                            $valores[] = $valortmp;
                        }
                    endforeach;
                    
                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    
                    $conteudo_full = $this->montarDivsDocumento($template_id,$conteudo,$conteudo_tpl);
                    $params['bg'] = $this->input->post('template_bg');
                    
                    $this->pdfviewer($params, $conteudo_full);
                    
                }
                else // multi-campos com desenho de regioes
                {
                    $campos = array();
                    $valores = array();
                    foreach($_POST as $etiqueta => $conteudo)
                    {
                        $etiq = strstr($etiqueta,'txt_'); // campo texto
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'img_'); // campo imagem
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                        
                        $etiq = strstr($etiqueta,'date_'); // campo data
                        if ($etiq)
                        {
                            $valores[] = $this->input->post($etiqueta);
                            $campos[] = $etiqueta;
                        }
                    }
                    
                    $conteudo = serialize($valores);
                    $conteudo_tpl = serialize($campos);
                    $template_id = $this->input->post('template_id');
                    
                    $conteudo_full = $this->montarDivsDocumento($template_id,$conteudo,$conteudo_tpl);
                    $params['bg'] = $this->input->post('template_bg');
                    
                    $this->pdfviewer($params, $conteudo_full);
                }
            }   
            //redirect('documentos/index');
        }
    }

    public function criar_varios($folderid="")
    {
        // Verifica permissões de acesso
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Você não possui permissão de acesso a esta página');        
            redirect('home');
        }
        
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('multiupload');
            
        $this->form_validation->set_rules('folder_list','Pasta','required');
                
        $this->dados['folder_list'] = '<option value="">-- selecione --</option>';
        $this->dados['folder_list'] .= '<optgroup label="CISMEP">';
        foreach($this->geraFolderList() as $key => $item)
        {
            if ($key > 0)
            {
                if ($folderid > 0 && $key == $folderid)
                {
                    $this->dados['folder_list'] .= '<option value="'.$key.'" selected="selected">'.$item.'</option>';
                    $this->dados['folderid'] = $key;
                }
                else
                {
                    $this->dados['folder_list'] .= '<option value="'.$key.'">'.$item.'</option>';
                    if (empty($this->dados['folderid']))
                        $this->dados['folderid'] = $key;
                }
            }
        }
        $this->dados['folder_list'] .= '</optgroup>';
        
        $this->dados['templates'] = '<option value="">-- selecione --</option>';
        $res = $this->banco->listagem('template','title','idorg = '. $this->org['id']);
        foreach($res as $row)
            $this->dados['templates'] .= '<option value="'. $row->id .'">'. $row->title .'</option>';
        
        $dados = array();   
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('documentos/criar_multi',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $nameid_destino = $this->input->post('folder_list');
            $pasta_destino = $nameid_destino;
            $pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','id = "'.$pasta_destino.'"');
            
            // UPLOAD DE ARQUIVO

            // Verifica se a pasta é apta para receber um arquivo
            if($this->verificaPastaTemplate($pasta_destino))
            {
                $this->session->set_flashdata('error','A pasta especificada não é apta para receber arquivos binários.');
                redirect('documentos/index#'. $nameid_destino);
            }
            
            
            // Salva cada arquivo
            //print_r($_POST); foreach($_POST['incluidos'] as $inc) { print '<h2>'.$inc.'</h2>'; } exit;
            
            
            $g = $this->session->userdata('grupos');
            $msgs_error = '';
            $msgs_ok = '';
            
            if (!empty($_POST['incluidos']))
            foreach($_POST['incluidos'] as $inc) :
                $ok = 0; $retorno = 0;
                $dados_arquivo = array('name' => $_POST['name'][$inc],'folder' => $nome_pasta);
                $tamanho = $_POST['size'][$inc];
                $tipo = $_POST['type'][$inc];
                // Pega o mime type
                $tipo = $this->verifica_mime_type($tipo,true);
                $conteudo = file_get_contents(base64_decode($_POST['full_name'][$inc]));
                //$conteudo = mysql_real_escape_string($conteudo);
                //if (!stristr($tipo,'pdf'))
                if($tamanho >= 15900000)
                {
                    $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                    redirect('documentos/index#'. $nameid_destino);
                    exit;
                }
                $conteudo = @gzdeflate($conteudo);
                if(!$conteudo)
                {
                    $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                    redirect('documentos/index#'. $nameid_destino);
                    exit;
                }
                $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo);
                if ($ok > 0)
                {
                    // Remove o arquivo da pasta
                    @unlink(base64_decode($_POST['full_name'][$inc]));
                    
                    $dados = array(
                        'id' => $ok,
                        'name' => $_POST['name'][$inc],
                        'desc' => $_POST['desc'][$inc],
                        'date_created' => date('Y-m-d H:i:s'),
                        'readonly' => $this->input->post('readonly'),
                        'idgrp' => $g[0],
                        'idusr' => $this->session->userdata('esta_logado'),
                        'idorg' => $this->org['id'],
                        'idtpl' => '',
                        'locked' => 'no'
                    );
                    
                    $retorno = $this->banco->atualiza('docsuser',$dados);
                    if ($retorno)
                        $this->banco->relacao_criar('docsuser',$ok,'folders',$pasta_destino);
                    
                    $msgs_ok .= 'O arquivo '.$_POST['name'][$inc].' foi gravado com sucesso.<br />';
                }
                else
                $msgs_error .= 'Falha ao gravar arquivo '.$_POST['name'][$inc].'! Arquivo muito grande? Edite o registro e tente novamente. <br />';
            endforeach;

            if (!empty($msgs_ok))
            {
                $this->session->set_flashdata('success',$msgs_ok);
                $this->session->set_flashdata('error',$msgs_error);
                redirect('documentos/index#'. $this->dados['folderid']);
            }
            else
            {
                $this->session->set_flashdata('error','Arquivos não enviados devido a um erro no sistema ou nenhum arquivo foi selecionado.');
                redirect('documentos/criar_varios');
            }
        }
    }

    public function lock($option="yes",$idfile)
    {
        if (empty($idfile) || !is_numeric($idfile))
        {
            echo 'Falha na chamada da URL';
            return false;
        }
        
        // Verifica quem pode editar:
        // Busca o nameid da pasta na qual o documento pertence (considera-se pasta unica)
        $fid = $this->banco->relacao('docsuser',$idfile,'folders');
        $folderid = $this->banco->campo('folders','nameid','id = '. $fid[0]);
        $foldername  = $this->banco->campo('folders','name','id = '. $fid[0]);
        $docname = $this->banco->campo('docsuser','name','id = '. $idfile); 
        
        // Verifica permissao
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            echo 'Somente administradores e donos do documento podem alterar esta opção.';
            return false;
        }
        
        // Tudo ok, trava o arquivo
        $dados = array(
            'id' => $idfile,
            'locked' => $option
        );
        $retorno = $this->banco->atualiza('docsuser',$dados);
        if ($retorno > 0) {
            //echo ($option == 'yes' ? 'Documento travado.' : 'Documento destravado.');
            if ($option == 'yes') {
                echo 'Documento travado.';
                // Registra no historico
                $this->historico('document','travou o documento ('.$idfile.') de nome <b>'.$docname.'</b> na pasta <i>'.$foldername.'</i>');
            } else {
                echo 'Documento destravado.';
                // Registra no historico
                $this->historico('document','destravou o documento ('.$idfile.') de nome <b>'.$docname.'</b> na pasta <i>'.$foldername.'</i>');
            }
            return true;
        } else {
            echo 'Falha ao travar/destravar documento!';
            return false;
        }
    }
    
    public function lockinfo($idfile)
    {
        return $this->banco->campo('docsuser','locked','id = '. (int) $idfile);
    }

    public function editar($idfile)
    {
        
        if (empty($idfile) || !is_numeric($idfile))
        {
            echo 'Falha na chamada da URL';
            return false;
        }
        
        // Busca o nameid da pasta na qual o documento pertence (considera-se pasta unica)
        $fid = $this->banco->relacao('docsuser',$idfile,'folders');
        $folderid = $this->banco->campo('folders','nameid','id = '. $fid[0]);
        
        // Verifica permissao
        $perm = $this->checaPermissao($folderid);
        if ($perm != 1 && $perm != 2)
        {
            $this->session->set_flashdata('notice','Somente administradores possuem permissão de acesso a esta página');        
            redirect('home');
        }
        
        
        
        // Gera o JavaScript do autocomplete do campo Nome
        $this->geraJavaScript('autocomplete',$folderid);
            
        // Regra de validacao do from
        $this->form_validation->set_rules('name','Nome','required');
        $this->form_validation->set_rules('desc','Descrição','required');
        
        // Busca todos os campos do documento
        $listagem = $this->banco->listagem('docsuser','name','id = '. (int) $idfile);
        $this->dados['resultado_query'] = $listagem;
        foreach($listagem as $f)
        {
            $ext = $this->verifica_mime_type($f->type);
            if ($f->readonly == 'yes' && $perm != 1)
            {
                $this->session->set_flashdata('notice','Documento configurado como somente leitura');       
                redirect('documentos/index#'. $folderid);
            }
        }
        
        // Lista de filetypes do form (talvez ficará obsoleto isto pois o automático funciona bem)
        $tmpdocs = $this->geraFileTypes();
        $this->dados['tipos_documentos'] = '<option value="pdf">Padrão (automático)</option>';
        foreach($tmpdocs as $item)
        {
            if ($item != '..png' || $item != '.image.png')
            {
                $this->dados['tipos_documentos'] .= '<option value="'.basename($item,'.png').'"';
                $this->dados['tipos_documentos'] .= (strtolower(basename($item,'.png')) == $ext ? ' selected="selected" >' : '>');
                $this->dados['tipos_documentos'] .= strtoupper(basename($item,'.png')).'</option>';
            }
        }
        $this->dados['tipos_documentos'] .= '<option value="avi">Outros vídeos</option>';
        $this->dados['tipos_documentos'] .= '<option value="gif">Outras imagens</option>';
        $this->dados['tipos_documentos'] .= '<option value="doc">Outros documentos</option>';
        $this->dados['tipos_documentos'] .= '<option value="ini">Outros arquivos</option>';
        
        // Lista de pastas (seleciona aquela que o arquivo pertence)
        $this->dados['folder_list'] = '<optgroup label="CISMEP">';
        foreach($this->geraFolderList() as $key => $item)
        {
            if ($key > 0)
            {
                if ($folderid > 0 && $key == $folderid)
                $this->dados['folder_list'] .= '<option value="'.$key.'" selected="selected">'.$item.'</option>';
                else
                $this->dados['folder_list'] .= '<option value="'.$key.'">'.$item.'</option>';
            }
        }
        $this->dados['folder_list'] .= '</optgroup>';
        $this->dados['folderid'] = $folderid;
        
        // Lista de icones
        $this->dados['icons'] = $this->geraFileTypes(true);
        
        // Lista de permissoes (mostra as permissoes de cada grupo com relacao a pasta selecionada)
        $this->dados['grupos'] = $this->geraListaPermissoesPasta($folderid);
        $this->dados['grupos'] .= '&nbsp;'. anchor('grupos/index','editar permissões',array('target' => '_blank'));
        
        $dados = array();   
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('_cabecalho',$this->cabecalho);
            $this->load->view('documentos/editar',$this->dados);
            $this->load->view('_rodape',$this->rodape);
        }
        else
        {
            // Dados do arquivo (para o historico inclusive)
            $pasta_destino = $this->input->post('folder_list');
            $id_pasta_destino = $this->banco->campo('folders','id','nameid = "'.$pasta_destino.'"');
            $nome_pasta = $this->banco->campo('folders','name','nameid = "'.$pasta_destino.'"');
            $dados_arquivo = array('name' => $this->input->post('name'),'folder' => $nome_pasta);
            
            // Remocao do arquivo anexo
            if ($this->input->post("remove_arquivo") == 1)
            {
                $conteudo = '';
                $tamanho = '';
                $tipo = '';
                
                $dados = array(
                'id' => $idfile,
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'content' => $conteudo,
                'size' => $tamanho,
                'type' => $tipo
                );
            }
            else
                {
                    $dados = array(
                            'id' => $idfile,
                            'name' => $this->input->post('name'),
                            'desc' => $this->input->post('desc'),
                            'type' => $this->input->post('type')
                        );
                }
            
            // Tratamento do UPLOAD DE ARQUIVO
            if (!empty($_FILES['filepath']['tmp_name']))
            {
                $tamanho = $_FILES['filepath']['size'];
                $tipo = $_FILES['filepath']['type'];
                $conteudo = file_get_contents($_FILES['filepath']['tmp_name']);
                //$conteudo = mysql_real_escape_string($conteudo);
                if($tamanho >= 15900000)
                {
                    $this->session->set_flashdata('error','O arquivo é muito grande! Apenas arquivos menores do que 16Mb são permitidos.');
                    redirect('documentos/index#'. $nameid_destino);
                    exit;
                }
                $conteudo = @gzdeflate($conteudo);
                if(!$conteudo)
                {
                    $this->session->set_flashdata('error','Falha ao compactar arquivo.');
                    redirect('documentos/index#'. $folderid);
                    exit;
                }
                $ok = $this->envia_arquivo($dados_arquivo,$conteudo,$tamanho,$tipo,$idfile);
                if ($ok > 0)
                {
                    $this->session->set_flashdata('success','O arquivo foi gravado com sucesso.');
                    $dados['id'] = $idfile;
                }
                else
                $this->session->set_flashdata('error','Falha ao gravar arquivo! Arquivo muito grande? Edite o registro e tente novamente.');
            }
            $template_id = '';
            
            $retorno = 0;
            
            // Faz a atualizacao do registro no banco de dados
            $retorno = $this->banco->atualiza('docsuser',$dados);
            if ($retorno > 0)
            {
                // Deleta o arquivo pra sempre caso esteje marcada a opção
                $removeu = $this->input->post('remover');
                if ($removeu > 0)
                {
                    $del = $this->banco->remover('docsuser',$removeu,$id_pasta_destino);
                    // Registra no historico
                    $this->historico('document','removeu o arquivo ('.$removeu.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
                } else $removeu = 0;
                
                // Regrava relacoes, caso houve alteracao da pasta de destino
                $mexeu = $this->input->post('mexeu');
                if ($mexeu > 0 && $removeu == 0)
                {
                    $r = array($retorno);
                    $p = array($id_pasta_destino);
                    $this->banco->relacao_alterar('docsuser',$r,'folders',$p);
                    // Registra no historico
                    $this->historico('document','moveu o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> para a pasta <i>'.$nome_pasta.'</i>');
                }
                // Registra no historico
                $this->historico('document','editou o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$nome_pasta.'</i>');
            }
            
                
            if ($retorno > 0)
            {
                $this->session->set_flashdata('success','O arquivo '.$dados['name'].' foi alterado com sucesso.');
                redirect('documentos/index#'.$pasta_destino);
            }
            else
            {
                $this->session->set_flashdata('error','O arquivo '.$dados['name'].' não foi alterado devido a um erro no sistema.');
                redirect('documentos/editar/'.$idfile);
            }
        }
    }

    private function envia_arquivo($dados,$conteudo,$tamanho,$tipo,$update="")
    {
        if(empty($update))
        {
            $lista = array(
            'size' => $tamanho,
            'content' => $conteudo,
            'type' => $tipo
            );
            $retorno = $this->banco->insere('docsuser',$lista);
            $this->historico('document','enviou um novo arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$dados['folder'].'</i>');
        }
        else
        {
            $lista = array(
            'id' => $update,
            'size' => $tamanho,
            'content' => $conteudo,
            'type' => $tipo
            );
            $retorno = $this->banco->atualiza('docsuser',$lista);
            $this->historico('document','alterou o arquivo ('.$retorno.') de nome <b>'.$dados['name'].'</b> na pasta <i>'.$dados['folder'].'</i>');
        }
        
        return $retorno;
    }
    
    public function envia_multi_arquivos()
    {
        //print_r($_FILES); exit;
        $error_message[0] = "Falha desconhecida ao tentar enviar arquivos.";
        $error_message[1] = "Um dos arquivos a serem enviados são muito grandes (load_max_filesize).";
        $error_message[2] = "Um dos arquivos a serem enviados são muito grandes (MAX_FILE_SIZE).";
        $error_message[3] = "O arquivo foi apenas parcialmente enviado.";
        $error_message[4] = "Escolha um arquivo para ser enviado.";
        
        $upload_dir  = getcwd() . '/files/_tmpfile/' . $this->session->userdata('esta_logado') . '/';
        if (!is_dir($upload_dir)) {
            $oldumask = umask(0);
            mkdir($upload_dir, 0777);
            umask($oldumask);
        }
        $num_files = count($_FILES['user_file']['name']);
        
        for ($i=0; $i < $num_files; $i++) {
            $upload_file = $upload_dir . basename($_FILES['user_file']['name'][$i]);
        
            if (!preg_match("/(gif|jpg|jpeg|png|doc|docx|xls|xlsx|xlw|ppt|pptx|pps|rtf|xml|odt|ods|odp|zip|pdf|txt)$/",$_FILES['user_file']['name'][$i])) {
                print "Os arquivos permitidos são apenas documentos do Office, imagens e arquivos ZIP...";
            } else {
                if (is_uploaded_file($_FILES['user_file']['tmp_name'][$i])) {
                    if (move_uploaded_file($_FILES['user_file']['tmp_name'][$i], 
                                        $upload_file)) {
                        /* Great success... 
                       $nome = $_FILES['user_file']['name'][$i];
                       $tipo = $_FILES['user_file']['type'][$i];
                       $tam = $_FILES['user_file']['size'][$i];*/
                       
                    } else {
                        print $error_message[$_FILES['user_file']['error'][$i]];
                    }
                } else {
                    print $error_message[$_FILES['user_file']['error'][$i]];
                }    
            }
        }
    }
    
    public function lista_multi_arquivos($tempo='')
    {
        $this->load->helper('file');
        $listagem = get_dir_file_info(getcwd() . '/files/_tmpfile/'.$this->session->userdata('esta_logado').'/', $top_level_only = FALSE);
        //print_r($listagem);
        echo '<div>Selecione abaixo os arquivos que deseja incluir:</div>';
        echo '<table border="0">';
        echo '<thead><tr>';
        echo '<td>Nome<td>';
        echo '<td>Descriçao<td>';
        echo '<td>Ext.</td>';
        echo '<td>Tam.<td>';
        //echo '<td>Data<td>';
        echo '';
        echo '</tr></thead><tbody>';
        $i = 0;
        if (!empty($listagem))
        {
            foreach($listagem as $item)
            {
                //$finfo = finfo_open(FILEINFO_MIME_TYPE);
                //$mime = finfo_file($finfo, $item['server_path']);
                //finfo_close($finfo);
                echo '<tr>';
                echo '<td><input type="checkbox" checked="checked" name="incluidos[]" value="'.$i.'" id="incluidos_'.$i.'" />&nbsp;<input type="text" name="name[]" id="name_'.$i.'" value="'.utf8_encode($item['name']).'" /><td>';
                echo '<td><input type="text" name="desc[]" id="desc_'.$i.'" /><td>';
                echo '<td>'.pathinfo($item['name'], PATHINFO_EXTENSION).'<input type="hidden" name="type[]" value="'.pathinfo($item['name'], PATHINFO_EXTENSION).'" /></td>';
                echo '<td>'.$this->format_bytes($item['size']).'<input type="hidden" name="size[]" value="'.$item['size'].'" />
                <input type="hidden" name="full_name[]" value="'.base64_encode($item['server_path']).'" /><td>';
                //echo '<td>'.date('d/m/Y H:i',$item['date']).'<td>';
                //echo '<td><a href="javascript:void(0)">remover</a></td>';
                echo '</tr>';
                $i++;
            }
        } else { echo '<tr><td>Nenhum arquivo.</td><td></td><td></td><td></td></tr>'; }
        echo '</tbody></table>';
    }

    private function format_bytes($size) 
    {
        $units = array(' bytes', ' KBytes', ' MB', ' GB', ' TB');
        for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
        return round($size, 2).$units[$i];
    }
    
    public function copiando($string)
    {
        if(empty($string)) return false;
        $codigo = base64_decode($string);
        $dados = explode('|||',$codigo);
        $fileid = (int) $dados[0];
        $folderid = (int) $dados[1];
        
        // TODO: verficar permissões
        
        $html = '<form id="copia">';
        $html .= '<p><strong>Arquivo:</strong> '.$this->banco->campo('docsuser','name','id = '.$fileid).'</p>';
        $html .= '<p><strong>Pasta:</strong> '.$this->banco->campo('folders','name','nameid = "'.$folderid.'"').'</p>';
        $html .= '<p>';
        $html .= 'Copiar o arquivo acima para a pasta:<br>';
        $html .= '<select name="folder_para" id="folder_para">';
        $html .= '<option value="" selected="selected">-- selecione --</option>';
        $html .= '<optgroup label="'.$this->org['name'].'">';
        foreach($this->geraFolderList() as $key => $item)
        {
            if ($key > 0)
            {
                $html .= '<option value="'.$key.'">'.$item.'</option>';
            }
        }
        $html .= '</optgroup>';
        $html .= '<input type="hidden" name="folder_de" id="folder_de" value="'.$folderid.'" >';
        $html .= '<input type="hidden" name="file_id" id="file_id" value="'.$fileid.'" >';
        $html .= '</p>';
        $html .= '</form>';
        echo $html;
    }
    
    public function copiando2($de_folder,$para_folder,$cod_file)
    {
        $de = $this->banco->campo('folders','id','nameid = "'.(int) $de_folder.'"');
        $para = $this->banco->campo('folders','id','nameid = "'.(int) $para_folder.'"');
        $cod = (int) $cod_file;
        
        if(empty($de) || empty($para) || empty($cod))
        {
            echo 'ERRO00: Chamada inválida.';
            return false;
        }
        
        // Verifica se o para aceita arquivos binários
        if($this->verificaPastaTemplate($para))
        {
            echo 'ERRO04: A pasta não pode receber arquivos binários.';
            return false;
        }
        
        $this->copiar($de,$para,$cod);
    }
    
    public function copiar($de_folder,$para_folder,$cod_file)
    {
        // Copia um arquivo para outra pasta
        // (altera apenas as relacoes no caso)
        
        // TODO: apenas alguns tipos de usuários podem fazer isto!
        
        if (empty($de_folder) || empty($para_folder) || empty($cod_file))
        {
            echo 'ERRO00: Falha ao copiar arquivo. Falha na chamada da URL.';
            return false;
        }
        
        // Verifica se o arquivo já existe lá
        $docsid = $this->banco->relacao('docsuser','','folders',(int) $para_folder);
        if(!empty($docsid) && in_array($cod_file,$docsid) )
        {
            echo 'ERRO01: Falha ao copiar arquivo. O arquivo já existe na pasta de destino.';
            return false;
        }
        unset($docsid);
        // Verifica se o arquivo existe mesmo né
        $docsid = $this->banco->relacao('docsuser','','folders',(int) $de_folder);
        if(empty($docsid) || !in_array($cod_file,$docsid) )
        {
            echo 'ERRO02: Falha ao copiar arquivo. O arquivo de origem não existe mais?';
            return false;
        }
        
        // Verifica se a pasta de destino existe
        $fnameid = $this->banco->campo('folders','nameid','id = '.(int) $para_folder);
        if(empty($fnameid) )
        {
            echo 'ERRO03: Falha ao copiar arquivo. Pasta de destino não existe.';
            return false;
        }
        
        // Efetua a cópia
        $tmp = $this->banco->relacao_criar('docsuser',$cod_file,'folders',$para_folder);
        
        echo 'ok';
        return false;
    }
    
    public function anexo($codigo,$docname="")
    {
        // Funcao que gera o arquivo no servidor, para anexo no email
        // Retorna o caminho real do arquivo
        if (empty($codigo))
        {
            echo 'ERRO00: Falha ao anexar arquivo. Falha na chamada da URL.';
            return false;
        }
                    
        $codigo = base64_decode($codigo);
        $dados = explode('|||',$codigo);
        
        // Verifica permissao
        $folderid = $this->banco->relacao('docsuser',(int)$dados[0],'folders');
        
        if (empty($folderid)) { echo 'ERRO01: Falha ao anexar arquivo. Pasta inválida.'; return false; }
        
        $foldernameid = $this->banco->campo('folders','nameid','id = '.$folderid[0]);
        
        $nome_pasta = $this->banco->campo('folders','name','id = '.$folderid[0]);
        
        if (empty($foldernameid)) { echo 'ERRO02: Falha ao anexar arquivo. Campo nameid inválido.'; return false; }
        
        $perm = $this->checaPermissao($foldernameid);
        if ($perm == 0 || $perm == 4) { echo 'ERRO03: Falha ao anexar arquivo. Permissão inválida ou login expirado?'; return false; }
        
        // Verifica se o arquivo nao esta travado
        if($this->lockinfo($codigo) == 'yes')
        {
            echo 'ERRO04: O arquivo está travado e não pôde ser anexado!'; return false;
        }
        
        // Busca dados do arquivo
        @ini_set("memory_limit","-1");
        $tmp = $this->banco->dados("docsuser",(int)$dados[0]);
        foreach($tmp as $item) :
            $name = $item->name;
            $size = $item->size;
            $type = $item->type;
            $dataatual = $item->date_created;
            $content = $item->content;
            $contenttpl = $item->contenttpl;
            $tpl = $item->idtpl;
        endforeach;
        
        // verifica se é de template ou arquivo fisico mesmo
        if ($tpl > 0 && empty($type)) // é template
        {
            if(!empty($contenttpl)) // documento de multiplos campos por desenho
            {
                $content = $this->montarDivsDocumento($tpl,$content, $contenttpl);
            }
            else
                {
                    // documento de multiplos campos a partir de documento simples
                    if (is_array(@unserialize($content)))
                        $content = $this->montarDivsDocumento($tpl,$content, '');
                }
            // Verifica se há o coringa {{ID}} e insere o ID no documento
            if(strstr($content,'{{ID}}'))
                $content = str_replace('{{ID}}',$this->gera_numeracao((int)$dados[0], strtotime($dataatual)),$content);
            if(strstr($content,'{{DATA}}'))
            {
                $meses = array('','janeiro','fevereiro','março','abril','maio','junho','julho',
                                'agosto','setembro','outubro','novembro','dezembro');
                $content = str_replace('{{DATA}}',date('d') . ' de '. $meses[date('n')] . ' de '.date('Y'),$content);
            }
            $params['bg'] = $this->banco->campo('template','bg_image','id = '.$tpl);
            // Registra no historico
            $this->historico('download','anexou o arquivo (baseado em template) de nome <b>'.$name.'</b> localizado na pasta <i>'.$nome_pasta.'</i>');
            echo $this->pdfviewer($params, $content, true);
        }
        else // arquivo fixo
        {
            // Tentando corrigir um bug que afeta alguns arquivos PDF
            if(strstr($type,"/"))
                $ext = $this->verifica_mime_type($type);
            else
                $ext = $type;
            $name = urlencode($name);
            
            $conteudo = $this->decompress($content);

            if (!$conteudo) {
                echo 'ERRO05: Falha ao anexar arquivo. Arquivo danificado! Envie o arquivo novamente.'; return false;
            }
            //if ($dados[1] != $dataatual) { echo 'ERRO06: Falha ao anexar arquivo. Data inválida.'; return false; }
            // Registra no historico
            $this->historico('download','anexou o arquivo de nome <b>'.$name.'</b> localizado na pasta <i>'.$nome_pasta.'</i>');
            
            //$this->load->helper('download');
            //force_download($name.'.'.$ext,$conteudo);
            if(empty($docname))
                $output = $name.$this->session->userdata('esta_logado').time().".".$ext;
            else
                $output = $docname.".".$ext;
            
            $outputfull = getcwd() . '/' . $this->org['folder'] . '/' . $output; 
            if(!file_put_contents($outputfull, $conteudo))
                echo '';
            else
                echo $output;
        }
        return false;
    }
    
    public function download($codigo,$idf="")
    {
        if (empty($codigo))
        {
            echo 'Falha na chamada da URL';
            return false;
        }
                    
        $codigo = base64_decode($codigo);
        $dados = explode('|||',$codigo);
        //echo 'Código do arquivo: '.$dados[0].'<br>';
        //echo 'Data do arquivo: '.$dados[1].'<br>';
        
        // Verifica permissao
        if(empty($idf))
            $folderid = $this->banco->relacao('docsuser',(int)$dados[0],'folders');
        else
            $folderid[0] = $idf;
        
        if (empty($folderid)) { echo 'ERRO01: Falha ao baixar arquivo. Pasta inválida ou arquivo não encontrado.'; return false; }
        $foldernameid = $this->banco->campo('folders','nameid','id = '.$folderid[0]);
        $nome_pasta = $this->banco->campo('folders','name','id = '.$folderid[0]);
        if (empty($foldernameid)) { echo 'ERRO02: Falha ao baixar arquivo. Campo nameid inválido.'; return false; }
        $perm = $this->checaPermissao($foldernameid);
        if($folderid[0] != 99 && $foldernameid != '999999') // 99 = pasta de orçamentos que pode ser copiada livremente portanto baixa-se livremente... (gambs?)
        if($perm == 0/* || $perm == 4*/) { echo 'ERRO03: Falha ao baixar arquivo. Permissão inválida ou login expirado.'; return false; }
        
        
        // Verifica se o arquivo nao esta travado
        if($this->lockinfo($codigo) == 'yes')
        {
            $this->session->set_flashdata('notice','Este documento está travado. Não é possível o download agora.');
            redirect('documentos/index#'.$foldernameid);
        }
        
        // Busca dados do arquivo
        @ini_set("memory_limit","-1");
        $tmp = $this->banco->dados("docsuser",(int)$dados[0]);
        foreach($tmp as $item) :
            $name = $item->name;
            $size = $item->size;
            $type = $item->type;
            $dataatual = $item->date_created;
            $content = $item->content;
            $contenttpl = $item->contenttpl;
            $tpl = $item->idtpl;
        endforeach;
        
        // verifica se é de template ou arquivo fisico mesmo
        if ($tpl > 0 && empty($type)) // é template
        {
            if(!empty($contenttpl)) // documento de multiplos campos por desenho
            {
                $content = $this->montarDivsDocumento($tpl,$content, $contenttpl);
            }
            else
                {
                    // documento de multiplos campos a partir de documento simples
                    if (is_array(@unserialize($content)))
                        $content = $this->montarDivsDocumento($tpl,$content, '');
                }
            // Verifica se há o coringa {{ID}} e insere o ID no documento
            // TODO: criar mais regras de caracter coringa como essa (ex: data atual, etc)
            if(strstr($content,'{{ID}}'))
                $content = str_replace('{{ID}}',$this->gera_numeracao((int)$dados[0], strtotime($dataatual)),$content);
            if(strstr($content,'{{DATA}}'))
            {
                $meses = array('','janeiro','fevereiro','março','abril','maio','junho','julho',
                                'agosto','setembro','outubro','novembro','dezembro');
                $content = str_replace('{{DATA}}',date('d') . ' de '. $meses[date('n')] . ' de '.date('Y'),$content);
            }
            $params['bg'] = $this->banco->campo('template','bg_image','id = '.$tpl);
            // Registra no historico
            $this->historico('download','baixou o arquivo (baseado em template) de nome <b>'.$name.'</b> localizado na pasta <i>'.$nome_pasta.'</i>');
            $this->pdfviewer($params, $content);
        }
        else // arquivo fixo
        {
            // Tentando corrigir um bug que afeta alguns arquivos PDF
            $ext = $this->verifica_mime_type($type);
            $name = urlencode($name);
            //if ($ext == 'pdf')
            //$conteudo = $content;
            //else
            $conteudo = $this->decompress($content);
            //$conteudo = @gzuncompress($content);
            if (!$conteudo) {

              // Nao funcionou:
              //$this->load->library('zlibdecompress');
              //$conteudo = $this->zlibdecompress->inflate(substr($content, 2));
                
              // Nao funcionou
              //$conteudo = $this->decompress($content);
              //if (!$conteudo)
              //{
                echo 'ERRO05: Falha ao baixar arquivo. Arquivo danificado! Envie o arquivo novamente.'; return false;
              //}
              
              // Nao funcionou:
              //$f = tempnam('/tmp', 'gz_fix');
              //file_put_contents($f, "\x1f\x8b\x08\x00\x00\x00\x00\x00" . $content);
              //$conteudo = file_get_contents('compress.zlib://' . $f);
            }
            //if ($dados[1] != $dataatual) { echo 'ERRO04: Falha ao baixar arquivo. Data inválida.'; return false; }
            // Registra no historico
            $this->historico('download','baixou o arquivo de nome <b>'.$name.'</b> localizado na pasta <i>'.$nome_pasta.'</i>');
            //header("Content-length: $size");
            //header("Content-type: $type");
            //header('Content-Disposition: attachment, filename="'.$name.'.'.$ext.'"');
            //echo $conteudo;
            $this->load->helper('download');
            force_download($name.'.'.$ext,$conteudo);
        }
        return false;
    }

    private function decompress( $compressed, $length = null ) {
    
    if ( false !== ($decompressed = @gzinflate( $compressed ) ) )
        return $decompressed;
    if ( false !== ( $decompressed = $this->compatible_gzinflate( $compressed ) ) )
        return $decompressed;
    if ( false !== ( $decompressed = @gzuncompress( $compressed ) ) )
        return $decompressed;
    if ( function_exists('gzdecode') ) {
        $decompressed = @gzdecode( $compressed );
        if ( false !== $decompressed )
            return $decompressed;
    }
    return $compressed;
    }
    
    private function compatible_gzinflate($gzData) {
    
    if ( substr($gzData, 0, 3) == "\x1f\x8b\x08" ) {
        $i = 10;
        $flg = ord( substr($gzData, 3, 1) );
        if ( $flg > 0 ) {
            if ( $flg & 4 ) {
                list($xlen) = unpack('v', substr($gzData, $i, 2) );
                $i = $i + 2 + $xlen;
            }
            if ( $flg & 8 )
                $i = strpos($gzData, "\0", $i) + 1;
            if ( $flg & 16 )
                $i = strpos($gzData, "\0", $i) + 1;
            if ( $flg & 2 )
                $i = $i + 2;
        }
        return @gzinflate( substr($gzData, $i, -8) );
    } else {
        return false;
    }
    }

    private function array_search_key2($needle, $haystack,$strict=false,$path=array())
    {
        if( !is_array($haystack) ) {
        return false;
        }
     
        foreach( $haystack as $key => $val ) {
            if( is_array($val) && $subPath = $this->array_search_key2($needle, $val, $strict, $path) ) {
                $path = array_merge($path, array($key), $subPath);
                return $path;
            } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
                $path[] = $key;
                return $path;
            }
        }
        return false;
    }

    public function array_search_key($needle_key, $array) 
    {
      foreach($array as $key=>$value)
      {
        if($value == $needle_key)
        {
            if (empty($key)) 
                return str_replace('/','',strstr($value, '/'));
            else
                {
                    if ((strstr($value,'/') && strstr($key,'/')) || is_numeric($key))
                    {
                        return str_replace('/','',strstr($value, '/'));
                    }
                    else
                        return $key;
                }
                
        }
        if(is_array($value))
        {
          if( ($result = $this->array_search_key($needle_key,$value)) !== false)
                return $result;
        }
      }
      return false;
    }
    
    public function verifica_mime_type($mime,$inverse=false)
    {
        if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT))
        {
            include(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT);
        }
        elseif (is_file(APPPATH.'config/mimes'.EXT))
        {
            include(APPPATH.'config/mimes'.EXT);
        }
        
        if ($inverse)
        {
            if(empty($mimes[$mime])) return "txt";
            $val = $mimes[$mime];
            if (is_array($val))
                return $val[0];
            else 
              return $val;
        }
        // Verificando os tipos mais comuns primeiro
        if (stristr($mime,'excel')) return "xls";
        if (stristr($mime,'jpeg')) return "jpg";
        if (stristr($mime,'jpg')) return "jpg";
        if (stristr($mime,'png')) return "png";
        
        $extensao = $this->array_search_key2($mime,$mimes);
        if (is_array($extensao)) $extensao = $extensao[0]; // TODO: não está funcionando bem...
        if (strlen($extensao) > 1)
        {
            return $extensao;
        }
        else
        {
            return "txt";
        }
    }
    
    public function format_filesize($size) 
    {
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
      if ($size == 0) 
      {
         return('n/a'); 
      } 
      else 
      {
         return (round($size/pow(1024, ($i = floor(log($size, 1024)))), $i > 1 ? 2 : 0) . $sizes[$i]); 
      }
    }
    
    private function geraListaPermissoesPasta($idpasta)
    {
        $html = '';
        // Busca o ID da pasta
        $pasta_destino = $this->banco->campo('folders','id','nameid = "'.$idpasta.'"');
        
        // Busca lista de grupos
        $res = $this->banco->relacao('folders',$pasta_destino,'groups');

        if ($res)
        {
            foreach($res as $item)
            {
                // Pega o nome do grupo
                $nome_grupo = $this->banco->campo('groups','name','id = '. $item);

                // Pega a permissao da pasta para o grupo selecionado
                $query = "type = 'folders___groups' and idobj1 = ".$pasta_destino." and idobj2 = ". $item;
                $dados = $this->banco->campo('relationship','permission',$query);
                
                if (empty($dados))
                    $marcar = 'Oculta';
                else {
                    if ($dados == 'none') $marcar = 'Oculta';
                    if ($dados == 'sol') $marcar = 'Pode solicitar documentos na pasta';
                    if ($dados == 'read') $marcar = 'Pode solicitar e ler documentos na pasta';
                    if ($dados == 'write') $marcar = 'Pode ler e gravar documentos na pasta';
                }
                $html .= '<strong>'.$nome_grupo.'</strong>: '. $marcar . '<br />';
            }
        }
        else
            {
                $html = 'Pasta oculta para todos os grupos (exceto administradores do sistema)';
            }
        
        return $html;
    }

    public function verificaPasta($id="",$onlybool=false)
    {
        if(empty($id)) return false;
        $id = $this->banco->campo('folders','id','nameid = "'.$id.'"');
        $check = $this->banco->relacao('docsuser','','folders',$id);
        if (count($check) > 0)
        {
            if (!$onlybool)
                echo '<br><span style="color:red">ERRO: a pasta selecionada já contém arquivos ou template definido! Selecione outra.</span>';
            else return false;
        }
        else
        {
            if (!$onlybool)
                echo '<br><span style="color:green">OK. Pasta pronta para receber um novo template.</span>';
            else return true;
        }
    }
    
    public function verificaPastaParaArquivo($id="")
    {
        if(empty($id)) return false;
        $id = $this->banco->campo('folders','id','nameid = "'.$id.'"');
        if ($this->verificaPastaTemplate($id))
        {
            echo '<br><span style="color:red">ERRO: a pasta selecionada não é apta para receber arquivos! Selecione outra.</span>';
        }
        else
        {
            echo '<br><span style="color:green">OK. Pasta pronta para receber arquivos.</span>';
        }
    }

    private function verificaPastaTemplate($id)
    {
        $check = $this->banco->relacao('docsuser','','folders',$id);
        $tpl = 0;
        if (count($check) == 0)
            return false;
        else
        {
            foreach($check as $iddoc)
            {
                $tpl = $this->banco->campo('docsuser','idtpl','id = '. $iddoc);
            }       
        }
        if ($tpl > 0)
            return true;
        else
            return false;
    }
    
    public function buscaNomes($folder)
    {
        $perm = $this->checaPermissao($folder);
        if ($perm != 1 && $perm != 2)
        {
            return false;
        }
        
        // Função que busca os nomes dos documentos da pasta selecionada, para o auto-complete em AJAX
        $idfolder = $this->banco->campo('folders','id','nameid like "'.$folder.'"');
        $res = $this->banco->listagem_docsuser($idfolder,'idorg = '. $this->org['id']);
        
        if(empty($res)) return false;
        $valores = array();
        foreach($res as $item)
        {
             if(stristr($item->name,$_GET['term']))
             {
                $valores[]['label'] =  $item->name;
             }
        }
        echo json_encode($valores);
    }

    private function geraFormTemplate($id,$doc="",$label="",$readonly=false,$apenas_os_campos=array())
    {
        // Procura o template e documentos relacionado a pasta
        $check = $this->banco->relacao('docsuser','','folders',$id);
        $tpl = array();
        if (count($check) == 0)
            return false;
        else
        {
            foreach($check as $iddoc)
            {
                $tpl[] = $this->banco->campo('docsuser','idtpl','id = '. $iddoc);
            }       
        }
        
        // Busca os campos do template-pai
        $dados = "";
        if($tpl[0] == 0) return false;
        $lista = $this->banco->dados('template',$tpl[0]);
        foreach($lista as $item)
        {
            if ($item->template_type == 2) // template de campo único
            {
                // definindo o CSS e o CKEditor
                if (!empty($item->bg_image))
                    $bg_image_tmp = array('bg_image_tmp' => base64_encode($item->bg_image));
                else
                    $bg_image_tmp = array('bg_image_tmp' => '');
                $this->session->set_userdata($bg_image_tmp);
                $this->geraJavaScript('template_regedit');
                
                if (empty($doc))
                    $valor = $item->template_value;
                else
                    $valor = $doc;      
                
                return '<tr>
                <td colspan="2">
                <input type="hidden" name="template_type" value="'.$item->template_type.'" />
                <input type="hidden" name="template_bg" value="'.$item->bg_image.'" />
                <input type="hidden" name="template_id" value="'.$item->id.'" />
                <textarea name="editor" '.($readonly ? 'readonly="readonly"' : '').' id="editor" cols="45" rows="5" >'.$valor.'</textarea>
                </td>
                </tr>';
            }
            elseif ($item->template_type == 3) // template unico de multiplos campos (sem def. de areas)
            {
                $number = 0;
                $fvalor = array();
                // carregando os valores dos campos
                if(!empty($doc))
                {
                    $fvalor = unserialize($doc);
                }
                $dados = "";
                $tmp_tpl = $item->template_value;
                // Extrai os campos definidos no documento
                preg_match_all("/(?<=(\[\[))(.*?)(?=\])/",$tmp_tpl,$campos_tpl);
                if(!empty($campos_tpl[0]))
                {
                    if(!empty($apenas_os_campos))
                    {
                        foreach($campos_tpl[0] as $cp)
                        {
                            if(in_array($cp,$apenas_os_campos))
                                $campos_tplate[] = $cp;
                        }
                    }
                    else {
                        $campos_tplate = $campos_tpl[0];
                    }
                }
                
                foreach($campos_tpl[0] as $campo)
                {
                    
                    if(!in_array($campo,$campos_tplate))
                    {
                        // Se não tiver, coloca como hidden
                        $dados .= "<input type=\"hidden\" name=\"campo_".$number."\" id='campo_".$number."' value='".(empty($fvalor[$number]) ? '' : $fvalor[$number])."' />";
                    }    
                    else 
                    {
                        // gerar o html
                        $dados .= "<tr>";
                        $dados .= "<td><label>";
                        $dados .= $campo;
                        $dados .= "</label></td>";
                        $dados .= "<td>";
                        
                            if (!empty($fvalor[$number]))
                            {
                                if (stristr($campo,'(tabela)') || stristr($campo,'(texto)'))
                                    $dados .= "<textarea ".($readonly ? 'readonly="readonly"' : '')."  name=\"campo_".$number."\"  id='campo_".$number."' class=\"editor\" cols=\"45\" rows=\"5\">".$fvalor[$number]."</textarea>";
                                else
                                    $dados .= "<input type='text' ".($readonly ? 'readonly="readonly"' : '')." name='campo_".$number."' value='".$fvalor[$number]."' /> ";
                            }
                            else
                            {
                                // se tiver a palavra tabela ou texto entre parenteses, o campo é especial
                                if (stristr($campo,'(tabela)') || stristr($campo,'(texto)'))
                                    $dados .= "<textarea name=\"campo_".$number."\" id='campo_".$number."' class=\"editor\" cols=\"45\" rows=\"5\"></textarea>";
                                else
                                    $dados .= "<input type='text' name='campo_".$number."' /> ";
                            }
                        
                        $dados .= "</td></tr>";
                    }
                    $number++;
                }
                return $dados . '<tr>
                <td colspan="2">
                <input type="hidden" name="template_type" value="'.$item->template_type.'" />
                <input type="hidden" name="template_bg" value="'.$item->bg_image.'" />
                <input type="hidden" name="template_id" value="'.$item->id.'" />
                </td>
                </tr>';
            }
            else // template de varios campos (desenho de areas)
            {   
                if (!empty($label))
                {
                    $contents = unserialize($doc);
                    $labels = unserialize($label);
                }
                
                $titulo = md5($item->title);
                $sessaosrl = explode("||",$item->template_value);
                // Procura pela string correta com o nome dos campos
                foreach($sessaosrl as $itemform) 
                {
                    // Verifica se a string pertence ao template desejado
                    if (strstr($itemform,$titulo))
                    {
                        // Descodifica a string
                        $itemform = unserialize($itemform);
                        if (!empty($itemform))
                        foreach($itemform[$titulo] as $itemf) :
                                
                                $fname = $itemf['fname']; // nome do box
                                $tname = $itemf['tname']; // tipo do box
                                $classe = $itemf['classe']; // classe do box
                                $w = $itemf['w']; // width
                                $h = $itemf['h']; // height
                                $x = $itemf['x']; // x
                                $y = $itemf['y']; // y
                                
                                // gerar o html
                                $dados .= "<tr>";
                                $dados .= "<td><label>";
                                $dados .= $fname;
                                $dados .= "</label></td>";
                                $dados .= "<td>";
                                if ($tname == 'text')
                                {
                                if (!empty($label)) { $key = array_search('txt_' . $classe,$labels); } else { $key = false; }
                                if ($key !== false) { $valor = $contents[$key]; } else { $valor = ''; }
                                $dados .= "<textarea ".($readonly ? 'readonly="readonly"' : '')."  name=\"txt_".$classe."\" id=\"".$classe."\" class=\"editor\" cols=\"45\" rows=\"5\">".$valor."</textarea>";
                                }
                                elseif ($tname == 'image')
                                $dados .= "<select ".($readonly ? 'readonly="readonly"' : '')."  name=\"img_".$classe."\" id=\"img_".$classe."\"><option value=\"\">Em breve...</option></select>";
                                elseif ($tname == 'data')
                                $dados .= '<script>$(function() { $( "#date_'.$classe.'" ).datepicker(); });</script><input name=\"date_'.$classe.'\" type="text" id="date_'.$classe.'">';
                                $dados .= '<input type="hidden" name="w_'.$classe.'" id="w_'.$classe.'" value="'.$w.'" />';
                                $dados .= '<input type="hidden" name="h_'.$classe.'" id="h_'.$classe.'" value="'.$h.'" />';
                                $dados .= '<input type="hidden" name="x_'.$classe.'" id="x_'.$classe.'" value="'.$x.'" />';
                                $dados .= '<input type="hidden" name="y_'.$classe.'" id="y_'.$classe.'" value="'.$y.'" />';
                                $dados .= "</td>";
                                $dados .= "</tr>";
                        
                        endforeach;
                    }
                }
            }
            $t = array($item->template_type,$item->bg_image,$item->id);
        }
            $dados .= '<input type="hidden" name="template_type" value="'.$t[0].'" />
                       <input type="hidden" name="template_bg" value="'.$t[1].'" />
                       <input type="hidden" name="template_id" value="'.$t[2].'" />';
            // Barra de formatação para os campos           
            //$this->geraJavaScript('template_regedit2');
        return $dados;
    }
    
    private function pdfviewer($params, $conteudo, $gravar=false)
    {
            if (!empty($conteudo))
                {
                    // pega o caminho onde estao as imagens
                    //$folderimg = '/'. basename(dirname(dirname(dirname(__FILE__)))) . '/' . $this->org['folder'];
                    $raiz = str_replace(array('index.php','/'),array('',''),$_SERVER['SCRIPT_NAME']);
                    $folderimg = (empty($raiz) ? '' : '/'.$raiz) . '/' . $this->org['folder'];
                    // convertendo o caminho das imagens do arquivo
                    if (stristr($conteudo,$folderimg))
                    {
                        $conteudo = str_replace($folderimg,$_SERVER['DOCUMENT_ROOT'] . $folderimg,$conteudo);
                        $conteudo = str_replace(base_url(),'/',$conteudo);
                    }
                }
            else
            {
                return 'Documento vazio!';
            }
            
            if (empty($params['bg'])) $params['bg'] = '';
            else $params['bg'] = 'background:url('.$params['bg'].') repeat-y;';
                
            header('Content-type: application/pdf');
            //header('Content-Disposition: attachment; filename="sample.pdf"');
            
            require_once("js/dompdf_old/dompdf_config.inc.php");
            //spl_autoload_register('DOMPDF_autoload');
            
            $css = '
            <style>
            body,td,th {
                font-family: Arial, sans-serif;
                font-size: 12px;
            }
            body {
                margin: 0px;
            }
            div.conteudo {
                margin: 0;
                padding: 0;
                width: 700px;
                height: 1000px;
                '.$params['bg'].';
                /*border: black 1px solid;*/
            }
            
            </style>';
            
            $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        '.$css.'
        </head>
        <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        
        <div class="conteudo">';
            $html .= $conteudo;
            $html .= "</div></body></html>";

            $dompdf = new DOMPDF();
            $dompdf->load_html($html);
            $dompdf->set_paper('A4');
            $dompdf->render();
            if(!$gravar)
                $dompdf->stream("sample.pdf");
            else
                {
                    $output = "documento".$this->session->userdata('esta_logado').time().".pdf";
                    $pdfoutput = $dompdf->output(); 
                    $filename = getcwd() . '/' . $this->org['folder'] . '/' . $output; 
                    $fp = fopen($filename, "a"); 
                    fwrite($fp, $pdfoutput); 
                    fclose($fp);
                    return $output;
                }
    }

    private function montarDivsDocumento($id,$valores,$campos)
    {
        $div = '';
        if (!empty($campos))
            $campo = unserialize($campos);
        $valor = unserialize($valores);
        // busca o conteudo do template
        $resultado = $this->banco->dados('template',$id);
        foreach($resultado as $item)
        {
            if ($item->template_type == 3) // documento com varios campos mas sem desenho de areas
            {
                $tmp_tpl = $item->template_value;
                // Extrai os campos definidos no documento
                $oscampos = array();
                preg_match_all("/(?<=(\[\[))(.*?)(?=\])/",$tmp_tpl,$campos_tpl);
                if(!empty($campos_tpl[0]))
                foreach($campos_tpl[0] as $campotmp)
                {
                    $oscampos[] = '/'.preg_quote($campotmp).'/';
                }
                $div = preg_replace($oscampos,$valor,$tmp_tpl);
                $div = str_replace(array('[[','*]]',']]'),array('','',''),$div);
            }
            else // documento com varios campos e com desenho de areas
            {
                $titulo = md5($item->title);
                $sessaosrl = explode("||",$item->template_value);
                // Procura pela string correta com o nome dos campos
                foreach($sessaosrl as $itemform) 
                {
                    // Verifica se a string pertence ao template desejado
                    if (strstr($itemform,$titulo))
                    {
                        // Descodifica a string
                        $itemform = unserialize($itemform);
                        if (!empty($itemform))
                        foreach($itemform[$titulo] as $itemf) :
                                
                                $fname = $itemf['fname']; // nome do box
                                $tname = $itemf['tname']; // tipo do box
                                $classe = $itemf['classe']; // classe do box
                                $w = $itemf['w']; // width
                                $h = $itemf['h']; // height
                                $x = $itemf['x']; // x
                                $y = $itemf['y']; // y
                                
                                // procura pelo campo na array (para textos apenas)
                                $key = array_search('txt_' . $classe,$campo);
                                if ($key !== false) :
                                    $div .= '<div id="'.$classe.'" name="'.$tname.'_'.$classe.'" style="z-index: 100; position: absolute; left: '.$x.'px; top: '.$y.'px; width: '.$w.'px; height: '.$h.'px;">';
                                    $div .= $valor[$key];
                                    $div .= '</div>';
                                endif;
                                
                                // procura pelo campo na array (para imagens apenas)
                                $key = array_search('img_' . $classe,$campo);
                                if ($key !== false) :
                                    $div .= '<div id="'.$classe.'" name="'.$tname.'_'.$classe.'" style="z-index: 100; position: absolute; left: '.$x.'px; top: '.$y.'px; width: '.$w.'px; height: '.$h.'px;">';
                                    $div .= $valor[$key];
                                    $div .= '</div>';
                                endif;
                                
                                // procura pelo campo na array (para datas apenas)
                                $key = array_search('data_' . $classe,$campo);
                                if ($key !== false) :
                                    $div .= '<div id="'.$classe.'" name="'.$tname.'_'.$classe.'" style="z-index: 100; position: absolute; left: '.$x.'px; top: '.$y.'px; width: '.$w.'px; height: '.$h.'px;">';
                                    $div .= $valor[$key];
                                    $div .= '</div>';
                                endif;
                                
                        endforeach;
                    }
                }
            }
        }
        return $div;
    }
}
/* End of file documentos.php */
/* Location: ./application/controllers/documentos.php */
?>