<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mensagens extends MY_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->geraMenu('mensagens');
		$this->load->model('sistema','banco',TRUE);
		if ($this->session->userdata('esta_logado') == 0)
		{
			$this->session->set_flashdata('notice','Seu tempo de logado expirou. Faça um novo login.');		
			redirect('login');
		}
		
    }
	
	public function listar($foldernameid="")
	{
		$this->index($foldernameid);
	}

	public function index($foldernameid="")
	{
		$this->mostra_solicitacoes($foldernameid);
	}
	
	public function criar_solicitacao($folderid)
	{
		// Verifica pasta
		if (empty($folderid))
		{
			echo 'Falha na chamada da URL';
			return;
		}
		
		// Somente admins acessam esta area!
		if ($this->checaPermissao($folderid) == 0)
		{
			$this->session->set_flashdata('notice','Sem permissão para solicitar documentos.');		
			redirect('home');
		}
		
		// Monta o menu de opcoes
		$this->geraMenuLateral();
		
		// Monta a arvore de arquivos
		$this->geraArvore();
		
		// Monta o box de perfil do usuario
		$this->geraPerfil();
		
		// Monta box com as ultimas atualizacoes do usuario ou grupo
		$this->geraHistorico();
		
		$this->dados['folderid'] = $folderid;
		$this->dados['folder_atual'] = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
		
		// Verifica usuario logado
		$this->dados['from_user'] = '';
		$res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id = '.$this->session->userdata('esta_logado'));
		foreach($res as $row)
			$this->dados['from_user'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
		
		// Busca os usuarios aptos a receberem solicitacoes
		$this->dados['to_user'] = '';
		$res = $this->busca_responsaveis($folderid);//$this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id != '.$this->session->userdata('esta_logado'));
		foreach($res as $chave=>$valor)
			$this->dados['to_user'] .= '<option value="'. $chave .'">'. $valor .'</option>';
		
		// Status disponivies para esta acao
		$this->dados['status'] = '';
		$res = $this->banco->listagem('messages_status','name','rule = "all" and id != 1'); // exceto o status de comentario
		foreach($res as $row)
			$this->dados['status'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
			
		$this->form_validation->set_rules('to_user','Responsável','required');
		$this->form_validation->set_rules('from_user','Solicitante','required');
		$this->form_validation->set_rules('subject','Assunto','required');
		$this->form_validation->set_rules('message','Mensagem','required');
			
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('mensagens/criar_solicitacao',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{
			
			$dados = array(
				'from_user' => $this->input->post('from_user'),
				'to_user' => $this->input->post('to_user'),
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message'),
				'date_created' => date('Y-m-d H:i:s'),
				'status' => $this->input->post('status'),
				'idorg' => $this->org['id']
			);
			$insere = $this->banco->insere('messages',$dados);
			if ($insere > 0)
			{
				// Cria a relacao com a pasta atual
				$folder_id = $this->banco->campo('folders','id','nameid like "'.$folderid.'"');
				$this->banco->relacao_criar('messages',$insere,'folders',$folder_id);
				
				// envia e-mail
				$nome_solicitante = $this->banco->campo('users','name','id = '.$dados['from_user']);
				$nome_responsavel = $this->banco->campo('users','name','id = '.$dados['to_user']);
				$folder_nome = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
				
				// Registra no historico
				$this->historico('comment','criou uma solicitação ('.$insere.') na pasta <b>'. $folder_nome .'</b> para <i>'. $nome_responsavel .'</i>');
				
				if ($this->enviar_email($dados['from_user'],$dados['to_user'],'nova_solicitacao',$params = array('usuario' => $nome_solicitante, 'pasta' => $folder_nome)))
					$this->session->set_flashdata('success','Solicitação enviada.');
				else
					$this->session->set_flashdata('notice','Solicitação enviada, mas houve falha no envio do e-mail de aviso.');
				redirect('mensagens/criar_solicitacao/'.$folderid);
			}
			else
			{
				$this->session->set_flashdata('error','A solicitação não foi criada devido a um erro no sistema.');
				redirect('mensagens/criar_solicitacao/'.$folderid);
			}
		}
	}

	public function editar($cod)
	{
		// Verifica id da solicitacao
		if (empty($cod) || (!is_numeric($cod)))
		{
			echo 'Falha na chamada da URL';
			return;
		}
		
		$this->dados['idsol'] = $cod;
		
		$this->dados['resultado_query'] = $this->banco->listagem('messages','date_created','id = '. $cod);
		
		// Busca a pasta correspondente
		$tmpfolders = $this->banco->relacao('messages',$cod,'folders');
		$folderid = $this->banco->campo('folders','nameid','id = '.$tmpfolders[0]);
		$this->dados['folderid'] = $folderid;
		$this->dados['folder_atual'] = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
		
		// Verifica a permissao (somente o destinatario pode alterar)
		$tmpquery = $this->dados['resultado_query'];
		$idusuario = 0;
		foreach($tmpquery as $val)
		{
			$to_user = $val->to_user;
			$from_user = $val->from_user;
			$status = $val->status;
			$this->dados['message_text'] = $val->message;
			$this->dados['subject_text'] = $val->subject;
		}
		
		if ($this->session->userdata('esta_logado') != $to_user)
		{
			if ($status != 4 && $status != 5)
			{
				$this->session->set_flashdata('notice','Sem permissão para edição desta solicitação.');		
				redirect('home');
			}
		}
		
		// Monta o menu de opcoes
		$this->geraMenuLateral();
		
		// Monta a arvore de arquivos
		$this->geraArvore();
		
		// Monta o box de perfil do usuario
		$this->geraPerfil();
		
		// Monta box com as ultimas atualizacoes do usuario ou grupo
		$this->geraHistorico();
		
		// Pega apenas o usuario que enviou
		$this->dados['from_user'] = '';
		$res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id = '.$from_user);
		foreach($res as $row)
			$this->dados['from_user'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
		
		// Busca os usuarios aptos a receberem solicitacoes (marca o q recebeu)
		$this->dados['to_user'] = '';
		$res = $this->busca_responsaveis($folderid,true);//$this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id != '.$this->session->userdata('esta_logado'));
		foreach($res as $chave=>$valor)
		{
			if ($chave != $to_user)
			$this->dados['to_user'] .= '<option value="'. $chave .'">'. $valor .'</option>';
			else
				$this->dados['to_user'] .= '<option value="'. $chave .'" selected="selected">'. $valor .'</option>';
		}
		
		// Status disponivies para esta acao
		$this->dados['status_item'] = $status;
		$this->dados['status'] = '';
		$res = $this->banco->listagem('messages_status','name desc','rule = "resp" and id != 1'); // exceto o status de comentario
		foreach($res as $row)
		{
			if ($status != $row->id)	
			$this->dados['status'] .= '<option value="'. $row->id .'">'. $row->name .'</option>';
			else
				$this->dados['status'] .= '<option value="'. $row->id .'" selected="selected">'. $row->name .'</option>';
		}
			
		$this->form_validation->set_rules('to_user','Responsável','required');
		$this->form_validation->set_rules('from_user','Solicitante','required');
		$this->form_validation->set_rules('subject','Assunto','required');
		$this->form_validation->set_rules('message','Mensagem','required');
			
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('_cabecalho',$this->cabecalho);
			$this->load->view('mensagens/editar_solicitacao',$this->dados);
			$this->load->view('_rodape',$this->rodape);
		}
		else
		{
			$nome_responsavel = $this->banco->campo('users','name','id = '. $this->input->post('to_user'));
			// inclui a mensagem de resposta no corpo da mensagem atual
			$txtreply = $this->input->post('message_reply');
			$txt = $this->input->post('message');
			if(!empty($txtreply))
			{
				$txt .= "\n\n";
				$txt .= "------------------ Resposta de " . $nome_responsavel . " (".date('d/m H:i').") ------------------";
				$txt .= "\n\n";
				$txt .= $this->input->post('message_reply');
			}
			
			
			$dados = array(
				'from_user' => $this->input->post('from_user'),
				'to_user' => $this->input->post('to_user'),
				'subject' => $this->input->post('subject'),
				'message' => $txt,
				'status' => $this->input->post('status'),
				'idorg' => $this->org['id'],
				'id' => $cod
			);
			$insere = $this->banco->atualiza('messages',$dados);
			if ($insere > 0)
			{
				
				// envia e-mail
				$nome_solicitante = $this->banco->campo('users','name','id = '.$dados['from_user']);
				$folder_nome = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
				$novo_status = $this->banco->campo('messages_status','name','id = '.$dados['status']);
				
				// Registra no historico
				$this->historico('comment','editou a solicitação ('.$cod.') feita na pasta <b>'. $folder_nome .'</b> por <i>'. $nome_solicitante .'</i>');
				
				
				// OBS: from_user agora é quem irá receber o e-mail de aviso! (resposta)
				if ($this->enviar_email($dados['to_user'],$dados['from_user'],'editar_solicitacao',$params = array('usuario' => $nome_solicitante, 'responsavel' => $nome_responsavel, 'pasta' => $folder_nome, 'status' => $novo_status)))
					$this->session->set_flashdata('success','Solicitação atualizada.');
				else
					$this->session->set_flashdata('notice','Solicitação atualizada, mas houve falha no envio do e-mail de aviso.');
			}
			else
			{
				$this->session->set_flashdata('error','A solicitação não foi atualizada devido a um erro no sistema.');
			}
			redirect('home');
		}
	}

	public function comentar($cod)
	{
		if (empty($cod) || (!is_numeric($cod)))
		{
			echo 'Falha na chamada da URL';
			return;
		}

		$dados['idsol'] = $cod;
		
		$dados['resultado_query'] = $this->banco->listagem('messages','date_created','id = '. $cod);
		
		// Busca a pasta correspondente
		$tmpfolders = $this->banco->relacao('messages',$cod,'folders');
		$folderid = $this->banco->campo('folders','nameid','id = '.$tmpfolders[0]);
		$dados['folderid'] = $folderid;
		$dados['folder_atual'] = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
		
		
		$tmpquery = $dados['resultado_query'];
		$idusuario = 0;
		foreach($tmpquery as $val)
		{
			$to_user = $val->to_user;
			$from_user = $val->from_user;
			$status = $val->status;
			$dados['message_text'] = $val->message;
			$dados['subject_text'] = $val->subject;
		}
		
		if ($this->session->userdata('esta_logado') != $from_user)
		{
			echo 'Sem permissão para inserir comentários.';		
			return false;
		}
		$nome_solicitante = $this->banco->campo('users','name','id = '.$from_user);
		$folder_nome = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
        echo form_open('mensagens/comentar_save',array('id' => 'formCom'));
		//echo '<form id="formCom" method="POST" action="'.site_url('mensagens/comentar_save').'">';
		echo '<p class="validadeTips"><strong>Assunto:</strong> '.$dados['subject_text'].'<br><strong>Solicitante:</strong> '.$nome_solicitante.'<br><strong>Pasta:</strong> '.$folder_nome.'</p>';
		echo '<fieldset>';
		echo '<div style="overflow:auto; width:auto; height:150px"><small>'.nl2br($dados['message_text']).'</small></div>';
		echo '<label for="name">Seu comentário</label>
		<textarea name="message_reply" id="message_reply" cols="45" rows="5" class="text ui-widget-content ui-corner-all" ></textarea>';
		echo '<input type="hidden" name="id" value="'.$cod.'" />';
		echo '</fieldset>';
		echo '</form>';
	}

	public function comentar_save()
	{
		if ($_POST)
		{
			if (empty($_POST['id'])) return false;
			if (empty($_POST['message_reply'])) return false;
			
			$cod = $_POST['id'];
			$txtreply = $_POST['message_reply'];
			
			// verifica se quem esta postando é o usuario apto para isto
			$dados['resultado_query'] = $this->banco->listagem('messages','date_created','id = '. $cod);
			$tmpquery = $dados['resultado_query'];
			$idusuario = 0;
			foreach($tmpquery as $val)
			{
				$to_user = $val->to_user;
				$from_user = $val->from_user;
				$status = $val->status;
				$dados['message_text'] = $val->message;
				$dados['subject_text'] = $val->subject;
			}

			if ($this->session->userdata('esta_logado') != $from_user)
			{
				$this->session->set_flashdata('error','Você não possui permissão para comentários.');
				redirect('home');
				return false;
			}
			
			$nome_responsavel = $this->banco->campo('users','name','id = '. $to_user);
			$nome_solicitante = $this->banco->campo('users','name','id = '.$from_user);
			$tmpfolders = $this->banco->relacao('messages',$cod,'folders');
			$folderid = $this->banco->campo('folders','nameid','id = '.$tmpfolders[0]);
			$folder_nome = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
			
			// inclui a mensagem de resposta no corpo da mensagem atual
			$txt = $dados['message_text'];
			if(!empty($txtreply))
			{
				$txt .= "\n\n";
				$txt .= "------------------ Resposta de " . $nome_solicitante . " (".date('d/m H:i').") ------------------";
				$txt .= "\n\n";
				$txt .= $txtreply;
			}
			
			// salva a postagem
			$dados = array(
				'message' => $txt,
				'id' => $cod
			);
			$insere = $this->banco->atualiza('messages',$dados);
			if ($insere > 0)
			{
				// Registra no historico
				$this->historico('comment','comentou na solicitação ('.$cod.') feita na pasta <b>'. $folder_nome .'</b> por <i>'. $nome_solicitante .'</i>');
				
				// envia e-mail
				if ($this->enviar_email($from_user,$to_user,'comentar_solicitacao',$params = array('usuario' => $nome_solicitante, 'responsavel' => $nome_responsavel, 'pasta' => $folder_nome, 'comentario' => $txtreply)))
					$this->session->set_flashdata('success','Comentário enviado.');
				else
					$this->session->set_flashdata('notice','Comentário enviado, mas houve falha no envio do e-mail de aviso.');
			}
			else
			{
				$this->session->set_flashdata('error','O comentário não foi enviado devido a um erro no sistema.');
			}
			redirect('home');
			return false;
		}
		else return false;
	}

	public function remover($cod)
	{
		if (empty($cod) || (!is_numeric($cod)))
		{
			echo 'Falha na chamada da URL';
			return;
		}

		$dados['idsol'] = $cod;
		
		$dados['resultado_query'] = $this->banco->listagem('messages','date_created','id = '. $cod);
		
		// Busca a pasta correspondente
		$tmpfolders = $this->banco->relacao('messages',$cod,'folders');
		$folderid = $this->banco->campo('folders','nameid','id = '.$tmpfolders[0]);
		$dados['folderid'] = $folderid;
		$dados['folder_atual'] = $this->banco->campo('folders','name','nameid like "'.$folderid.'"');
		
		
		$tmpquery = $dados['resultado_query'];
		$idusuario = 0;
		foreach($tmpquery as $val)
		{
			$to_user = $val->to_user;
			$from_user = $val->from_user;
			$status = $val->status;
			$dados['message_text'] = $val->message;
			$dados['subject_text'] = $val->subject;
		}
		
		// Somente quem criou a solicitacao pode remove-la
		if ($this->session->userdata('esta_logado') != $from_user)
		{
			echo 'Sem permissão para remover esta solicitação.';		
			return false;
		}
		
		// tudo ok, pode remover
		if ($this->banco->remover('messages',$cod))
		{
			// Registra no historico
			$this->historico('comment','removeu a solicitação ('.$cod.') feita na pasta <b>'. $dados['folder_atual'] .'</b>');
				
			$this->session->set_flashdata('success','Solicitação removida.');
		}
		else
			{
				$this->session->set_flashdata('error','Falha ao remover solicitação.');
			}
		redirect('home');
	}

	private function busca_responsaveis($folderid,$incluir_logado=false)
	{
		// busca os responsaveis pela pasta de acordo com suas permissoes
		$users = array();
		// pega os administradores
		if(!$incluir_logado)
			$res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id != '.$this->session->userdata('esta_logado') . ' and isadmin = "yes"');
		else
			$res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and isadmin = "yes"');
			
		foreach($res as $item)
			$users[$item->id] = $item->name . ' (admin)';
		
		// Buscando os usuarios com permissao de escrita na pasta atual
		$idpasta = $this->banco->campo('folders','id','nameid like "'.$folderid.'"');
		// Relacao dos grupos com algum acesso a pasta atual
		$relpasta = $this->banco->relacao('folders',$idpasta,'groups');
		// Para cada grupo detectado, verifica se possui permissao de escrita na pasta atual
		if (empty($relpasta)) { return $users; }
		foreach($relpasta as $val) :
			$tipoperm = $this->banco->campo('relationship','permission','idobj1 = "'.$idpasta.'" and idobj2 = "'.$val.'" and type like "folders___groups"');
			if (!empty($tipoperm) && $tipoperm == 'write')
			{
				// Grupo com permissao de escrita detectado! Busca os usuarios deste grupo.
				$relusers = $this->banco->relacao('users','','groups',$val);
				if (!empty($relusers))
				{
					$res = $this->banco->listagem('users','name','islocked = "no" and idorg = '. $this->org['id'] .' and id in ('.implode(',',$relusers).')');
					if ($res)
					foreach($res as $item)
						$users[$item->id] = $item->name;
				}
			}
		endforeach;
		asort($users);
		return $users;
	}
}
/* End of file mensagens.php */
/* Location: ./application/controllers/mensagens.php */