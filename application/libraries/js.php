<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Js {
        
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }
    
    public function handsonColors()
    {
        $js = '
            var amareloclaroRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(255,255,153)\',
                textAlign: \'right\',
                fontWeight: \'bold\'
              });
            };
            
            var yellowRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(255,255,153)\',
                fontWeight: \'bold\'
              });
            };
            
            var amareloRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(255,255,153)\',
                textAlign: \'right\'
              });
            };
          
            var laranjaRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(255,204,153)\',
                fontWeight: \'bold\'
              });
            };
            
            var embranco = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'white\',
                color: \'white\'
              });
            };
            
            var verdeclaroRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(204,255,204)\'
              });
            };
            
            var verdeclarobRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(204,255,204)\',
                fontWeight: \'bold\'
              });
            };
            
            var verdeclaromoneyRenderer = function (instance, td, row, col, prop, value, cellProperties) {
              Handsontable.TextCell.renderer.apply(this, arguments);
              $(td).css({
                background: \'rgb(204,255,204)\',
                textAlign: \'right\'
              });
            };
        ';
        return $js;
    }
              
    public function granaFunctions()
    {
        $js = '
            function limpagrana(amount)
            {
                var tmp = 0;
                if(typeof amount == "undefined" || amount == null) return 0;
                amount = amount.toString();
                var tamanho = amount.length;
                
                // Suspeitando que já está no formato correto
                if(amount.indexOf(".") == (tamanho-3) || amount.indexOf(".") == (tamanho-2))
                    return parseFloat(amount);
                
                tmp = amount.replace(/[R$.]+/g,"").replace(",",".");
                
                tmp = parseFloat(tmp);
                
                if(isNaN(tmp)) return 0;
                
                return tmp;
            }
            
            function grana(amount) 
            {
                        var i = parseFloat(amount);
                        if(isNaN(i)) { i = 0.00; }
                        var minus = \'\';
                        if(i < 0) { minus = \'-\'; }
                        i = Math.abs(i);
                        i = parseInt((i + .005) * 100);
                        i = i / 100;
                        s = new String(i);
                        if(s.indexOf(\'.\') < 0) { s += \'.00\'; }
                        if(s.indexOf(\'.\') == (s.length - 2)) { s += \'0\'; }
                        s = minus + s;
                        return formatReal(s.replace(".",""));
                        //return s.replace(".",",");
            }
            
            function formatReal( int )
            {
                var tmp = int+"";
                var neg = false;
                if(tmp.indexOf("-") == 0)
                {
                    neg = true;
                    tmp = tmp.replace("-","");
                }
                
                if(tmp.length == 1) tmp = "0"+tmp
            
                tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
                if( tmp.length > 6)
                    tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
                
                if( tmp.length > 9)
                    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");
                
                if( tmp.length > 12)
                    tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");
                
                if(tmp.indexOf(".") == 0) tmp = tmp.replace(".","");
                if(tmp.indexOf(",") == 0) tmp = tmp.replace(",","0,");
            
                return (neg ? "-"+tmp : tmp);
            }
        ';
        return $js;  
    }

    public function currentAba($url)
    {
        $js = '
        // Seleciona o menu correspondente
        $("ul.nav-tabs li").each(function(){
            $(this).removeClass("active");
            if($(\'a\', this).attr(\'href\') == \''.$url.'\')
                $(this).addClass("active");
        });
        ';
        return $js;
    }
}