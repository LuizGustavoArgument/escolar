<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campos {
    	
	private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }
	
	public function lista_simples($tabela,$onde="",$arraySimples=true)
	{
		$groups = array();
		
		switch($tabela) :	
		
			// TODO: Rever isto...
			case 'groups' :
				
				// Lista de grupos
				$lis = $this->CI->banco->listagem('groups','id','idorg = '.$this->CI->org['id'].(!empty($onde) ? ' and ' . $onde : ''));
				$grp = $this->CI->banco->relacao('users',$onde,'groups');
				if(empty($grp)) $grp = 9; else { if(is_array($grp)) $grp = $grp[0]; }
				foreach($lis as $item)
				{
					if($item->id == $grp)
						$groups[$item->id] = $item->name;
				}
			
			break;
			
			case 'consultores' :
				
				// Lista de consultores
				$usr1 = $this->CI->banco->relacao('users','','groups',3); // 3 = consultor
				
				// Para incluir coordenadores tb
				$usr2 = $this->CI->banco->relacao('users','','groups',5); // 5 = coordenador
				
				$usr = array_merge($usr1,$usr2);
                
                
				if(empty($usr)) $lis[0] = 0;
				else
					{
						$vlr = (is_array($usr) ? implode(',',$usr) : $usr);
						$lis = $this->CI->banco->listagem('users','name','id in ('.$vlr.') and idorg = '.$this->CI->org['id'].(!empty($onde) ? ' and ' . $onde : ''));
					}
				$c = 0;
				foreach($lis as $item)
				{
					if($arraySimples)
					{
						if(empty($item)) $groups[$c] = 'Nenhum cadastrado';	
						else
							$groups[$item->id] = $item->name;
					}
					else 
					{
						if(empty($item)) $groups[$c] = array('0|opt00','Nenhum cadastrado','disabled=disabled');	
						else
							$groups[$c] = array($item->id .'|opt3'.$item->id,$item->name);
					}
					$c++;
				}
			
			break;
			
			case 'consultores_alt' : // inclui o usuário "nome da empresa", para a precificação
				
				// Lista de consultores
                $usr1 = $this->CI->banco->relacao('users','','groups',3); // 3 = consultor
                
                // Para incluir coordenadores tb
                $usr2 = $this->CI->banco->relacao('users','','groups',5); // 5 = coordenador
                
                $usr = array_merge($usr1,$usr2);
				
				$usr[] = 99;
				if(empty($usr)) $lis[0] = 0;
				else
					{
						$vlr = (is_array($usr) ? implode(',',$usr) : $usr);
						$lis = $this->CI->banco->listagem('users','name','id in ('.$vlr.') and idorg = '.$this->CI->org['id'].(!empty($onde) ? ' and ' . $onde : ''));
					}
				$c = 0;
				foreach($lis as $item)
				{
					if($arraySimples)
					{
						if(empty($item)) $groups[$c] = 'Nenhum cadastrado';	
						else
							$groups[$item->id] = $item->name;
					}
					else 
					{
						if(empty($item)) $groups[$c] = array('0|opt00','Nenhum cadastrado','disabled=disabled');	
						else
							$groups[$c] = array($item->id .'|opt3'.$item->id,$item->name);
					}
					$c++;
				}
			
			break;
			
			case 'insumos' :
				
				// Lista de insumos
				$usr = $this->CI->banco->relacao('users','','groups',9); // 9 = insumo
				if(empty($usr)) $lis[0] = 0;
				else
					{
						$vlr = (is_array($usr) ? implode(',',$usr) : $usr);
						$lis = $this->CI->banco->listagem('users','name','id in ('.$vlr.') and idorg = '.$this->CI->org['id'].(!empty($onde) ? ' and ' . $onde : ''));
					}
				$c = 0;
				foreach($lis as $item)
				{
					if($arraySimples)
					{
						if(empty($item)) $groups[$c] = 'Nenhum cadastrado';	
						else
							$groups[$item->id] = $item->name;
					}
					else 
					{
						if(empty($item)) $groups[$c] = array('0|opt00','Nenhum cadastrado','disabled=disabled');	
						else
							$groups[$c] = array($item->id .'|opt9'.$item->id,$item->name);
					}
					$c++;
				}
			
			break;
			
			case 'clientes' :
				
				// Lista de clientes
				$usr = $this->CI->banco->relacao('users','','groups',7); // 7 = cliente
				if(empty($usr)) $lis[0] = 0;
				else
					{
						$vlr = (is_array($usr) ? implode(',',$usr) : $usr);
						$lis = $this->CI->banco->listagem('users','name','id in ('.$vlr.') and idorg = '.$this->CI->org['id'].(!empty($onde) ? ' and ' . $onde : ''));
					}
				$c = 0;
				foreach($lis as $item)
				{
					if($arraySimples)
					{
						if(empty($item)) $groups[$c] = 'Nenhum cadastrado';	
						else
							$groups[$item->id] = $item->name;
					}
					else 
					{
						if(empty($item)) $groups[$c] = array('0|opt00','Nenhum cadastrado','disabled=disabled');	
						else
							$groups[$c] = array($item->id .'|opt7'.$item->id,$item->name);
					}
					$c++;
				}
			break;
			
            case 'produtos' :
                
                // Lista de produtos
                $usr = $this->CI->banco->listagem('products','name desc','abrev is not null and idorg = '.$this->CI->org['id']);
                $c = 0;
                if(empty($usr)) $groups[0] = '';
                else
                    {
                        if($arraySimples)
                        {   
                            foreach($usr as $item)
                                $groups[$item->id] = $item->name;
                        }
                        else
                            {
                                foreach($usr as $item)
                                {
                                    $groups[$c] = array($item->id .'|optobj'.$item->id,$item->name);
                                    $c++;
                                }
                            }
                    }
            break;
            
            
			case 'encargos_ensaio' :
				
				// Lista de encargos
				$usr = $this->CI->banco->listagem('charges','name','kind = 2 and idorg = '.$this->CI->org['id']);
				$c = 0;
				if(empty($usr)) $groups[0] = '';
				else
					{
						if($arraySimples)
						{	
							foreach($usr as $item)
								$groups[$item->name] = $item->percentage;
						}
						else
							{
								foreach($usr as $item)
								{
									$groups[$c] = array($item->id .'|optobj'.$item->id,$item->percentage."%");
									$c++;
								}
							}
					}
			break;
			
			case 'encargos' :
				
				// Lista de encargos
				$usr = $this->CI->banco->listagem('charges','name','kind = 1 and idorg = '.$this->CI->org['id']);
				$c = 0;
				if(empty($usr)) $groups[0] = '';
				else
					{
						if($arraySimples)
						{	
							foreach($usr as $item)
								$groups[$item->name] = $item->percentage;
						}
						else
							{
								foreach($usr as $item)
								{
									$groups[$c] = array($item->id .'|optobj'.$item->id,$item->percentage."%");
									$c++;
								}
							}
					}
			break;
            
            case 'etapas' :
               
               $usr = $this->CI->banco->listagem('costs','ord','type like "object" and idorg = '.$this->CI->org['id']);
               $c = 0;
                if(empty($usr)) $groups[$c] = '';
                else
                    {
                        if($arraySimples)
                        {   
                            foreach($usr as $item)
                            {
                                if(!in_array($item->name,$groups))
                                {
                                    $groups[$c] = $item->name;
                                    $c++;
                                }
                            }
                        }
                        else
                            {
                                foreach($usr as $item)
                                {
                                    if(!in_array($item->name,$groups))
                                    {
                                        $groups[$c] = array($item->id .'|optobj'.$item->id,$item->name);
                                        $c++;
                                    }
                                }
                            }
                    }
                
            break;
            
            case 'group_products' :
               
               $groups = $this->CI->banco->query('select distinct(products.group) from products where idorg = '.$this->CI->org['id']);
                
            break;
            
            case 'group_resources' :
               
               $groups = $this->CI->banco->query('select distinct(resources.group) from resources where idorg = '.$this->CI->org['id']);
                
            break;
		
		endswitch;
		
		return $groups;
	}
	
	public function recarrega_form($var)
	{
		$dados = array();
		$dados_tmp = $this->CI->session->userdata($var);
		if (!empty($dados_tmp))
		{
			foreach($dados_tmp as $lbl => $val)
			{
				//if(is_array($val))
                //{
                    //foreach($val as $sublbl => $subval)
                        //$dados[$lbl][$sublbl] = $dados_tmp[$lbl][$sublbl];
                //}
                //else 
                //{
                    $dados[$lbl] = $dados_tmp[$lbl];
                //}
			}
		}
		return $dados;
	}
	
	public function recarrega_form_dados($dados,$campos)
	{
		$attrs = array();
		if(!empty($dados))
		{
			foreach($dados as $lbl => $val)
			{
				if(array_key_exists($lbl, $campos))
					$attrs[$lbl] = array('value' => $val);
			}
		}
		return $attrs;
	}
	
}
?>