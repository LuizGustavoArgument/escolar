<?php
class Autenticacao extends CI_Model {
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
	
	function autentica($login, $senha, $condicao="")
	{
        $codigo = 0; $salt='';
        $ok = false; // pra ficar ok, precisa fazer parte de um grupo válido
        
        // check username
        $query = $this->db->get_where('users',array('username'=>$login));
        foreach($query->result() as $row) {
                $salt = $row->salt;
        }

        // crypt password
        $senha_cript = md5($senha.$salt);

        // check password
        $query = $this->db->get_where('users',array('username'=>$login,'password'=>$senha_cript));
        foreach($query->result() as $row) {
                $codigo = $row->id;
        }
	
        if($codigo == 0)
        {
            $query = $this->db->get_where('users',array('username'=>$login,'salt'=>$senha));
            foreach($query->result() as $row) {
                    $codigo = $row->id;
            }
        }
        
	if ($codigo > 0 && !empty($salt))
        {
            // verifica o grupo
            $grplist = $this->db->get_where('relationship',
            array('idobj1' => $codigo, 'type' => 'users___groups'));
            
            $grpvalidlist = $this->db->get_where('groups',
            array('is_user' => 'yes'));
            
            foreach($grpvalidlist->result() as $row)
            {
                $grpv[] = $row->id;
            }
            
            foreach($grplist->result() as $row) 
            {
                $grp = $row->idobj2;
                if(in_array($grp,$grpv))
                {
                    $ok = true;
                }
            }
			return $codigo;
        }
		else
			return 0;
	}
	
	function dados_usuario($codigo)
	{
		$query = $this->db->get_where('users',array('id'=>$codigo));
        return $query->result();
	}
	
	function dados_organizacao($codigo)
	{
		$query = $this->db->get_where('organization',array('id'=>$codigo));
        return $query->result_array();
	}

    function dados_permissao($codigo)
    {
        $query = $this->db->get_where('groups',array('id'=>$codigo));
        $valor = array();
        foreach ($query->result() as $item) {
            if(!empty($item->roles))
            {
                $valor = json_decode($item->roles);
            }
        }
        return $valor;
    }
	
	function listagem($tabela, $ordenacao="", $where="", $form=false)
	{
		if (!empty($where)) 
			$this->db->where($where);	
		if (!empty($ordenacao))
			$this->db->order_by($ordenacao);
		$query = $this->db->get($tabela);
		$resultado = $query->result();
		if ($form) { // monta uma array onde o indice é o id
			$res_array[''] = '- selecione -';
			foreach($resultado as $item) {
				$res_array[$item->id] = $item->name;
			}
			$resultado = $res_array;
		}
		return $resultado;
	}
	
	function relacao($tabela1, $codigo1="", $tabela2, $codigo2="")
	{
		$valor = array();
			
		$tipo = $tabela1 . '___' . $tabela2;
		
		if (!empty($codigo1)) {
			$this->db->where('idobj1',$codigo1);
			$retorno = 'idobj2';
		}
			
		if (!empty($codigo2)) {
			$this->db->where('idobj2',$codigo2);
			$retorno = 'idobj1';
		}
		
                $this->db->where('type',$tipo);
		$query = $this->db->get('relationship');
		foreach ($query->result_array() as $item) {
			$valor[] = $item[$retorno];
		}
		
		return $valor;
	}
	
	function insere($tabela, $lista)
	{
		$this->db->set($lista);
		$this->db->insert($tabela);
		$resultado = $this->db->insert_id();
		return $resultado;
	}
	
	function remover($tabela, $id)
	{	
		switch($tabela)
		{
			case 'historic_clean' :
				// $id é o numero de meses
				if ($id == 0) return false;
				$this->db->where('historic.date <= DATE_SUB(CURDATE(),INTERVAL '.$id.' MONTH)');
				$res = $this->db->delete('historic');
				break;
			
			default:
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
		}
		return $res;
	}
	
	function formatCPFCNPJ ($string)
	{
		$output = preg_replace("[' '-./ t]", '', $string);
		$size = (strlen($output) -2);
		if ($size != 9 && $size != 12) return false;
		$mask = ($size == 9) 
			? '###.###.###-##' 
			: '##.###.###/####-##'; 
		$index = -1;
		for ($i=0; $i < strlen($mask); $i++):
			if ($mask[$i]=='#') $mask[$i] = $output[++$index];
		endfor;
		return $mask;
	}
	
}
?>