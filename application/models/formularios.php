<?php

class Formularios extends CI_Model {
	
	public $org = array();

    function __construct()
    {
        parent::__construct();
		if($this->session->userdata('orgdata'))
		{
			$this->org = $this->session->userdata('orgdata');
			$this->org = $this->org[0];
		}
		else
			$this->org['id'] = 1;
    }
    
    function produtos_criar(&$form, $data)
    {
        $resultado = false;
        
        $tipo = $data['tipo'];
        
        if(empty($tipo))
        {
            $form->add_error('group', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Relacao de campos
        $campos_users = array('name','desc','group','abrev','active','date_created','idorg');
        
        $tabela = 'products';
        
        $data['idorg'] = $this->org['id'];
        
        // Verificando erros
        if(empty($data['group']))
        {
            $form->add_error('group', 'Você precisa escrever o nome do grupo.');
            return false; 
        }
        
        switch($tipo):
        
            // Cadastro de usuários
            case 'cadastrar' :

                // Limpa o array de campos invalidos
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Para o checkbox
                if(empty($lista['active'])) $lista['active'] = 0; else $lista['active'] = 1;
                
                // Grava no banco
                $this->db->set($lista);
                $this->db->insert($tabela);
                $resultado = $this->db->insert_id();
                
            break;
            
            case 'alterar' :
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo')
                        $lista2[$dd] = $label;
                }
                
                // Para o checkbox
                if(empty($lista2['active'])) $lista2['active'] = 0; else $lista2['active'] = 1;
                
                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
            
            break;
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
        return $resultado;
    }

    function recursos_criar(&$form, $data)
    {
        $resultado = false;
        
        $tipo = $data['tipo'];
        
        if(empty($tipo))
        {
            $form->add_error('group', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Relacao de campos
        $campos_users = array('name','desc','group','active','date_created','idorg');
        
        $tabela = 'resources';
        
        $data['idorg'] = $this->org['id'];
        
        // Verificando erros
        if(empty($data['group']))
        {
            $form->add_error('group', 'Você precisa escrever o nome do grupo.');
            return false; 
        }
        
        switch($tipo):
        
            // Cadastro de usuários
            case 'cadastrar' :

                // Limpa o array de campos invalidos
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Para o checkbox
                if(empty($lista['active'])) $lista['active'] = 0; else $lista['active'] = 1;
                
                $prod = $data['product'];
                unset($data['product']);
                
                // Grava no banco
                $this->db->set($lista);
                $this->db->insert($tabela);
                $resultado = $this->db->insert_id();
                
                // Para o produto
                // (cria a relação)
                $tmp = $this->relacao_criar('resources',$resultado,'products',$prod[0]);
                
            break;
            
            case 'alterar' :
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo')
                        $lista2[$dd] = $label;
                }
                
                $prod = $data['product'];
                unset($data['product']);
                
                // Para o checkbox
                if(empty($lista2['active'])) $lista2['active'] = 0; else $lista2['active'] = 1;
                
                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
                
                // Redefine relacao
                $tmp = $this->relacao_remover('resources',$lista['id'],'products');
                $tmp = $this->relacao_criar('resources',$lista['id'],'products',$prod[0]);
                
            break;
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
        return $resultado;
    }


    function entrevistas_criar(&$form, $data)
    {
        $resultado = false;
        
        $this->load->model('entidades');
        
        $tabela = 'ent_questoes';
        
        // Verificando erros
        if(empty($data['tipo']))
        {
            $form->add_error('tipo', 'Você precisa escrever o tipo.');
            return false; 
        }
        
        switch($tipo):
        
            // Cadastro de usuários
            case 'cadastrar' :

                // Para o checkbox
                if(empty($lista['active'])) $lista['active'] = 0; else $lista['active'] = 1;
                
                // Pega a descricao
                $desc = $lista['desc'];
                
                $d = explode("\n",$desc);
                foreach($d as $linha)
                {
                    $l = explode("|",$linha);
                    $qg = (empty($l[0]) ? '' : $l[0]);
                    $qgn = (empty($l[1]) ? '' : $l[1]);
                    $qt = (empty($l[2]) ? '' : $l[2]);
                    $qtn = (empty($l[3]) ? '' : $l[3]);
                    $qr = (empty($l[4]) ? '' : $l[4]);
                }
                
                // Grava no banco
                $atributos = array(
                    'tipo' => $tipo,
                    'questao_tipo' => 'TEXTO',
                    'questao_grupo' => $qg,
                    'questao_gruponum' => $qgn,
                    'questao_titulo' => $qt,
                    'questao_titulonum' => $qtn,
                    'questao_resumo' => $qr
                );
                
                $idowner = $this->session->userdata('esta_logado');
                $idrule = 1;
                
                $identidade = $this->entidades->nova_entidade('ent_questoes',$atributos,0,$idowner,$idrule);
                
            break;
            
            case 'alterar' :
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo')
                        $lista2[$dd] = $label;
                }
                
                // Para o checkbox
                if(empty($lista2['active'])) $lista2['active'] = 0; else $lista2['active'] = 1;
                
                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
            
            break;
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
        return $resultado;
    }

    function grupos_criar(&$form, $data)
    {
        $resultado = false;
        
        $tipo = $data['tipo'];
        
        if(empty($tipo))
        {
            $form->add_error('name', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Relacao de campos
        $campos_users = array('name','desc','code','is_user','roles','date_created','idorg');
        
        $tabela = 'groups';
        
        $data['idorg'] = $this->org['id'];
        
        // Verificando erros
        if(empty($data['name']))
        {
            $form->add_error('name', 'Você precisa escrever o nome do grupo.');
            return false; 
        }
        
        switch($tipo):
        
            // Cadastro de usuários
            case 'cadastrar' :

                // Limpa o array de campos invalidos
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // json encoding
                unset($data['name'],$data['desc'],$data['code'],$data['is_user'],$data['roles'],$data['date_created'],$data['idorg']);
                $roles = json_encode($data);
                
                $lista['is_user'] = $lista['is_user'][0];
                $lista['roles'] = $roles;
                
                // Grava no banco
                $this->db->set($lista);
                $this->db->insert($tabela);
                $resultado = $this->db->insert_id();
                
            break;
            
            case 'alterar' :
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                $lista['is_user'] = $lista['is_user'][0];
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo')
                        $lista2[$dd] = $label;
                }
                
                // json encoding
                unset($data['name'],$data['desc'],$data['code'],$data['is_user'],$data['roles'],$data['date_created'],$data['idorg']);
                $roles = json_encode($data);
                $lista2['roles'] = $roles;

                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
            
            break;
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
        return $resultado;
    }

    function encargos_criar(&$form, $data)
    {
        $resultado = false;
        
        $tipo = $data['tipo'];
        
        if(empty($tipo))
        {
            $form->add_error('name', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Relacao de campos
        $campos_users = array('name', 'kind', 'desc',
         'percentage', 'ord', 'formula');
        
        $tabela = 'charges';
        
        $data['idorg'] = $this->org['id'];
        $data['kind'] = $data['kind'][0];
        
        // Verificando erros
        if(empty($data['kind']))
        {
            $form->add_error('kind', 'Você precisa selecionar um tipo.');
            return false; 
        }

        // Verificando erros
        if(($data['kind'] == 1 || $data['kind'] == 2) && empty($data['percentage']))
        {
            $form->add_error('percentage', 'Você precisa definir a porcentagem padrão para o tipo selecionado.');
            return false; 
        }
        
        // Verificando erros
        if(!is_numeric(str_replace(",",".",$data['percentage'])))
        {
            $form->add_error('percentage', 'O formato do número precisa ser decimal.');
            return false; 
        }
        
        switch($tipo):
        
            // Cadastro de encargos
            case 'cadastrar' :

                // Limpa o array de campos invalidos
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Busca a formula, caso esteja setada
                $formula = '';
                $formula = $lista['formula'];
                unset($lista['formula']);
                
                // Grava no banco
                $this->db->set($lista);
                $this->db->insert($tabela);
                $resultado = $this->db->insert_id();
                
                // Grava na tabela de relacao
                if($resultado > 0)
                if(!empty($formula))
                {
                    $r = $this->relacao_criar('charges',$resultado,'charges',$resultado,'none',$formula);
                }
                
                
            break;
            
            case 'alterar' :
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo')
                        $lista2[$dd] = $label;
                }
                
                // Busca a formula, caso esteja setada
                $formula = '';
                $formula = $lista2['formula'];
                unset($lista2['formula']);

                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
                
                // Grava na tabela de relacao
                if($resultado > 0)
                if(!empty($formula))
                {
                    $r = $this->relacao_criar('charges',$resultado,'charges',$resultado,'none',$formula);
                }
            
            break;
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
        return $resultado;
    }
    
    function usuarios_criar(&$form, $data)
    {
		// Ações de criação/edição/remoção de clientes
		// Exemplo de chamada no controller: $this->form->model('formularios', 'usuarios_criar');
		
		$resultado = false;
        
        $tipo = $data['tipo'];
        
        
        if(empty($tipo))
        {
            $form->add_error('groups', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Antes de tudo, verifica a senha, caso seja alteração
        if(($tipo == 'alterar' || $tipo == 'alterar_admin'))
        {
            if(empty($data['password_atual']) && !empty($data['password']))
            {
                $form->add_error('password_atual', 'Você precisa informar a senha atual.');
                return false; 
            }
            
            if(!empty($data['password_atual']) && !empty($data['password']))
            {   
                // Verifica senha
                $salt = '';
                $this->db->select('salt,password');
                $this->db->from('users');
                $this->db->where('id',$data['id']);
                $query = $this->db->get();
                $res = $query->result_array();
                if ($res)
                foreach($res as $item)
                {
                    $salt = $item['salt'];
                    $senha = $item['password'];
                }

                if(!empty($salt) && !empty($senha))
                {
                    if(empty($data['password_naosei'])) :
                        $passchk = md5($data['password_atual'] . $salt);
                        if($passchk != $senha)
                        {
                            $form->add_error('password_atual', 'Esta senha não bate com a atual.');
                            return false;
                        }
                    endif;
                }
                else
                    {
                        $form->add_error('password_atual', 'Erro ao alterar a senha.');
                        return false;
                    }
                
            }
        }
        unset($data['password_atual']);
        
        // Relacao de campos
        $campos_users = array('groups','username','password','salt','name','email','phone','city','uf','isadmin','islocked','date_created','idorg');
        $campos_usersdata = array('iduser','address','address_num', 'address_cpl', 'address_ref','country','fax','mobile',
                                    'website','cnpj','contact_name1', 'contact_cargo1', 'contact_email1','contact_mobile1','contact_phone1', 'contact_fax',
                                    'contact_cpf','logo'
                                    ,'contact_name2', 'contact_cargo2', 'contact_email2','contact_mobile2','contact_phone2'
                                    ,'contact_name3', 'contact_cargo3', 'contact_email3','contact_mobile3','contact_phone3'
                                    ,'contact_name4', 'contact_cargo4', 'contact_email4','contact_mobile4','contact_phone4'
                                    ,'contact_name5', 'contact_cargo5', 'contact_email5','contact_mobile5','contact_phone5','outros');
        
        $tabela = 'users';
        
        $data['idorg'] = $this->org['id'];
        
		if(empty($tipo))
		{
			$form->add_error('groups', 'Nenhum tipo foi definido para este grupo.');
			return false; 
		}

        // Verificando erros
        if(empty($data['groups'][0]))
        {
            $form->add_error('groups', 'Você precisa definir um grupo.');
            return false; 
        }
        
        // Verifica se é usuário
        /*
        $onde = (is_array($data['groups']) ? 'id in ('.implode(',',$data['groups']).')' : 'id = '.$data['groups']);
        if ( $this->campo('groups','is_user',$onde) == 'yes')
            $usuario = true;
        else
            $usuario = false;
		*/
		switch($tipo):
		
			// Cadastro de usuários
			case 'cadastrar' :
            case 'cadastrar_p' :
				
				// Verifica username
				$username = $data['username'];
				$res = $this->banco->campo('users','username','username like "'.$username.'"');
                if (!empty($res))
                {
                    $form->add_error('username', 'Login informado ('.$username.') já existe. Tente outro.');
                    return false;
                }
				
				// Gravando senha
				$data['salt'] = $this->generatePassword();
				$data['password'] = md5($data['password'] . $data['salt']);
                
                // Se nao for array o grupo, é porque é insumo
                if(!is_array($data['groups']))
                {
                    $tmpgrp = $data['groups'];
                    unset($data['groups']);
                    $data['groups'][0] = $tmpgrp;
                }
				
				// Se for administrador...
				$data['isadmin'] = ((!empty($data['groups'][0]) && $data['groups'][0] == 6) ? 'yes' : 'no');
				
				// Verifica na definição de grupos se é travado ou não
				$data['islocked'] = 'no';//( ($usuario) ? 'no' : 'yes');
                // Grava no banco
				$lista = $this->limpaArrayForm($data, $campos_users);
                unset($lista['groups']);
                
                // Grava quem criou o registro
                $lista['idhist'] = $this->session->userdata('esta_logado');
				$this->db->set($lista);
				$this->db->insert($tabela);
				$resultado = $this->db->insert_id();
				
				// Gravando relação de grupo
                $data['iduser'] = $resultado;
                $tmp = $this->relacao_criar('users',$data['iduser'],'groups',$data['groups'][0]);
                
                
                // Criando as pastas no GED
                // Se for user, cria a pasta
                $gcmp = $this->campo('groups','is_user','id = '.$data['groups'][0]);
                if($gcmp == 'yes')
                {
                    $folder_level = 2;
                    $folder_parent = $this->campo('folders','nameid','name like "Users"'); // Subpasta de Users
                        
                    $fdados = array(
                        'name' => $username,
                        'desc' => 'Pasta do '.$data['name'],
                        'date_created' => date('Y-m-d H:i:s'),
                        'nameid' => rand(111111,999999),
                        'folder_level' => $folder_level,
                        'folder_parent' => $folder_parent,
                        'idorg' => 1,
                        'num' => 0,
                        'owner' => $data['iduser'],
                        'permission' => 'write'
                    );
                    
                    $this->db->set($fdados);
                    $this->db->insert('folders');
                    $fresultado = $this->db->insert_id();
                    
                    if($fresultado > 0)
                    {
                        // Pasta DOCS
                        $folder_level = 3;
                        $folder_parent = $this->campo('folders','nameid','id = '. $fresultado); // Subpasta de Users
                        
                        unset($fdados);
                        
                        $fdados = array(
                            'name' => 'docs',
                            'desc' => 'Pasta fixa para contratos e propostas para '.$data['name'],
                            'date_created' => date('Y-m-d H:i:s'),
                            'nameid' => rand(111111,999999),
                            'folder_level' => $folder_level,
                            'folder_parent' => $folder_parent,
                            'idorg' => 1,
                            'num' => 0,
                            'owner' => $data['iduser'],
                            'permission' => 'read'
                        );
                        
                        $this->db->set($fdados);
                        $this->db->insert('folders');
                        $fresultado1 = $this->db->insert_id();
                        
                        // Pasta CONSULTOR 
                        unset($fdados);
                        
                        $fdados = array(
                            'name' => 'escola',
                            'desc' => 'Pasta privativa para anexo de documentos do usuário '.$data['name'],
                            'date_created' => date('Y-m-d H:i:s'),
                            'nameid' => rand(111111,999999),
                            'folder_level' => $folder_level,
                            'folder_parent' => $folder_parent,
                            'idorg' => 1,
                            'num' => 0,
                            'owner' => $data['iduser'],
                            'permission' => 'sol' // permissão que configura para que o consultor/administrador possa escrever na pasta
                        );
                        
                        $this->db->set($fdados);
                        $this->db->insert('folders');
                        $fresultado2 = $this->db->insert_id();
                    }
                }
                
                
                
                
                
                
                
				// Para a tabela usersdata
				$tabela = 'usersdata';
					
				// Grava no banco
				$lista = $this->limpaArrayForm($data, $campos_usersdata);
                
                // Trata o upload de imagem (caso possui)
                if(!empty($data['logo']['full_path']))
                {
                    $lista['logo'] = $this->uploadImg($data['logo']);
                }
                else 
                {
                    $lista['logo'] = '';
                }
                
                $lista['contact_cpf'] = $data['contact_cpf'];
                $lista['country'] = $data['country'];
                
				$this->db->set($lista);
				$this->db->insert($tabela);
				$resultado = $this->db->insert_id();

				break;
		
            case 'alterar' :
            case 'alterar_admin' :
                
                // Se nao for array o grupo, é porque é insumo
                if(!is_array($data['groups']))
                {
                    $tmpgrp = $data['groups'];
                    unset($data['groups']);
                    $data['groups'][0] = $tmpgrp;
                }
                
                // Limpa o array de campos invalidos
                $campos_users[] = 'id';
                $lista = $this->limpaArrayForm($data, $campos_users);
                
                $campos_usersdata[] = 'id';
                $listadata = $this->limpaArrayForm($data, $campos_usersdata);
                
                // Gravando relação de grupo
                $tmp = $this->relacao_remover('users',$lista['id'],'groups');
                $tmp = $this->relacao_criar('users',$lista['id'],'groups',$lista['groups'][0]);
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($lista as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo' && $dd != 'groups')
                        $lista2[$dd] = $label;
                }
                
                // Certifica que o campo id (entre outros) não está incluso na lista de update
                foreach($listadata as $dd => $label) {
                    if ($dd != 'id' && $dd != 'salvar' && $dd != 'tipo' && $dd != 'groups')
                        $listadata2[$dd] = $label;
                }
                
                // Verifica se há alteração de senha (somente se preenchido)
                if(empty($lista2['password']))
                    unset($lista2['password']);
                else
                    {
                        $salt = '';
                        $this->db->select('salt');
                        $this->db->from('users');
                        $this->db->where('id',$lista['id']);
                        $query = $this->db->get();
                        $res = $query->result_array();
                        if ($res)
                        foreach($res as $item)
                        {
                            $salt = $item['salt'];
                        }
                        $lista2['password'] = md5($lista2['password'] . $salt);
                    }
                
                // Se for admin, pode alterar tudo
                if($tipo == 'alterar')
                {
                    unset($lista2['username']);
                }
                
                // Se for administrador...
                $lista2['isadmin'] = ($data['groups'][0] == 6 ? 'yes' : 'no');
                
                // Faz a alteração
                $this->db->where('id',$lista['id']);
                $resultado = $this->db->update($tabela,$lista2);
                if ($resultado)
                    $resultado = $lista['id'];
                else
                    $resultado = 0;
                
                if($resultado)
                {
                    // Se mudou o nome ou username, precisa mudar as pastas!
                        // Verifica primeiro se é user mesmo:
                        $gcmp = $this->campo('groups','is_user','id = '.$data['groups'][0]);
                        if($gcmp == 'yes')
                        {
                            $lisAlt = $this->banco->listagem('folders','id','owner = ' . $lista['id']);
                            if(!empty($lisAlt))
                            foreach($lisAlt as $l)
                            {
                                if($l->name == 'escola')
                                {
                                    $dataAlt = array('name' => 'escola',
                                            'desc' => 'Pasta privativa para anexo de documentos do usuário '.$data['name'],
                                            'id' => $l->id);
                                }
                                elseif($l->name == 'docs')
                                {
                                    $dataAlt = array('name' => 'docs',
                                            'desc' => 'Pasta fixa para contratos e propostas para '.$data['name'],
                                            'id' => $l->id);
                                }
                                else 
                                {
                                    if($l->folder_level == 2)
                                    {
                                        $dataAlt = array('name' => $data['username'],
                                                'desc' => 'Pasta do '.$data['name'],
                                                'id' => $l->id);
                                    }
                                }
                                if(!empty($dataAlt))
                                {
                                    $this->db->where('id',$dataAlt['id']);
                                    unset($dataAlt['id']);
                                    $Alt = $this->db->update('folders',$dataAlt);
                                    unset($dataAlt);
                                }
                            }
                        }
                }
                
                
                
                // Verifica se existe a ligação com usersdata
                $this->db->from('usersdata');
                $this->db->where('iduser',$lista['id']);
                $query = $this->db->get();
                $listagem = $query->result_array();
                
                if(!empty($listagem)) {
                
                    // Trata o upload de imagem (caso possui)
                    if(!empty($data['logo']['full_path']))
                    {
                        $listadata2['logo'] = $this->uploadImg($data['logo']);
                    }
                    else 
                    {
                        unset($listadata2['logo']);
                    }
                    
                    $listadata2['contact_cpf'] = $data['contact_cpf'];
                    $listadata2['country'] = $data['country']; 
                    
                    // Faz a alteração
                    $this->db->where('iduser',$lista['id']);
                    $resultado = $this->db->update('usersdata',$listadata2);
                    
                    if ($resultado)
                    {
                        $resultado = $lista['id'];
                    }
                    else
                        $resultado = 0;
                    
                } else {
                    
                    // Cria o registro
                    $listatmp = $this->limpaArrayForm($data, $campos_usersdata);
                    $listatmp['iduser'] = $lista['id'];
                    
                    // Trata o upload de imagem (caso possui)
                    if(!empty($data['logo']['full_path']))
                    {
                        $listatmp['logo'] = $this->uploadImg($data['logo']);
                    }
                    else 
                    {
                        $listatmp['logo'] = '';
                    }
                    
                    $listatmp['contact_cpf'] = $data['contact_cpf'];
                    $listatmp['country'] = $data['country'];
                    
                    $this->db->set($listatmp);
                    $this->db->insert('usersdata');
                    $resultado = $this->db->insert_id();
                    
                }    
            
            break;

        
        
		endswitch;
		
        ($resultado > 0 ? $this->session->set_flashdata('success','Informações salvas.') : $this->session->set_flashdata('error','Ocorreu um erro!'));
        if ($resultado > 0) { $this->historico($tabela,$tipo.' ('.$resultado.')'); }
        
		return $resultado;
    }

    function uploadImg($logo)
    {
        
        if($logo['is_image'] != 1)
        {
            return false;
        }
        
        // Renomeando
        $newfile = $logo['file_path'].md5(time().$logo['file_name']).$logo['file_ext'];
        if(rename($logo['full_path'],$newfile))
        {
            $this->historico('upload',$newfile);
            return str_replace($logo['file_path'],'',$newfile);
        }
        
        return false;
    }

    function orcamento_email(&$form, $data)
    {
        $resultado = false;
        
        $tipo = 'cadastrar';
        
        if(empty($tipo))
        {
            $form->add_error('group', 'Nenhum tipo foi definido para este grupo.');
            return false; 
        }
        
        // Verificando erros
        if(empty($data['de']))
        {
            $form->add_error('de', 'Você precisa digitar o email de remetente.');
            return false; 
        }

        if(empty($data['para']))
        {
            $form->add_error('para', 'Você precisa digitar o email de destinatário.');
            return false; 
        }
        
        if(empty($data['assunto']))
        {
            $form->add_error('assunto', 'Você precisa digitar o assunto.');
            return false; 
        }
        
        if(empty($data['mensagem']))
        {
            $form->add_error('mensagem', 'Mensagem vazia!');
            return false; 
        }
        
        if(empty($data['attach']))
        {
            $form->add_error('attach', 'É obrigatório que haja o anexo.');
            return false; 
        }
        
        
        switch($tipo):
        
            // Envia o email e altera o status
            case 'cadastrar' :

                if(empty($data['apenasque']))
                {
                    // Envia o email
                    $this->load->library('email');
                    
                    $config['mailtype'] = 'html';

                    $this->email->initialize($config);
                    
                    $this->email->from($data['de'], 'InBloom');
                    $this->email->to($data['para']); 
                    if(!empty($data['cc']))
                        $this->email->cc($data['cc']); 
                    $this->email->bcc($data['de']); 
                    
                    $this->email->subject(strip_tags($data['assunto']));
                    $html = '<html><head></head><body>'.$data['mensagem'].'</body></html>';
                    //$html = $data['mensagem'];
                    $this->email->message($html);
                    
                    $this->email->attach(getcwd().'/'.$this->org['folder'].'/'.$data['attach']);
                    
                    if($this->email->send())
                        $this->historico('email','Para: ' . $data['para'] .' || Assunto: ' . strip_tags($data['assunto']) .' || Mensagem: '. strip_tags($data['mensagem']));
                }
                 
                // Altera o status
                $lista['idstatus'] = 2;
                $this->db->where('id',(int) $data['idserv']);
                $this->db->update('services',$lista);
                $resultado = $data['idserv'];
                
            break;
            
            
        endswitch;
        
        ($resultado > 0 ? $this->session->set_flashdata('success','Ação executada com sucesso.') : $this->session->set_flashdata('error','Ocorreu um erro:'));
        if ($resultado > 0) { $this->historico('services','service ('.$data['idserv'].') | status ('.$lista['idstatus'].')'); }
        
        return $resultado;
    }

	function orcamento_criar(&$form, $data)
	{
		$resultado = false;
		
		$extras = $this->leStringSeguranca($data['extras']);
		
		if($this->verStringSeguranca($extras,array('tipo' => 'precificacao')) === FALSE) return false;
		
		// Verifica o passo
		$tipo = $extras['param'];
		
		// Recarrega
		$dados_tmp = $this->session->userdata('dados_precificacao');
		if (!empty($dados_tmp))
		{
			foreach($dados_tmp as $lbl => $val)
			{
				if(!array_key_exists($lbl, $data))
				{
					$data[$lbl] = $dados_tmp[$lbl];
				}
			}
		}
		$this->session->unset_userdata('dados_precificacao');
		
		// Salva tudo
		$dados_tmp = array('dados_precificacao' => $data);
		$this->session->set_userdata($dados_tmp);
		$resultado = true;
		
		return $resultado;
	}

	function verStringSeguranca($extras=array(),$comparar=array())
	{
		if(empty($extras))
		{
			$form->add_error('extras', 'Falha de segurança.');
			return false;
		}
		
		if(empty($extras['idusu']) || $extras['idusu'] != $this->session->userdata('esta_logado'))
		{
			$form->add_error('extras', 'Falha de segurança. Usuário não permitido.');
			return false;
		}
		
		if(!empty($comparar))
		{
			if(!empty($comparar['tipo']) && ($comparar['tipo'] != $extras['tipo']))
			{
				$form->add_error('extras', 'Falha de segurança. Tipo incompatível.');
				return false;
			}
			
			if(!empty($comparar['param']) && ($comparar['param'] != $extras['param']))
			{
				$form->add_error('extras', 'Falha de segurança. Parâmetro inválido.');
				return false;
			}
		}
		
		return true;
	} 
	
	function leStringSeguranca($str)
	{
		$code = explode('_',base64_decode($str));
		
		return array('tipo' => $code[0], 'param' => $code[1], 'idusu' => $code[2], 'tempo' => $code[3]);
	}

	function generatePassword($length=8,$level=1)
	{

	   list($usec, $sec) = explode(' ', microtime());
	   srand((float) $sec + ((float) $usec * 100000));
	
	   $validchars[1] = "0123456789abcdfghjkmnpqrstvwxyz";
	   $validchars[2] = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	   $validchars[3] = "0123456789_!@#$%&*()-=+/abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_!@#$%&*()-=+/";
	
	   $password  = "";
	   $counter   = 0;
	
	   while ($counter < $length) {
			 $actChar = substr($validchars[$level], rand(0, strlen($validchars[$level])-1), 1);
		
			 if (!strstr($password, $actChar)) {
				$password .= $actChar;
				$counter++;
			 }
	   }

     return $password;
	}
	
	function limpaArrayForm($valores,$campos)
	{
		// Elimina os campos que não fazem parte do que será gravado
		// $valores = array com as labels e valores
		// $campos = labels permitidos
		
		$novalista = array();
		
		foreach($campos as $campo)
		{
			if(array_key_exists($campo,$valores))
				$novalista[$campo] = $valores[$campo];
		}
		
		return $novalista;
	}
	
    function relacao($tabela1, $codigo1="", $tabela2, $codigo2="",$perm="",$ver_conteudo=false)
    {
        $valor = array();
            
        $tipo = $tabela1 . '___' . $tabela2;
        
        $this->db->where('type',$tipo);
        
        if (!empty($codigo1)) {
            $this->db->where('idobj1',$codigo1);
            $retorno = 'idobj2';
        }   
        
        if (!empty($codigo2)) {
            $this->db->where('idobj2',$codigo2);
            $retorno = 'idobj1';
        } 
        
        if (empty($codigo1) && empty($codigo2))
        {
            return false;
        }
        
        
        //$this->db->where('idorg',$this->org['id']);
        if (!empty($perm)) {
            $this->db->where('permission',$perm);
        }
        
        $query = $this->db->get('relationship');
        $res = $query->result_array();
        
        if ($res)
        {
            if ($ver_conteudo) $retorno = 'content';
            foreach ($res as $item) {
                $valor[] = $item[$retorno];
            }
        }
        
        return $valor;
    }
    
    function relacao_remover($obj1,$id1,$obj2,$id2='')
    {
        $res_temp = false;
            
        // Remover a relacao
        if(empty($id2) && !empty($id1))
            $lst2 = $this->relacao($obj1,$id1,$obj2);
        elseif (!empty($id2) && empty($id1))
            $lst2 = $this->relacao($obj1,'',$obj2,$id2);
        else
            $lst2 = $this->relacao($obj1,$id1,$obj2,$id2);
        
        if(!empty($lst2))
        foreach($lst2 as $item)
        {
            $this->db->where('idobj1',(empty($id1) ? $item : $id1));
            $this->db->where('type',$obj1.'___'.$obj2);
            $this->db->where('idobj2',(empty($id2) ? $item : $id2));
            $res_temp = $this->db->delete('relationship');
        }
        return $res_temp;
    }
    
	function relacao_criar($tabela1, $codigo1="", $tabela2, $codigo2="",$perm="none",$content="")
	{
		
		$tipo = $tabela1 . '___' . $tabela2;
		
		// verifica se ja existe e altera o existente
		if (!empty($codigo1) && !empty($codigo2)) 
		{
			$this->db->where('idobj1',$codigo1);
			$this->db->where('idobj2',$codigo2);
			$this->db->where('type',$tipo);
			$q = $this->db->get('relationship');
			$n = $q->result();
			$altera = 0;
			if($q->num_rows() > 0)
			{				
				foreach($n as $i)
				{
					$altera = $i->id;
				}
			}
		}
		
		if($altera > 0)
		{
			$dados = array('content' => $content, 'permission' => $perm, 'date_created' => date('Y-m-d H:i:s')); // UPDATE 03/11/2011: setar o date_created?
			$this->db->where('id',$altera);
			$query = $this->db->update('relationship',$dados);
			$resultado = $altera;
		}
		else
		{
			$this->db->set('type',$tipo);
		
			if (!empty($codigo1)) {
				$this->db->set('idobj1',$codigo1);
			}
				
			if (!empty($codigo2)) {
				$this->db->set('idobj2',$codigo2);
			}
			$this->db->set('date_created',date('Y-m-d H:i:s'));
			$this->db->set('idorg',$this->org['id']);
			$this->db->set('permission',$perm);
			
			$this->db->set('content',$content);

			$query = $this->db->insert('relationship');
			$resultado = $this->db->insert_id();
		}
		return $resultado;
	}

	function campo($tabela, $nome_campo, $where="")
	{
		$ret = '';
		$this->db->select($nome_campo);
		if (!empty($where))
			$this->db->where($where);
		$query = $this->db->get($tabela);
		$num_linhas = $query->num_rows();
		$res = $query->result_array();
		if ($res)
		foreach($res as $item)
		{
			if ($num_linhas > 1)
				$ret[] = $item[$nome_campo];
			else
				$ret = $item[$nome_campo];
		}
		return $ret;
	}
    
    function historico($module,$action="")
    {
        if(empty($module)) return false;
        $data = date('Y-m-d H:i:s');
        $idusu = $this->session->userdata('esta_logado');
        $dados = array(
            'idusu' => $idusu,
            'module' => $module,
            'action' => $action,
            'date' => $data,
            'info' => $this->session->userdata('ip_address'),
            'idorg' => $this->org['id']
        );
         $id = $this->banco->insere('historic',$dados);
        return true;
    }
}

/* End of file Example.php */
/* Location: /application/models/Example.php */