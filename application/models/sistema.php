<?php
class Sistema extends CI_Model {
	
	public $org = array();
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->org = $this->session->userdata('orgdata');
		$this->org = $this->org[0];
    }
	
	/*
	 * 
	 * Funções basicas
	 * 
	 */
	
	function dados($tabela, $codigo)
	{
		$query = $this->db->get_where($tabela,array('id'=>$codigo));
		return $query->result();
	}
	
	function dados_usuario($codigo)
	{
		$query = $this->db->get_where('users',array('id'=>$codigo));
        return $query->result();
	}
	
	function dados_organizacao($codigo)
	{
		$query = $this->db->get_where('organization',array('id'=>$codigo));
        return $query->result_array();
	}
	
	function campo($tabela, $nome_campo, $where="")
	{
		$ret = '';
		$this->db->select($nome_campo);
		if (!empty($where))
			$this->db->where($where);
		$query = $this->db->get($tabela);
		$num_linhas = $query->num_rows();
		$res = $query->result_array();
		if ($res)
		foreach($res as $item)
		{
			if ($num_linhas > 1)
				$ret[] = $item[$nome_campo];
			else
				$ret = $item[$nome_campo];
		}
		return $ret;
	}
	
	function insere($tabela, $lista)
	{
		$this->db->set($lista);
		$this->db->insert($tabela);
		$resultado = $this->db->insert_id();
		return $resultado;
	}
    
    function insere2($tabela, $lista)
    {
        // Funcao que insere mas analisa se há dados semelhantes na tabela e atualiza caso positivo
        foreach($lista as $dd => $label) {
            if ($dd != 'id' && $dd != 'salvar' && $dd != 'date_created'
            && $dd != 'date_updated' && $dd != 'date_pgto' && $dd != 'debit' && $dd != 'credit'
            && $dd != 'percent' && $dd != 'commission' && $dd != 'stdrvalue')
            {
                $this->db->where($dd,$label);
                $data[$dd] = $label;
            }
        }
        $query = $this->db->get($tabela);
        
        if($query->num_rows() == 0)
        {
            $this->db->set($lista);
            $this->db->insert($tabela);
            $resultado = $this->db->insert_id();
        }
        else
            {
                foreach($data as $lbl => $vl)
                    $this->db->where($lbl,$vl);                
                $resultado = $this->db->update($tabela,$lista);
                
                // Busca o ID
                if(empty($data['id']))
                {
                    foreach($data as $lbl => $vl)
                        $this->db->where($lbl,$vl);
                    $data['id'] = 0;
                    $restmp = $this->db->get($tabela);
                    foreach($restmp->result() as $t)
                    {
                        $data['id'] = $t->id;
                    }
                }
                
                if ($resultado)
                    $resultado = $data['id'];
                else
                    $resultado = 0;
            }
        return $resultado;
    }
	
	function atualiza($tabela, $lista)
	{
		foreach($lista as $dd => $label) {
			if ($dd != 'id' && $dd != 'salvar')
				$data[$dd] = $label;
			//if (($tabela == 'users') && empty($data[$dd]) && $dd != 'remover') unset($data[$dd]);
		}
		
		$this->db->where('id',$lista['id']);
		$resultado = $this->db->update($tabela,$data);
		
		if ($resultado)
			$resultado = $lista['id'];
		else
			$resultado = 0;
			
		return $resultado;
	}
    
    function atualiza2($tabela, $lista, $onde)
    {
        foreach($lista as $dd => $label) {
           $data[$dd] = $label;
        }
        
        foreach($onde as $dd => $label) {
           $this->db->where($dd,$label);
        }
        
        $this->db->update($tabela,$data);
        $ar = $this->db->affected_rows();
        
        if($ar == 0)
        {
            // Se existe, nao precisa fazer nada
            $q = $this->db->get_where($tabela,$onde);
            if($q->num_rows() == 0)
                $resultado = $this->db->insert($tabela,$data);
            $ar = 1;
        }
        $resultado = $ar;
        return $resultado;
    }
    
    function isUser($tipo="cliente",$id)
    {
        // Verifica se o ID pertence a um cliente, consultor, coordenador, administrador ou insumo
        $rel = $this->relacao('users',$id,'groups');
        if(!$rel) return false;
        switch($tipo)
        {
            case 'cliente':
                if(in_array(7,$rel))
                    return true;
                break;
            
            case 'coordenador':
                if(in_array(5,$rel))
                    return true;
                break;
            
            case 'consultor':
                if(in_array(3,$rel))
                    return true;
                break;
            
            case 'administrador':
                if(in_array(6,$rel))
                    return true;
                break;
            
            case 'insumo':
                if(in_array(9,$rel))
                    return true;
                break;
            
            default:
                return false;
                break;
        }
        return false;
    }
    
    function idclientes($ids=array())
    {
        // Monta a lista de ids de clientes, de acordo com a lista de consultores informada
        $ret = array();
        if(empty($ids)) return $ret;
        
        foreach($ids as $uid)
        {
            // Faz parte de algum serviço
            $id2 = $this->listagem('services','id');
            if(!empty($id2))
            {
                foreach($id2 as $id2tmp)
                {
                    $def = unserialize($id2tmp->data_array);
                    if(in_array($uid,$def['consultants']) || $uid == $id2tmp->id_createdby)
                    {
                        $ret = array_merge($ret,$def['clients']);
                    }
                    
                    unset($def);
                }
            }
        }
        $ret = array_unique($ret);
        return $ret;
    }
    
    function idconsultores($ids=array())
    {
        // Monta a lista de ids de consultores, de acordo com os ids de cliente informados
        $ret = array();
        if(empty($ids)) return $ret;
        
        foreach($ids as $uid)
        {
            // Faz parte de algum serviço
            $id2 = $this->listagem('services','id');
            if(!empty($id2))
            {
                foreach($id2 as $id2tmp)
                {
                    $def = unserialize($id2tmp->data_array);
                    if(in_array($uid,$def['clients']))
                    {
                        $ret = array_merge($ret,$def['consultants']);
                    }
                    unset($def);
                }
            }
        }
        $ret = array_unique($ret);
        return $ret;
    }
    
    function idconsultoresserv($ids=array())
    {
        // Monta a lista de ids de consultores envolvidos, de acordo com o ids dos serviços informado
        $ret = array();
        if(empty($ids)) return $ret;
        
        foreach($ids as $uid)
        {
            // Criou o serviço
            $id1 = $this->campo('services','id_createdby','id = '.$uid);
            if(!empty($id1))
            {
                if(is_array($id1)) :
                foreach($id1 as $id1tmp)
                {
                    $ret[] = $id1tmp;
                }
                else :
                    $ret[] = $id1;
                endif;
            }
                
            // Faz parte de algum serviço
            $id2 = $this->listagem('services','id','id = '. $uid);
            if(!empty($id2))
            {
                foreach($id2 as $id2tmp)
                {
                    $def = unserialize($id2tmp->data_array);
                    $ret = array_merge($ret,$def['consultants']);
                    unset($def);
                }
            }
        }
        $ret = array_unique($ret);
        return $ret;
    }
    
    function idservicos($ids=array())
    {
        // Monta a lista de serviços que o usuário participa de alguma forma
        $ret = array();
        if(empty($ids)) return $ret;
        
        /*foreach($ids as $uid)
        {
            // Criou o serviço
            $id1 = $this->campo('services','id','id_createdby = '.$uid);
            if(!empty($id1))
            {
                if(is_array($id1)) :
                foreach($id1 as $id1tmp)
                {
                    $ret[] = $id1tmp;
                }
                else :
                    $ret[] = $id1;
                endif;
            }
            // Faz parte de algum serviço
            $id2 = $this->listagem('services','id');
            if(!empty($id2))
            {
                foreach($id2 as $id2tmp)
                {
                    $def = unserialize($id2tmp->data_array);
                    if(in_array($uid,$def['clients']) || in_array($uid,$def['consultants']))
                    {
                        $ret[] = $id2tmp->id;
                    }
                    unset($def);
                }
            }
        }
        $ret = array_unique($ret);*/
        return $ret;
    }

    function idprodutos($rel=array(),$uid)
    {
        // Monta a lista de serviços que o usuário participa de alguma forma e que contém os produtos informados
        $ret = array();
        
        $idservs = $this->idservicos(array($uid)); // Lista de serviços que o usuário participa
        if(empty($idservs)) return $ret;
        
        // A partir da lista, procura pelos produtos
        foreach($idservs as $ids)
        {
            $rel = $this->relacao('services',$ids,'products');
            if(!empty($rel))
            {
                $p = implode(",",$rel);
                $prods = $this->listagem('products','id','abrev is not null and id in ('. $p .')');
                if(!empty($prods))
                {
                   $ret[] = $ids;
                }
            }
        }
        
        return $ret;
    }
    
    function getProductList($uid,$nivel)
    {
        // Se for admin, mostra tudo
        if($nivel == 'yes')
        {
            $prods = $this->listagem('products','id','abrev is not null');
            if(!empty($prods))
            {
                 return $prods;
            }
        }
            
        // Busca lista de produtos executáveis que o usuário pode ver
        $idservs = $this->idservicos(array($uid));
        if(empty($idservs)) return array();
        
        foreach($idservs as $ids)
        {
            $rel = $this->relacao('services',$ids,'products');
            if(!empty($rel))
            {
                $p = implode(",",$rel);
                $prods = $this->listagem('products','id','abrev is not null and id in ('. $p .')');
                if(!empty($prods))
                {
                     return $prods;
                }
            }
        }
        return array();
    }
    
    function getProductListByService($sid)
    {
        // Busca lista de produtos executáveis de um determinado serviço
        $rel = $this->relacao('services',$sid,'products');
        if(!empty($rel))
        {
            $p = implode(",",$rel);
            $prods = $this->campo('products','id','abrev is not null and id in ('. $p .')');
            if(!empty($prods))
            {
                 if(!is_array($prods)) $prods = array($prods);
                 return $prods;
            }
        }
        return array();
    }

	function listagem($tabela, $ordenacao="", $where="",$onlyarray=false)
	{
		if (!empty($where)) 
			$this->db->where($where);	
		if (!empty($ordenacao))
			$this->db->order_by($ordenacao);
		$query = $this->db->get($tabela);
		
		if($onlyarray)
            $resultado = $query->result_array();
        else
            $resultado = $query->result();
        
        if($query->num_rows() == 0) return array();
			
		return $resultado;
	}
	
	function listagem_users($where="",$order="",$onlyqtde=false,$onlyarray=false,$limit="")
	{
		$this->db->select('users.*, usersdata.id as idusersdata, usersdata.address, usersdata.address_num, usersdata.address_cpl, usersdata.address_ref, usersdata.country, 
		usersdata.fax, usersdata.mobile, usersdata.website, usersdata.cnpj,
		usersdata.contact_name1, usersdata.contact_cargo1, usersdata.contact_email1, usersdata.contact_mobile1, usersdata.contact_phone1, 
		usersdata.contact_fax, usersdata.contact_cpf, usersdata.logo,
		usersdata.contact_name2, usersdata.contact_cargo2, usersdata.contact_email2, usersdata.contact_mobile2, usersdata.contact_phone2,
		usersdata.contact_name3, usersdata.contact_cargo3, usersdata.contact_email3, usersdata.contact_mobile3, usersdata.contact_phone3,
		usersdata.contact_name4, usersdata.contact_cargo4, usersdata.contact_email4, usersdata.contact_mobile4, usersdata.contact_phone4,
		usersdata.contact_name5, usersdata.contact_cargo5, usersdata.contact_email5, usersdata.contact_mobile5, usersdata.contact_phone5');
		$this->db->from('users');
		$this->db->join('usersdata','usersdata.iduser = users.id','left');
		
		if(!empty($where))
			$this->db->where($where);
		if(!empty($order))
			$this->db->order_by($order);
        if(!empty($limit))
            $this->db->limit($limit);
		
		$query = $this->db->get();
		if ($onlyqtde)
			$resultado = $query->num_rows();
		else
			{
				if($onlyarray)
					$resultado = $query->result_array();
				else
					$resultado = $query->result();
				
			}
		return $resultado;
	}
	
    /*
	function custo($tipo,$id,$idservice)
	{
		$retorno = array('unit' => '','value' => '','percent' => '');
        if(!empty($id))
		  $lis = $this->listagem('costs','ord','iduserto = '.$id.' and idserviceto = '.$idservice.' and type like "'.$tipo.'"');
        else 
	      $lis = $this->listagem('costs','ord','idserviceto = '.$idservice.' and type like "'.$tipo.'"');

		foreach($lis as $item)
		{
			$retorno['value'] = $item->stdrvalue;
			$retorno['unit'] = $item->unit;
			$retorno['percent'] = $item->percentage;
		}
		return $retorno;
	}
	*/
	
	
	
	
	function resultadofinal($categoria,$uf,$where="",$onlyqtde=false)
	{
		// Condicao:
		// users.idhist = 1
		// judges.idstatus = 6
		// para cada judges.idpoll, judges.finalscore precisa estar preenchido em judges.cont = 1,2 e 3
		// 
		$this->db->select('judges.finalscore AS nota, polls.title, polls.desc, users.id, users.name, users.city, users.uf, users.email');
		$this->db->from('judges');
		$this->db->join('polls','polls.id = judges.idpoll');
		$this->db->join('users','users.id = judges.iduserfrom');
		$onde = 'judges.idpoll = '.$categoria.' AND judges.cont IN (1,2,3) AND judges.finalscore > 0 AND judges.idstatus = 6 AND users.idhist = 1';
		$this->db->where($onde);
		if($uf != 'nacional')
		{
			$onde = 'users.uf like "'.$uf.'"';
			$this->db->where($onde);
		}
		if(!empty($where))
			$this->db->where($where);
		
		$query = $this->db->get();
		if ($onlyqtde)
			$resultado = $query->num_rows();
		else
			$resultado = $query->result();
		
		return $resultado;
	}
	
    function listagem_custos($where="",$order="",$qtde=0,$qtde2=-1,$onlyqtde=false)
    {
        //TODO: terminar
        $this->db->select('relationship.*, users.name as usersname, users.username as usersusername, 
        costs');
        $this->db->from('relationship');
        $this->db->join('status','status.id = services.idstatus','left');
        if(!empty($where))
            $this->db->where($where);
        if(!empty($order))
            $this->db->order_by($order);
        if($qtde > 0 && $qtde2 == -1)
            $this->db->limit($qtde);
        elseif($qtde > 0 && $qtde2 > 0)
            $this->db->limit($qtde,($qtde2-1));
        
        $query = $this->db->get();
        if ($onlyqtde)
            $resultado = $query->num_rows();
        else
            $resultado = $query->result();
        
        return $resultado;
    }
    
	function listagem_financeiro($quem="cliente",$where="",$order="",$qtde=0,$qtde2=-1,$onlyqtde=false)
	{
		$this->db->select('financial.*, services.name as servicesname, services.id as servicesid, 
		services.idstatus as servicesstatus, users.name as usersname, users.username as usersusername, 
		status.name as statusname, status.color as statuscolor');
		$this->db->from('financial');
		if($quem == 'cliente')
        {
            $this->db->join('users','users.id = financial.iduserfrom', 'left');
            $this->db->join('services','services.id = financial.idserviceto', 'left');
        }
        if($quem == 'funcionario')
        {
            $this->db->join('users','users.id = financial.iduserto', 'left');
            $this->db->join('services','services.id = financial.idservicefrom', 'left');
        }
        
		$this->db->join('status','status.id = services.idstatus','left');
        $this->db->where('services.idstatus >= 3 and services.idstatus != 4 and services.idstatus != 8 and services.idstatus != 9'); // somente em proposta aprovada
		if(!empty($where))
			$this->db->where($where);
		if(!empty($order))
			$this->db->order_by($order);
		if($qtde > 0 && $qtde2 == -1)
			$this->db->limit($qtde);
		elseif($qtde > 0 && $qtde2 > 0)
			$this->db->limit($qtde,($qtde2-1));
		
		$query = $this->db->get();
		if ($onlyqtde)
			$resultado = $query->num_rows();
		else
			$resultado = $query->result();
		
		return $resultado;
	}
	
	function listagem_msgs($where="",$order="",$qtde=0,$qtde2=-1,$onlyqtde=false)
	{
		$this->db->select('messages.*, users.id as cod, users.email, users.username as cnpj, users.name as coop, users.city, users.uf, users.idhist as conclusao');
		$this->db->from('messages');
		$this->db->join('users','users.id = messages.iduserfrom');
		if(!empty($where))
			$this->db->where($where);
		if(!empty($order))
			$this->db->order_by($order);
		if($qtde > 0 && $qtde2 == -1)
			$this->db->limit($qtde);
		elseif($qtde > 0 && $qtde2 > 0)
			$this->db->limit($qtde,($qtde2-1));
		$this->db->group_by('messages.iduserfrom');
		
		$query = $this->db->get();
		if ($onlyqtde)
			$resultado = $query->num_rows();
		else
			$resultado = $query->result();
		
		return $resultado;
	}
	
	function listagem_gestor($where="",$order="",$qtde=0,$qtde2=-1,$onlyqtde=false)
	{
		$this->db->select('relationship.*, polls.title, polls.desc, users.id as cod, users.username as cnpj, users.name as coop, users.city, users.uf, users.email, users.idhist as conclusao');
		$this->db->from('relationship');
		$this->db->join('polls','polls.id = relationship.idobj2');
		$this->db->join('users','users.id = relationship.idobj1');
		$this->db->where('relationship.type = "users___polls"');
		if(!empty($where))
			$this->db->where($where);
		if(!empty($order))
			$this->db->order_by($order);
		if($qtde > 0 && $qtde2 == -1)
			$this->db->limit($qtde);
		elseif($qtde > 0 && $qtde2 > 0)
			$this->db->limit($qtde,($qtde2-1));
		
		$query = $this->db->get();
		if ($onlyqtde)
			$resultado = $query->num_rows();
		else
			$resultado = $query->result();
		
		return $resultado;
	}
	
	function listagem_docs($id_da_pasta, $condicao="")
	{
		$rel = $this->relacao('docs', '', 'folders', $id_da_pasta);
		if (!empty($condicao)) 
			$this->db->where($condicao);
		if (!empty($rel))
			$this->db->where('id in ('.implode(',',$rel).')');
		else
			return false;
		$query = $this->db->get('docs');
		$resultado = $query->result();
			
		return $resultado;
	}
        
        function listagem_docsuser($id_da_pasta, $condicao="")
	{
		$rel = $this->relacao('docsuser', '', 'folders', $id_da_pasta);
		if (!empty($condicao)) 
			$this->db->where($condicao);
		if (!empty($rel))
			$this->db->where('id in ('.implode(',',$rel).')');
		else
			return false;
		$query = $this->db->get('docsuser');
		$resultado = $query->result();
			
		return $resultado;
	}
	
	function listar($tabela, $itens_por_pagina, $pagina_atual, $ordenacao="", $where="")
	{
		if (!empty($ordenacao))	
			$this->db->order_by($ordenacao);
		if (!empty($where)) $this->db->where($where);
		$query = $this->db->get($tabela, $itens_por_pagina, $pagina_atual);
		$resultado = $query->result();
		
		return $resultado;
	}
    
    function custo_user($id,$calendario=false)
    {
        if($calendario == false)
            $lis = $this->banco->campo('costs','stdrvalue','type like "user" and name = "Mult" and iduserto = '.$id);
        else
            $lis = "";
        return $lis;
/*
        $tabela1 = 'users';
        $tabela2 = 'charges';
                    
        $tipo = $tabela1 . '___' . $tabela2;
        
        $this->db->where('type',$tipo);
        $this->db->where('idobj1',$id);
        if($salario)
            $this->db->where('idobj2',88); // 88 = salario
        else
            $this->db->where('idobj2',99); // 99 = custo por hora ou unidade
        
        $query = $this->db->get('relationship');
        $res = $query->result_array();
        
        $valor = "";
        
        if ($res)
        {
            $retorno = 'content';
            foreach ($res as $item) {
                $valor[] = $item[$retorno];
            }
        }
        
        return (is_array($valor) ? $valor[0] : $valor);
*/        
    }
    
    function custo_operacao($id,$calcula=false)
    {

        $tabela1 = 'charges';
        $tabela2 = 'charges';
                    
        $tipo = $tabela1 . '___' . $tabela2;
        
        $this->db->where('type',$tipo);
        $this->db->where('idobj1',$id);
        $this->db->where('idobj2',$id);
        $this->db->where('permission','operacao');
        
        $query = $this->db->get('relationship');
        $res = $query->result_array();
        
        $valor = "";
        
        if ($res)
        {
            $retorno = 'content';
            foreach ($res as $item) {
                $valor[] = $item[$retorno];
            }
        }
        if(!$calcula)
            return (is_array($valor) ? $valor[0] : $valor);
        else
            {
                if(empty($valor)) return 0;
                return $this->calcula_operacao(is_array($valor) ? $valor[0] : $valor);
            }
    }
	
    function calcula_operacao($exp)
    {
        //captura entre {} ==> /\{(.*?)\}/i
        preg_match_all('/\{(.*?)\}/i',$exp,$ops);
        // $ops[1] contém os operandos sem o {}
        $rep = array();
        if(!empty($ops[1]))
        {
            foreach($ops[1] as $n => $tmp)
            {
                $v = explode(":",$tmp);
                // Busca o valor na tabela
                $val = $this->banco->campo($v[0],'percentage','id = '.$v[1]);
                $rep[$n] = $val;
            }
            
            foreach($ops[0] as $n => $tmp)
            {
                $pat[$n] = '/'.$tmp.'/';
            }
        }
        $res = preg_replace($pat,$rep,$exp);
        eval('$calc = round((float) ('.$res.'),2);');
        return $calc;
    }
    
	function relacao($tabela1, $codigo1="", $tabela2, $codigo2="",$perm="",$ver_conteudo=false,$content="")
	{
		$valor = array();
			
		$tipo = $tabela1 . '___' . $tabela2;
		
		if (!empty($codigo1)) {
			$this->db->where('idobj1',$codigo1);
			$retorno = 'idobj2';
		}	
		
		if (!empty($codigo2)) {
			$this->db->where('idobj2',$codigo2);
			$retorno = 'idobj1';
		} 
		
		if (empty($codigo1) && empty($codigo2))
		{
			return false;
		}
		
		
		//$this->db->where('idorg',$this->org['id']);
		if (!empty($perm)) {
			$this->db->where('permission',$perm);
		}
                
                if (!empty($content)) {
			$this->db->where('content',$perm);
		}
		
                $this->db->where('type',$tipo);
		$query = $this->db->get('relationship');
		$res = $query->result_array();
		
		if ($res)
		{
			if ($ver_conteudo) $retorno = 'content';
			foreach ($res as $item) {
				if(!empty($item[$retorno]))
				    $valor[] = $item[$retorno];
			}
		}
		
		return $valor;
	}
	
	function relacao_criar($tabela1, $codigo1="", $tabela2, $codigo2="",$perm="none",$content="")
	{
		
		$tipo = $tabela1 . '___' . $tabela2;
		
		// verifica se ja existe e altera o existente
		if (!empty($codigo1) && (!empty($codigo2))) 
		{
			$this->db->where('idobj1',$codigo1);
			$this->db->where('idobj2',$codigo2);
			$this->db->where('type',$tipo);
			$q = $this->db->get('relationship');
			$n = $q->result();
			$altera = 0;
			if($q->num_rows() > 0)
			{				
				foreach($n as $i)
				{
					$altera = $i->id;
				}
			}
		}
		
		if($altera > 0)
		{
			$dados = array('content' => $content, 'permission' => $perm, 'date_created' => date('Y-m-d H:i:s')); // UPDATE 03/11/2011: setar o date_created?
			$this->db->where('id',$altera);
			$query = $this->db->update('relationship',$dados);
			$resultado = $altera;
		}
		else
		{
			$this->db->set('type',$tipo);
		
			if (!empty($codigo1)) {
				$this->db->set('idobj1',$codigo1);
			}
				
			if (!empty($codigo2)) {
				$this->db->set('idobj2',$codigo2);
			}
			$this->db->set('date_created',date('Y-m-d H:i:s'));
			$this->db->set('idorg',$this->org['id']);
			$this->db->set('permission',$perm);
			
			$this->db->set('content',$content);

			$query = $this->db->insert('relationship');
			$resultado = $this->db->insert_id();
		}
		return $resultado;
	}
	
	function relacao_alterar($tabela1, $array_codigo1, $tabela2, $array_codigo2,$array_perm=array(),$array_content=array())
	{
		$tipo = $tabela1 . '___' . $tabela2;
		
		// Remove os registros semelhantes
		/*foreach($array_codigo1 as $codigo1)
		{
			$this->db->where('type',$tipo);
			$this->db->where('idobj1',$codigo1);
			$valor = $this->db->delete('relationship');
		}*/
		// BUGFIX ID 68
		for($i=0;$i<count($array_codigo1);$i++)
		{
			$this->db->where('type',$tipo);
			$this->db->where('idobj1',$array_codigo1[$i]);
			$this->db->where('idobj2',$array_codigo2[$i]);
			$valor = $this->db->delete('relationship');
		}
		
		// Insere os registros
		if (!empty($array_codigo2))
		for($i=0;$i<count($array_codigo1);$i++)
		{
			if (!empty($array_perm))
			{
				$dados = array(
					'type' => $tipo,
					'idobj1' => $array_codigo1[$i],
					'idobj2' => $array_codigo2[$i],
					'date_created' => date('Y-m-d H:i:s'),
					'idorg' => $this->org['id'],
					'permission' => $array_perm[$i]
				);
			}
			else
			{
				$dados = array(
					'type' => $tipo,
					'idobj1' => $array_codigo1[$i],
					'idobj2' => $array_codigo2[$i],
					'date_created' => date('Y-m-d H:i:s'),
					'idorg' => $this->org['id'],
					'permission' => 'none'
				);
			}
			
			$this->db->set($dados);
			$final = $this->db->insert('relationship');
		}

		return true;
	}
	
	function contar($tabela, $where="")
	{
		if (!empty($where)) $this->db->where($where);
		$query = $this->db->get($tabela);
		$resultado = $query->num_rows();
		
		return $resultado;
	}
	
	function backup($tabela, $tipo, $campos="*", $where="")
	{
		$this->load->dbutil();
		$res = '';
		
		if(!empty($where)) $where = ' WHERE ' . $where; else $where='';
		if($tabela != 'historic')
		{
			$query = $this->db->query("SELECT ".$campos." FROM ".$tabela.$where);
		} else {
			$query = $this->db->query("SELECT users.name, users.email, historic.action, historic.date, historic.info FROM historic LEFT JOIN users ON historic.idusu = users.id ".$where);
		}
		
		if ($tipo == 'csv')
		{
			$res = $this->dbutil->csv_from_result($query); 
		}
		elseif ($tipo == 'xml')
		{
			$res = $this->dbutil->xml_from_result($query); 
		}
		elseif ($tipo == 'sql')
		{
			$prefs = array(
                'tables'      => array($tabela),    // Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',             // gzip, zip, txt
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );

			$res = & $this->dbutil->backup($prefs); 
		}
		
		return $res;
	}
	
	function query($sql)
	{
		$res = $this->db->query($sql);
		return $res->result_array();
	}
	
	function queryfull($sql)
	{
		$res = $this->db->query($sql);
		return $res->result();
	}
    
    function naolidas($user_id)
    {
        $login = $this->get_field('users', 'username', $user_id);
        if(empty($login)) return 0;
            
        $sql_string = 
        'SELECT chat.recd FROM chat WHERE chat.to LIKE "'.$login.'"
        AND chat.recd = 0';
        $query = $this->db->query($sql_string);
        return $query->num_rows();
    }
    
    function offline($user_id)
    {
        $email = $this->get_field('users', 'email', $user_id);
        if(empty($email)) return TRUE;
        
        $sql_string = 
        'SELECT last_activity FROM ci_sessions WHERE user_data LIKE "%'.$this->get_field('users','username', $user_id).'%"
        AND user_data LIKE "%'.$email.'%"';
        $query = $this->db->query($sql_string);
        if ($query->num_rows() > 0) 
        {
            foreach($query->result() as $item)
            {
                $tempo = $item->last_activity;
                if((time()-$tempo) >= 4000)
                    return TRUE;
                else
                    return FALSE;
            }
        }
        return TRUE;
    }
    
    function get_field($table, $field, $id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        $row = $query->row();
        return $row->$field;
    } 
	
	function desativar($id)
	{
		// Remover a relacao users polls
		$lst2 = $this->relacao('users',$id,'polls');
		if(!empty($lst2))
		foreach($lst2 as $item)
		{
			$this->db->where('idobj1',$id);
			$this->db->where('type','users___polls');
			$this->db->where('idobj2',$item);
			$res_temp = $this->db->delete('relationship');
		}
		// Remover a relacao users polls_itens
		unset($lst2);
		$lst2 = $this->relacao('users',$id,'polls_itens');
		if(!empty($lst2))
		foreach($lst2 as $item)
		{
			$this->db->where('idobj1',$id);
			$this->db->where('type','users___polls_itens');
			$this->db->where('idobj2',$item);
			$res_temp = $this->db->delete('relationship');
		}
		// Remove o histórico
		$this->db->where('idusu',$id);
		$res_temp3 = $this->db->delete('historic');
		// Desativa usuario
		$data = array('islocked' => 'yes');
		$this->db->where('id',$id);
		$resultado = $this->db->update('users',$data);
		return $resultado;
	}
	
	function remove($tabela,$onde)
	{
		$this->db->where($onde);
		$res = $this->db->delete($tabela);
		return $res;
	}
	
    function relacao_remover($obj1,$id1='',$obj2,$id2='',$perm="",$content="")
    {
        $res_temp = false;
            
        // Remover a relacao
        if(empty($id2) && !empty($id1))
            $lst2 = $this->relacao($obj1,$id1,$obj2);
        elseif (!empty($id2) && empty($id1))
            $lst2 = $this->relacao($obj1,'',$obj2,$id2);
        else
            $lst2 = $this->relacao($obj1,$id1,$obj2,$id2);
        
        if(!empty($lst2))
        foreach($lst2 as $item)
        {
            $this->db->where('idobj1',(empty($id1) ? $item : $id1));
            $this->db->where('type',$obj1.'___'.$obj2);
            $this->db->where('idobj2',(empty($id2) ? $item : $id2));
            if(!empty($perm)) $this->db->where('permission',$perm);
            if(!empty($content)) $this->db->where('content',$content);
            $res_temp = $this->db->delete('relationship');
        }
        return $res_temp;
    }
    
	function remover($tabela, $id, $extra="")
	{
		switch($tabela)
		{
			case 'users' :
				
                // IDs que não podem ser removidos
                if($id == 99 || $id == 1) return false;
                
				// Remover a relacao users vs qualquer coisa
				$lst2 = $this->relacao('users',$id,'groups');
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$id);
					$this->db->where('type','users___groups');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
					$grupo_atual = $item;
				}

				unset($lst2);
				$lst2 = $this->relacao('users',$id,'services');
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$id);
					$this->db->where('type','users___services');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
				}
                
				unset($lst2);
				$lst2 = $this->relacao('users',$id,'charges');
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$id);
					$this->db->where('type','users___charges');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
				}
				
                // Remove arquivos
                //$this->db->where('owner',$id);
                //$res_temp3 = $this->db->delete('folders');
                //$this->db->where('idusr',$id);
                //$res_temp3 = $this->db->delete('docs');
                
				// Remove o histórico
				$this->db->where('idusu',$id);
				$res_temp3 = $this->db->delete('historic');
                
                // Remove o chat
                //$login = $this->campo('users','username','id = '.$id);
                //$this->db->where('from',$login);
                //$res_temp3 = $this->db->delete('chat');
                //$this->db->where('to',$login);
                //$res_temp3 = $this->db->delete('chat');
                
                // Remove custos
                $this->db->where('iduserfrom',$id);
                $res_temp3 = $this->db->delete('costs');
                $this->db->where('iduserto',$id);
                $res_temp3 = $this->db->delete('costs');
				
                // Remove planilha de custos
                $this->db->where('iduserfrom',$id);
                $res_temp3 = $this->db->delete('financial');
                $this->db->where('iduserto',$id);
                $res_temp3 = $this->db->delete('financial');
                
				// Remover o userdata
                $this->db->where('iduser',$id);
                $res = $this->db->delete('usersdata');
                
				// Remover o proprio
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
			
			case 'groups' :
				
				// Remover o grupo dos usuarios cadastrados
				$lst2 = $this->relacao('users','','groups',$id);
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$item);
					$this->db->where('type','users___groups');
					$this->db->where('idobj2',$id);
					$res_temp = $this->db->delete('relationship');
				}
				// Remover o proprio
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
			
			
				case 'docs' :
				// Elimina as relacoes
				$lst1 = $this->relacao_remover('users','','docs',$id);
				
                                // Elimina o proprio
                                $this->db->where('id',$id);
                                $res = $this->db->delete($tabela);
                                
				break;
                            
                            case 'docsuser' :
				// Elimina as relacoes
				$lst1 = $this->relacao_remover('docsuser',$id,'folder');
				
                                // Elimina o proprio
                                $this->db->where('id',$id);
                                $res = $this->db->delete($tabela);
                                
				break;
				
			
			case 'template' :
				// Verifica e elimina os docs associados ao template
				$lst0 = $this->listagem('docs','date_created','idtpl = '. $id);
				$cods = array();
				if(!empty($lst0))
				foreach($lst0 as $item)
				{
					// Cod. dos documentos
					$cods[] = $item->id;
					// Elimina o registro
					$this->db->where('id',$item->id);
					$res_temp2 = $this->db->delete('docs');
				}
				if(!empty($cods))
				foreach($cods as $iddoc)
				{
					// Elimina as relacoes de docs com os folders
					$lst1 = array();
					$lst1 = $this->relacao('docs',$iddoc,'folders');
					if(!empty($lst1))
					foreach($lst1 as $item)
					{
						$this->db->where('idobj1',$iddoc);
						$this->db->where('type','docs___folders');
						$this->db->where('idobj2',$item);
						$res_temp = $this->db->delete('relationship');
					}
				}

				// Elimina o proprio
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
			
			case 'folders' :
				// Elimina os docs
				//$real_id = $this->campo('folders','id','nameid="'.$id.'"');
				$real_id = $id;
				$lst1 = $this->relacao('docs','','folders',$real_id);
				if(!empty($lst1))
				foreach($lst1 as $item)
				{
					$this->db->where('idobj1',$item);
					$this->db->where('type','docs___folders');
					$this->db->where('idobj2',$real_id);
					$res_temp = $this->db->delete('relationship');
					$this->db->where('id',$item);
					$res = $this->db->delete('docs');
				}
				// Elimina as relacoes
				$lst2 = $this->relacao('folders',$real_id,'groups');
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$real_id);
					$this->db->where('type','users___groups');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
				}
				// Elimina o proprio
				$this->db->where('id',$real_id);
				$res = $this->db->delete($tabela);
				break;
			
			case 'messages' :
				// Elimina as relacoes (solicitacoes)
				$lst1 = $this->relacao('messages',$id,'folders');
				if(!empty($lst1))
				foreach($lst1 as $item)
				{
					$this->db->where('idobj1',$id);
					$this->db->where('type','messages___folders');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
				}
				// Elimina as relacoes (comentarios)
				$lst2 = $this->relacao('messages',$id,'docs');
				if(!empty($lst2))
				foreach($lst2 as $item)
				{
					$this->db->where('idobj1',$id);
					$this->db->where('type','messages___docs');
					$this->db->where('idobj2',$item);
					$res_temp = $this->db->delete('relationship');
				}
				// Elimina o proprio
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
			
			case 'historic' :
				// Elimina o historico de um usuario em especifico
				$this->db->where('idusu',$id);
				$res = $this->db->delete($tabela);
				break;
			
			case 'historic_full' :
				// Elimina todo o historico
				$res = $this->db->truncate('historic');
				break;
			
			case 'historic_clean' :
				// $id é o numero de meses
				if ($id == 0) return false;
				$this->db->where('historic.date <= DATE_SUB(CURDATE(),INTERVAL '.$id.' MONTH)');
				$res = $this->db->delete('historic');
				break;
                
            /* ********** NOVO *********** */
            
            case 'services' :
                
                // 1. Remove as relacoes
                $lst2 = $this->relacao('users','','services',$id);
                if(!empty($lst2))
                foreach($lst2 as $item)
                {
                    $this->db->where('idobj1',$item);
                    $this->db->where('type','users___services');
                    $this->db->where('idobj2',$id);
                    $res_temp = $this->db->delete('relationship');
                }
                unset($lst2);
                
                $lst2 = $this->relacao('services',$id,'products');
                if(!empty($lst2))
                foreach($lst2 as $item)
                {
                    $this->db->where('idobj1',$id);
                    $this->db->where('type','services___products');
                    $this->db->where('idobj2',$item);
                    $res_temp = $this->db->delete('relationship');
                }
                unset($lst2);
                
                //  2. Remove o histórico para este serviço
                $this->db->where('module','services');
                $this->db->where('action','service ('.$id.')%');
                $res_temp = $this->db->delete('historic');
                
                // 3. Remove os documentos do serviço
                $this->db->where('idserv',$id);
                $res_temp = $this->db->delete('docs');
                
                // 4. Remove os custos relacionados
                $this->db->where('idserviceto',$id);
                $res_temp = $this->db->delete('costs');
                
                // 5. Remove a parte do financeiro
                $this->db->where('idserviceto',$id);
                $res_temp = $this->db->delete('financial');
                
                $this->db->where('idservicefrom',$id);
                $res_temp = $this->db->delete('financial');
                
                // REMOVE PRODUTO!
                // 6.1. Para cada subtype...
                $query = $this->db->get('entity_subtypes');
                $num_linhas = $query->num_rows();
                $res = $query->result();
                foreach($res as $item1)
                {
                    // 6.2. Pega a entidade...
                    $nome_entidade = $item1->subtype;
                    $this->db->where('subtype',$item1->id);
                    $query2 = $this->db->get('entities');
                    $res2 = $query2->result();
                    foreach($res2 as $item2)
                    {
                        // 6.3. Deleta o metadata correspondente...
                        $lis_ent = $this->resultado_metadata($nome_entidade,'',true);
                        // Cada entidade possui uma quantidade de registros que precisam ser mantidos!
                        switch($nome_entidade)
                        {
                               case 'pee_receitas' : $qtde = 3; break;    
                               case 'pee_despesas' : $qtde = 3; break;     
                               case 'aee_plan_receitas' : $qtde = 6; break;
                               case 'aee_plan_despesas' : $qtde = 6; break;
                               case 'aee' : $qtde = 4; break;              
                               case 'aee_plan_mensal' : $qtde = 4; break;  
                               case 'pn_passivos' : $qtde = 4; break;      
                               case 'pn_ativos' : $qtde = 5; break;        
                               case 'pn_aval' : $qtde = 4; break;          
                               case 'pee_plan_receitas' : $qtde = 6; break;
                               case 'pee_plan_despesas' : $qtde = 6; break;
                               case 'pee' : $qtde = 4; break;              
                               case 'pee_plan_mensal' : $qtde = 4; break;  
                               case 'pn_investimento' : $qtde = 7; break;  
                               case 'pn_folha' : $qtde = 6; break;         
                               case 'pn_fixos' : $qtde = 4; break;         
                               case 'pn_equilibrio' : $qtde = 4; break;    
                               case 'pn_fluxo' : $qtde = 5; break;         
                               case 'aee_relatorio' : $qtde = 4; break;    
                               case 'pee_relatorio' : $qtde = 4; break;    
                               case 'polls' : $qtde = 7; break;            
                               case 'pollsitens' : $qtde = 8; break;       
                               case 'pollsresps' : $qtde = 5; break;       
                               case 'avulsos' : $qtde = 6; break;          
                               default: $qtde = 6; break;
                        }
                        if(!empty($lis_ent))
                        {
                            foreach($lis_ent as $item_ent)
                            {
                                if(!empty($item_ent['idservico']) && $item_ent['idservico'] == $id) :
                                      $a = 1;
                                      foreach($item_ent['id'] as $val3)
                                      {
                                           if($a > $qtde) : // TESTE: Deixa apenas os registros basicos para evitar erros ao criar novos
                                               $this->db->where('id',$val3);
                                               $res = $this->db->delete('metadata');
                                           endif;
                                           $a++;
                                      }
                                endif;
                                
                                if(!empty($item_ent['id_servico']) && $item_ent['id_servico'] == $id) :
                                      $a = 1;
                                      foreach($item_ent['id'] as $val3)
                                      {
                                           if($a > $qtde) :
                                               $this->db->where('id',$val3);
                                               $res = $this->db->delete('metadata');
                                           endif;
                                           $a++;
                                      }
                                endif;
                                
                                if(!empty($item_ent['idservice']) && $item_ent['idservice'] == $id) :
                                      $a = 1;
                                      foreach($item_ent['id'] as $val3)
                                      {
                                           if($a > 0) :
                                               $this->db->where('id',$val3);
                                               $res = $this->db->delete('metadata');
                                           endif;
                                           $a++;
                                      }
                                endif;
                                
                                if(!empty($item_ent['id_service']) && $item_ent['id_service'] == $id) :
                                      $a = 1;
                                      foreach($item_ent['id'] as $val3)
                                      {
                                           if($a > 0) :
                                               $this->db->where('id',$val3);
                                               $res = $this->db->delete('metadata');
                                           endif;
                                           $a++;
                                      }
                                endif;
                            }
                        }
                    }
                }

                //6.4. Faz manutenção da tabela metastrings
                $this->manutencao();
                
                //6.5. Deleta entradas nas tabelas jos_
                $this->db->where('userId',$id);
                $query = $this->db->get('jos_flux_pgc');
                if($query->num_rows() > 0)
                {
                    $this->db->where('userId',$id);
                    $res = $this->db->delete('jos_flux_pgc');
                }
                
                $this->db->where('userid',$id);
                $query = $this->db->get('jos_flux_entries');
                if($query->num_rows() > 0)
                {
                    $this->db->where('userid',$id);
                    $res = $this->db->delete('jos_flux_entries');
                }
                
                // 7. Remove o serviço propriamente dito
                $this->db->where('id',$id);
                $res = $this->db->delete('services');
                
                break;
			
			default:
				$this->db->where('id',$id);
				$res = $this->db->delete($tabela);
				break;
		}
		return $res;
	}
    
    function manutencao()
    {
        $query3 = $this->db->get('metastrings');
        $res3 = $query3->result();
        if($query3->num_rows() > 0)
        foreach($res3 as $item3)
        {
            $ok1 = false;
            $ok2 = false;
            $this->db->where('name_id',$item3->id);
            $query1 = $this->db->get('metadata');
            $num_linhas = $query1->num_rows();
            
            if($num_linhas == 0)
            {
                if(!empty($item3->string))
                    $ok1 = true;
                
            }
            
            $this->db->where('value_id',$item3->id);
            $query2 = $this->db->get('metadata');
            $num_linhas = $query2->num_rows();
            
            if($num_linhas == 0)
            {
                if(!empty($item3->string))
                    $ok2 = true;
                
            }
            
            if($ok1 && $ok2)
            {
                $this->db->where('id',$item3->id);
                $res = $this->db->delete('metastrings');
            }
        }
        return;
    }
    
	/*
	 * 
	 * FIM DAS Funções basicas
	 * 
	 */
	 
	function resultado_metadata($entidade,$onde="",$comid="",$ordem="")
    {
        // Lista os valores e atributos de uma entidade, valores por linha
         
        $this->db->select('metadata.*, attr.string AS campo, vlr.string AS valor, entity_subtypes.subtype AS subtype');
        $this->db->from('metadata');
        $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
        $this->db->join('metastrings AS vlr','vlr.id = metadata.value_id');
        $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
        $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
        $this->db->where('entity_subtypes.subtype = "'.$entidade.'"');
        $this->db->where('metadata.enabled = "yes"');
        $this->db->order_by('metadata.id');
        
        if(!empty($onde))
            $this->db->where($onde);
        
        
        if(!empty($ordem))
            $this->db->order_by($ordem);
        
        $query = $this->db->get();
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        foreach($res as $item)
        {
            $resultado[$item->campo][] = array('id' => $item->id, 'value' => $item->valor, 'enabled' => $item->enabled);
        }
        
        $elemlist = $this->campos_metadata($entidade);
        if (empty($elemlist)) return array();

        //echo current($elemlist);
        //print_r($elemlist);
        //return;
        
        $listagem_total = (empty($resultado) ? 0 : count($resultado[current($elemlist)]));
        if($listagem_total == 0) return array();
        
        for($i=0;$i<$listagem_total;$i++)
        {
            foreach($elemlist as $item)
            {
                if(empty($resultado[$item][$i])) break;
                if(!empty($comid) && !is_bool($comid))
                {
                    $linhas[$i][$item] = $resultado[$item][$i]['id'];
                }
                else
                    {
                        if(!empty($comid))
                            $linhas[$i]['id'][$item] = $resultado[$item][$i]['id'];
                        $linhas[$i][$item] = $resultado[$item][$i]['value'];
                    }
            }
        }
        return $linhas;
    }
    
    function campos_metadata($entidade,$cond="",$ordem="metadata.id",$distinct=true)
    {
        // Retorna os atributos de uma determinada entidade
        if($distinct)
        {
            $sql = '
            SELECT DISTINCT attr.string AS campo FROM (`metadata`) 
            JOIN `metastrings` AS attr ON `attr`.`id` = `metadata`.`name_id` 
            JOIN `entities` AS ent ON `ent`.`guid` = `metadata`.`entity_guid` 
            LEFT JOIN `entity_subtypes` ON `ent`.`subtype` = `entity_subtypes`.`id` 
            WHERE `entity_subtypes`.`subtype` LIKE "'.$entidade.'" 
            AND `metadata`.`enabled` = "yes"
            ';
            
            if(!empty($cond))
                $sql .= ' AND ' . $cond;
            if(!empty($ordem))
                $sql .= ' ORDER BY ' . $ordem;
            
            $query = $this->db->query($sql);
        }
        else
        {
            $this->db->select('attr.string AS campo, metadata.id AS id');
            $this->db->from('metadata');
            $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
            $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
            $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
            $this->db->where('entity_subtypes.subtype like "'.$entidade.'"');
            $this->db->where('metadata.enabled = "yes"');
            
            if(!empty($cond))
                $this->db->where($cond);
            
            if(!empty($ordem))
                $this->db->order_by($ordem);
            
            $query = $this->db->get();
        }
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        $resultado = array();
        foreach($res as $item)
        {
            if($distinct)
            $resultado[] = $item->campo;
            else
            $resultado[$item->id] = $item->campo;
        }
        return $resultado;
    }
}
?>