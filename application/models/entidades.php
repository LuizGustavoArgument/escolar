<?php
class Entidades extends CI_Model {
	
	public $org = array();
	
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->org = $this->session->userdata('orgdata');
		$this->org = $this->org[0];
    }
	
    /* FUNCOES BASICAS */
    
	function dados($tabela, $dados, $retorno="")
	{
		$query = $this->db->get_where($tabela,$dados);
		if(empty($retorno))
		  return $query->result();
        else
        {
            $resultado = '';
            $num_linhas = $query->num_rows();
            if($num_linhas == 0)
            {
                return $resultado;
            }
            else
                {
                    foreach ($query->result_array() as $row)
                    {
                        if ($num_linhas > 1)
                            $resultado[] = $row[$retorno];
                        else
                            $resultado = $row[$retorno];
                    }
                }
        }
        return $resultado;
	}
    
    function insere($tabela, $lista)
    {
        $this->db->set($lista);
        $this->db->insert($tabela);
        $resultado = $this->db->insert_id();
        return $resultado;
    }
    
    function insere_metadata($lista)
    {
        $tabela = 'metadata';
        $query = $this->db->get_where($tabela,$lista);

        if($query->num_rows() == 0)
        {
            $lista['time_created'] = time(); // unix timestamp da data de criação
            $this->db->set($lista);
            $this->db->insert($tabela);
            $resultado = $this->db->insert_id();
        }
        else
            {
                foreach ($query->result() as $row)
                {
                    $resultado = $row->id;
                }
            }
        return $resultado;
    }
    
    function insere_metastring($lista='')
    {
        //if(empty($lista)) $lista = '';
        $tabela = 'metastrings';
        $query = $this->db->get_where($tabela,array('string'=>$lista));

        if($query->num_rows() == 0)
        {
            $this->db->set(array('string' => $lista));
            $this->db->insert($tabela);
            $resultado = $this->db->insert_id();
        }
        else
            {
                foreach ($query->result() as $row)
                {
                    $resultado = $row->id;
                }
            }
        return $resultado;
    }
    
    function insere_entidade($lista)
    {
        $tabela = 'entities';
        $query = $this->db->get_where($tabela,$lista);

        if($query->num_rows() == 0)
        {
            $lista['time_created'] = $lista['time_updated'] = $lista['last_action'] = time(); // unix timestamp da data de criação
            $this->db->set($lista);
            $this->db->insert($tabela);
            $resultado = $this->db->insert_id();
        }
        else
            {
                foreach ($query->result() as $row)
                {
                    $resultado = $row->guid;
                }
            }
        return $resultado;
    }
    
    function insere_subtype($lista)
    {
        $tabela = 'entity_subtypes';
        $query = $this->db->get_where($tabela,array('type' => 'object', 'subtype' => $lista));

        if($query->num_rows() == 0)
        {
            $this->db->set(array('type' => 'object', 'subtype' => $lista));
            $this->db->insert($tabela);
            $resultado = $this->db->insert_id();
        }
        else
            {
                foreach ($query->result() as $row)
                {
                    $resultado = $row->id;
                }
            }
        return $resultado;
    }
    
    function atualiza($tabela, $lista, $campo="guid")
    {
        foreach($lista as $dd => $label) {
            if ($dd != $campo)
                $data[$dd] = $label;
        }
        
        $this->db->where($campo,$lista[$campo]);
        $resultado = $this->db->update($tabela,$data);
        
        if ($resultado)
            $resultado = $lista[$campo];
        else
            $resultado = 0;
            
        return $resultado;
    }
    
    function remove($tabela, $lista, $campo="guid")
    {
        foreach($lista as $dd => $label) {
            if ($dd != $campo)
                $data[$dd] = $label;
        }
        
        $this->db->where($campo,$lista[$campo]);
        $resultado = $this->db->delete($tabela);
        
        if ($resultado)
            $resultado = $lista[$campo];
        else
            $resultado = 0;
            
        return $resultado;
    }
    
    /* FUNCOES PARA AS ENTIDADES */
	
    // Cria uma nova entidade com seus atributos e valores
	function nova_entidade($nome,$campos,$idserv,$idowner,$idrule=1)
    {
        // Cria o subtype
        $subtipo = $this->insere_subtype($nome);
            
        $lista = array(
            /*'guid' => '',*/ // ID universal de identificacao da entidade (auto increment)
            'type' => 'object', // tipo (por padrão, é sempre object)
            'subtype' => $subtipo, // subtipo
            'owner_guid' => 1, // quem criou (id do usuario) CORREÇÃO: sempre será 1 = admin!
            'site_guid' => $this->org['id'], // qual org ou site pertence (padrão é 1)
            'container_guid' => $idserv, // pertence a o quê (costuma ser o ID do servico)
            'access_id' => $idrule, // quem pode ver isto (id do nivel de acesso, de 5 => admin até 1 => qualquer um logado)
            'enabled' => 'yes' // padrao é "yes" - ativa ou desativa esta entidade
        );
        
        // Cria a entidade
        $id_entity = $this->insere_entidade($lista);

        if($id_entity > 0)
        {
            // Cria os campos desta entidade (metadados)
            foreach($campos as $campo => $valor)
            {
                // Cria o metastring para cada um
                $id_name = $this->insere_metastring($campo);
                $id_value = $this->insere_metastring($valor);
                
                $value_type = "text";
                if(is_numeric($valor)) $value_type = "integer";
                
                // Cria a entrada no metadata
                $meta = array(
                    'entity_guid' => $id_entity,
                    'name_id' => $id_name,
                    'value_id' => $id_value,
                    'value_type' => $value_type,
                    'owner_guid' => $idowner,
                    'access_id' => $idrule,
                    'time_created' => time(),
                    'enabled' => 'yes'
                );
                
                $id_metadata[$campo] = $this->insere('metadata',$meta);
            }
        }

        return $id_entity;
    }

    function campos_entidade($entidade,$campo="guid",$cond="")
    {
        $tabela = 'entities';
        $this->db->select($campo);
        $this->db->from($tabela);
        $this->db->join('entity_subtypes','entities.subtype = entity_subtypes.id', 'LEFT');
        $this->db->where('entity_subtypes.subtype like "'.$entidade.'"');
        $this->db->where('entities.enabled = "yes"');
        
        if(!empty($cond))
            $this->db->where($cond);
        
        $query = $this->db->get();
        
        if($query->num_rows() == 0)
        {
            $resultado = '';
        }
        else
            {
                foreach ($query->result_array() as $row)
                {
                    if($query->num_rows() > 1)
                    $resultado[] = $row[$campo];
                    else
                    $resultado = $row[$campo];
                }
            }
        return $resultado;
    }
    
    function campos_metadata($entidade,$cond="",$ordem="metadata.id",$distinct=true)
    {
        // Retorna os atributos de uma determinada entidade
        if($distinct)
        {
            $sql = '
            SELECT DISTINCT attr.string AS campo FROM (`metadata`) 
            JOIN `metastrings` AS attr ON `attr`.`id` = `metadata`.`name_id` 
            JOIN `entities` AS ent ON `ent`.`guid` = `metadata`.`entity_guid` 
            LEFT JOIN `entity_subtypes` ON `ent`.`subtype` = `entity_subtypes`.`id` 
            WHERE `entity_subtypes`.`subtype` LIKE "'.$entidade.'" 
            AND `metadata`.`enabled` = "yes"
            ';
            
            if(!empty($cond))
                $sql .= ' AND ' . $cond;
            if(!empty($ordem))
                $sql .= ' ORDER BY ' . $ordem;
            
            $query = $this->db->query($sql);
        }
        else
        {
            $this->db->select('attr.string AS campo, metadata.id AS id');
            $this->db->from('metadata');
            $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
            $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
            $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
            $this->db->where('entity_subtypes.subtype like "'.$entidade.'"');
            $this->db->where('metadata.enabled = "yes"');
            
            if(!empty($cond))
                $this->db->where($cond);
            
            if(!empty($ordem))
                $this->db->order_by($ordem);
            
            $query = $this->db->get();
        }
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        $resultado = array();
        foreach($res as $item)
        {
            if($distinct)
            $resultado[] = $item->campo;
            else
            $resultado[$item->id] = $item->campo;
        }
        return $resultado;
    }
    
    function valores_metadata($entidade,$campo,$cond="",$ordem="metadata.id",$distinct=false)
    {
        // Retorna os valores de um determinado atributo
        if($distinct)
        {
            $sql = 'SELECT DISTINCT `vlr`.`string` AS valor, metadata.id AS id 
            FROM (`metadata`) 
            JOIN `metastrings` AS attr ON `attr`.`id` = `metadata`.`name_id` 
            JOIN `metastrings` AS vlr ON `vlr`.`id` = `metadata`.`value_id` 
            JOIN `entities` AS ent ON `ent`.`guid` = `metadata`.`entity_guid` 
            LEFT JOIN `entity_subtypes` ON `ent`.`subtype` = `entity_subtypes`.`id` 
            WHERE `attr`.`string` like "'.$campo.'" 
            AND `entity_subtypes`.`subtype` like "'.$entidade.'" 
            AND `metadata`.`enabled` = "yes"';
            
            if(!empty($cond))
                $sql .= ' AND ' . $cond;
            if(!empty($ordem))
                $sql .= ' ORDER BY ' . $ordem;
            
            $query = $this->db->query($sql);
        }
        else 
        {
            $this->db->select('metadata.id AS id, vlr.string AS valor');
            $this->db->from('metadata');
            $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
            $this->db->join('metastrings AS vlr','vlr.id = metadata.value_id');
            $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
            $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
            $this->db->where('attr.string like "'.$campo.'"');
            $this->db->where('entity_subtypes.subtype like "'.$entidade.'"');
            $this->db->where('metadata.enabled = "yes"');
            if(!empty($cond))
                $this->db->where($cond);
            if(!empty($ordem))
                $this->db->order_by($ordem);
            
            $query = $this->db->get();
        }
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        foreach($res as $item)
            $resultado[$item->id] = $item->valor;
        
        return $resultado;
    }
    
    function listagem_metadata($entidade,$onde="",$ordem="")
    {
        // Lista os valores e atributos de uma entidade
         
        $this->db->select('metadata.*, attr.string AS campo, vlr.string AS valor, entity_subtypes.subtype AS subtype');
        $this->db->from('metadata');
        $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
        $this->db->join('metastrings AS vlr','vlr.id = metadata.value_id');
        $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
        $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
        $this->db->where('entity_subtypes.subtype like "'.$entidade.'"');
        $this->db->where('metadata.enabled = "yes"');
        $this->db->order_by('metadata.id');
        
        if(!empty($onde))
            $this->db->where($onde);
        
        
        if(!empty($ordem))
            $this->db->order_by($ordem);
        
        $query = $this->db->get();
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        foreach($res as $item)
        {
            $resultado[$item->campo][] = array('id' => $item->id, 'value' => $item->valor, 'enabled' => $item->enabled);
        }
        
        //return count($resultado);
        
        /*if(!empty($ondevalor))
        {
            for($i=0;$i<count($resultado);$i++)
            {
                if($resultado[$item->campo][$i]['value'] == $ondevalor)
                {
                    $linhas[] = $i;
                }
            }
        }*/

        return $resultado;
    }

    function resultado_metadata($entidade,$onde="",$comid="",$ordem="")
    {
        // Lista os valores e atributos de uma entidade, valores por linha
         
        $this->db->select('metadata.*, attr.string AS campo, vlr.string AS valor, entity_subtypes.subtype AS subtype');
        $this->db->from('metadata');
        $this->db->join('metastrings AS attr','attr.id = metadata.name_id');
        $this->db->join('metastrings AS vlr','vlr.id = metadata.value_id');
        $this->db->join('entities AS ent','ent.guid = metadata.entity_guid');
        $this->db->join('entity_subtypes','ent.subtype = entity_subtypes.id', 'LEFT');
        $this->db->where('entity_subtypes.subtype = "'.$entidade.'"');
        $this->db->where('metadata.enabled = "yes"');
        $this->db->order_by('metadata.id');
        
        if(!empty($onde))
            $this->db->where($onde);
        
        
        if(!empty($ordem))
            $this->db->order_by($ordem);
        
        $query = $this->db->get();
        
        $num = $query->num_rows();
        
        if($num == 0) return array();
        
        $res = $query->result();
        
        foreach($res as $item)
        {
            $resultado[$item->campo][] = array('id' => $item->id, 'value' => $item->valor, 'enabled' => $item->enabled);
        }
        
        $elemlist = $this->campos_metadata($entidade);
        if (empty($elemlist)) return array();

        //echo current($elemlist);
        //print_r($elemlist);
        //return;
        
        $listagem_total = (empty($resultado) ? 0 : count($resultado[current($elemlist)]));
        if($listagem_total == 0) return array();
        
        for($i=0;$i<$listagem_total;$i++)
        {
            foreach($elemlist as $item)
            {
                if(empty($resultado[$item][$i])) break;
                if(!empty($comid) && !is_bool($comid))
                {
                    $linhas[$i][$item] = $resultado[$item][$i]['id'];
                }
                else
                    {
                        if(!empty($comid))
                            $linhas[$i]['id'][$item] = $resultado[$item][$i]['id'];
                        $linhas[$i][$item] = $resultado[$item][$i]['value'];
                    }
            }
        }
        return $linhas;
    }

    function atualiza_metadata($entidade,$campos=array(),$atualizar=array(),$idowner,$idrule=1)
    {
        /** Insere, atualiza ou remove uma determinada lista de dados de uma entidade
         * 
         * @access  public
         * @param   string  O nome da entidade
         * @param   array   Os atributos da entidade e seus valores
         * @param   array   Os campos e os ids de cada entrada na tabela metadata (em caso de atualização)
         * @return  array   Retorna os ids de cada entrada na tabela metadata afetada
         *
        */

        $atr = $this->campos_metadata($entidade,'','',true);
        $ids = array();
        $valores = array();
        foreach($atr as $item)
        {
            // Pega apenas os que pertencem aos atributos da entidade
            $valores[$item] = $campos[$item];
            if(!empty($atualizar[$item]))
                $ids[$item] = $atualizar[$item];
        }
        // Se for remoção, desativa os campos
        $ativo = 'yes';
        if(!empty($campos['remover']) && $campos['remover'] == 1)
        {
            $ativo = 'no';
        }
        
        
        $id_entity = $this->campos_entidade($entidade); // busca o guid da entidade
        $id_metadata = array();
        
        if($id_entity > 0)
        {
            // Atualiza ou cria os campos desta entidade (metadados)
            foreach($valores as $campo => $valor)
            {
                // Cria o metastring para cada um
                $id_name = $this->insere_metastring($campo);
                $id_value = $this->insere_metastring($valor);
                
                $value_type = "text";
                if(is_numeric($valor)) $value_type = "integer";
                
                // Cria a entrada no metadata
                $meta = array(
                    'entity_guid' => $id_entity,
                    'name_id' => $id_name,
                    'value_id' => $id_value,
                    'value_type' => $value_type,
                    'owner_guid' => $idowner,
                    'access_id' => $idrule,
                    'time_created' => time(),
                    'enabled' => $ativo
                );
                
                if(empty($ids))
                {
                    //Elemento novo
                    $id_metadata[$campo] = $this->insere('metadata',$meta);
                }
                else
                    {
                        //Apenas se possui id
                        if(!empty($ids[$campo]))
                        {
                            $meta['id'] = $ids[$campo];
                            $id_metadata[$campo] = $this->atualiza('metadata',$meta,'id');
                        }
                    }
            }
        }
        return $id_metadata;
    }

    /* Para o PCR */

    function getCat($prod=1)
    {
        $query = $this->db->query("SELECT `id`,`catId`,`type`,`catCode`,`catName`,`macro` FROM `jos_flux_pgc` WHERE `enab` = ".$prod." 
        GROUP BY `catId` ORDER BY `type` DESC, `catId`");
        $res = $query->result_array();
        return $res;
    }
    
    function getAct($user,$prod=1)
    {
        $query = $this->db->query("SELECT `e`.`id`,`e`.`pgcId`,`e`.`type`,`p`.`catId`,`p`.`name`,`p`.`code` FROM `jos_flux_entries` AS `e` 
        INNER JOIN `jos_flux_pgc` AS `p` ON `e`.`pgcId` = `p`.`id` 
        WHERE `e`.`userid` = ".$user." AND `e`.`status` = 2 AND (`p`.`userId` = 55 OR `p`.`userId` = ".$user.") AND `e`.`trans` = ".($prod == 1 ? 0 : $prod)."
        GROUP BY `e`.`pgcId` 
        ORDER BY `e`.`type` DESC, `p`.`name`");
        $res = $query->result_array();
        return $res;
    }
    
    function getEnt($user,$prod=1)
    {
        $query = $this->db->query("SELECT `id`,`type`,`pgcId`,`value`,`proj_day` FROM `jos_flux_entries` WHERE `userid` = ".$user."
         AND `status` = 2 
         AND `trans` = ".($prod == 1 ? 0 : $prod)."
         ORDER BY `proj_day`");
        $res = $query->result_array();
        return $res;
    }
    
    function getDates($user,$prod=1)
    {
        $query = $this->db->query("SELECT `proj_day` FROM `jos_flux_entries` WHERE `status` = 2 AND `userid` = ".$user." AND `trans` = ".($prod == 1 ? 0 : $prod)." GROUP BY `proj_day` ORDER BY `proj_day`");
        $res = array();//$db->loadResultArray();
        foreach($query->result_array() as $row)
        {
            $res[] = $row['proj_day'];
        }
        return $res;
    }
    
    /* Demais funcções adaptadas para o PCR */
    function flux_pgc($grupo)
    {
        $query = $this->db->query("SELECT * FROM `jos_flux_cat` WHERE `catName` LIKE '%".$grupo."%'");
        $res = $query->result_array();
        return $res;
    }
    
    function flux_getpgc($campo,$sigla,$userid,$prod=1)
    {
        $query = $this->db->query("SELECT ".$campo." FROM `jos_flux_pgc` WHERE `code` LIKE '%".$sigla."%' AND `userId` = ".$userid." AND `enab` = ".$prod);
        $tmp = $query->result_array();
        if(empty($tmp))
        {
            $query = $this->db->query("SELECT ".$campo." FROM `jos_flux_pgc` WHERE `code` LIKE '%".$sigla."%' AND `userId` = ".$userid." AND `enab` = 1");
        }
        $res = '';        
        foreach($query->result_array() as $row)
        {
            $res = $row[$campo];
        }
        
        return $res;
    }
    
    function atualiza_pgc($pgc,$remove=false)
    {
        // Busca registro semelhante
        $tabela = 'jos_flux_pgc';
        $idant = $pgc['id'];
        $this->db->where('id',$idant);
        $query = $this->db->get($tabela);
        $retorno = $query->result();
        if(empty($retorno))
        {
            // Insere
            $ret = $this->insere($tabela, $pgc);
        }
        else
        {
            if(!$remove)
            {
                // Atualiza
                $ret = $this->atualiza($tabela, $pgc, 'id');
            }
            else 
            {
                // Remove
                $ret = $this->remove($tabela, $pgc, 'id');
            }
            
        }
        return $ret;
    }
    
    function atualiza_entries($ent,$os_ini,$idservice,$idprod=1)
    {
        $tabela = 'jos_flux_entries';
        
        // Apaga o que estava antes, sem dó
        
        $mes = date('n',$os_ini);
        $query = $this->db->query("DELETE FROM `jos_flux_entries` WHERE 
        MONTH(FROM_UNIXTIME(proj_day))=".$mes." AND `userid` = ".$idservice." AND `trans` = ".($idprod == 1 ? 0 : $idprod));
        //$retorno = $query->result();
        $ret = 0;
        
        if(!empty($ent))
        foreach($ent as $ent_item)
            $ret = $this->insere($tabela, $ent_item);
        
        return $ret;
    }
}
?>