<div class="span-24 miolo">
    <div class="span-5 colborder">
  		<!-- Coluna da esquerda (menu) -->
  		<div class="etapa">
           <h3><?php echo anchor('relatorios/usuario','Por usuário','style="text-decoration:none"'); ?></h3>
          <p>Veja o histórico de ações separados por usuário.</p>
      </div>
      <div class="etapa-ativa">
          <h3><?php echo anchor('relatorios/index','Configurações','style="text-decoration:none"'); ?></h3>
          <p>Ajustes do histórico de ações.</p>
      </div>
  	</div>
  	<div class="span-14 colborder">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <h3>Opções gerais do histórico de ações</h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('relatorios/index'); ?>">
        <table width="100%" border="0">
          <tr>
            <td width="30%">Manter o histórico por:</td>
            <td width="70%"><select name="hist_time" id="hist_time">
              <?php echo $hist_time; ?>
            </select></td>
          </tr>
           <tr>
            <td width="30%">Limpar todo o histórico do usuário:</td>
            <td width="70%"><select name="hist_user" id="hist_user">
              <?php echo $hist_user; ?>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Exportar todo o histórico para:</td>
            <td width="70%"><select name="hist_export" id="hist_export">
              <?php echo $hist_export; ?>
            </select><?php if (!empty($hist_fileexp)) echo '<br>'. $hist_fileexp; ?></td>
          </tr>
          <tr>
            <td width="30%"><input name="remover" type="checkbox" value="1" /> Eliminar todo o histórico</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/blueprint/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="button" class="button negative" onclick="history.back()"><img src="<?php echo base_url();  ?>css/blueprint/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
         </table>
          	</form>
        </div>
  	</div>
  	<div class="span-3 last">
  		<!-- Coluna da direita (auxiliar) -->

  	</div>
</div>
  	