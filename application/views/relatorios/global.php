
<div class="span-24 miolo">
    <div class="span-1 colborder">
  		<!-- Coluna da esquerda (menu) -->
  		&nbsp;
  	</div>
  	<div class="span-9 colborder">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <h3>Estatísticas gerais</h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('relatorios/global'); ?>"><p><?php echo $info_geral; ?></p>
        <table width="100%" border="0">
          <tr>
            <td width="30%">Total de usuários cadastrados:</td>
            <td width="70%">
              <?php echo $total_histusu; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Total de cooperativas cadastradas:</td>
            <td width="70%">
              <?php echo $total_histusu5; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Total de julgadores cadastradas:</td>
            <td width="70%">
              <?php echo $total_histusu4; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Total de gestores regionais cadastrados:</td>
            <td width="70%">
              <?php echo $total_histusu3; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Total de gestores nacionais cadastradas:</td>
            <td width="70%">
              <?php echo $total_histusu2; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Total de administradores gerais cadastradas:</td>
            <td width="70%">
              <?php echo $total_histusu1; ?>
            </td>
          </tr>
           <tr>
            <td width="30%">Total de perguntas efetuadas:</td>
            <td width="70%">
              <?php echo $total_histperg; ?>
           </td>
          </tr>
           <tr>
            <td width="30%">Total de respostas feitas:</td>
            <td width="70%">
              <?php echo $total_histresp; ?>
           </td>
          </tr>
          <tr>
            <td width="30%">Formulários julgados:</td>
            <td width="70%">
              <?php echo $total_histjudok; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Formulários não-julgados:</td>
            <td width="70%">
              <?php echo $total_histjudnook; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Formulários concluídos:</td>
            <td width="70%">
              <?php echo $total_histpolls; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Cooperativas com participação concluída:</td>
            <td width="70%">
              <?php echo $total_histpollsend; ?>
            </td>
          </tr>

           <tr>
            <td width="30%">Total de acessos ao sistema:</td>
            <td width="70%">
              <?php echo $total_histacessos; ?>
            </td>
          </tr>
            <tr>
            <td width="30%">Total de itens no histórico:</td>
            <td width="70%">
              <?php echo $total_hist; ?>
            </td>
          </tr>
          <tr>
            <td width="30%"></td>
            <td width="70%"><table><tr><td></td><td><button type="button" class="button negative" onclick="history.back()">Voltar</button></td></tr></table></td>
          </tr>
         </table>
          	</form>
        </div>
  	</div>
  	<div class="span-7 last">
  		<!-- Coluna da direita (auxiliar) -->
      <div>
        <h3>Painel de Vencedores</h3>
        <hr />
        <form id="form_vence" name="form_vence" method="post" enctype="multipart/form-data" action="<?php echo site_url('relatorios/geral_resultado/true'); ?>">
        <table width="100%" border="0">
          <tr>
            <td colspan="3"> Cat.:
            <select name="filtro1" id="filtro1">
              <?php echo $filtro1_categorias; ?>
            </select>
            </td>
          </tr>
          <tr>
            <td colspan="3"> &nbsp;&nbsp;UF:
            <select name="filtro2" id="filtro2">
              <?php echo $filtro2_ufs; ?>
            </select>
            <button type="button" class="button positive" onclick="geraresultados()" style="float:right">Filtrar</button>
            </td>
          </tr>
        </table>
      </form>
          <div id="form_vence_res">
          <?php echo $resultado; ?>
          </div>
      </div>

  	</div>
</div>	
  	