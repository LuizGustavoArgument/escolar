<div class="span-24 miolo">
    <div class="span-5 colborder">
  		<!-- Coluna da esquerda (menu) -->
  		<div class="etapa-ativa">
          <h3><?php echo anchor('relatorios/usuario','Por usuário','style="text-decoration:none"'); ?></h3>
          <p>Veja o histórico de ações separados por usuário.</p>
      </div>
      <div class="etapa">
          <h3><?php echo anchor('relatorios/index','Configurações','style="text-decoration:none"'); ?></h3>
          <p>Ajustes do histórico de ações.</p>
      </div>
  	</div>
  	<div class="span-14 colborder">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <h3>Relatório por usuário</h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('relatorios/usuario'); ?>">
        <table width="100%" border="0">
           <tr>
            <td width="30%">Selecione o usuário:</td>
            <td width="70%"><select name="hist_user" id="hist_user">
              <?php echo $hist_user; ?>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Selecione o tipo de ação:</td>
            <td width="70%"><select name="hist_tipo" id="hist_tipo">
              <?php echo $hist_tipo; ?>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Data inicial:</td>
            <td width="70%"><input type="text" name="datainicial" id="datainicial" value="<?php echo $datainicial; ?>" size="10"/></td>
          </tr>
           <tr>
            <td width="30%">Data final:</td>
            <td width="70%"><input type="text" name="datafinal" id="datafinal" value="<?php echo $datafinal; ?>" size="10"/></td>
          </tr>
          <tr>
            <td colspan="2">
  <?php if ($_POST) : ?>
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
    <td>Módulo</td>
    <td>Usuário</td>
    <td>Ação</td>
    <td>Data</td>
    <td>IP</td>
    </tr>
  </thead>
  <tbody>
  <?php $cont = 0; if (!empty($resultado_query)) { ?>
  <?php foreach($resultado_query as $item) : ?>
  <tr>
    <td><?php switch($item->module)
			{
				case 'login':
				case 'logout':
					$titulo = '<span style="color:red">Autenticação</span><br>';
					break;
				
        case 'user':
        case 'group':
          $titulo = '<span style="color:tomato">Administração</span><br>';
          break;

				case 'formulario':
					$titulo = '<span style="color:blue">Formulários</span><br>';
					break;
				
				case 'pergunta':
					$titulo = '<span style="color:green">Perguntas</span><br>';
					break;
					
				case 'resposta':
					$titulo = '<span style="color:orange">Respostas</span><br>';
					break;
				
				case 'conclusao':
					$titulo = '<span style="color:tomato">Conclusão</span><br>';
					break;
					
				case 'atribuicao':
					$titulo = '<span style="color:slategrey">Atribuição</span><br>';
					break;

        case 'email':
          $titulo = '<span style="color:purple">E-mail</span><br>';
          break;
				
				default:
					$titulo = '<span style="color:purple">'.$item->module.'</span><br>';
					break;
			} echo $titulo; ?></td>
    <td><?php echo $lista_user[$item->idusu]; ?></td>
    <td><?php echo $item->action; ?></td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date)); ?></td>
    <td><?php echo $item->info; ?></td>
  </tr>
  <?php $cont++; endforeach; ?>
  <?php }  else { echo '<td>Nenhum dado encontrado.</td><td></td><td></td><td></td><td></td>'; } ?>
  </tbody>
  <tfoot>
  <tr><td colspan="5"><p>Total de <?php echo $cont; ?> ocorrências.</p><small>Relatório gerado às <?php echo date('H:i'); ?> do dia <?php echo date('d/m/Y'); ?></small></td></tr>
  </tfoot>
</table>
<?php endif; ?>
          </td></tr>
          <tr>
            <td width="30%"></td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/blueprint/plugins/buttons/icons/tick.png" alt="next"/> Gerar relatório</button></td><td><button type="button" class="button negative" onclick="history.back()"><img src="<?php echo base_url();  ?>css/blueprint/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
         </table>
          	</form>
        </div>
  	</div>
  	<div class="span-3 last">
  		
  	</div>
</div>
  	