{$header}

{if $js != ''}{$js}{/if}
{if $errors != ''}{$errors}{/if}
<section>
{if $barra != ''}{$barra}{/if}
<form name="peerel" id="peerel" action="{$form.baseurl}{$form.action}" method="{$form.method}" enctype="multipart/form-data">

<div class="row-fluid">
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Capa</a></li>
    <li><a href="#tabs-2">1. Metas</a></li>
    <li><a href="#tabs-9">2. Cálculo</a></li>
    <li><a href="#tabs-3">3. Custos</a></li>
    <li><a href="#tabs-4">4. Processos</a></li>
    <li><a href="#tabs-5">5. RH</a></li>
    <li><a href="#tabs-6">6. Marketing</a></li>
    <li><a href="#tabs-7">7. Asp. Legais</a></li>
    <li><a href="#tabs-8">8. Diversos</a></li>
  </ul>
  <div id="tabs-1">
  <div class="col-10">
  <div class="col-3">
  <fieldset style="margin-top:25px">
  {if $foto != ''}
  <p><img src="{$form.baseurl}/uploads/{$foto}" class="img-rounded" style="width:200px"></p>
  {/if}
  </fieldset>
  </div>
  <div class="col-7">
  <fieldset style="margin-top:25px">
  <h3>PLANO ESTRATÉGICO EMPRESARIAL</h3>
  <label>{$form.cliente_label}:</label>{$form.cliente}
  <label>{$form.periodo_label}:</label>{$form.periodo}
  </fieldset>
  </div>
  </div>
  <div class="col-10">
  <fieldset>
  <label>{$form.equipe_label}:</label>{$form.equipe}
  <label>{$form.tabela_prior_label}:</label>{$form.tabela_prior}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-2">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl1_sint_label}:</label>{$form.tbl1_sint}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-9">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl2_sint_label}:</label>{$form.tbl2_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl2_prior1_label}:</label>{$form.tbl2_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl2_prior2_label}:</label>{$form.tbl2_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl2_prior3_label}:</label>{$form.tbl2_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-3">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl3_sint_label}:</label>{$form.tbl3_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl3_prior1_label}:</label>{$form.tbl3_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl3_prior2_label}:</label>{$form.tbl3_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl3_prior3_label}:</label>{$form.tbl3_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-4">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl4_sint_label}:</label>{$form.tbl4_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl4_prior1_label}:</label>{$form.tbl4_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl4_prior2_label}:</label>{$form.tbl4_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl4_prior3_label}:</label>{$form.tbl4_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-5">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl5_sint_label}:</label>{$form.tbl5_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl5_prior1_label}:</label>{$form.tbl5_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl5_prior2_label}:</label>{$form.tbl5_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl5_prior3_label}:</label>{$form.tbl5_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-6">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl6_sint_label}:</label>{$form.tbl6_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl6_prior1_label}:</label>{$form.tbl6_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl6_prior2_label}:</label>{$form.tbl6_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl6_prior3_label}:</label>{$form.tbl6_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-7">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl7_sint_label}:</label>{$form.tbl7_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl7_prior1_label}:</label>{$form.tbl7_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl7_prior2_label}:</label>{$form.tbl7_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl7_prior3_label}:</label>{$form.tbl7_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
  <div id="tabs-8">
  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.tbl8_sint_label}:</label>{$form.tbl8_sint}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl8_prior1_label}:</label>{$form.tbl8_prior1}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl8_prior2_label}:</label>{$form.tbl8_prior2}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  <label>{$form.tbl8_prior3_label}:</label>{$form.tbl8_prior3}
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>
  </div>
  
</div>
</div>

</form>
</section>
{$footer}