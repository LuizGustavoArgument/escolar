<script type="text/javascript">

$(document).ready(function(){
	$("form#submit").submit(function() {
        $.ajax({  
            type: "POST",  
            url: "editor_salvar",  
            data: $('form#submit').serialize(),
            cache: false,  
            success: function(data){  
                $('form#submit').hide();
                $('div#saved').html('<p>Mensagem: '+data+'<p>');
                $('div#saved').fadeIn('slow');
            }  
        });  
    return false;
    });
    <?php if (!empty($tname)) : ?>
    $("select#tname option[selected]").removeAttr("selected");
	$("select#tname option[value='<?php echo $tname; ?>']").attr("selected","selected");
	<?php endif; ?>
});

</script>
<div id="saved" style="color:red; font-size:15px; display:none"></div>
<form name="submit_<?php echo $classe; ?>" id="submit" method="post">
<?php echo form_open('' ,array('name' => 'submit_'.$classe, 'id' => 'submit')); ?>
<strong>Preencha os dados do <?php echo $classe; ?>:</strong><br>
<label for="fname">Nome do campo:</label><input id="fname" class="text" name="fname" size="20" type="text" value="<?php echo $fname; ?>" />
&nbsp;&nbsp;&nbsp;<label for="fname">Tipo de dado:</label>
<!-- Removido outros tipos:  <option value="image">Imagem</option><option value="data">Data</option> 24/08/2011 -->
<select id="tname" class="text" name="tname"><option value="text">Texto</option></select>
<input type="checkbox" name="remover" value="1" id="remover" /> Remover
<input type="hidden" name="w" id="w" value="<?php echo $w; ?>" />
<input type="hidden" name="h" id="h" value="<?php echo $h; ?>" />
<input type="hidden" name="x" id="x" value="<?php echo $x; ?>" />
<input type="hidden" name="y" id="y" value="<?php echo $y; ?>" />
<input type="hidden" name="classe" id="classe" value="<?php echo $classe; ?>" />
<input type="hidden" name="template_name" id="template_name" value="<?php echo $template_name; ?>" />
&nbsp;&nbsp;&nbsp;<input type="submit" value="Salvar" />
</form>

