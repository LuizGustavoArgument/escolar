{$header}

{if $js != ''}{$js}{/if}
{if $errors != ''}{$errors}{/if}
<section class="form_admin">
<form name="produtos" id="produtos" action="{$form.baseurl}{$form.action}" method="{$form.method}" enctype="multipart/form-data">

<div class="row-fluid">
<fieldset>
  <div class="col-2">
  {if isset($form.groups)}
  {$form.groups}<span class="form-inline" style="color:red">{$form.groups_error}</span>
  {/if}
  <br>{$foto}<br>
  <a href="#" onclick="$('#imgaltera').show();$(this).hide();">Alterar imagem...</a>
  <div id="imgaltera" style="display:none" class="col-2">
  {$form.logo}
  </div>
  {if $formsalvar != ''}
  <br><br><p>{$formsalvar}</p>
  {/if}
  </div>
  <div class="col-8">
    
    {if isset($extrahtml)}
    <div class="form-inline">{$extrahtml}</div>
    {/if}
  	<div class="form-inline">
  	<label>{if isset($form.username_label)}{$form.username_label}:&nbsp;&nbsp;{/if}</label>{$form.username}<span class="form-inline" style="color:red">{$form.username_error}</span>
  	</div>

  	<div class="form-inline"><blockquote>
  	{if isset($form.password_label)}
  	    {if isset($form.password_atual) }
  	    <br><label style="margin-right:3px">Senha atual:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>{$form.password_atual}<span class="form-inline" style="color:red">{$form.password_atual_error}</span><br>
	  	{if $ehadmin == 'yes'}<label class="checkbox" for="password_naosei"><input name="password_naosei" id="password_naosei" value="naosei" type="checkbox" onchange="if(this.checked) document.getElementById('password_atual').value='naosei'; else document.getElementById('password_atual').value='';"> Não sei a senha atual!</label><br>{/if}
	  	{else}
	  	<br>
	  	{/if}
	  	<label>Nova senha:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>{$form.password}<span class="form-inline" style="color:red">{$form.password_error}</span>
	  	<br><label style="margin-right:2px;">{$form.password_again_label}:&nbsp;</label>{$form.password_again}<span class="form-inline" style="color:red">{$form.password_again_error}</span>
  	{/if}
  	{if !isset($form.password_label)}
  	{if isset($form.password_atual)}{$form.password_atual}{/if}
  	{$form.password}
  	{$form.password_again}
            {/if}{if $ehadmin && $groupset == 7}<strong>Dados para acesso:</strong><br>Login: <span style="color:red">{$ologin}</span><br>Senha: <span style="color:red">{$asenha}</span> {/if}
  	</blockquote>
  	</div><hr>

  	<label>{$form.name_label}:</label>{$form.name}<span class="form-inline" style="color:red">{$form.name_error}</span>
  	<label>{$form.website_label}:</label>{$form.website}
  	<label>{$form.email_label}:</label>{$form.email}<span class="form-inline" style="color:red">{$form.email_error}</span>
  	<label>{$form.contact_fax_label}:</label>{$form.contact_fax}
  	<label>{$form.phone_label}:</label>{$form.phone}<span class="form-inline" style="color:red">{$form.phone_error}</span>
  	<label>{$form.cnpj_label}:</label>{$form.cnpj}
  	<label>{$form.contact_cpf_label}:</label>{$form.contact_cpf}<span class="form-inline" style="color:red">{$form.contact_cpf_error}</span>
  </div>
</fieldset>
<fieldset>
  <legend>Arquivos</legend>
  <div id="listinha2"></div>
</fieldset>
</div>
<div class="row-fluid">
  <div class="col-10">
  <fieldset>
  <legend>Dados complementares</legend>
  <label>{$form.address_label}:</label>{$form.address}
  <label>{$form.address_num_label}:</label>{$form.address_num}
  <label>{$form.address_cpl_label}:</label>{$form.address_cpl}
  <label>{$form.fax_label}:</label>{$form.fax} <!-- bairro -->
  <label>{$form.mobile_label}:</label>{$form.mobile} <!-- cep -->
  <label>{$form.city_label}:</label>{$form.city}
  <label>{$form.uf_label}:</label>{$form.uf}
  <label>{$form.country_label}:</label>{$form.country}
  <label>{$form.address_ref_label}:</label>{$form.address_ref}
  <label>{$form.outros_label}:</label>{$form.outros}
  
  </fieldset>
  <fieldset>
  <legend>Contatos</legend>
  <ul class="navbar nav-tabs" id="myTab">
    <li><a href="#contato1" data-toggle="tab">Contato 1</a></li>
    <li><a href="#contato2" data-toggle="tab">Contato 2</a></li>
    <li><a href="#contato3" data-toggle="tab">Contato 3</a></li>
    <li><a href="#contato4" data-toggle="tab">Contato 4</a></li>
    <li><a href="#contato5" data-toggle="tab">Contato 5</a></li>
  </ul>
  
  <div class="tab-content">
  <div class="tab-pane" id="contato1">
      <label>{$form.contact_name1_label}:</label>{$form.contact_name1}
      <label>{$form.contact_cargo1_label}:</label>{$form.contact_cargo1}
      <label>{$form.contact_email1_label}:</label>{$form.contact_email1}
      <label>{$form.contact_mobile1_label}:</label>{$form.contact_mobile1}
      <label>{$form.contact_phone1_label}:</label>{$form.contact_phone1}
  </div>
  
  <div class="tab-pane" id="contato2">
      <label>{$form.contact_name2_label}:</label>{$form.contact_name2}
      <label>{$form.contact_cargo2_label}:</label>{$form.contact_cargo2}
      <label>{$form.contact_email2_label}:</label>{$form.contact_email2}
      <label>{$form.contact_mobile2_label}:</label>{$form.contact_mobile2}
      <label>{$form.contact_phone2_label}:</label>{$form.contact_phone2}
  </div>
  
  <div class="tab-pane" id="contato3">
      <label>{$form.contact_name3_label}:</label>{$form.contact_name3}
      <label>{$form.contact_cargo3_label}:</label>{$form.contact_cargo3}
      <label>{$form.contact_email3_label}:</label>{$form.contact_email3}
      <label>{$form.contact_mobile3_label}:</label>{$form.contact_mobile3}
      <label>{$form.contact_phone3_label}:</label>{$form.contact_phone3}
  </div>
  
  <div class="tab-pane" id="contato4">
      <label>{$form.contact_name4_label}:</label>{$form.contact_name4}
      <label>{$form.contact_cargo4_label}:</label>{$form.contact_cargo4}
      <label>{$form.contact_email4_label}:</label>{$form.contact_email4}
      <label>{$form.contact_mobile4_label}:</label>{$form.contact_mobile4}
      <label>{$form.contact_phone4_label}:</label>{$form.contact_phone4}
  </div>
  
  <div class="tab-pane" id="contato5">
      <label>{$form.contact_name5_label}:</label>{$form.contact_name5}
      <label>{$form.contact_cargo5_label}:</label>{$form.contact_cargo5}
      <label>{$form.contact_email5_label}:</label>{$form.contact_email5}
      <label>{$form.contact_mobile5_label}:</label>{$form.contact_mobile5}
      <label>{$form.contact_phone5_label}:</label>{$form.contact_phone5}
  </div>
  </div>
  </fieldset>
  <script>
    $(function () {
    $('#myTab a:first').tab('show');
    $('#website').datepicker({
				format: 'dd/mm/yyyy',
				viewMode: 2
			});
    })
  </script>
  {if $formsalvar != ''}
  <p>{$formsalvar}</p>
  {/if}
  </div>
</div>
{$form.date_created}
{if $form.id != ''}{$form.id}{/if}
{if $form.tipo != ''}{$form.tipo}{/if}
</form>
</section>
{$footer}