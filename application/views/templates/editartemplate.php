<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>GED - Editor de templates</title>
<link rel="stylesheet" href="<?php echo base_url();  ?>js/jqueryui/css/redmond/jquery-ui-1.9.1.custom.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url();  ?>js/jqueryui/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>js/jqueryui/js/jquery-ui-1.9.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">

$(document).ready(function(){
   
   
	// Boxer plugin
	$.widget("ui.boxer", $.ui.mouse, {
	  
	  options: {
	    appendTo: 'body',
	    distance: 0
	  },

	  
	  _init: function() {
		this.element.addClass("ui-boxer");
	
		this.dragged = false;
	
		this._mouseInit();
	
		this.helper = $(document.createElement('div'))
		  .css({border:'1px dotted black'})
		  .addClass("ui-boxer-helper");
	  },
	
	  destroy: function() {
		this.element
		  .removeClass("ui-boxer ui-boxer-disabled")
		  .removeData("boxer")
		  .unbind(".boxer");
		this._mouseDestroy();
	
		return this;
	  },
	
	  _mouseStart: function(event) {
		var self = this;
	
		this.opos = [event.pageX, event.pageY];
	
		if (this.options.disabled)
		  return;
	
		var options = this.options;
	
		this._trigger("start", event);
	
		$(options.appendTo).append(this.helper);
	
		this.helper.css({
		  "z-index": 100,
		  "position": "absolute",
		  "left": event.clientX,
		  "top": event.clientY,
		  "width": 0,
		  "height": 0
		});
	  },
	
	  _mouseDrag: function(event) {
		var self = this;
		this.dragged = true;
	
		if (this.options.disabled)
		  return;
	
		var options = this.options;
	
		var x1 = this.opos[0], y1 = this.opos[1], x2 = event.pageX, y2 = event.pageY;
		if (x1 > x2) { var tmp = x2; x2 = x1; x1 = tmp; }
		if (y1 > y2) { var tmp = y2; y2 = y1; y1 = tmp; }
		this.helper.css({left: x1, top: y1, width: x2-x1, height: y2-y1});
		
		this._trigger("drag", event);
	
		return false;
	  },
	
	  _mouseStop: function(event) {
		var self = this;
	
		this.dragged = false;
	
		var options = this.options;
	
		var clone = this.helper.clone()
		  .removeClass('ui-boxer-helper').appendTo(this.element);
	
		this._trigger("stop", event, { box: clone });
	
		this.helper.remove();
	
		return false;
	  }
	
	});
	
	// Using the boxer plugin
	$('#canvas').boxer({
	  stop: function(event, ui) {
		var offset = ui.box.offset();
		var classe = 'box'+Math.floor(Math.random()*1001);
		ui.box.css({ border: '1px solid white', background: '#006666', color: 'white', padding: '0.5em' })
		  .append('<strong><u>'+classe+'</u></strong><br>')
		  .append('X:' + offset.left + ', Y:' + (offset.top-190)) // correcao de 150 por causa do cabecalho do site
		  .append('<br>')
		  .append('Largura:' + ui.box.width() + ', Altura:' + ui.box.height())
		  //.append('<br><a href="javascript:void(0);" onclick="$(\'.'+classe+'\').remove();$(\'#propriedades\').html(\'\');">remover</a>')
		  .append('<br><a href="javascript:void(0);" onclick="geraOptions(\''+classe+'\',\''+ui.box.width()+'\',\''+ui.box.height()+'\',\''+offset.left+'\',\''+(offset.top-190)+'\')">editar</a>');
		ui.box.addClass(classe);
		// Usando a funcao draggable
		ui.box.mover(classe);
		ui.box.redimensionar(classe);
		}
	});
	
	$.fn.mover = function(myclasse) {
		// Usando a funcao draggable
		$('.'+myclasse).draggable({containment: "#canvas", snap: true, grid: [ 10,10 ], 
			stop: function(event, ui) {
				var newoffset = $(this).position();
				var w = $(this).width();
				var h = $(this).height();
				//alert('x:' + newoffset.left + ', y:' + newoffset.top);
				$(this).html('<strong><u>'+myclasse+'</u></strong><br>')
				.css({ border: '1px solid white', background: '#006666', color: 'white', padding: '0.5em' })
				//.append('x:' + newoffset.left + ', y:' + (newoffset.top-190))  // correcao de 150 por causa do cabecalho do site
				//.append('<br>')
		  		//.append('w:' + w + ', h:' + h)
				//.append('<br><a href="javascript:void(0);" onclick="$(\'.'+classe+'\').remove();$(\'#propriedades\').html(\'\');">remover</a>')
				.append('<br><a href="javascript:void(0);" onclick="geraOptions(\''+myclasse+'\',\''+w+'\',\''+h+'\',\''+newoffset.left+'\',\''+(newoffset.top-190)+'\')">editar</a>');
				$(this).resizable("enable");
			}
		});
		return false;
	}
	
	$.fn.redimensionar = function(myclasse) { 
		// Usando a funcao resizeable
		$('.'+myclasse).resizable({ containment: "#canvas", grid: [ 10,10 ], 
			stop: function(event, ui) {
				var newoffset = $(this).position();
				var w = $(this).width();
				var h = $(this).height();
				//alert('x:' + newoffset.left + ', y:' + newoffset.top);
				$(this).html('<strong><u>'+myclasse+'</u></strong><br>')
				.css({ border: '1px solid white', background: '#006666', color: 'white', padding: '0.5em' })
				//.append('x:' + newoffset.left + ', y:' + (newoffset.top-190))  // correcao de 150 por causa do cabecalho do site
				//.append('<br>')
		  		//.append('w:' + w + ', h:' + h)
				//.append('<br><a href="javascript:void(0);" onclick="$(\'.'+classe+'\').remove();$(\'#propriedades\').html(\'\');">remover</a>')
				.append('<br><a href="javascript:void(0);" onclick="geraOptions(\''+myclasse+'\',\''+w+'\',\''+h+'\',\''+newoffset.left+'\',\''+(newoffset.top-190)+'\')">editar</a>');
			}
		});
		return false;
	}
	
	<?php echo $js_divs; ?>

});

function geraOptions(classe,w,h,x,y) 
{
	var js = '<script type="text/javascript">';
	js += '$(document).ready(function(){ $("form#submit").submit(function(){$.ajax({ type: "POST", url: "editor_salvar", data: $("form#submit").serialize(), cache: false, success: function(data){ $("div#saved").html(data); $("form#submit").hide(function(){ $("div#saved").fadeIn(); }); } }); return false; });});<';  
    js += '/script>';

	$('div#propriedades').show();
	//$('div#propriedades').load('salvartemplate.php?classe='+classe+'&w='+w+'&h='+h+'&x='+x+'&y='+y);
	$('div#propriedades').load('editor_propriedades/<?php echo md5($title); ?>/'+classe+'/'+w+'/'+h+'/'+x+'/'+y);
}

</script>

<style type="text/css">
div#canvas {
	width: 700px;
	height: 1000px;
	margin: 0;
	padding: 0;
	<?php if (!empty($bg_image)) : ?> background:url(<?php echo $bg_image; ?>); <?php endif; ?>
	background-color:#FFF;
	background-position:top left;
	border: black 1px solid;
}
div#propriedades {
	width: 694px;
	height: 40px;
	margin: 0;
	padding: 3px;
	background-color:#FFFFCE;
	background-position:top left;
	border: black 1px dotted;
	font-size: 11px;
}
body,td,th {
	font-family: Arial, sans-serif;
	font-size: 12px;
}
body {
	margin: 0;
}

</style>
</head>

<body onload="window.opener.name='origem';">
<h1>GED - Editor de templates</h1>
<p>
<?php if ($template_types == 3) : ?>
<button name="coringa1" onclick="var valor = prompt('Digite abaixo o nome do campo.\nDICAS\n- Coloque * no final do nome, caso queira que este campo apareça na listagem de documentos.\n- Coloque a palavra (texto) ou (tabela), com os parênteses, para criar um campo com múltiplas linhas e opção de formatação.', 'Nome'); if (valor != '') { CKEDITOR.instances.editor.insertText('[['+valor+']]'); } else return false;" >  INSERIR CAMPO  </button>&nbsp;&nbsp;<button name="coringa2" onclick="var valor = '{{ID}}'; if (valor != '') { CKEDITOR.instances.editor.insertText(valor); } else return false;" >  INSERIR CÓDIGO DO DOCUMENTO  </button>&nbsp;&nbsp;<button name="coringa3" onclick="var valor = '{{DATA}}'; if (valor != '') { CKEDITOR.instances.editor.insertText(valor); } else return false;" >  INSERIR DATA ATUAL  </button>
<?php endif; ?>
<?php if ($template_types == 2) : ?>
<button name="coringa2" onclick="var valor = '{{ID}}'; if (valor != '') { CKEDITOR.instances.editor.insertText(valor); } else return false;" >  INSERIR CÓDIGO DO DOCUMENTO  </button>&nbsp;&nbsp;<button name="coringa3" onclick="var valor = '{{DATA}}'; if (valor != '') { CKEDITOR.instances.editor.insertText(valor); } else return false;" >  INSERIR DATA ATUAL  </button>
<?php endif; ?>
<br /></p>
<?php echo form_open('template/salvar' ,array('name' => 'form_tpl', 'id' => 'form_tpl', 'target' => 'origem', 'onsubmit' => 'window.close()')); ?>

<table width="700" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="83" align="center"><strong>Template</strong></td>
    <td width="628"><?php echo $title; ?><input type="hidden" name="title" id="title" value="<?php echo $title; ?>" />
    <input type="hidden" name="desc" id="desc" value="<?php echo $desc; ?>" /></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
    
    <?php if ($template_types == 1) { ?>
    <div id="propriedades"></div>
    <div id="canvas">
    <?php echo $reload_divs; ?>
    </div>
    <?php } else { ?>
    <?php echo $reload_divs; ?>
    <?php } ?>
    
    </td>
    </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td><label>
    	<input type="hidden" name="template_type" id="template_type" value="<?php echo $template_types; ?>" />
    	<input type="hidden" name="bg_image" id="bg_image" value="<?php echo $bg_image; ?>" />
        <?php if ($template_id > 0) : ?> <input type="hidden" name="template_id" id="template_id" value="<?php echo $template_id; ?>" /> <?php endif; ?>
      <input type="submit" name="gerar" id="gerar" value="Salvar template" />
    </label></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>