<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Salvar novo template</h3>
        <hr />
        <?php echo form_open('template/salvar_bd' ,array('name' => 'form_save', 'id' => 'form_save')); ?>
        
        <table width="100%" border="0">
          <tr>
            <td width="30%">T&iacute;tulo do template</td>
            <td width="70%">
              <label>
                <?php echo $title; ?>
              </label>
              <input type="hidden" name="title" id="title" value="<?php echo $title; ?>" />
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o</td>
            <td><label>
                <?php echo $desc; ?>
              </label>
              <input type="hidden" name="desc" id="desc" value="<?php echo $desc; ?>" /></td>
          </tr>
          <tr>
            <td>Imagem de fundo</td>
            <td><label>
              
                <?php echo $bg_image_name; ?>

            </label>
            <input type="hidden" name="bg_image" id="bg_image" value="<?php echo $bg_image; ?>" /></td>
          </tr>
           <tr>
            <td>Tipo de template</td>
            <td><label>
              
                <?php echo $template_type_name; ?>

            </label>
            <input type="hidden" name="template_type" id="template_type" value="<?php echo $template_type; ?>" /></td>
          </tr>
          <tr>
            <td>Preview do template</td>
            <td><label>
              
                <?php echo $preview; ?>

            </label> 
            <input type="hidden" name="page" id="page" value="<?php echo $page; ?>" />
            <?php if ($template_id > 0) : ?> <input type="hidden" name="template_id" id="template_id" value="<?php echo $template_id; ?>" /> <?php endif; ?>
           </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="ok"/> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/template/index'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
       
        </form>
        </div>
  	</section>