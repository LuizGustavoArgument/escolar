<script language="javascript">
function submitFormInPopUp(formid)
{
	var myPopup = window.open('','Prvwindow','status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=0, scrollbars=1, width=750, height=600');
	formid.action = "<?php echo site_url('template/editor'); ?>";
	formid.target = "Prvwindow";
	formid.submit();
	//if (!myPopup.opener)
	//	myPopup.opener = self;
}

</script>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Listagem de templates</h3>
        <hr />
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
    <td>Nome do template</td>
    <td>Tipo</td>
    <td>Criado por</td>
    <td>Data</td>
    <td>&nbsp;</td>
  </tr>
  </thead>
  <tbody>
  <?php $template_types = array('1' => 'Desenho de regiões (vários campos)', '2' => 'Página simples (campo único)', '3' => 'Página simples com vários campos'); ?>
  <?php foreach($resultado_query as $item) : ?>
  <tr>
    <td><strong><?php echo ($item->desc ? '<abbr title="'.$item->desc.'">'.$item->title.'</abbr>' : $item->title); ?></strong></td>
    <td><?php echo $template_types[$item->template_type]; ?></td>
    <td><?php echo '<em>admin</em>'; ?></td>
    <td><?php echo date('d/m/Y',strtotime($item->date_created)); ?></td>
    <td>
    <?php // se for o admin
	if ($perm == 1)
	{
		echo form_open('template/editor' ,array('name' => 'form'.$item->id.'', 'id' => 'form'.$item->id.'', 'target' => 'Prvwindow'));
		echo '<input type="hidden" name="title" value="'.$item->title.'" />';
		echo '<input type="hidden" name="desc" value="'.$item->desc.'" />';
		echo '<input type="hidden" name="bg_image" value="'.$item->bg_image.'" />';
		echo '<input type="hidden" name="template_type" value="'.$item->template_type.'" />';
		echo '<input type="hidden" name="template_id" value="'.$item->id.'" />';
		echo '<a href="#" onclick="submitFormInPopUp(document.form'.$item->id.');">editar</a>';
		echo '&nbsp;|&nbsp;<a href="" onclick="if(confirm(\'ATENÇÃO: eliminando este template, TODOS os documentos que foram criados com base nele também serão eliminados! Tem certeza?\')){document.location.href=\''.site_url('template/remover/'.$item->id).'\';} else return false;">remover</a>';
		echo '</form>';
	} 
	?></td>
  </tr>
  <?php endforeach; ?>
  </tbody>
  <tfoot>
  <tr>
    <td colspan="5"><?php echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>

        </div>
  	</section>