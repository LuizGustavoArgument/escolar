{$header}

{if $js != ''}{$js}{/if}
{if $errors != ''}{$errors}{/if}
<section>
{if $barra != ''}{$barra}{/if}
<form name="aeerel" id="aeerel" action="{$form.baseurl}{$form.action}" method="{$form.method}" enctype="multipart/form-data">

<div class="row-fluid">

  <div class="col-10">
  <div class="col-3">
  <fieldset style="margin-top:25px">
  {if $foto != ''}
  <p><img src="{$form.baseurl}/uploads/{$foto}" class="img-rounded" style="width:200px"></p>
  {/if}
  </fieldset>
  </div>
  <div class="col-7">
  <fieldset style="margin-top:25px">
  <h3>AVALIAÇÃO DE EMPRESA</h3>
  <label>{$form.cliente_label}:</label>{$form.cliente}
  <label>{$form.periodo_label}:</label>{$form.periodo}
  </fieldset>
  </div>
  </div>
  <div class="col-10">
  <fieldset>
  <label>{$form.equipe_label}:</label>{$form.equipe}
  <label>{$form.relatorio_label}:</label>{$form.relatorio}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>

  <div class="col-10">
  <fieldset style="margin-top:25px">
  <label>{$form.anexo1_label}:</label>{$form.anexo1}
  <br><label>{$form.anexo2_label}:</label>{$form.anexo2}
  <br><label>{$form.anexo3_label}:</label>{$form.anexo3}
  </fieldset>
  {if $formsalvar != ''}
  {$formsalvar}
  {/if}
  </div>

</div>

</form>
</section>
{$footer}