<?php
if ($_POST)
{
	$cont = date('YmdHis');
		
	$fname        = htmlspecialchars(trim($_POST['fname']));  
    $tname        = htmlspecialchars(trim($_POST['tname']));
	$w        = htmlspecialchars(trim($_POST['w']));
	$h        = htmlspecialchars(trim($_POST['h']));
	$x        = htmlspecialchars(trim($_POST['x']));
	$y        = htmlspecialchars(trim($_POST['y']));
	$classe        = htmlspecialchars(trim($_POST['classe']));
	
	if (!empty($_POST['remover']) && $_POST['remover'] == 1)
	{
		$sessaotmp = $this->session->userdata('newdata');
		$sessaosrl = explode("||",$sessaotmp);
		$sessao = "";
		if (is_array($sessaosrl))
		foreach($sessaosrl as $item) 
		{
			if (!stristr($item,$classe))
				$sessao .= $item . "||";
		}
		
		if (!empty($sessao))
			$this->session->set_userdata('newdata',$sessao);
		?>
		<script type="text/javascript">

		$(document).ready(function(){
			$(".<?php echo $classe; ?>").remove();
		});
		
		</script>
		<?php
		//$novo = $this->session->userdata('newdata');
		//echo $novo;
		echo 'Região removida.';
	}
	else
	{
		// Procura se já não existe o registro (remove e insere outra vez)
		$alterou = false;
		$sessaotmp = $this->session->userdata('newdata');
		$sessaosrl = explode("||",$sessaotmp);
		$sessao = "";
		if (is_array($sessaosrl))
		foreach($sessaosrl as $item) 
		{
			if (!stristr($item,$classe))
				$sessao .= $item . "||";
			else // faz a alteracao
				{
					$newdata[$_POST['template_name']][$cont] = array(
						'classe' => $classe,
						'fname' => $fname,
						'tname' => $tname,
						'w' => $w,
						'h' => $h,
						'x' => $x,
						'y' => $y 
					);
					$sessao .= serialize($newdata);
					$alterou = true;
				}
		}
		
		if (!empty($sessao))
			$this->session->set_userdata('newdata',$sessao);
		
		if (!$alterou)
		{
			// Insere o registro na memoria (sessao)
			$newdata[$_POST['template_name']][$cont] = array(
				'classe' => $classe,
				'fname' => $fname,
				'tname' => $tname,
				'w' => $w,
				'h' => $h,
				'x' => $x,
				'y' => $y 
			);
			
			
			$serialdata = serialize($newdata);
			$novo = $this->session->userdata('newdata');
			if (!$novo) $novo = "";
			$novo .= $serialdata . "||";
			$this->session->set_userdata('newdata',$novo);
			
			//$novo = $this->session->userdata('newdata');
			//echo $novo;
			
			echo 'Região salva com sucesso!';
		}
		else
			{
				//$novo = $this->session->userdata('newdata');
				//echo $novo;
				echo 'Alteração salva.'; 
			}
	}	
	?>
	
	<script type="text/javascript">

	$(document).ready(function(){
		$(".<?php echo $classe; ?>").fadeOut('fast').css('backgroundColor','green').fadeIn();
	});
	
	</script>
	
	<?php
}
?>
