<?php 
//session_start(); 
$_SESSION['KCFINDER'] = array();
$_SESSION['KCFINDER']['disabled'] = false;
$_SESSION['KCFINDER']['uploadURL'] = $pasta_org;
?>

<script language="javascript">
function openFileBrowser(field) {
    window.KCFinder = {};
    window.KCFinder.callBack = function(url) {
        window.KCFinder = null;
		jQuery("select#"+field+" option[selected]").removeAttr("selected");
		jQuery("select#"+field+" option[value='"+url+"']").attr("selected","selected");
    };
    window.open('<?php echo base_url(); ?>js/kcfinder/browse.php', 'kcfinder_single',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600');
}

function submitFormInPopUp()
{
	var myPopup = window.open('','Prvwindow','status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=0, scrollbars=1, width=750, height=600');
	document.form1.action = "<?php echo site_url('template/editor'); ?>";
	document.form1.target = "Prvwindow";
	document.form1.submit();
	//if (!myPopup.opener)
	//	myPopup.opener = self;
}

</script>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Criar novo template</h3>
        <hr />
        <?php echo form_open('template/editor' ,array('name' => 'form1', 'id' => 'form1', 'target' => 'Prvwindow')); ?>
        
        <table width="100%" border="0">
          <tr>
            <td width="30%">T&iacute;tulo do template</td>
            <td width="70%">
              <label>
                <input type="text" name="title" id="title" required="required" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ></textarea></td>
          </tr>
          <tr>
            <td>Imagem de fundo</td>
            <td><label>
              <select name="bg_image" id="bg_image">
                <?php echo $bg_images; ?>
              </select>
            </label>&nbsp;&nbsp;[ <a href="#" onclick="openFileBrowser('bg_image');">Ver imagens</a> ]<br /><small><strong>Obs:</strong> O tamanho ideal para o fundo das p&aacute;ginas &eacute; de 700x1000 pixels</small></td>
          </tr>
           <tr>
            <td>Tipo de template</td>
            <td><label>
              <select name="template_type" id="template_type">
                <?php echo $template_types; ?>
              </select>
            </label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><table><tr><td><button type="button" class="button positive" onclick="submitFormInPopUp();"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="ok"/> Abrir editor</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/template/index'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
        <input type="hidden" name="saved_template_type" id="saved_template_type" />
        <input type="hidden" name="saved_template_value" id="saved_template_value" />
        </form>
        </div>
  	</section>
  	