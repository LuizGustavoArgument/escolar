<?php
	session_start();
	
	// Must be already set
	$_SESSION['username'] = $this->session->userdata('nome_usuario');
?>


<link rel="stylesheet" type="text/css"   href="<?php echo base_url();  ?>js/jqueryui/css/redmond/jquery-ui-1.9.1.custom.css"/> 

<!-- Chat styles   -->
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url();  ?>css/chat.css" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url();  ?>css/screen.css" />

<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url();  ?>css/screen_ie.css" />
<![endif]--> 

<script type="text/javascript" src="<?php echo base_url();  ?>js/jqueryui/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>js/chat.js"></script>
  
<!-- drag the chat box  -->
<script type="text/javascript"   src="<?php echo base_url();  ?>js/jqueryui/js/jquery-ui-1.9.1.custom.js"></script>
<!--<script src="<?php echo base_url();  ?>js/jquery/ui/jquery.ui.draggable.js"></script>-->

<script type="text/javascript">
	$(function(){ 
		if($(".chatbox")[0]) {
			$(".chatbox").draggable();
		}
		
		$('.chatboxhead').live('click',function () {
			$(".chatbox").draggable();
		});

	});
</script> 
    
<!--   *****   DISPLAY ONLINE USERS LIST *********************************  -->
	<table width="96%" cellspacing="1" cellpadding="2" id="chatUsersTbl">
       <tbody>
          <tr>
             <th>ID</th>
             <th>Nome</th>
          </tr>
                               
		<?php
		/*
			These users list get from users controller.Not in the chat controller.
		*/ 
		foreach($UsersList->result() as $Users){ ?>
          <tr>
            <td><?php echo $Users->user_id; ?></td>
            <td>
               <a href="javascript:void(0)" onClick="javascript:chatWith('<?php echo $Users->user_name; ?>');">
                  <?php echo $Users->user_name; ?>
               </a>
            </td>
         </tr>
		<?php 	
		}
		?>	  	  	
		</tbody>
	</table>
