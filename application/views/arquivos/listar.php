
<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <!--<div style="float:left"><h3>Arquivos da pasta <abbr title="<?php echo $desc; ?>"><?php echo $name; ?></abbr></h3><?php /*if (!stristr($this->uri->segment(2),'fullscreen')) { echo anchor('documentos/listar_fullscreen/'.$this->uri->segment(3),'Ver em tela cheia'); } */ ?></div>
        
        <div style="clear:both"><h4><?php echo $desc; ?></h4></div>
        <hr />-->
        <p align="right"><strong><i class="icon-folder-open"></i>&nbsp;<?php echo $desc; ?></strong></p>
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
  	<td>Cód.</td>
    <td>Nome</td>
    <td>Descrição</td>
    <td>Criado por</td>
    <td>Data</td>
    <td>&nbsp;</td>
  </tr>
  </thead>
  <tbody>
  <?php if ($perm) {  // era perm < 4 ?>
  <?php if (!empty($resultado_query)) { ?>
  <?php $CI =& get_instance(); ?>
  <?php foreach($resultado_query as $item) : ?>
  <?php 
  // Se for a pasta de nome "consultor", apenas mostra os arquivos criados pela pessoa logada
  $mostrar = false;
  if($CI->banco->campo('folders','name','id = '.$idf) == 'escola' && $item->idusr == $this->session->userdata('esta_logado')) 
  {
    $mostrar = true;
  }
  if($CI->banco->campo('folders','name','id = '.$idf) != 'escola')
  {
    $mostrar = true;
  }
  
  if($mostrar) :
  ?>
  <?php 
  if(empty($icons[$CI->verifica_mime_type($item->type)])) $ico = $icons['txt']; else $ico = $icons[$CI->verifica_mime_type($item->type)];
  $icon = '<img src="'.$ico.'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
  ?>
  <tr>
    <td>
    <abbr title="Identificador único do documento">
    <?php
	// Gera a numeracao de acordo com a opcao
	echo $CI->gera_numeracao($item->id,strtotime($item->date_created));
    $baixar = base64_encode($item->id . '|||' . $item->date_created);
    //$icon = '<img src="'.$icons[$CI->verifica_mime_type($item->type)].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />';
	?>
    </abbr></td>
    <td>
    <?php if (empty($item->content)) { echo $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>"; } else { ?>
    <?php echo $icon; ?>&nbsp;<strong><?php echo anchor('documentos/download/'.$baixar.'/'.$idf,$item->name,array("title" => "Arquivo ". strtoupper(basename($ico,".png")))); ?></strong>
    <?php } ?>
    </td>
    <td><?php echo $item->desc; ?></td>
    <td><?php echo '<abbr title="'.$grupos[$item->idgrp] .'">'. $usuarios[$item->idusr] .'</abbr>'; ?></td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
    <?php 
    $folderid_real = $idf;
    $folderid = $this->banco->campo('folders','nameid','id in ('.(is_array($folderid_real) ? implode(',',$folderid_real) : $folderid_real).')');
    if(is_array($folderid)) $folderid = $folderid[0];
    $strRemove = base64_encode($item->id . '|||' . $item->date_created. '|||'.$folderid.'|||'.$item->idusr);
    
    $hasDoc = '';
    if(!empty($item->idserv))
        $hasDoc = $CI->banco->campo('services','document','id = ' . (int) $item->idserv);
    
    $acoes = '
        '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar.'/'.$idf,'<i class="icon-download"></i>')).'&nbsp;|&nbsp;
        './*($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? anchor('arquivos/editar/'.$item->id,'<i class="icon-pencil"></i>') . '&nbsp;|&nbsp;' : '') .*/'
        '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? '<a href="javascript:void(0)" onclick="if(confirm(\'Tem certeza?\')){removeArq(\''.$strRemove.'\');} else return false;"><i class="icon-remove"></i></a>' : '') .'
    ';
    
    if(($item->idtpl == 1) || ($item->idtpl == 0 && $baixar == $hasDoc))
    {
        // Só admin altera ou remove
        if($this->session->userdata('nivel') == 'yes')
        {
            $acoes = '
            '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar.'/'.$idf,'<i class="icon-download"></i>')).'&nbsp;|&nbsp;
            './*($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? anchor('arquivos/editar/'.$item->id,'<i class="icon-pencil"></i>') . '&nbsp;|&nbsp;' : '') .*/'
            '.($this->session->userdata('nivel') == 'yes' || ($item->idusr == $this->session->userdata('esta_logado')) ? '<a href="javascript:void(0)" onclick="if(confirm(\'Tem certeza?\')){removeArq(\''.$strRemove.'\');} else return false;"><i class="icon-remove"></i></a>' : '') .'
            ';
        }
        else 
        {
            $acoes = '
            '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar.'/'.$idf,'<i class="icon-download"></i>')).'
            ';
        }
        
        if($item->idtpl == 1)
        {
            $acoes = '
            '.(($item->locked == 'yes') ? '<small style="color:red">Travado!</small>' : anchor('documentos/download/'.$baixar.'/'.$idf,'<i class="icon-download"></i>')).'
            ';
        }
    }
    echo $acoes;
    ?>
    </td>
  </tr>
  <?php endif; ?>
  <?php endforeach; ?>
  <?php } else { echo '<tr><td></td><td>Nenhum arquivo encontrado nesta pasta.</td><td></td><td></td><td></td><td></td></tr>'; }?>
  <?php } else { echo '<tr><td></td><td>Os arquivos desta pasta estão ocultos.</td><td></td><td></td><td></td><td></td></tr>'; } ?>
  </tbody>
  <tfoot>
  <tr>
    <td colspan="6"><?php //echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>
<script>
    $(function() {
        $( "#caixa-copia" ).dialog({
            resizable: false,
            autoOpen: false,
            show: "blind",
            modal: true,
            buttons: {
                "Copiar": function() {
                    var info1 = $("form#copia #folder_de").val();
                    var info2 = $("form#copia #folder_para").val();
                    var info3 = $("form#copia #file_id").val();
                    $.get("<?php echo site_url('documentos/copiando2'); ?>/"+info1+"/"+info2+"/"+info3,function(data){
                       if(data == "ok")
                       {
                           alert('O arquivo foi copiado com sucesso.');
                       }
                       else
                       {
                           alert('Falha ao copiar arquivo: '+data);
                       }
                    });
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
</script>
<script>function removeArq(str)
        {
            $.get("<?php echo site_url('arquivos/remover'); ?>/"+str,function(data){
                if(data) {alert(data);} else {
                    document.location.reload();
                }
            });
        }</script>
<div id="caixa-copia" title="Cópia de arquivos"></div>


</section>
