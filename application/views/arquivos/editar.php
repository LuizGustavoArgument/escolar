<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
<?php $CI =& get_instance(); ?>
<?php foreach ($resultado_query as $item) : ?>
        <div class="conteudo">
        <h4>Editar: <?php echo $item->name; ?></h4>
        
        <hr />
        <?php echo form_open('arquivos/editar/'.$item->id ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        <input type="hidden" name="permission" id="permission" value="0" />
<input type="hidden" name="document_option" value="opcao1" />
          <table width="100%" border="0" id="opcao1">
          <tr><td>&nbsp;</td><td><i class="icon-folder-open"></i>&nbsp;<?php echo $folder_list; ?></td></tr>
          <!--<tr>
            <td width="30%">Situação do documento</td>
            <td width="70%">
           
            
            <script>
			$(function() {
				var slider = $( "#travar" ).slider({
					min: 0,
					max: 1,
					range: "min",
					animate: true,
					value: <?php echo ( $item->locked == 'yes' ? '1' : '0'); ?>,
					slide: function( event, ui ) {
						var valores = new Array('no','yes');
						$( "#travar_status" ).load('<?php echo site_url('documentos/lock'); ?>/'+valores[ui.value]+'/<?php echo $item->id; ?>');
					}
				});
			});
			</script>
            <div class="demo">
            <span id="travar_status" style="font-size:9px; text-align:center"><?php echo ( $item->locked == 'yes' ? 'Documento travado.' : 'Documento destravado.'); ?></span>
            <div id="travar" style="width: 100px;"></div>
            </div>
            </td>
          </tr>-->
          <tr>
            <td>Arquivo</td>
            <td>
            <?php if (!empty($item->content)) { ?>
			<?php
			$ext = $CI->verifica_mime_type($item->type);
            if(empty($icons[$ext])) $ico = $icons['txt']; else $ico = $icons[$ext];
            $icon = '<img src="'.$ico.'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
			//$icon = '<img src="'.$icons[$ext].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
			$size = $CI->format_filesize($item->size);
			$baixar = base64_encode($item->id . '|||' . $item->date_created); 
			$link = anchor('documentos/download/'.$baixar,$item->name);
			echo $icon . '&nbsp;' . $link . ' (Formato <strong>'. $ext . '</strong>) - '. $size . '<br />';
			
			//if ($item->readonly == 'no')
			//echo ' <input name="remove_arquivo" type="checkbox" value="1" /> Remover este arquivo<br /> <input name="filepath" type="file" />';
			//else
			//echo '<small>Arquivo configurado como "somente leitura"';
			?>
           
            <?php } else { 
			
			echo '<input name="filepath" type="file" />';
			
			}
			?>
            </td>
          </tr>
          <tr>
            <td width="30%">Nome do documento</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" value="<?php echo $item->name; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ><?php echo $item->desc; ?></textarea></td>
          </tr>
          <tr>
            <td>Tipo de documento</td>
            <td><label>
              <select name="type" id="type">
                <?php echo $tipos_documentos; ?>
              </select>
            </label></td>
          </tr>
           
           
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%"><input type="hidden" name="mexeu" id="mexeu" value="0" /></td>
            <td width="70%"><table><tr><td><button type="submit" class="btn btn-success positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td>
                <button type="button" class="btn negative" onclick="history.back();"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        <?php endforeach; ?>
        </div>
  	</section>