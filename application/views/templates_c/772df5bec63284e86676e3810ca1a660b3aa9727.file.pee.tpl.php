<?php /* Smarty version Smarty-3.1.13, created on 2013-07-15 08:42:26
         compiled from "application\views\templates\pee.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1227551dab80e09f231-88782886%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '772df5bec63284e86676e3810ca1a660b3aa9727' => 
    array (
      0 => 'application\\views\\templates\\pee.tpl',
      1 => 1373888543,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1227551dab80e09f231-88782886',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_51dab80e2f2a50_10948032',
  'variables' => 
  array (
    'header' => 0,
    'js' => 0,
    'errors' => 0,
    'barra' => 0,
    'form' => 0,
    'foto' => 0,
    'formsalvar' => 0,
    'footer' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51dab80e2f2a50_10948032')) {function content_51dab80e2f2a50_10948032($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['header']->value;?>


<?php if ($_smarty_tpl->tpl_vars['js']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['js']->value;?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['errors']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
<?php }?>
<section>
<?php if ($_smarty_tpl->tpl_vars['barra']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['barra']->value;?>
<?php }?>
<form name="peerel" id="peerel" action="<?php echo $_smarty_tpl->tpl_vars['form']->value['baseurl'];?>
<?php echo $_smarty_tpl->tpl_vars['form']->value['action'];?>
" method="<?php echo $_smarty_tpl->tpl_vars['form']->value['method'];?>
" enctype="multipart/form-data">

<div class="row-fluid">
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Capa</a></li>
    <li><a href="#tabs-2">1. Metas</a></li>
    <li><a href="#tabs-9">2. Cálculo</a></li>
    <li><a href="#tabs-3">3. Custos</a></li>
    <li><a href="#tabs-4">4. Processos</a></li>
    <li><a href="#tabs-5">5. RH</a></li>
    <li><a href="#tabs-6">6. Marketing</a></li>
    <li><a href="#tabs-7">7. Asp. Legais</a></li>
    <li><a href="#tabs-8">8. Diversos</a></li>
  </ul>
  <div id="tabs-1">
  <div class="span10">
  <div class="span3">
  <fieldset style="margin-top:25px">
  <?php if ($_smarty_tpl->tpl_vars['foto']->value!=''){?>
  <p><img src="<?php echo $_smarty_tpl->tpl_vars['form']->value['baseurl'];?>
/uploads/<?php echo $_smarty_tpl->tpl_vars['foto']->value;?>
" class="img-rounded" style="width:200px"></p>
  <?php }?>
  </fieldset>
  </div>
  <div class="span7">
  <fieldset style="margin-top:25px">
  <h3>PLANO ESTRATÉGICO EMPRESARIAL</h3>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['cliente_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['cliente'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['periodo_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['periodo'];?>

  </fieldset>
  </div>
  </div>
  <div class="span10">
  <fieldset>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['equipe_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['equipe'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tabela_prior_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tabela_prior'];?>

  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-2">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl1_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl1_sint'];?>

  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-9">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl2_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-3">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl3_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-4">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl4_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-5">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl5_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-6">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl6_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-7">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl7_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
  <div id="tabs-8">
  <div class="span10">
  <fieldset style="margin-top:25px">
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_sint_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_sint'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior1'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior2'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['tbl8_prior3'];?>

  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </fieldset>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>

  <?php }?>
  </div>
  </div>
  
</div>
</div>

</form>
</section>
<?php echo $_smarty_tpl->tpl_vars['footer']->value;?>
<?php }} ?>