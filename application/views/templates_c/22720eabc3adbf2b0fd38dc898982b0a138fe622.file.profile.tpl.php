<?php /* Smarty version Smarty-3.1.13, created on 2015-04-22 14:52:26
         compiled from "application\views\templates\profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1711451d80c5d8181a5-36591435%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '22720eabc3adbf2b0fd38dc898982b0a138fe622' => 
    array (
      0 => 'application\\views\\templates\\profile.tpl',
      1 => 1429724979,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1711451d80c5d8181a5-36591435',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_51d80c5daee8c4_39239483',
  'variables' => 
  array (
    'header' => 0,
    'js' => 0,
    'errors' => 0,
    'form' => 0,
    'foto' => 0,
    'formsalvar' => 0,
    'extrahtml' => 0,
    'ehadmin' => 0,
    'groupset' => 0,
    'ologin' => 0,
    'asenha' => 0,
    'footer' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51d80c5daee8c4_39239483')) {function content_51d80c5daee8c4_39239483($_smarty_tpl) {?><?php echo $_smarty_tpl->tpl_vars['header']->value;?>


<?php if ($_smarty_tpl->tpl_vars['js']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['js']->value;?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['errors']->value!=''){?><?php echo $_smarty_tpl->tpl_vars['errors']->value;?>
<?php }?>
<section>
<form name="produtos" id="produtos" action="<?php echo $_smarty_tpl->tpl_vars['form']->value['baseurl'];?>
<?php echo $_smarty_tpl->tpl_vars['form']->value['action'];?>
" method="<?php echo $_smarty_tpl->tpl_vars['form']->value['method'];?>
" enctype="multipart/form-data">

<div class="row-fluid">
<fieldset>
  <div class="col-2">
  <?php if (isset($_smarty_tpl->tpl_vars['form']->value['groups'])){?>
  <?php echo $_smarty_tpl->tpl_vars['form']->value['groups'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['groups_error'];?>
</span>
  <?php }?>
  <br><?php echo $_smarty_tpl->tpl_vars['foto']->value;?>
<br>
  <a href="#" onclick="$('#imgaltera').show();$(this).hide();">Alterar imagem...</a>
  <div id="imgaltera" style="display:none" class="col-2">
  <?php echo $_smarty_tpl->tpl_vars['form']->value['logo'];?>

  </div>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <br><br><p><?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>
</p>
  <?php }?>
  </div>
  <div class="col-8">
    
    <?php if (isset($_smarty_tpl->tpl_vars['extrahtml']->value)){?>
    <div class="form-inline"><?php echo $_smarty_tpl->tpl_vars['extrahtml']->value;?>
</div>
    <?php }?>
  	<div class="form-inline">
  	<label><?php if (isset($_smarty_tpl->tpl_vars['form']->value['username_label'])){?><?php echo $_smarty_tpl->tpl_vars['form']->value['username_label'];?>
:&nbsp;&nbsp;<?php }?></label><?php echo $_smarty_tpl->tpl_vars['form']->value['username'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['username_error'];?>
</span>
  	</div>

  	<div class="form-inline"><blockquote>
  	<?php if (isset($_smarty_tpl->tpl_vars['form']->value['password_label'])){?>
  	    <?php if (isset($_smarty_tpl->tpl_vars['form']->value['password_atual'])){?>
  	    <br><label style="margin-right:3px">Senha atual:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><?php echo $_smarty_tpl->tpl_vars['form']->value['password_atual'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['password_atual_error'];?>
</span><br>
	  	<?php if ($_smarty_tpl->tpl_vars['ehadmin']->value=='yes'){?><label class="checkbox" for="password_naosei"><input name="password_naosei" id="password_naosei" value="naosei" type="checkbox" onchange="if(this.checked) document.getElementById('password_atual').value='naosei'; else document.getElementById('password_atual').value='';"> Não sei a senha atual!</label><br><?php }?>
	  	<?php }else{ ?>
	  	<br>
	  	<?php }?>
	  	<label>Nova senha:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><?php echo $_smarty_tpl->tpl_vars['form']->value['password'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['password_error'];?>
</span>
	  	<br><label style="margin-right:2px;"><?php echo $_smarty_tpl->tpl_vars['form']->value['password_again_label'];?>
:&nbsp;</label><?php echo $_smarty_tpl->tpl_vars['form']->value['password_again'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['password_again_error'];?>
</span>
  	<?php }?>
  	<?php if (!isset($_smarty_tpl->tpl_vars['form']->value['password_label'])){?>
  	<?php if (isset($_smarty_tpl->tpl_vars['form']->value['password_atual'])){?><?php echo $_smarty_tpl->tpl_vars['form']->value['password_atual'];?>
<?php }?>
  	<?php echo $_smarty_tpl->tpl_vars['form']->value['password'];?>

  	<?php echo $_smarty_tpl->tpl_vars['form']->value['password_again'];?>

            <?php }?><?php if ($_smarty_tpl->tpl_vars['ehadmin']->value&&$_smarty_tpl->tpl_vars['groupset']->value==7){?><strong>Dados para acesso:</strong><br>Login: <span style="color:red"><?php echo $_smarty_tpl->tpl_vars['ologin']->value;?>
</span><br>Senha: <span style="color:red"><?php echo $_smarty_tpl->tpl_vars['asenha']->value;?>
</span> <?php }?>
  	</blockquote>
  	</div><hr>

  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['name_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['name'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['name_error'];?>
</span>
  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['website_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['website'];?>

  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['email_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['email'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['email_error'];?>
</span>
  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_fax_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_fax'];?>

  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['phone_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['phone'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['phone_error'];?>
</span>
  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['cnpj_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['cnpj'];?>

  	<label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cpf_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cpf'];?>
<span class="help-inline" style="color:red"><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cpf_error'];?>
</span>
  </div>
</fieldset>
<fieldset>
  <legend>Arquivos</legend>
  <div id="listinha2"></div>
</fieldset>
</div>
<div class="row-fluid">
  <div class="span10">
  <fieldset>
  <legend>Dados complementares</legend>
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['address'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_num_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_num'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_cpl_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_cpl'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['fax_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['fax'];?>
 <!-- bairro -->
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['mobile_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['mobile'];?>
 <!-- cep -->
  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['city_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['city'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['uf_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['uf'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['country_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['country'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_ref_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['address_ref'];?>

  <label><?php echo $_smarty_tpl->tpl_vars['form']->value['outros_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['outros'];?>

  <hr>
  </fieldset>
  <fieldset>
  <legend>Contatos</legend>
  <ul class="navbar nav-tabs" id="myTab">
    <li><a href="#contato1" data-toggle="tab">Contato 1</a></li>
    <li><a href="#contato2" data-toggle="tab">Contato 2</a></li>
    <li><a href="#contato3" data-toggle="tab">Contato 3</a></li>
    <li><a href="#contato4" data-toggle="tab">Contato 4</a></li>
    <li><a href="#contato5" data-toggle="tab">Contato 5</a></li>
  </ul>
  
  <div class="tab-content">
  <div class="tab-pane" id="contato1">
      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name1'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo1'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email1'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile1'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone1_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone1'];?>

  </div>
  
  <div class="tab-pane" id="contato2">
      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name2'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo2'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email2'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile2'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone2_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone2'];?>

  </div>
  
  <div class="tab-pane" id="contato3">
      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name3'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo3'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email3'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile3'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone3_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone3'];?>

  </div>
  
  <div class="tab-pane" id="contato4">
      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name4_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name4'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo4_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo4'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email4_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email4'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile4_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile4'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone4_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone4'];?>

  </div>
  
  <div class="tab-pane" id="contato5">
      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name5_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_name5'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo5_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_cargo5'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email5_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_email5'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile5_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_mobile5'];?>

      <label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone5_label'];?>
:</label><?php echo $_smarty_tpl->tpl_vars['form']->value['contact_phone5'];?>

  </div>
  </div>
  </fieldset>
  <script>
    $(function () {
    $('#myTab a:first').tab('show');
    $('#website').datepicker({
				format: 'dd/mm/yyyy',
				viewMode: 2
			});
    })
  </script>
  <?php if ($_smarty_tpl->tpl_vars['formsalvar']->value!=''){?>
  <p><?php echo $_smarty_tpl->tpl_vars['formsalvar']->value;?>
</p>
  <?php }?>
  </div>
</div>
<?php echo $_smarty_tpl->tpl_vars['form']->value['date_created'];?>

<?php if ($_smarty_tpl->tpl_vars['form']->value['id']!=''){?><?php echo $_smarty_tpl->tpl_vars['form']->value['id'];?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['form']->value['tipo']!=''){?><?php echo $_smarty_tpl->tpl_vars['form']->value['tipo'];?>
<?php }?>
</form>
</section>
<?php echo $_smarty_tpl->tpl_vars['footer']->value;?>
<?php }} ?>