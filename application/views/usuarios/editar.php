<script>
$(function() {
		$( "#tabs" ).tabs();
	});
</script>
<div class="span-24 miolo">
    
    <div class="span-22">

			<div class="prepend-2">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <div id="tabs">
        <ul>
        <li><a href="#tabs-1">Editar usuário</a></li>
        <li><a href="#tabs-2">Dados cadastrais</a></li>
        </ul>
<div id="tabs-1">
<?php foreach ($resultado_query as $valor) : ?>
        <h3>Editar o usuário <?php echo $valor->name; ?></h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('usuarios/editar/'.$valor->id); ?>">
        <table width="100%" border="0">
          <tr>
            <td width="30%">Nível</td>
            <td width="70%"><select name="isadmin" id="isadmin">
              <option value="yes" <?php if ($valor->isadmin == 'yes') echo 'selected="selected"'; ?>>Administrador</option>
              <option value="no" <?php if ($valor->isadmin == 'no') echo 'selected="selected"'; ?>>Usuário comum</option>
            </select><br /><input type="checkbox" name="idhist" id="idhist_1" value="1" <?php if ($valor->idhist == '1') echo 'checked="checked"'; ?> />  Confirmou e preencheu todos os formulários (no caso de Cooperativa)
            </td>
          </tr>
           <tr>
            <td width="30%">Organização</td>
            <td width="70%"><select name="idorg" id="idorg">
              <option value="1">Padrão</option>
            </select></td>
          </tr>
          <tr>
            <td>Associar ao(s) grupo(s)<br /><small>É obrigatório selecionar pelo menos um!</small></td>
             <td>
             <?php if (!empty($grupos)) { ?>
               <?php foreach ($grupos as $item) : ?>
               <?php if(in_array($item->id,$this->grp_filter)) : ?>
               <label>
                 <input type="checkbox" name="grupos[]" value="<?php echo $item->id; ?>" id="grupos_<?php echo $item->id; ?>" onclick="document.getElementById('mexeu').value=1" <?php if (in_array($item->id,$grupos_usuario)) echo ' checked="checked" '; ?>  />
                 <?php echo '<abbr title="'.$item->desc.'">' . $item->name .'</abbr>'; ?></label>
               <br />
               <?php endif; ?>
               <?php endforeach; ?>
             <?php } else echo 'Atenção: crie grupos antes de criar os usuários!'; ?>
             </td>
          </tr>
          <tr>
            <td>Associar ao(s) formulário(s)<br /><small>Apenas no caso de cooperativas!</small></td>
             <td>
             <?php if (!empty($enquetes)) { ?>
               <?php foreach ($enquetes as $item) : ?>
               <label>
                 <input type="checkbox" name="enquetes[]" value="<?php echo $item->id; ?>" id="enquetes_<?php echo $item->id; ?>" onclick="document.getElementById('mexeu').value=1" <?php if (in_array($item->id,$enquetes_usuario)) echo ' checked="checked" '; ?>  />
                 <?php echo '<abbr title="'.$item->desc.'">' . $item->title .'</abbr>'; ?></label>
               <br />
               <?php endforeach; ?>
             <?php } else echo 'Atenção: crie as enquetes antes!'; ?>
             </td>
          </tr>
          <tr>
             <td>CPF/CNPJ</td>
             <td><label>
               <input type="text" name="username" id="username"  disabled="disabled" maxlength="30" value="<?php echo $valor->username; ?>" />
             </label></td>
          </tr>
          <tr>
            <td>Senha</td>
            <td><label>
              <input type="password" name="password" id="password" value="" />
            </label></td>
          </tr>
          <tr>
            <td>Redigite a senha</td>
            <td><label>
              <input type="password" name="password_again" id="password_again" value="" />
            </label><br />
            <input name="change" type="checkbox" value="1" /> Desejo mudar a senha do usuário.</td>
          </tr>
          <tr>
            <td width="30%">Nome</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" value="<?php echo $valor->name; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">E-mail</td>
            <td><label>
              <input name="email" id="email" type="email" placeholder="nome@exemplo.com" required="required" value="<?php echo $valor->email; ?>" /><div id="email_ok"></div>
            </label></td>
          </tr>
           <tr>
             <td>Telefone</td>
             <td><label>
               <input type="tel" name="phone" id="phone" value="<?php echo $valor->phone; ?>" />
             </label></td>
           </tr>
            <tr>
             <td>Cidade</td>
             <td><label>
               <input type="text" name="city" id="city" required="required" value="<?php echo $valor->city; ?>" />
             </label></td>
           </tr>
            <tr>
             <td>UF</td>
             <td><select name="uf" id="uf" required="required">
                <option value="" <?php echo ($valor->uf == '' ? 'selected' : ''); ?>>--</option>
                <option value="AC" <?php echo ($valor->uf == 'AC' ? 'selected' : ''); ?>>AC</option>  
                <option value="AL" <?php echo ($valor->uf == 'AL' ? 'selected' : ''); ?>>AL</option>  
                <option value="AM" <?php echo ($valor->uf == 'AM' ? 'selected' : ''); ?>>AM</option>  
                <option value="AP" <?php echo ($valor->uf == 'AP' ? 'selected' : ''); ?>>AP</option>  
                <option value="BA" <?php echo ($valor->uf == 'BA' ? 'selected' : ''); ?>>BA</option>  
                <option value="CE" <?php echo ($valor->uf == 'CE' ? 'selected' : ''); ?>>CE</option>  
                <option value="DF" <?php echo ($valor->uf == 'DF' ? 'selected' : ''); ?>>DF</option>  
                <option value="ES" <?php echo ($valor->uf == 'ES' ? 'selected' : ''); ?>>ES</option>  
                <option value="GO" <?php echo ($valor->uf == 'GO' ? 'selected' : ''); ?>>GO</option>  
                <option value="MA" <?php echo ($valor->uf == 'MA' ? 'selected' : ''); ?>>MA</option>  
                <option value="MG" <?php echo ($valor->uf == 'MG' ? 'selected' : ''); ?>>MG</option>  
                <option value="MS" <?php echo ($valor->uf == 'MS' ? 'selected' : ''); ?>>MS</option>  
                <option value="MT" <?php echo ($valor->uf == 'MT' ? 'selected' : ''); ?>>MT</option>  
                <option value="PA" <?php echo ($valor->uf == 'PA' ? 'selected' : ''); ?>>PA</option>  
                <option value="PB" <?php echo ($valor->uf == 'PB' ? 'selected' : ''); ?>>PB</option>  
                <option value="PE" <?php echo ($valor->uf == 'PE' ? 'selected' : ''); ?>>PE</option>  
                <option value="PI" <?php echo ($valor->uf == 'PI' ? 'selected' : ''); ?>>PI</option>  
                <option value="PR" <?php echo ($valor->uf == 'PR' ? 'selected' : ''); ?>>PR</option>  
                <option value="RJ" <?php echo ($valor->uf == 'RJ' ? 'selected' : ''); ?>>RJ</option>  
                <option value="RN" <?php echo ($valor->uf == 'RN' ? 'selected' : ''); ?>>RN</option>  
                <option value="RO" <?php echo ($valor->uf == 'RO' ? 'selected' : ''); ?>>RO</option>  
                <option value="RR" <?php echo ($valor->uf == 'RR' ? 'selected' : ''); ?>>RR</option>  
                <option value="RS" <?php echo ($valor->uf == 'RS' ? 'selected' : ''); ?>>RS</option>  
                <option value="SC" <?php echo ($valor->uf == 'SC' ? 'selected' : ''); ?>>SC</option>  
                <option value="SE" <?php echo ($valor->uf == 'SE' ? 'selected' : ''); ?>>SE</option>  
                <option value="SP" <?php echo ($valor->uf == 'SP' ? 'selected' : ''); ?>>SP</option>  
                <option value="TO" <?php echo ($valor->uf == 'TO' ? 'selected' : ''); ?>>TO</option>
                </select></td>
                   </tr>
           <tr>
             <td><abbr title="Ativar usuário para acesso ao sistema">Ativar usuário?</abbr></td>
             <td><select name="islocked" id="islocked">
               <option value="no" <?php if ($valor->islocked == 'no') echo 'selected="selected"'; ?>>Sim</option>
               <option value="yes" <?php if ($valor->islocked == 'yes') echo 'selected="selected"'; ?>>N&atilde;o</option>
             </select></td>
           </tr>
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%"><input type="hidden" name="mexeu" id="mexeu" value="0" />
            <input name="remover" type="checkbox" value="<?php echo $valor->id; ?>" /> Remover usuário e todos os dados associados (cuidado com esta opção)!</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/grupos/index'"> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
<?php endforeach; ?>
</div>
<div id="tabs-2">
<h3>Dados cadastrais (adquiridos via formulário)</h3>
        <hr />
        <table width="100%" border="0">
        <?php foreach ($dados_cadastrais as $titulo => $conteudo) : ?>
        <tr>
        <td><strong><?php echo $conteudo['title']; ?></strong></td>
        <td><?php echo $conteudo['content']; ?></td>
        </tr>
        <?php endforeach; ?>
        </table>
</div>
</div>
        </div>
  	</div>
  	
  	</div>
</div>