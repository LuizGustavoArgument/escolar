<div class="span-24 miolo">
    
    <div class="span-22">

			<div class="prepend-2">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>

        
        <div class="conteudo">
        <h3>Criar novo usuário do sistema</h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('usuarios/criar'); ?>">
        <table width="100%" border="0">
          <tr>
            <td width="30%">Nível</td>
            <td width="70%"><select name="isadmin" id="isadmin">
              <option value="yes">Administrador</option>
              <option value="no" selected="selected">Usuário comum</option>
            </select><br /><input type="checkbox" name="idhist" id="idhist_1" value="1" /> Confirmou e preencheu todos os formulários (no caso de Cooperativa)
             </td>
          </tr>
           <tr>
            <td width="30%">Organização</td>
            <td width="70%"><select name="idorg" id="idorg">
              <option value="1">Padrão</option>
            </select></td>
          </tr>
          <tr>
            <td>Associar ao(s) grupo(s)<br /><small>É obrigatório selecionar pelo menos um!</small></td>
             <td>
             <?php if (!empty($grupos)) { ?>
               <?php foreach ($grupos as $item) : ?>
               <?php if(in_array($item->id,$this->grp_filter)) : ?>
               <label>
                 <input type="checkbox" name="grupos[]" value="<?php echo $item->id; ?>" id="grupos_<?php echo $item->id; ?>" <?php if(!empty($grupo_marcado) && $grupo_marcado == $item->id) echo 'checked="checked"'; ?> />
                 <?php echo '<abbr title="'.$item->desc.'">' . $item->name .'</abbr>'; ?></label>
               <br />
               <?php endif; ?>
               <?php endforeach; ?>
             <?php } else echo 'Atenção: crie grupos antes de criar os usuários!'; ?>
             </td>
          </tr>
          <tr>
             <td>Associar ao(s) formulário(s)<br /><small>Apenas no caso de cooperativas!</small></td>
             <td>
             <?php if (!empty($enquetes)) { ?>
               <?php foreach ($enquetes as $item) : ?>
               <label>
                 <input type="checkbox" name="enquetes[]" value="<?php echo $item->id; ?>" id="enquetes_<?php echo $item->id; ?>" />
                 <?php echo '<abbr title="'.$item->desc.'">' . $item->title .'</abbr>'; ?></label>
               <br />
               <?php endforeach; ?>
             <?php } else echo 'Atenção: crie as enquetes antes!'; ?>
             </td>
          </tr>
          <tr>
             <td>CPF/CNPJ</td>
             <td><label>
               <input type="text" name="username" id="username" required="required" maxlength="30" value="<?php echo set_value('username'); ?>" /><div id="username_ok"></div>
             </label><br /><small><strong>Atenção:</strong><br>Formato <u>CNPJ</u> <em>11.111.111/1111-11</em> (no caso de cooperativa)<br>Formato <u>CPF</u> <em>111.111.111-11</em>, no caso de outros usuários.<br> <strong>SEMPRE utilize os pontos e traços!</strong></small></td>
          </tr>
          <tr>
            <td>Senha</td>
            <td><label>
              <input type="password" name="password" id="password" required="required" value="<?php echo set_value('password'); ?>" />
            </label></td>
          </tr>
          <tr>
            <td>Redigite a senha</td>
            <td><label>
              <input type="password" name="password_again" id="password_again" required="required" value="<?php echo set_value('password_again'); ?>" />
            </label><div id="passhow" style="display:none"></div></td>
          </tr>
          <tr>
            <td width="30%">Nome</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" value="<?php echo set_value('name'); ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">E-mail</td>
            <td><label>
              <input name="email" id="email" type="email" placeholder="nome@exemplo.com" required="required" value="<?php echo 'usu@usu.com';//echo set_value('email'); ?>" /><div id="email_ok"></div>
            </label></td>
          </tr>
           <tr>
             <td>Telefone</td>
             <td><label>
               <input type="tel" name="phone" id="phone" />
             </label></td>
           </tr>
           <tr>
             <td>Cidade</td>
             <td><label>
               <input type="text" name="city" id="city" required="required" value="<?php echo set_value('city'); ?>" />
             </label></td>
           </tr>
            <tr>
             <td>UF</td>
             <td><select name="uf" id="uf" required="required">
                <option value="">--</option>
        <option value="AC">AC</option>  
        <option value="AL">AL</option>  
        <option value="AM">AM</option>  
        <option value="AP">AP</option>  
        <option value="BA">BA</option>  
        <option value="CE">CE</option>  
        <option value="DF">DF</option>  
        <option value="ES">ES</option>  
        <option value="GO">GO</option>  
        <option value="MA">MA</option>  
        <option value="MG">MG</option>  
        <option value="MS">MS</option>  
        <option value="MT">MT</option>  
        <option value="PA">PA</option>  
        <option value="PB">PB</option>  
        <option value="PE">PE</option>  
        <option value="PI">PI</option>  
        <option value="PR">PR</option>  
        <option value="RJ">RJ</option>  
        <option value="RN">RN</option>  
        <option value="RO">RO</option>  
        <option value="RR">RR</option>  
        <option value="RS">RS</option>  
        <option value="SC">SC</option>  
        <option value="SE">SE</option>  
        <option value="SP">SP</option>  
        <option value="TO">TO</option>
        </select></td>
           </tr>
           <tr>
             <td><abbr title="Ativar usuário para acesso ao sistema">Ativar usuário?</abbr></td>
             <td><select name="islocked" id="islocked">
               <option value="no" selected="selected">Sim</option>
               <option value="yes">N&atilde;o</option>
             </select></td>
           </tr>
           <tr>
           <td colspan="2">Obs: os outros dados cadastrais (endereço, cpf, etc) serão preenchidos assim que o usuário preencher os dados cadastrais (no caso de cooperativa).</td>
           </tr>
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/grupos/index'"> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
		</div>

		
	</div>