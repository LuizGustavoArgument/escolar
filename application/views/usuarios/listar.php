<div class="block">
		<div class="column span-15 colborder">

			<div class="prepend-2">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <h3>Lista de entrevistados</h3>
        <hr />
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
    <td>Nome</td>
    <td>Grupo(s)</td>
    <td>E-mail</td>
    <td>Data de criação</td>
    <td>Progresso</td>
  </tr>
  </thead>
  <tbody>
  <?php foreach($resultado_query as $item) : ?>
  <tr>
  <?php
  $grupostmp = $this->banco->relacao('users',$item->id,'groups');
  $gruposnames = array();
  if (!empty($grupostmp))
  {
  	foreach($grupostmp as $grp)
  		$gruposnames[] = $grupos[$grp];
  	$grupos_names = implode(', ',$gruposnames);
  }
  else $grupos_names = '<span style="color:red">Sem grupo!</span>';
  ?>
    <td><strong><?php echo ($item->isadmin == 'yes' ? '<abbr title="Administrador do sistema">'.$item->name.'</abbr>' : $item->name); ?></strong></td>
    <td><?php echo $grupos_names; ?></td>
    <td><?php echo $item->email; ?></td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td><strong><?php echo ($item->islocked == 'yes' ? '<span style="color:red">Inapto.</span>&nbsp;<input type="button" id="liberando" name="'.$item->id.'" value="Liberar" />' : $usu_progress[$item->id]. '%'); //echo anchor('usuarios/editar/'.$item->id,'editar'); ?></strong></td>
  </tr>
  <?php endforeach; ?>
  </tbody>
  <tfoot>
  <tr>
    <td colspan="5"><?php //echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>

        </div>
  	</div>
  	</div>
    </div>
    <?php 
	// Somente se for coordenador ou administrador
	if ($this->session->userdata('nivel2') == 2) { ?>
    
	<script>
	$("#liberando").click(function(){
		alert("Por segurança, apenas os coordenadores do centro ou administradores do sistema podem liberar o entrevistado.\nVocê também poderá solicitar a liberação enviando um e-mail para contato@orange.inf.br com o seu nome e CPF do entrevistado.");
	});
	</script>
	
	
	
    <?php 
	} else {
	?>
	
    <script>
	$("#liberando").click(function(){
			$.ajax({ 
				url: "<?php echo base_url(); ?>usuarios/liberar/"+$("#liberando").attr("name"), 
				success: function(data){ 
					if (data == 'ok') 
						window.location.reload(true); 
					else 
						alert('Falha ao liberar usuário. Entre em contato com a ORANGE eTI (contato@orange.inf.br) e informe o CPF do usuário.');
				}
			});
	});
	</script>
    
    
    <?php
	}
	?>