<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Enviar solicitação de criação ou alteração de documento</h3>
        <hr />
        <?php echo form_open('mensagens/criar_solicitacao/'.$folderid ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        
        <table width="100%" border="0">
          <tr>
            <td width="30%">Pasta selecionada</td>
            <td width="70%">
              <?php echo $folder_atual; ?>
            </td>
          </tr>
           <tr>
            <td>Solicitante</td>
            <td><label>
              <select name="from_user" id="from_user">
                <?php echo $from_user; ?>
              </select>
            </label>
             </td>
          </tr>
           <tr>
            <td>Responsável</td>
            <td><label>
              <select name="to_user" id="to_user">
                <?php echo $to_user; ?>
              </select>
            </label></td>
          </tr>
          <tr>
            <td>Status</td>
            <td><label>
              <select name="status" id="status">
                <?php echo $status; ?>
              </select>
            </label></td>
          </tr>
          <tr>
            <td width="30%">Assunto</td>
            <td width="70%">
              <label>
                <input type="text" name="subject" id="subject" required="required" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Mensagem</td>
            <td><textarea name="message" id="message" cols="45" rows="5" required="required" ></textarea></td>
          </tr>
          
          
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Enviar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/home'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
  	</section>