<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Editar solicitação</h3>
        <hr />
        <?php echo form_open('mensagens/editar/'.$idsol ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        
        <table width="100%" border="0">
          <tr>
            <td width="30%">Pasta selecionada</td>
            <td width="70%">
              <?php echo $folder_atual; ?>
            </td>
          </tr>
           <tr>
            <td>Solicitante</td>
            <td><label>
              <select name="from_user" id="from_user" <?php if ($status_item == 4 || $status_item == 5) echo 'disabled="disabled" '; ?>>
                <?php echo $from_user; ?>
              </select>
            </label>
             </td>
          </tr>
           <tr>
            <td>Responsável</td>
            <td><label>
              <select name="to_user" id="to_user" d <?php if ($status_item == 4 || $status_item == 5) echo 'disabled="disabled" '; ?>>
                <?php echo $to_user; ?>
              </select>
            </label></td>
          </tr>
          
          <tr>
            <td width="30%">Assunto</td>
            <td width="70%">
              <label>
                <input type="text" name="subject" id="subject" required="required" <?php if ($status_item == 4 || $status_item == 5) echo 'readonly="readonly" '; ?>  value="<?php echo $subject_text; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Mensagem</td>
            <td><textarea name="message" id="message" cols="45" rows="5" required="required" readonly="readonly" ><?php echo $message_text; ?></textarea></td>
          </tr>
          
          <?php if ($status_item != 4 && $status_item != 5) : // se não tiver concluido ou cancelado, mostra abaixo.?>
          <tr>
            <td valign="top">Resposta</td>
            <td><textarea name="message_reply" id="message_reply" cols="45" rows="5" ></textarea></td>
          </tr>
          <?php endif; ?>
          <tr bgcolor="#FFFFCC">
            <td><strong>Status</strong></td>
            <td><label>
              <select name="status" class="title" id="status" <?php if ($status_item == 4 || $status_item == 5) echo 'disabled="disabled" '; ?>>
                <?php echo $status; ?>
              </select>
            </label></td>
          </tr>
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td>
            <?php if ($status_item != 4 && $status_item != 5) : // se não tiver concluido ou cancelado, mostra abaixo. ?>
            <button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Enviar</button>
            <?php endif; ?>
            </td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/home'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
  	</section>