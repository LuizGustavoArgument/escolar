

        
        <hr />
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
  	<td>De</td>
    <td>Para</td>
    <td>Assunto</td>
    <td>Status</td>
    <td>Data de criação</td>
    <td>&nbsp;</td>
  </tr>
  </thead>
  <tbody>

  <?php if (!empty($resultado_query)) { ?>

  <?php foreach($resultado_query as $item) : ?>
 
  <tr>
    <td><abbr title="<?php echo $emails[$item->from_user]; ?>"><?php echo $usuarios[$item->from_user]; ?></abbr></td>
    <td>
    <abbr title="<?php echo $emails[$item->to_user]; ?>"><?php echo $usuarios[$item->to_user]; ?></abbr>
    </td>
    <td><?php echo $item->subject; ?></td>
    <td><?php echo $statuses[$item->status]; ?></td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
	<?php 
	if ($item->status < 4) 
	{ 
		if ($item->from_user == $this->session->userdata('esta_logado'))
			echo '<a href="#" onclick="comentario('.$item->id .');">comentar</a>&nbsp;|&nbsp;' . anchor('mensagens/remover/'.$item->id,'remover');
		elseif ($item->to_user == $this->session->userdata('esta_logado'))
			echo anchor('mensagens/editar/'.$item->id,'editar');
	} 
	else 
	{ 
		if ($item->from_user == $this->session->userdata('esta_logado'))
			echo anchor('mensagens/editar/'.$item->id,'ver') . '&nbsp;|&nbsp;' . anchor('mensagens/remover/'.$item->id,'remover');
		else
		    echo anchor('mensagens/editar/'.$item->id,'ver');
		//else
			//echo anchor('mensagens/ocultar/'.$item->id,'ocultar');
	}
	?>
    </td>
  </tr>
  <?php endforeach; ?>
  <?php } else { echo '<tr><td colspan=6>Nenhuma solicitação encontrada.</td></tr>'; }?>

  </tbody>
  <tfoot>
  <tr>
    <td colspan="6"><?php //echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>
<div id="caixaComentario" title="Novo comentário"></div>
<?php
if (!empty($resultado_query))
{
// Scripts para o DATATABLE, devido ao AJAX...
$jscript = '
$(\'#datatable\').dataTable( {
	"aaSorting": [ [0,\'asc\'], [1,\'asc\'] ],
	"oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" }
} );
';

$jscript = '
$("#caixaComentario").dialog({
	autoOpen: false,
	height: 540,
	width: 430,
	modal: true,
	buttons: {
		"Salvar": function() {
			var texto = $("textarea#message_reply").val();
			if ( texto.length > 0 ) {
				$("form#formCom").submit();
				$( this ).dialog( "close" );
			} else { alert("Escreva a sua resposta!"); }
		},
		Cancel: function() {
			$( this ).dialog( "close" );
		}
	}							 
});
';

$js_out = '
jQuery.fn.dataTableExt.oSort[\'string-case-asc\']  = function(x,y) {
	return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort[\'string-case-desc\'] = function(x,y) {
	return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};
';

$js_out .= '
function comentario(id){
	$("#caixaComentario").load("'. site_url('mensagens/comentar/') .'/"+id);
	$("#caixaComentario").dialog("open");
	return false;
}
';
}
else
{
$jscript = '';
$js_out = '';
}
?>

<script language="javascript">
<!--
$(document).ready(function() {
	<?php echo $jscript; ?>					   
});
<?php echo $js_out; ?>	
-->
</script>