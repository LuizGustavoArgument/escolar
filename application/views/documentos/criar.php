<script language="javascript">
function mudaOpcao(valor1,valor2)
{
	document.getElementById(valor1+'_tit').style.display = 'none';
	document.getElementById(valor2+'_tit').style.display = 'block';
	
	document.getElementById(valor1+'_val').style.display = 'none';
	document.getElementById(valor2+'_val').style.display = 'block';
	
	if (valor2 == 'opcao2')
	document.getElementById('statusPasta').style.display = 'none';
	else
	document.getElementById('statusPasta').style.display = 'block';
}

function verificaPasta(pasta)
{
	$("#statusPasta").load("<?php echo site_url('documentos/verificaPasta'); ?>/"+pasta);
	return false;
}
</script>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div class="conteudo">
        <h3>Criar novo documento</h3>
        
        <hr />
        <?php echo form_open('documentos/criar/'.$folderid ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        <table width="100%" border="0">
          <tr>
            <td width="30%">Pasta selecionada</td>
            <td width="70%"><select name="folder_list" id="folder_list" onchange="verificaPasta(this.value)">
              <?php echo $folder_list; ?>
            </select><small id="statusPasta"></small></td>
          </tr>
          <tr>
            <td>Selecione uma a&ccedil;&atilde;o</td>
            <td>
              <label>
                <input type="radio" name="document_option" value="opcao1" id="document_option_0" onclick="mudaOpcao(this.value,'opcao2');" />
                Enviar arquivo</label>
              <br />
              <label>
                <input name="document_option" type="radio" id="document_option_1" value="opcao2" checked="checked" onclick="mudaOpcao(this.value,'opcao1');" />
                Criar a partir de template</label>
              <br />
           </td>
          </tr>
          </table>
          <table width="100%" border="0" id="opcao1">
          <tr>
            <td><div id="opcao1_tit">Template selecionado</div><div id="opcao2_tit" style="display:none">Envie um arquivo</div></td>
            <td><div id="opcao1_val"><select name="template_list" id="template_list" onchange="verificaPasta(document.getElementById('folder_list').value)">
              <?php echo $templates; ?>
            </select></div>
            <div id="opcao2_val" style="display:none"><input name="filepath" type="file" /><br /><small>Enviar vários arquivos? <?php echo anchor('documentos/criar_varios','Clique aqui!'); ?></small></div></td>
          </tr>
          <tr>
            <td width="30%">Nome do documento</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ></textarea></td>
          </tr>
          <tr>
            <td>Tipo de documento</td>
            <td><label>
              <select name="type" id="type">
                <?php echo $tipos_documentos; ?>
              </select>
            </label></td>
          </tr>
           <tr>
             <td><abbr title="Ou seja, não permitirá a alteração dos dados do documento (ou o upload do mesmo arquivo alterado, no caso de arquivo enviado)">Somente leitura?</abbr></td>
             <td><select name="readonly" id="readonly">
               <option value="yes">Sim</option>
               <option value="no" selected="selected">N&atilde;o</option>
             </select></td>
           </tr>
           <tr>
            <td>Tipo de permiss&atilde;o</td>
            <td><label>
              <select name="permission" id="permission">
                <option value="0" selected="selected">Herdar da configuração de grupos</option>
              </select>
            </label></td>
          </tr>
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="button" class="button negative" onclick="history.back();"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
  	</section>