
<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
        <div style="float:left"><h3>Arquivos da pasta <abbr title="<?php echo $desc; ?>"><?php echo $name; ?></abbr></h3><?php /*if (!stristr($this->uri->segment(2),'fullscreen')) { echo anchor('documentos/listar_fullscreen/'.$this->uri->segment(3),'Ver em tela cheia'); } */ ?></div>
        <div style="float:right"><?php echo $perm_edit; ?></div>
        <div style="clear:both"><h4><?php echo $desc; ?></h4>
        <h6 align="right"><?php echo $perm_text; ?>&nbsp;&nbsp;<?php echo $perm_link; ?>.</h6></div>
        <hr />
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <tr>
  	<td>Cód.</td>
    <td>Nome</td>
    <td>Descrição</td>
    <td>Criado por</td>
    <td>Data de criação</td>
    <td>&nbsp;</td>
  </tr>
  </thead>
  <tbody>
  <?php if ($perm < 4) { ?>
  <?php if (!empty($resultado_query)) { ?>
  <?php $CI =& get_instance(); ?>
  <?php foreach($resultado_query as $item) : ?>
  <?php //$icon = '<img src="'.$icons[$CI->verifica_mime_type($item->type)].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; ?>
  <?php 
  if(empty($icons[$CI->verifica_mime_type($item->type)])) $ico = $icons['txt']; else $ico = $icons[$CI->verifica_mime_type($item->type)];
  $icon = '<img src="'.$ico.'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />'; 
  ?>
  <tr>
    <td>
    <abbr title="Identificador único do documento">
    <?php
	// Gera a numeracao de acordo com a opcao
	echo $CI->gera_numeracao($item->id,strtotime($item->date_created));
	?>
    </abbr></td>
    <td>
	<?php if (empty($item->content)) { echo $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>"; } else { ?>
	<?php echo $icon; ?>&nbsp;<strong><?php echo $item->name; ?></strong>
    <?php } ?>
    </td>
    <td><?php echo (strstr($item->desc,'1') ? '...' : word_limiter($item->desc,15)); ?></td>
    <td><?php echo $usuarios[$item->idusr]; ?> (<?php echo $grupos[$item->idgrp]; ?>)</td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
	<?php 
	// se for o admin ou o dono do arquivo, permite alteração
	if ($perm == 1 || ($item->idusr == $this->session->userdata('esta_logado')))
	{ echo anchor('documentos/editar/'.$item->id,'editar') . '&nbsp;|&nbsp;'; }
	// Se tiver travado, não pode baixar!
	if ($item->locked == 'yes') { echo '<small style="color:red">Travado!</small>'; } else {
	$baixar = base64_encode($item->id . '|||' . $item->date_created); echo anchor('documentos/download/'.$baixar,'baixar'); }
    // Cópia de documentos
    if ($perm == 1 || ($item->idusr == $this->session->userdata('esta_logado')))
    {
    $cp = site_url('documentos/copiando/'.base64_encode($item->id . '|||' . $folderid . '|||' . $item->date_created));
    echo '&nbsp;|&nbsp;<a href="javascript:void(0);" onclick="$.get(\''.$cp.'\',function(data){$(\'#caixa-copia\').html(data); $(\'#caixa-copia\').dialog(\'open\');});">copiar</a>';
    }
	?>
    </td>
  </tr>
  <?php endforeach; ?>
  <?php } else { echo '<tr><td></td><td>Nenhum arquivo encontrado nesta pasta.</td><td></td><td></td><td></td><td></td></tr>'; }?>
  <?php } else { echo '<tr><td></td><td>Os arquivos desta pasta estão ocultos.</td><td></td><td></td><td></td><td></td></tr>'; } ?>
  </tbody>
  <tfoot>
  <tr>
    <td colspan="6"><?php //echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>
<script>
    $(function() {
        $( "#caixa-copia" ).dialog({
            resizable: false,
            autoOpen: false,
            show: "blind",
            modal: true,
            buttons: {
                "Copiar": function() {
                    var info1 = $("form#copia #folder_de").val();
                    var info2 = $("form#copia #folder_para").val();
                    var info3 = $("form#copia #file_id").val();
                    $.get("<?php echo site_url('documentos/copiando2'); ?>/"+info1+"/"+info2+"/"+info3,function(data){
                       if(data == "ok")
                       {
                           alert('O arquivo foi copiado com sucesso.');
                       }
                       else
                       {
                           alert('Falha ao copiar arquivo: '+data);
                       }
                    });
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
</script>
<div id="caixa-copia" title="Cópia de arquivos"></div>
 <?php echo ($sol_list ? $sol_list : ''); ?>
             <div id="sol_list"></div>

</section>
