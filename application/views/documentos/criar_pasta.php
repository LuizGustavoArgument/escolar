<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
			<section>
        <div class="conteudo">
        <h3>Criar nova pasta</h3>
        <hr />
        <?php echo form_open('documentos/criar_pasta' ,array('name' => 'form1', 'id' => 'form1')); ?>
        <table width="100%" border="0">
          <tr>
            <td width="30%">Criar dentro da pasta</td>
            <td width="70%"><select name="folder_list" id="folder_list" required="required">
              <?php echo $folder_list; ?>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Nome da pasta</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ></textarea></td>
          </tr>
          <tr>
            <td>Acessível ao(s) grupo(s)</td>
            <td><label>
              Após a criação da pasta, configure o acesso na sessão <?php echo anchor('grupos/index','grupos',array('target' => '_blank')); ?>
            </label></td>
          </tr>
          
           
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/documentos/index'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
        </form>
        </div>
  	</section>