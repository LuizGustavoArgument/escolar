<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
			<section>
<?php foreach ($resultado_query as $item) : ?>
        <div class="conteudo">
        <h3>Editar pasta <?php echo $item->name; ?></h3>
        <hr />
        <?php echo form_open('documentos/editar_pasta/'. $item->nameid ,array('name' => 'form1', 'id' => 'form1')); ?>
        <table width="100%" border="0">
          <tr>
            <td width="30%">Criada dentro da pasta</td>
            <td width="70%">
              <?php echo $folder_list; ?>
            </td>
          </tr>
          <tr>
            <td width="30%">Nome da pasta</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" value="<?php echo $item->name; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ><?php echo $item->desc; ?></textarea></td>
          </tr>
          <tr>
            <td>Acessível ao(s) grupo(s)</td>
            <td>
                <?php echo $grupos; ?>
                <label>
              Configure o acesso a esta pasta na sessão <?php echo anchor('grupos/index','grupos',array('target' => '_blank')); ?>
            </label>
            </td>
          </tr>
          <tr>
            <td rowspan="3">Opções</td>
            <td>
              
                 
                   <input type="radio" name="num" value="0" id="nums_0" <?php echo $opcoes[0]; ?> />
                   Utilizar numeração única automática do sistema (padrão)
               
                   
              
                   
                
            </td>
          </tr>
          <tr>
            <td><input type="radio" name="num" value="99" id="nums_1" <?php echo $opcoes[1]; ?> />
                   Numerar a partir do número 
                   <input name="num_val" id="num_val" type="text" size="1" value="<?php echo $opcoes[3]; ?>" /></td>
          </tr>
          <tr>
            <td><input type="radio" name="num" value="-1" id="nums_2" <?php echo $opcoes[2]; ?> />
                   Numerar os documentos, agrupando-os por ano <br />(ex: 33/2011, 34/2011, 1/2012)</td>
          </tr>
           
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%"><input name="remover" type="checkbox" value="<?php echo $item->id; ?>" /> Remover a pasta e seus arquivos</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/documentos/index'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
        </form>
<?php endforeach; ?>
        </div>
  	</section>
  	