<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>

        <div style="float:left"><h3><?php echo $name_tpl; ?> (<abbr title="<?php echo $desc; ?>"><?php echo $name; ?></abbr>)</h3></div>
        <div style="float:right"><?php echo $perm_edit; ?></div>
        <div style="clear:both"><h4><?php echo $desc_tpl; ?><br /><small>Criado em <?php echo $date_tpl; ?></small></h4>
        <h6 align="right"><?php echo $perm_text; ?>&nbsp;&nbsp;<?php echo $perm_link; ?>.</h6></div>
        <hr />
<div style="overflow:auto">
  <table width="100%" border="0" id="datatable">
  <thead class="ui-state-default">
  <!-- Monta as colunas -->
  <tr>
  <?php $col = 0; foreach($cols_tpl as $label) { ?>
  	<td><?php echo $label; $col++; ?></td>
  <?php } ?>
  </tr>
  <!-- Fim da montagem das colunas -->
  </thead>
  <tbody>
  <?php $c = 0; ?>
  <?php $CI =& get_instance(); ?>
  <?php if ($perm < 4) { ?>
  <?php if (!empty($resultado_query)) { ?>
  <?php foreach($resultado_query as $item) : ?>
  <?php if ($c > 0) { ?>
  <?php if (empty($item->contenttpl) && !is_array(@unserialize($item->content))) { // lista documentos de campo simples ?>
  <tr>
    <td><abbr title="Identificador único do documento">
    <?php
	// Gera a numeracao de acordo com a opcao
	echo $CI->gera_numeracao($item->id,strtotime($item->date_created));
	?>
    </abbr></td>
    <td>
	<?php if (empty($item->content)) { echo $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>"; } else { ?>
	<strong><?php echo $item->name; ?></strong>
    <?php } ?>
    </td>
    <td><?php echo $item->desc; ?></td>
    <td><?php echo $usuarios[$item->idusr]; ?> (<?php echo $grupos[$item->idgrp]; ?>)</td>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
	<?php 
	// se for o admin ou o dono do arquivo, permite alteração
	if ($perm == 1 || ($item->idusr == $this->session->userdata('esta_logado')))
	{ echo anchor('documentos/editar_registro/'.$folderid.'/'.$item->id,'editar') . '&nbsp;|&nbsp;'; }
	// Se tiver travado, não pode baixar!
	if ($item->locked == 'yes') { echo '<small style="color:red">Travado!</small>'; } else {
	$baixar = base64_encode($item->id . '|||' . $item->date_created); echo anchor('documentos/download/'.$baixar,'baixar'); }
	?>
    </td>
  </tr>
  <?php } elseif (empty($item->contenttpl) && is_array(@unserialize($item->content))) { // lista documentos de multiplo campos (sem regioes) ?>
  <tr>
    <td><abbr title="Identificador único do documento">
    <?php
	// Gera a numeracao de acordo com a opcao
	echo $CI->gera_numeracao($item->id,strtotime($item->date_created));
	?>
    </abbr></td>
    <?php
	$conts = unserialize($item->content);
	if (!is_array($conts)) $conts = array();
	
	for($i=0;$i<($col);$i++) :
	if (!empty($cols_asterisk[$i+1]) && $cols_asterisk[$i+1] == $i+1)
		echo '<td>'.(!empty($conts[$i]) ? word_limiter(strip_tags($conts[$i]),15) : '').'</td>';
	endfor;
	//print_r($cols_asterisk);
	?>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
	<?php 
	// se for o admin ou o dono do arquivo, permite alteração
	if ($perm == 1 || ($item->idusr == $this->session->userdata('esta_logado')))
	{ echo anchor('documentos/editar_registro/'.$folderid.'/'.$item->id,'editar') . '&nbsp;|&nbsp;'; }
	if ($item->locked == 'yes') { echo '<small style="color:red">Travado!</small>'; } else {
	$baixar = base64_encode($item->id . '|||' . $item->date_created); echo anchor('documentos/download/'.$baixar,'baixar'); }
	?>
    </td>
  </tr>
  <?php } else { // lista de documentos de multiplos campos (com regioes) ?>
  <tr>
    <td><abbr title="Identificador único do documento">
    <?php
	// Gera a numeracao de acordo com a opcao
	echo $CI->gera_numeracao($item->id,strtotime($item->date_created));
	?>
    </abbr></td>
    <?php
	$conts = unserialize($item->content);
	if (!is_array($conts)) $conts = array();
	for($i=0;$i<($col-3);$i++) :
		echo '<td>'.(!empty($conts[$i]) ? word_limiter(strip_tags($conts[$i]),15) : '').'</td>';
	endfor;
	?>
    <td><?php echo date('d/m/Y H:i',strtotime($item->date_created)); ?></td>
    <td>
	<?php 
	// se for o admin ou o dono do arquivo, permite alteração
	if ($perm == 1 || ($item->idusr == $this->session->userdata('esta_logado')))
	{ echo anchor('documentos/editar_registro/'.$folderid.'/'.$item->id,'editar') . '&nbsp;|&nbsp;'; }
	if ($item->locked == 'yes') { echo '<small style="color:red">Travado!</small>'; } else {
	$baixar = base64_encode($item->id . '|||' . $item->date_created); echo anchor('documentos/download/'.$baixar,'baixar'); }
	?>
    </td>
  </tr>
  <?php } // fim dos tipos de listagem ?>
  <?php } $c++; ?>
  <?php endforeach; ?>
  <?php } else { echo '<tr><td></td><td>Nenhum documento encontrado nesta pasta.</td><td></td><td></td><td></td><td></td></tr>'; }?>
  <?php } else { echo '<tr><td></td><td>Os documentos desta pasta estão ocultos.</td><td></td><td></td><td></td><td></td></tr>'; } ?>
  <?php 
  if ($c == 1) { 
  	echo '<tr>';
  	for($i=0;$i<$col;$i++)
		echo '<td></td>';
	echo '</tr><tr>';
	for($i=0;$i<$col;$i++)
	{
		if ($i == 1)
		echo '<td>Template definido, mas não há nenhum documento inserido.</td>'; 
		else
		echo '<td></td>';
	}
	echo '</tr>';
  } 
  ?>
  </tbody>
  <tfoot>
  <tr>
    <td colspan="<?php echo $col; ?>"><?php //echo $this->pagination->create_links(); ?></td>
  </tr>
  </tfoot>
</table>
</div>
<?php echo ($sol_list ? $sol_list : ''); ?>
             <div id="sol_list"></div>

</section>