<?php 
session_start(); 
$_SESSION['KCFINDER'] = array();
$_SESSION['KCFINDER']['disabled'] = false;
$_SESSION['KCFINDER']['uploadURL'] = $pasta_org;
?>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
			<section>
		<div class="conteudo">
        <h3>Criar novo registro baseado em template</h3>
        <hr />
        <?php echo form_open('documentos/criar_registro/'.$folderid ,array('name' => 'form1', 'id' => 'form1', 'target' => '_blank', 'enctype' => 'multipart/form-data')); ?>
        <table width="100%" border="0">
          
          <tr>
            <td width="30%">Nome do registro</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" />
              </label>
            </td>
          </tr>
           <tr>
             <td><abbr title="Ou seja, n�o permitir� a altera��o dos dados do documento (ou o upload do mesmo arquivo alterado, no caso de arquivo enviado)">Somente leitura?</abbr></td>
             <td><select name="readonly" id="readonly">
               <option value="yes">Sim</option>
               <option value="no" selected="selected">N&atilde;o</option>
             </select>
             <input type="hidden" name="preview" id="preview" value="0" />
             </td>
           </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc" id="desc" cols="45" rows="5" required="required" ></textarea></td>
          </tr>
          <?php echo $formulario; ?>
          
           
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive" onclick="document.getElementById('preview').value=0"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="submit" class="button" onclick='document.getElementById("preview").value=1;'><img src="<?php echo base_url();  ?>css/plugins/link-icons/icons/pdf.png" alt="visualizar"/> Visualizar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/documentos/index#<?php echo $folderid; ?>'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
        <div id="pdfview"></div>
        </form>
		</div>
  	</section>