<style type="text/css">
/* Drag drop upload styles */
#output
{
	/*min-width: 940px;*/
	min-height: 280px;
	padding: 10px;
	border: 2px dotted #fff;
	-moz-border-radius: 15px;
	text-align: center;
	position: relative;
}
#output:before
{
	content: "Arraste os arquivos para esta área.";
	color: #fff;
	font-size: 20px;
	font-weight: bold;
	opacity: 0.5;
	text-shadow: 1px 1px #000;
	position: absolute;
	width: 100%;
	left: 0;
	top: 50%;
	margin: -25px 0 0;
	z-index: 1;
}
#output ul li
{
	float: left;
	margin: 0 10px 10px 0;
	list-style: none;
	position: relative;
	z-index: 2;
}
#output	li a
	{
		opacity: 0.5;
	}
#output	li.loaded a
	{
		opacity: 1;
	}
#output li.loaded .progressBar
{
			display: none;
}
#output	li .progressBar
{
	position: absolute;
	top:50%;
	left: 50%;
	margin: -10px 0 0 -100px;
	width: 200px;
	height: 20px;
	border: 1px solid #000;
	-moz-border-radius: 10px;
	-moz-box-shadow: 1px 1px 2px #fff;
}
#output	li .progressBar p
{
	width: 20px;
	height: 20px;
	-moz-border-radius: 10px;
	background-color: #1E528C;
}
</style>
<script language="javascript">
function mudaOpcao(valor1,valor2)
{
	document.getElementById(valor1+'_tit').style.display = 'none';
	document.getElementById(valor2+'_tit').style.display = 'block';
	
	document.getElementById(valor1+'_val').style.display = 'none';
	document.getElementById(valor2+'_val').style.display = 'block';
	
	if (valor2 == 'opcao2')
	document.getElementById('statusPasta').style.display = 'none';
	else
	document.getElementById('statusPasta').style.display = 'block';
}

function verificaPasta(pasta)
{
	$("#statusPasta").load("<?php echo site_url('documentos/verificaPastaParaArquivo'); ?>/"+pasta);
	return false;
}

var TCNDDU = TCNDDU || {};
var boundary = '';
var builder = '';

//setInterval("$('#lista_arquivos').load('<?php echo site_url('documentos/lista_multi_arquivos'); ?>/'+(new Date).getTime());", 1000);

(function(){
	var dropContainer,
		dropListing,
		imgPreviewFragment = document.createDocumentFragment(),
		domElements;
	
	TCNDDU.setup = function () {
		dropListing = document.getElementById("output-listing01");
		dropContainer = document.getElementById("output");
		
		dropContainer.addEventListener("dragenter", function(event){dropListing.innerHTML = '';event.stopPropagation();event.preventDefault();}, false);
		dropContainer.addEventListener("dragover", function(event){event.stopPropagation();event.preventDefault();}, false);
		dropContainer.addEventListener("drop", TCNDDU.handleDrop, false);
	};
	
	/*TCNDDU.uploadProgressXHR = function (event) {
		if (event.lengthComputable) {
			var percentage = Math.round((event.loaded * 100) / event.total),
				loaderIndicator = event.target.log.firstChild.nextSibling.firstChild;
				console.log(loaderIndicator);
				console.log(event.target.self);
				console.log(event.target);
			if (percentage < 100) {
				loaderIndicator.style.width = (percentage*2) + "px";
				loaderIndicator.textContent = percentage + "%";
			}
		}
	};
	
	TCNDDU.loadedXHR = function (event) {
		var currentImageItem = event.target.log;
		
		currentImageItem.className = "loaded";
		console.log("xhr upload of "+event.target.log.id+" complete");
	};*/
	
	TCNDDU.uploadError = function (error) {
		console.log("error: " + error.code);
	};
	
	TCNDDU.processXHR = function (file, index, bin) {
		var xhr = new XMLHttpRequest(),
			container = document.getElementById("item"+index),
			fileUpload = xhr.upload,
			progressDomElements = [
				document.createElement('div'),
				document.createElement('p')
			];
		
		progressDomElements[0].className = "progressBar";
		progressDomElements[1].textContent = "0%";
		progressDomElements[0].appendChild(progressDomElements[1]);
		
		container.appendChild(progressDomElements[0]);
		
		fileUpload.log = container;
		
		fileUpload.addEventListener("progress", function(event) {
			if (event.lengthComputable) {
				var percentage = Math.round((event.loaded * 100) / event.total),
				loaderIndicator = container.firstChild.nextSibling.firstChild;
				if (percentage < 100) {
					loaderIndicator.style.width = (percentage*2) + "px";
					loaderIndicator.textContent = percentage + "%";
				}
			}
		}, false);
		
		fileUpload.addEventListener("load", function(event) {
			container.className = "loaded";
			console.log("xhr upload of "+container.id+" complete");
			$('#lista_arquivos').load('<?php echo site_url('documentos/lista_multi_arquivos'); ?>/'+(new Date).getTime());
		}, false);
		
		fileUpload.addEventListener("error", TCNDDU.uploadError, false);

		xhr.open("POST", "<?php echo site_url('documentos/envia_multi_arquivos'); ?>");
		//xhr.overrideMimeType('text/plain; charset=x-user-defined-binary');
		xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' 
			+ boundary);
		//xhr.sendAsBinary(bin)
		xhr.sendAsBinary(builder)
	};
	
	TCNDDU.handleDrop = function (event) {
		
		
		var dt = event.dataTransfer,
			files = dt.files,
			count = files.length;
		
		event.stopPropagation();
		event.preventDefault();
		
		
		//var xhr = new XMLHttpRequest();
		boundary = '------multipartformboundary' + (new Date).getTime();
    	var dashdash = '--';
    	var crlf     = '\r\n';

    	builder += dashdash;
    	builder += boundary;
    	builder += crlf;
		
		
		/* Para cada arquivo enviado... */
		for (var i = 0; i < count; i++) {
			//if(files[i].size < 1048576) {
				var file = files[i],
					droppedFileName = file.name,
					reader = new FileReader();
					reader.index = i;
					reader.file = file;
				
				if(typeof(FileReader.prototype.addEventListener) === "function")
					reader.addEventListener("loadend", TCNDDU.buildImageListItem, false);
				else
					reader.onload = TCNDDU.buildImageListItem;
				reader.readAsDataURL(file);
				
				// Outro metodo...
				/* Generate headers. */            
				builder += 'Content-Disposition: form-data; name="user_file[]"';
				if (droppedFileName) {
				  builder += '; filename="' + droppedFileName + '"';
				}
				builder += crlf;
		
				builder += 'Content-Type: application/octet-stream';
				builder += crlf;
				builder += crlf; 
		

				builder += file.getAsBinary();
				builder += crlf;
		

				builder += dashdash;
				builder += boundary;
				builder += crlf;
				
			//} else {
			//	alert("file is too big, needs to be below 1mb");
			//}
		}
		/* Mark end of the request. */
		builder += dashdash;
		builder += boundary;
		builder += dashdash;
		builder += crlf;
		$('#lista_arquivos').load('<?php echo site_url('documentos/lista_multi_arquivos'); ?>/'+(new Date).getTime());
	};
	
	TCNDDU.buildImageListItem = function (event) {
		domElements = [
			document.createElement('li'),
			document.createElement('a'),
			document.createElement('img'),
			document.createElement('p')
		];
	
		var data = event.target.result,
			index = event.target.index,
			file = event.target.file,
			getBinaryDataReader = new FileReader();
		
		domElements[2].src = data // base64 encoded string of local file(s)
		domElements[2].width = 300;
		domElements[2].height = 200;
		domElements[1].appendChild(domElements[2]);
		domElements[0].id = "item"+index;
		domElements[0].appendChild(domElements[1]);
		
		imgPreviewFragment.appendChild(domElements[0]);
		
		dropListing.appendChild(imgPreviewFragment);
		
		if(typeof(FileReader.prototype.addEventListener) === "function")
		getBinaryDataReader.addEventListener("loadend", function(evt){TCNDDU.processXHR(file, index, evt.target.result);}, false);
		else
		getBinaryDataReader.onload = function(evt){TCNDDU.processXHR(file, index, evt.target.result);};
		
		getBinaryDataReader.readAsBinaryString(file);
	};
	window.addEventListener("load", TCNDDU.setup, false);
})();

</script>

<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
	
        <div class="conteudo">
        <h3>Enviar vários arquivos</h3>
        <hr />
        <?php echo form_open('documentos/criar_varios/'.$folderid ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        <table width="100%" border="0">
          <tr>
            <td width="30%">Pasta selecionada</td>
            <td width="70%"><select name="folder_list" id="folder_list" onchange="verificaPasta(this.value)">
              <?php echo $folder_list; ?>
            </select><small id="statusPasta"></small></td>
          </tr>
          <tr><!-- <p></p><p></p><h3 align="center">Arraste os arquivos para esta área.</h3><p align="center"><strong>Tipos suportados:</strong> Arquivos do Office, OpenOffice, Imagens e ZIP.</p> -->
            <td colspan="2"><strong>Obs:</strong> Este recurso funciona apenas no <a href="http://www.getfirefox.com">Firefox 3.6</a> ou superior.<br /><strong>Formatos suportados:</strong> texto simples, PDF, arquivos do Office, imagens e ZIP.<br /><div id="output"><ul id="output-listing01"></ul></div></td>
          </tr>
          <tr>
            <td colspan="2"><div id="lista_arquivos"></div></td>
          </tr>
           <tr>
             <td><abbr title="Ou seja, não permitirá a alteração dos dados do documento (ou o upload do mesmo arquivo alterado, no caso de arquivo enviado)">Somente leitura?</abbr></td>
             <td><select name="readonly" id="readonly">
               <option value="yes">Sim</option>
               <option value="no" selected="selected">N&atilde;o</option>
             </select></td>
           </tr>
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar selecionados</button></td><td><button type="button" class="button negative" onclick="history.back();"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
  	</section>