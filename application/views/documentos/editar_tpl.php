<?php 
//session_start(); 
$_SESSION['KCFINDER'] = array();
$_SESSION['KCFINDER']['disabled'] = false;
$_SESSION['KCFINDER']['uploadURL'] = $pasta_org;
?>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
		<div class="conteudo">
        <h3>Editar registro baseado em template</h3>
        <hr />
        <?php foreach($resultado_query as $item) : ?>
        <?php 
		if ($item->readonly == 'yes') 
		{
			$config_form_read = " readonly='readonly'"; 
			$recado = "<tr><td colspan='2'><span style='color:red'><small>Documento definido como somente-leitura.</small></span></td></tr>";
		} 
		else 
		{ 
			$config_form_read = "";
			$recado = "";
		} 
		?>
		
		<?php echo form_open('documentos/editar_registro/'.$folderid.'/'.$idfile ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data', 'target' => '_blank')); ?>
        <table width="100%" border="0">
          <?php echo $recado; ?>
          <tr>
            <td width="30%">Nome do registro</td>
            <td width="70%">
              <label>
                <input type="text" <?php echo $config_form_read; ?> name="name" id="name" required="required" value="<?php echo $item->name; ?>" />
              </label>
                         <script>
			$(function() {
				var slider = $( "#travar" ).slider({
					min: 0,
					max: 1,
					range: "min",
					animate: true,
					value: <?php echo ( $item->locked == 'yes' ? '1' : '0'); ?>,
					slide: function( event, ui ) {
						var valores = new Array('no','yes');
						$( "#travar_status" ).load('<?php echo site_url('documentos/lock'); ?>/'+valores[ui.value]+'/<?php echo $item->id; ?>');
					}
				});
			});
			</script>
            <div class="demo" style="float:right; padding:0px">
            <span id="travar_status" style="font-size:9px; text-align:center"><?php echo ( $item->locked == 'yes' ? 'Documento travado.' : 'Documento destravado.'); ?></span>
            <div id="travar" style="width: 100px;"></div>
            </div>
            </td>
          </tr>
           <tr>
             <td><abbr title="Ou seja, n�o permitir� a altera��o dos dados do documento (ou o upload do mesmo arquivo alterado, no caso de arquivo enviado)">Somente leitura?</abbr></td>
             <td>
            
             <select name="readonly" id="readonly">
               <option value="yes" <?php if($item->readonly == 'yes') echo 'selected="selected"'; ?>>Sim</option>
               <option value="no" <?php if($item->readonly == 'no') echo 'selected="selected"'; ?>>N&atilde;o</option>
             </select>
             <input type="hidden" name="preview" id="preview" value="0" />
             </td>
           </tr>
          <tr>
            <td valign="top">Descri&ccedil;&atilde;o detalhada</td>
            <td><textarea name="desc"  <?php echo $config_form_read; ?> id="desc" cols="45" rows="5" required="required" ><?php echo $item->desc; ?></textarea></td>
          </tr>
          <?php echo $formulario; ?>
          
          <tr>
            <td>Permiss&otilde;es dos grupos</td>
            <td>
               <?php echo $grupos; ?>
           </td>
          </tr> 
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%"><input name="remover" type="checkbox" value="<?php echo $item->id; ?>" /> Remover este registro</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive" onclick="document.getElementById('preview').value=0"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button></td><td><button type="submit" class="button" onclick='document.getElementById("preview").value=1;'><img src="<?php echo base_url();  ?>css/plugins/link-icons/icons/pdf.png" alt="visualizar"/> Visualizar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/documentos/index#<?php echo $folderid; ?>'"><img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
        <div id="pdfview"></div>
        </form>
		<?php endforeach; ?>
        </div>
  	</section>
  	