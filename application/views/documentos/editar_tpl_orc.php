<?php 
session_start(); 
$_SESSION['KCFINDER'] = array();
$_SESSION['KCFINDER']['disabled'] = false;
$_SESSION['KCFINDER']['uploadURL'] = $pasta_org;
?>
	<?php include(dirname(dirname(__FILE__)) . "/mensagens.php"); ?>            
<section>
		<div class="conteudo">
        <h3>Editar orçamento baseado em template</h3>
        <hr />
        <?php foreach($resultado_query as $item) : ?>
        <?php 
		if ($item->readonly == 'yes') 
		{
			$config_form_read = " readonly='readonly'"; 
			$recado = "<tr><td colspan='2'><span style='color:red'><small>Documento definido como somente-leitura.</small></span></td></tr>";
		} 
		else 
		{ 
			$config_form_read = "";
			$recado = "";
		} 
		?>
		
		<?php echo form_open('documentos/editar_registro/'.$folderid.'/'.$idfile.'/orcamento' ,array('name' => 'form1', 'id' => 'form1', 'enctype' => 'multipart/form-data')); ?>
        <table width="100%" border="0">
          <?php echo $recado; ?>
          <tr>
            <td width="30%">Nome do registro</td>
            <td width="70%">
              <label>
                <?php echo $item->name; ?>
                 <input type="hidden" name="name" id="name" value="<?php echo $item->name; ?>" />
                 <input type="hidden" name="desc" id="desc" value="<?php echo $item->desc; ?>" />
                 <input type="hidden" name="readonly" id="readonly" value="<?php echo $item->readonly; ?>" />
                <input type="hidden" name="preview" id="preview" value="0" />
              </label>
                         
            </td>
          </tr>
          
          <?php echo $formulario; ?>
          <!--
          <tr>
            <td>Permiss&otilde;es dos grupos</td>
            <td>
               <?php echo $grupos; ?>
           </td>
          </tr>
          --> 
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%"></td>
            <td width="70%"><table><tr>
                <td>
                <button type="submit" class="button positive" onclick="document.getElementById('preview').value=0">
                    <img src="<?php echo base_url();  ?>css/plugins/buttons/icons/tick.png" alt="next"/> Salvar</button>
                </td>
                <!--<td>
                    <button type="submit" class="button" onclick='document.getElementById("preview").value=1;'>
                        <img src="<?php echo base_url();  ?>css/plugins/link-icons/icons/pdf.png" alt="visualizar"/> Visualizar</button>
                </td>-->
                <td><button type="button" class="button negative" onclick="window.close();">
                    <img src="<?php echo base_url();  ?>css/plugins/buttons/icons/cross.png" alt="cancel"/> Cancelar</button>
                    </td></tr>
                    </table>
                    </td>
          </tr>
        </table>
        <div id="pdfview"></div>
        </form>
		<?php endforeach; ?>
        </div>
  	</section>
  	