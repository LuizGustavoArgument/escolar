<?php
if($this->session->userdata('esta_logado') > 0) :
    //session_start();
    $_SESSION['username'] = $this->session->userdata('login_usuario');
else :
    //session_start();
    //session_destroy();
endif;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo $titulo; ?> - Área do Aluno</title>
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Sistema de Gestão.">
    <meta name="author" content="Luiz Otavio F. Tinoco, contato@luizotavio.info">
    <meta name="Designer" content="Luiz Otavio F. Tinoco">
	<meta name="Distribution" content="IU">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <style>
            body {
                /* padding-top: 60px; */
                padding-bottom: 40px;
            }
            
            .form_admin fieldset{
                margin: 3% 0;
                padding-bottom: 3%;
                border-bottom: 1px solid #e5e5e5;
            }

            .page-header{
                margin-top: 2%;
            }

        </style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">

    <script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <!-- dataTables Styles -->
    <style type="text/css" title="currentStyle">
		@import "<?php echo base_url();  ?>js/media/css/demo_page.css";
		@import "<?php echo base_url();  ?>js/media/css/demo_table.css";
		@import "<?php echo base_url();  ?>js/media/css/jquery.dataTables.css";
		@import "<?php echo base_url();  ?>js/extras/TableTools/media/css/TableTools.css";
	</style>
    
	<!-- JQuery UI -->  	
    <link type="text/css" href="<?php echo base_url(); ?>css/overcast/jquery-ui-1.9.0.custom.css" rel="Stylesheet">
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.9.0.custom.min.js"></script>
    
  
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.meio.mask.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/extras/TableTools/media/js/TableTools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/extras/Grouping/jquery.dataTables.rowGrouping.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/holder.js"></script>

    <?php 
    // Usando a versão mais nova do handsontable no módulo pee
    if($this->uri->segment(1) == "peecachu") { ?>
    <script src="<?php echo base_url(); ?>js/dist/jquery.handsontable.full.js"></script>
    <link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/dist/jquery.handsontable.full.css">
    <?php } else { ?>
    <script src="<?php echo base_url(); ?>js/jquery.handsontable.full.js"></script>
    <link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/jquery.handsontable.full.css">
    <?php } ?>

    <script src="<?php echo base_url(); ?>js/demo/js/samples.js"></script>
    <script src="<?php echo base_url(); ?>js/demo/js/highlight/highlight.pack.js"></script>
    <link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/demo/js/highlight/styles/github.css">

    <script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/ckeditor.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/adapters/jquery.js"></script>-->


    <link href="<?php echo base_url(); ?>css/datepicker.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
    <link href="<?php echo base_url(); ?>css/sortable.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>js/jquery.sortable.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url();  ?>js/jstree/jquery.jstree.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/jquery.livequery.js"></script>
    
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.treetable.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.treetable.theme.default.css" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url();  ?>js/jquery.treetable.js"></script>

    <!-- CSS personalizado -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/padrao.css" />
	
    <link href="<?php echo base_url(); ?>css/literally.css" rel="stylesheet">


    <!-- Full Calendar -->
    <link href='<?php echo base_url();  ?>js/fullcalendar/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='<?php echo base_url();  ?>js/fullcalendar/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='<?php echo base_url();  ?>js/fullcalendar/fullcalendar/fullcalendar.min.js'></script>
<script type="text/javascript" charset="utf-8">
//<!--
<?php echo ($js_out ? $js_out : ''); ?>

function windowpop(url, width, height) {
    var leftPosition, topPosition;
    leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    window.open(url, "Window2", "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=yes,location=no,directories=no");
}

$(document).ready(function() {

$("div#loading").hide();

<?php echo $js; ?>

$("input:text").setMask();

$('input:text[alt=phone]').setMask("(99) 9999-99999").change(function(event) {
var target, phone, element;
target = (event.currentTarget) ? event.currentTarget : event.srcElement;
if(typeof target === 'undefined') return false;
phone = target.value.replace(/\D/g, '');
element = $(target);
element.unsetMask();
if(phone.length > 10) {
    element.setMask("(99) 99999-9999");
} else {
    element.setMask("(99) 9999-99999");
    }
});

if ($("div.alert").length > 0) { $("div.alert-danger").animate({ opacity: 1.0 },8000).fadeOut(); }

        if($(".chatbox")[0]) {
            $(".chatbox").draggable();
        }
        
        $('.chatboxhead').live('click',function () {
            $(".chatbox").draggable();
        });
});

//-->
</script>
    
</head>


  <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->
      
  	<div id="loading" style="position:absolute; padding:5px; text-align: center; background-color: tomato; color:white"><strong>Processando...</strong></div>
    
    <div class="navbar-nav navbar-fixed-top">
      <div class="navbar navbar-inner">
        <div class="container">           
            <a class="brand" href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url(); ?>files/escola/arglogo.png" alt="Argument Tecnologia">
            </a>
            <div class="nav">
                <ul class="nav">
                    <?php if($this->session->userdata('esta_logado')) echo $menu; ?>
                </ul>
            </div>
        </div>
      </div>
    </div>


 
  <div class="container">
      <?php if (empty($submenu) && empty($arvore)) { echo '<div class="col-12">'; } else { ?>
      <div class="row">
        <div class="col-2">
          <ul class="navbar nav">
          </ul>
          <div class="well sidebar">
            <ul class="nav flex-column">
              <?php echo (empty($submenu) ? '' : $submenu); ?>
            </ul>
          </div>
          <div class="well sidebar">

              <?php if(!empty($arvore)) : ?>
    
                <?php echo $arvore; ?>
    
             <?php 
else :
    
    echo $leftlogo;
    echo '<p align="center"><a href="'.site_url('cadastro/eu').'" title="Clique aqui para alterar o seu cadastro">'. $bemvindo . '</a></p>';
             
             endif; ?>

          </div>
        </div>
        <div class="col-10">
        <?php } ?>
          <div class="row">
          </div>
          <div class="row">
              
              <?php echo (empty($modmenu) ? '' : $modmenu); ?>
              
          </div>
          <?php if((empty($titulo) && !empty($subtitulo)) || (empty($titulo) && empty($subtitulo))) {
              
              if(!empty($subtitulo))
              echo '<div class="row"><h3>'.$subtitulo.'</h3></div>';
              
          } else { ?>
          <div class="page-header">
            
              <h3>
                <?php echo $titulo; ?>
              <br>
              <h6>
                <?php echo $subtitulo; ?>
              </h6>
             </h3>
             
             <hr>
             
          </div>
          <?php } ?>
          
          
  
    