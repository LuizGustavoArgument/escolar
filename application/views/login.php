		 <div>
		<?php 
		// Cabecalho para mensagens do sistema
		include(dirname(__FILE__)."/mensagens.php");
		?>
		<div class="notice" style="display:none;"></div>
         <form action="login" method="post" enctype="multipart/form-data" name="login" id="login">

				<div><label><abbr title="Digite o seu endereço de e-mail, ou o CPF ou o login (nome de usuário)">Login:</abbr></label><input name="usuario" id="usuario" type="text" class="required"></div>
				<div><label><abbr title="Digite sua senha.">Senha:</abbr></label><input name="senha" id="senha" type="password"  class="required password"></div>
				<div><button type="submit" class="btn btn-success"><img src="<?php echo base_url();  ?>css/blueprint/plugins/buttons/icons/tick.png" alt="ok"> Entrar</button></div>
				

          </form>  
          </div>
          
          <hr>




<script>
var currentAnchor = null;
function checkAnchor()
{
	if(currentAnchor != document.location.hash)
	{
		currentAnchor = document.location.hash;
		if(currentAnchor)
		{
			var local = currentAnchor.substring(1);
			if(local.length > 0)
			return mostraDiv(local);
		}
	}
	return false;
}

function mostraDiv(local)
{
	if (local == "bemvindo")
		$("div.notice").fadeIn("slow").html('Cadastro liberado! Agora, autentique-se no sistema informando o seu LOGIN (ou CPF ou e-mail) e sua SENHA abaixo.');
	else return false;
}
checkAnchor();
</script>