<div class="span-24 miolo">
    
    <div class="span-22">

			<div class="prepend-2">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
<?php foreach ($resultado_query as $item) : ?>
        <h3>Editar grupo <?php echo $item->name; ?></h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('grupos/editar/'.$item->id); ?>">
        <table width="100%" border="0">
           <tr>
            <td width="30%">Organização</td>
            <td width="70%"><select name="idorg" id="idorg">
              <option value="1">Padrão</option>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Nome do grupo</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" value="<?php echo $item->name; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td width="30%">Código do grupo</td>
            <td width="70%">
              <label>
                <input type="text" name="code" id="code" maxlength="10" value="<?php echo $item->code; ?>" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descrição</td>
            <td><label>
              <textarea name="desc" id="desc" cols="45" rows="5"><?php echo $item->desc; ?></textarea>
            </label></td>
          </tr>
           
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">
            <input type="hidden" name="mexeu" id="mexeu" value="0" />
            <input name="remover" type="checkbox" value="<?php echo $item->id; ?>" /> Remover grupo</td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive" onclick="submitFormInPopUp();"> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/grupos/index'"> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
<?php endforeach; ?>
        </div>
  	</div>
  	
  	</div>
    </div>