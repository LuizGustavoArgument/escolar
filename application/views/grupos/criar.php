<div class="span-24 miolo">
    
    <div class="span-22">

			<div class="prepend-2">
  		<!-- Coluna do centro (conteudo principal) -->
        <?php 
        // Cabecalho para mensagens do sistema
        include(dirname(dirname(__FILE__)) . "/mensagens.php");
        ?>
        <div class="conteudo">
        <h3>Criar novo grupo</h3>
        <hr /><form id="form1" name="form1" method="post" enctype="multipart/form-data" action="<?php echo site_url('grupos/criar'); ?>">
        <table width="100%" border="0">
           <tr>
            <td width="30%">Organização</td>
            <td width="70%"><select name="idorg" id="idorg">
              <option value="1">Padrão</option>
            </select></td>
          </tr>
          <tr>
            <td width="30%">Nome do grupo</td>
            <td width="70%">
              <label>
                <input type="text" name="name" id="name" required="required" />
              </label>
            </td>
          </tr>
          <tr>
            <td width="30%">Código do grupo (ordem)</td>
            <td width="70%">
              <label>
                <input type="text" name="code" id="code" maxlength="10" />
              </label>
            </td>
          </tr>
          <tr>
            <td valign="top">Descrição</td>
            <td><label>
              <textarea name="desc" id="desc" cols="45" rows="5"></textarea>
            </label></td>
          </tr>
          <tr>
            <td valign="top"><strong>Regras de acesso</strong></td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td valign="top">Cadastros</td>
            <td><p>
              <label>
                <input type="radio" name="cadastros" value="5" id="cadastros_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="4" id="cadastros_1" />
                Tudo exceto grupos</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="3" id="cadastros_2" />
                Apenas seus dados cadastrais</label>
              <br />
              <label>
                <input type="radio" name="cadastros" value="1" id="cadastros_3" />
                Proibido</label><br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Custos</td>
            <td><p>
              <label>
                <input type="radio" name="custos" value="5" id="custos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="custos" value="4" id="custos_1" />
                Somente os que ele criou</label>
              <br />
              <label>
                <input type="radio" name="custos" value="3" id="custos_2" />
                Somente listagem</label>
              <br />
              <label>
                <input type="radio" name="custos" value="1" id="custos_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Serviços</td>
            <td><p>
              <label>
                <input type="radio" name="servicos" value="5" id="servicos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="4" id="servicos_1" />
                Somente os que ele participa/coordena</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="3" id="servicos_2" />
                Somente os que ele participa</label>
              <br />
              <label>
                <input type="radio" name="servicos" value="2" id="servicos_3" />
                Apenas a listagem (ver status)</label>
dos que ele participa           <br />
              <label>
                <input type="radio" name="servicos" value="1" id="servicos_4" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Produtos</td>
            <td><p>
              <label>
                <input type="radio" name="produtos" value="5" id="produtos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="produtos" value="4" id="produtos_1" />
                Somente consulta</label>
              <br />
              <label>
                <input type="radio" name="produtos" value="1" id="produtos_2" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Financeiro</td>
            <td><p>
              <label>
                <input type="radio" name="financeiro" value="5" id="financeiro_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="4" id="financeiro_1" />
                Somente os que ele faz parte</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="3" id="financeiro_2" />
                Somente listagem</label>
              <br />
              <label>
                <input type="radio" name="financeiro" value="1" id="financeiro_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Documentos</td>
            <td><p>
              <label>
                <input type="radio" name="documentos" value="5" id="documentos_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="documentos" value="4" id="documentos_1" />
                Somente a pasta pessoal e a dos clientes</label>
              <br />
              <label>
                <input type="radio" name="documentos" value="3" id="documentos_2" />
                Somente a pasta pessoal</label>
              <br />
              <label>
                <input type="radio" name="documentos" value="1" id="documentos_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Mensagens (chat)</td>
            <td><p>
              <label>
                <input type="radio" name="mensagens" value="5" id="mensagens_0" />
                Liberado</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="4" id="mensagens_1" />
                Todos exceto clientes que não pertencem a ele</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="3" id="mensagens_2" />
                Apenas o seu consultor</label>
              <br />
              <label>
                <input type="radio" name="mensagens" value="1" id="mensagens_3" />
                Proibido</label>
              <br />
            </p></td>
          </tr>
          <tr>
            <td valign="top">Outras regras</td>
            <td><p>
              <label>
                <input type="radio" name="outras" value="5" id="outras_0" />
                Permitir remoção de registros</label>
              <br />
              <label>
                <input type="radio" name="outras" value="4" id="outras_1" />
                Não permitir remoção de registros</label>
              <br />
            </p></td>
          </tr>
          
          </table>
          <table width="100%" border="0">
          <tr>
            <td width="30%">&nbsp;<input type="hidden" name="mexeu" id="mexeu" value="0" /></td>
            <td width="70%"><table><tr><td><button type="submit" class="button positive" onclick="submitFormInPopUp();"> Salvar</button></td><td><button type="button" class="button negative" onclick="document.location.href='<?php echo base_url();  ?>/grupos/index'"> Cancelar</button></td></tr></table></td>
          </tr>
        </table>
       
        </form>
        </div>
  	</div>
  	
  	</div>
    </div>