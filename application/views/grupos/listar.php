﻿	<div class="span-24 miolo">
    
    <div class="span-18 colborder">

			<div class="prepend-2">
            <h1>Grupos de Usuários</h1>
				<p>
                <?php foreach($resultado_query as $item) : ?>
                <?php if(in_array($item->id,$this->grp_filter)) : ?>
                <table>
                <tr><td>
                <h2><?php echo (!empty($item->code) ? $item->code .' -' : '') . ' ' . $item->name; ?></h2>
                </td></tr>
                <tr><td>
                <h3><?php echo $item->desc; ?></h3>
                <p>[ <?php echo anchor('grupos/editar/'.$item->id,'editar grupo'); ?> ]</p>
                
                <p>&nbsp;</p>
                

                </td></tr>
                <tr>
                <td>
                <table>
                <tr><td><strong>CPF/CNPJ</strong></td><td><strong>Nome</strong></td><td><strong>E-mail</strong></td><td>Status</td><td>Formulários</td><td>Opções</td></tr>
				<?php 
				if(!empty($usu_id[$item->id]))
				for($i=0;$i<count($usu_id[$item->id]);$i++) :?>
                <?php 
				$cad = $dados_cadastrais[$item->id][$i];
				if(!empty($cad))
				{
					if ($usu_type[$item->id][$i] === false)
					$cadastro_status = '<span style="color:green"><small>Iniciada</small></span>';
					else
					$cadastro_status = $usu_type[$item->id][$i];
					 foreach ($cad as $titulo => $conteudo) {
						if(stristr($conteudo['title'],"Nome Completo"))
							$nome = ($conteudo['content'] != 99 ? $conteudo['content'] : '');
						if(stristr($conteudo['title'],"E-mail"))
							$email = ($conteudo['content'] != 99 ? $conteudo['content'] : '');
					 }
				}
				else
				{
					if ($usu_type[$item->id][$i] === false)
					$cadastro_status = '<span style="color:red"><small>Não iniciada</small></span>';
					else
					$cadastro_status = $usu_type[$item->id][$i];
					$nome = '';
					$email = '';
				}
				?>
                <tr><td><strong><?php echo $usu_login[$item->id][$i]; ?></strong></td>
				<td>
				<?php echo (empty($nome) ? '<span style="color:grey">'.$usu_name[$item->id][$i].'</span>' : $nome); ?></td>
				<td>
				<?php echo (empty($email) ? '<span style="color:grey">'.$usu_email[$item->id][$i].'</span>' : $email); ?>
                <?php //echo (empty($usu_cpf[$item->id][$i]) ? '' : '<br><small>'.$usu_cpf[$item->id][$i].'</small>'); ?></td>
				<td><?php echo $usu_status[$item->id][$i];?></td>
                <td><div align="center"><?php echo $usu_admin[$item->id][$i]; ?></div></td>
				<td><?php echo anchor('usuarios/editar/'.$usu_id[$item->id][$i],'ver dados cadastrais');?></td></tr>
				<?php endfor; ?>
                </table>
				<?php echo anchor('usuarios/criar/'.$item->id,'Adicionar novo usuário neste grupo'); ?>
                </td>
                </tr>
                </table>
                <?php endif; ?>
                 <?php endforeach; ?>
                </p>
				<hr>
				<script>
				$("#liberando").click(function(){
						$.ajax({ 
							url: "<?php echo base_url(); ?>usuarios/liberar/"+$("#liberando").attr("name"), 
							success: function(data){ 
								if (data == 'ok') 
									window.location.reload(true); 
								else 
									alert('Falha ao liberar usuário. Entre em contato com a administração e informe o CPF do usuário.');
							}
						});
				});
				</script>
				
			</div>
		</div>

		<div class="span-5 last">
			<div>
            <img src="<?php echo base_url(); ?>img/png/Users.png" border="0" alt="Centros de aplicação"><br />
				<?php if ($this->uri->segment(3) === FALSE) { ?>
				<?php foreach($resultado_query as $item) : ?>
                <?php if(in_array($item->id,$this->grp_filter)) : ?>
                <h3><?php echo anchor('grupos/index/'.$item->id,$item->name); ?></h3>
                <?php endif; ?>
				<?php endforeach; } else { echo anchor('grupos/index','Mostrar todos os grupos'); } ?>
				<h4><?php echo anchor('grupos/criar','Criar novo grupo'); ?></h4>
			</div>
		</div>
	</div>