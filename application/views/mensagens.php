<?php 		
// Mensagens do sistema
//$opa = validation_errors('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>', '</div>');
if ($this->session->flashdata('error')) 
	echo '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'. $this->session->flashdata('error') .'</div>';
if ($this->session->flashdata('notice')) 
	echo '<div class="alert"><button type="button" class="close" data-dismiss="alert">×</button>'. $this->session->flashdata('notice') .'</div>';
if ($this->session->flashdata('success')) 
	echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>'. $this->session->flashdata('success') .'</div>';
//echo $opa;
?>