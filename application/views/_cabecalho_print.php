<!--[if IE 8]> <html class="ie8" lang="pt"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="pt"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="pt"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo $titulo; ?> - Sistema de Gestão</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de Gestão.">
    <meta name="author" content="Luiz Otavio F. Tinoco, contato@luizotavio.info">
    <meta name="Designer" content="Luiz Otavio F. Tinoco">
    <meta name="Distribution" content="IU">
    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body { padding-top: 60px; /* 60px to make the container go all the way
      to the bottom of the topbar */ }
    </style>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/ico/apple-touch-icon-57-precomposed.png">
    
    <!-- dataTables Styles -->
    <style type="text/css" title="currentStyle">
        @import "<?php echo base_url();  ?>js/media/css/demo_page.css";
        @import "<?php echo base_url();  ?>js/media/css/demo_table.css";
        @import "<?php echo base_url();  ?>js/media/css/jquery.dataTables.css";
        @import "<?php echo base_url();  ?>js/extras/TableTools/media/css/TableTools.css";
    </style>
    
    
    
    <!-- Idealforms 
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.idealforms.css">-->
    
    <!-- JQuery UI -->      
    <link type="text/css" href="<?php echo base_url(); ?>css/overcast/jquery-ui-1.9.0.custom.css" rel="Stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui-1.9.0.custom.min.js"></script>
    
    <!-- Normalize forms CSS 
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/assets/css/formalize.css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>css/assets/js/jquery.formalize.min.js"></script> 
    
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.metadata.js"></script>-->   
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.meio.mask.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/extras/TableTools/media/js/TableTools.min.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url();  ?>js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/localization/messages_ptbr.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/jquery.slideto.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/min/jquery.idealforms.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();  ?>js/i18n/errors_pt.js"></script>-->

<script src="<?php echo base_url(); ?>js/jquery.handsontable.js"></script>
<script src="<?php echo base_url(); ?>js/lib/bootstrap-typeahead.js"></script><!-- if you need the autocomplete feature -->
<script src="<?php echo base_url(); ?>js/lib/jquery.autoresize.js"></script><!-- if you need the autoexpanding textarea -->
<script src="<?php echo base_url(); ?>js/lib/jQuery-contextMenu/jquery.contextMenu.js"></script><!-- if you need the context menu -->
<script src="<?php echo base_url(); ?>js/lib/jQuery-contextMenu/jquery.ui.position.js"></script><!-- if you need the context menu -->
<link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/lib/jQuery-contextMenu/jquery.contextMenu.css">
<link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/jquery.handsontable.css">
<script src="<?php echo base_url(); ?>js/demo/js/samples.js"></script>
<script src="<?php echo base_url(); ?>js/demo/js/highlight/highlight.pack.js"></script>
<link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>js/demo/js/highlight/styles/github.css">

<script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();  ?>js/ckeditor/adapters/jquery.js"></script>


<link href="<?php echo base_url(); ?>css/datepicker.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url(); ?>css/sortable.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>js/jquery.sortable.js"></script>

<script type="text/javascript" src="<?php echo base_url();  ?>js/jstree/jquery.jstree.js"></script>

<!-- CSS personalizado -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/padrao.css" />

<script type="text/javascript" charset="utf-8">
//<!--
<?php echo ($js_out ? $js_out : ''); ?>

$(document).ready(function() {

    $("div#loading").hide();
        $("div#loading").bind("ajaxStart", function(){
            $(this).show(0,function(){
                $("button").each(function(){ $(this).attr("disabled", "disabled"); $(this).css("cursor","progress"); });
            });
        }).bind("ajaxStop", function(){
            $(this).hide(0,function(){
                $("button").each(function(){ $(this).removeAttr("disabled"); $(this).css("cursor","pointer"); }); });
        });

<?php echo $js; ?>

$("input:text").setMask();

if ($("div.alert").length > 0) { $("div.alert-danger").animate({ opacity: 1.0 },8000).fadeOut(); }

// Convertendo alguns campos editaveis para hidden
/*
$("input[type='text']").each(function(){
  var name = $(this).attr('name'); // grab name of original
  var value = $(this).attr('value'); // grab value of original
  var idname = $(this).attr('id'); // grab id of original
  //create new visible input
  var html = '<input type="hidden" name="'+name+'" id="'+idname+'" value="'+value+'" />';
  $(this).after(html).remove(); // add new, then remove original input
});
*/

});
//-->
</script>
    
</head>


  <body>
    <div id="loading" style="position:absolute; padding:5px; text-align: center; background-color: tomato; color:white"><strong>Processando...</strong></div>
    <div class="navbar navbar-fixed-top navbar-inverse">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="glyphicon-bar">
            </span>
            <span class="glyphicon-bar">
            </span>
            <span class="glyphicon-bar">
            </span>
          </a>
          <a class="brand" href="#">
            Sistema Escola
          </a>
          <div class="navbar-collapse">
            <ul class="nav">
              
            </ul>
          </div>
          <form class="navbar-form pull-right">
            <div class="navbar-collapse">
            <ul class="nav">
              <li>
                <a href="#">
                  <?php echo $bemvindo; ?>
                </a>
              </li>
              
            </ul>
          </div>
          </form>
        </div>
      </div>
    </div>
    
    
 
  <div class="container-fluid">
      
        <div class="col-12">
          <div class="row-fluid">
          </div>
          <div class="row-fluid">
          </div>
          <div class="well">
            <div>
              <h1>
                <?php echo $titulo; ?>
              </h1>
              <p>
                <?php echo $subtitulo; ?>
              </p>
            </div>
          </div>
          
          
  
    