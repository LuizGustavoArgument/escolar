<?php
class  MY_Controller  extends  CI_Controller  {
    
	public $cabecalho = array();
	public $rodape = array();
	public $dados = array();
	public $org = array();
	
    public function __construct()
    {
		parent::__construct();
		$this->cabecalho['js'] = '';
		$this->cabecalho['js_out'] = '';
		$this->rodape['conteudo'] = $this->geraRodape();
		$this->org = $this->session->userdata('orgdata');
		$this->org = $this->org[0];
		$this->load->library('campos');
        $this->load->model('sistema','banco',TRUE);
		//$this->load->library('firephp'); // DEBUG is on the table
	}
	
    public function ocultarMenus()
    {
        // Relacao de menus
        $menus = array('cadastros','custos','agenda',
        'produtos','financeiro','arquivos','documentos');
        
        foreach($menus as $i)
        {    
            $this->session->set_userdata('pagina_atual', $i);
            $c = $this->checaPermissao();
            $this->geraJavaScript('ocultar_'.$i,$c);
            $this->session->unset_userdata('pagina_atual');
        }
    }
    
    public function isProfessor($id="")
    {
        if(empty($id))
            $id = $this->session->userdata('esta_logado');
        $rel = $this->banco->relacao('users',$id,'groups');
        if(!is_array($rel)) $rel = array($rel);
        if(in_array(3,$rel))
            return true;
        return false;
    }
    
    public function isAluno($id="")
    {
        if(empty($id))
            $id = $this->session->userdata('esta_logado');
        $rel = $this->banco->relacao('users',$id,'groups');
        if(!is_array($rel)) $rel = array($rel);
        if(in_array(7,$rel))
            return true;
        return false;
    }
    
    public function isAdmin($id="")
    {
        if(empty($id))
            $id = $this->session->userdata('esta_logado');
        $rel = $this->banco->relacao('users',$id,'groups');
        if(!is_array($rel)) $rel = array($rel);
        if(in_array(6,$rel))
            return true;
        return false;
    }
    
    public function geraStatusText($id,$idserv="")
    {
        // lista de status com cores
        if(empty($id)) return '';
        $contratoCheck = "";
        if(!empty($idserv))
        {
            $chk = $this->banco->campo('services','document','id = ' . (int) $idserv);
            if(empty($chk))
                 $contratoCheck = '<br style="clear:both"><span style="color:red"><i class="fas fa-info-circle"></i>&nbsp;Sem contrato</span>';
        }
        
        // tipos de status
        $os_status = array(
            1 => 'Em elaboração' . $contratoCheck,
            2 => 'Em elaboração',
            3 => 'Aprovado<br style="clear:both"><span style="color:red"><i class="fas fa-info-circle"></i>&nbsp;Sem contrato</span>',
            4 => 'Proposta Perdida',
            5 => 'Aprovado' . $contratoCheck,
            6 => 'Em execução' . $contratoCheck,
            7 => 'Concluído',
            8 => 'Em elaboração (Rascunho)',
            9 => 'Em elaboração (Incompleto)'
        );
        
        $res = $this->banco->listagem('status','name','id = '. (int) $id);
        foreach($res as $row)
        {
            $ret = '<div class="statusbox" id="'.$row->color.'"></div>&nbsp;'.$os_status[$row->id];
        }
        
        return $ret;
    }

    function geraLblProduto($id)
    {
        if(!is_numeric($id))
        {
            return '';
        }
        else
            {
                $id = 'id = '. $id;
            }
        $listagem = $this->banco->listagem('products','name',$id);
        $ret = '';
        if(!empty($listagem))
        foreach($listagem as $item)
        {
            $abr = strtoupper($item->abrev);
            $color = 'success';
            if($item->abrev == 'aee')
            {
                $abr = strtoupper('ae');
                $color = 'warning';
            }
            if($item->abrev == 'pee')
            {
                $color = 'important';
            }
            
            $ret .= '<span class="label label-'.$color.'">'.$abr.'</span>&nbsp;';
        }
        return $ret;
    }
    
	public function geraMenu($padrao)
	{
		$versao = '';
		$submenu = array('login' => '');
		$padrao = strtolower($padrao);
		
		$bemvindo = '' . character_limiter($this->session->userdata('nome_usuario'),20,'...');
		
		$tit = '&nbsp;';
		$stit = '&nbsp;';
		
		switch($padrao)
		{
			case 'login' :
				$bemvindo = 'Você não está autenticado';
				$saida = '<li>&nbsp</li>';
				$tit = 'Login';
				$stit = 'Autentique-se';
				break;	
				
			case 'home' :
				// Conteudo central
				$this->dados['central'] = '404: Página não encontrada.';
				
				// Título principal
				$tit = 'Olá '.$this->session->userdata('nome_usuario');
				$stit = 'Você está na área do aluno. Veja suas disciplinas abaixo e clique em acessar.';
				
				break;
				
			
			case 'cadastro' :
				
				
				 // Título principal
				$tit = 'Cadastros';
				$stit = 'Inserção de novos usuários, atributos e grupos.';
				
				break;
			
			case 'custos' :
				
				 // Título principal
				$tit = 'Custos';
				$stit = 'Atribuição e listagem de custo/hora à usuários e tabelas de horários disponíveis.';
				
				break;
				
			case 'agenda' :
				
				 // Título principal
				$tit = 'Agenda';
				$stit = 'Gestão de consultas';
				
				break;
				
			case 'produtos' :
				
				 
				 // Título principal
				$tit = 'A Escola';
				$stit = 'Gestão de disciplinas e turmas.';
				
				break;
				
			case 'financeiro' :
				
				 // Título principal
				$tit = 'Financeiro';
				$stit = 'Planilha financeiro para controle dos pagamentos de serviços e entre funcionários.';
				
				break;
                
            default :
				$this->dados['central'] = '404: Página não encontrada.';
				$tit = '404';
				$stit = 'Not found.';
				
				break;
		}
        
        // Submenus
        $submenu['default'] = '';
        $submenu['financeiro'] = '
                  
                  <li class="nav-item '.(strstr(current_url(),site_url('financeiro/cliente')) ? 'active' : '').'">
                    <a href="'.site_url('financeiro/cliente').'" class="nav-link">
                      Por paciente
                    </a>
                  </li>
                  <li class="nav-item '.(strstr(current_url(),site_url('financeiro/funcionario')) ? 'active' : '').'">
                    <a href="'.site_url('financeiro/funcionario').'" class="nav-link">
                      Por profissional
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('financeiro/geral') ? 'active' : '').'">
                    <a href="'.site_url('financeiro/geral').'" class="nav-link">
                      Controle geral
                    </a>
                  </li>
                  ';
        $submenu['produtos'] = '
                  
                  <li class="nav-item '.(current_url() == site_url('produtos/novo') ? 'active' : '').'">
                    <a href="'.site_url('produtos/novo').'" class="nav-link">
                      Nova disciplina
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('produtos/listar') ? 'active' : '').'">
                    <a href="'.site_url('produtos/listar').'" class="nav-link">
                      Listagem de disciplinas
                    </a>
                  </li>
                  <!--<li class="nav-item '.(current_url() == site_url('produtos/novorecurso') ? 'active' : '').'">
                    <a href="'.site_url('produtos/novorecurso').'" class="nav-link">
                      Nova turma
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('produtos/listarrecurso') ? 'active' : '').'">
                    <a href="'.site_url('produtos/listarrecurso').'" class="nav-link">
                      Listagem de turmas
                    </a>
                  </li>-->
                  <li class="nav-item '.(current_url() == site_url('produtos/coord_pro') ? 'active' : '').'">
                    <a href="'.site_url('produtos/coord_pro').'" class="nav-link">
                      Associar disciplina a professor
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('produtos/coord_aux') ? 'active' : '').'">
                    <a href="'.site_url('produtos/coord_aux').'" class="nav-link">
                      Associar disciplina a turma
                    </a>
                  </li>
                  ';
        $submenu['agenda'] = '
                  
                  <li class="nav-item '.(current_url() == site_url('agenda/novo') ? 'active' : '').'">
                    <a href="'.site_url('agenda/novo').'" class="nav-link">
                      Nova
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('agenda/listar') ? 'active' : '').'">
                    <a href="'.site_url('agenda/listar').'" class="nav-link">
                      Listagem
                    </a>
                  </li>
                  
                  <li class="nav-item '.(current_url() == site_url('agenda/status') ? 'active' : '').'">
                    <a href="'.site_url('agenda/status').'" class="nav-link">
                      Alterar status
                    </a>
                  </li>
                  ';
        $submenu['custos'] = '
                  
                  <li class="nav-item nav-item '.(current_url() == site_url('custos/atribuir') ? 'active' : '').'">
                    <a href="'.site_url('custos/atribuir').'" class="nav-link">
                      Atribuir custo ou horário a professor
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('custos/custohora') ? 'active' : '').'">
                    <a href="'.site_url('custos/custohora').'" class="nav-link">
                      Listagem de custos e horários padrão
                    </a>
                  </li>
                  ';
        $submenu['cadastro'] = '<li class="nav-item '.(current_url() == site_url('cadastro/inserindo') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/inserindo').'" class="nav-link">
                      Novo cadastro
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('cadastro/pessoa/7') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/pessoa/7').'" class="nav-link">
                      Novo aluno
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('cadastro/listar') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/listar').'" class="nav-link">
                      Listagem geral
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('cadastro/listar/cliente') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/listar/cliente').'" class="nav-link">
                      Lista alunos
                    </a>
                  </li>
                  <!--<li class="nav-item '.(current_url() == site_url('cadastro/listar/insumos') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/listar/insumos').'" class="nav-link">
                      Lista Insumos
                    </a>
                  </li>-->
                  <li class="nav-item '.(current_url() == site_url('cadastro/inserirgrupo') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/inserirgrupo').'" class="nav-link">
                      Novo grupo
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('cadastro/listargrupo') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/listargrupo').'" class="nav-link">
                      Listar grupos
                    </a>
                  </li>
                  <li class="nav-item '.(current_url() == site_url('cadastro/coord') ? 'active' : '').'">
                    <a href="'.site_url('cadastro/coord').'" class="nav-link">
                      Associar aluno à turma
                    </a>
                  </li>
                  ';
        
        if($this->isAdmin()) {
            $submenu['home'] = '
                      <li style="width:100%" class="nav-item '.(current_url() == site_url('cadastro/pessoa') ? 'active' : '').'">
                        <a href="'.site_url('cadastro/inserindo').'" class="nav-link" style="padding-right: 0px">
                            <i class="fas fa-user"></i>&nbsp;Novo cadastro
                        </a>
                      </li>
                      <li style="width:100%" class="nav-item '.(current_url() == site_url('produtos/novo') ? 'active' : '').'">
                        <a href="'.site_url('produtos/novo').'" class="nav-link" style="padding-right: 0px">
                          <i class="fas fa-plus"></i>&nbsp;Nova disciplina
                        </a>
                      </li>
                      ';
            $saida = '<li class="nav-item '.($this->uri->segment(1) == 'home' ? 'active' : '').'">'.anchor('home','Início', array('class'=>'nav-link '));
            $saida .= '</li>';
            $saida .= '<li class="nav-item '.($this->uri->segment(1) == 'cadastro' ? 'active' : '').'">'.anchor('cadastro','Cadastros',array('class'=>'nav-link '.($this->uri->segment(1) == 'cadastro' ? 'active' : ''))).'';
            // $saida .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'.$submenu['cadastro'].'</ul></li>';
            $saida .= '<li class="nav-item '.($this->uri->segment(1) == 'custos' ? 'active' : '').'">'.anchor('custos','Horários',array('class'=>'nav-link '.($this->uri->segment(1) == 'custos' ? 'active' : ''))).'';
            // $saida .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'.$submenu['custos'].'</ul></li>';
            $saida .= '<li class="nav-item '.($this->uri->segment(1) == 'produtos' ? 'active' : '').'">'.anchor('produtos','Escola',array('class'=>'nav-link '.($this->uri->segment(1) == 'produtos' ? 'active' : ''))).'';
            // $saida .= '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'.$submenu['produtos'].'</ul></li>';
            $saida .= '<li>'.anchor('login/encerra_sessao','SAIR', array('class'=>'nav-link ')).'</li>';
        } 
        
        if($this->isAluno()) {
            
            $submenu['home'] = '
                      <li style="width:100%" class="nav-item '.(current_url() == site_url('cadastro/eu') ? 'active' : '').'">
                        <a href="'.site_url('cadastro/eu').'" class="nav-link" style="padding-right: 0px">
                            <i class="fas fa-user"></i>&nbsp;Meus arquivos
                        </a>
                      </li>
                      ';
            $submenu['produtos'] = '
                      <li style="width:100%" class="nav-item '.(current_url() == site_url('cadastro/eu') ? 'active' : '').'">
                        <a href="'.site_url('cadastro/eu').'" class="nav-link" style="padding-right: 0px">
                          <i class="fas fa-user"></i>&nbsp;Meus arquivos
                        </a>
                      </li>
                      ';
            $submenu['cadastro'] = '';
            $submenu['custos'] = '';
            $submenu['produtos'] = '';
            $saida = '<li class="nav-item '.($this->uri->segment(1) == 'home' ? 'active' : '').'">'.anchor('home','Início');
            $saida .= '</li>';
            $saida .= '<li class="nav-item '.($this->uri->segment(1) == 'cadastro' ? 'active' : '').'">'.anchor('cadastro/eu','Meu cadastro',array('class'=>'dropdown-toggle '.($this->uri->segment(1) == 'cadastro' ? 'active' : ''))).'';
            $saida .= '<li class="nav-item '.($this->uri->segment(1) == 'produtos' ? 'active' : '').'">'.anchor('home','Escola',array('class'=>'dropdown-toggle '.($this->uri->segment(1) == 'produtos' ? 'active' : ''))).'';
            $saida .= '<li>'.anchor('login/encerra_sessao','SAIR').'</li>';
            
        }
        
        

		$this->cabecalho['nome'] = $padrao;
		$this->cabecalho['menu'] = $saida;
		$this->cabecalho['submenu'] = (empty($submenu[$padrao]) ? '' : $submenu[$padrao]);
        $this->cabecalho['modmenu'] = $this->geraModMenu((!empty($modmenu[$padrao]) ? $modmenu[$padrao] : array()));
		$this->cabecalho['bemvindo'] = $bemvindo;
		$this->cabecalho['titulo'] = $tit;
		$this->cabecalho['subtitulo'] = $stit;
		$this->cabecalho['versao'] = $versao;
        
        // Busca a logo do usuário logado
        $logocli = '';
        
        if($this->session->userdata('esta_logado'))
            $logocli = $this->banco->campo('usersdata','logo','iduser = '.$this->session->userdata('esta_logado'));
        
        if(!empty($logocli))
            $this->cabecalho['leftlogo'] = '<a href="'.site_url('cadastro/eu').'" title="Clique aqui para alterar o seu cadastro"><img src="'.site_url('uploads').'/'.$logocli.'" class="img-rounded" alt="Logo atual" style="width:100%"></a>';
        else
            $this->cabecalho['leftlogo'] = '<a href="'.site_url('cadastro/eu').'" title="Clique aqui para alterar o seu cadastro"><img src="'.site_url('img').'/semfoto.jpg" class="img-rounded" alt="Sem logo" style="width:100%"></a>';
        
	}
    
    public function modmenu_itens($idserv)
    {
        if(empty($idserv) || $idserv == 0)
            return;
            
        // Criando o menu extra
        $hazdoc = $this->banco->campo('services','document','id = '. (int) $idserv);
        $idstat = $this->banco->campo('services','idstatus','id = '. (int) $idserv);
        $servname = $this->banco->campo('services','name','id = '. (int) $idserv);
        
        if(!empty($hazdoc))
        {
            $hazdoc = anchor('documentos/download/'.$hazdoc,'Ver contrato');
        }
        else
        {
            
            $nohaz = base64_encode('e_'.$idstat.'_'.$idserv.'_'.time());
            $hazdoc = anchor('agenda/status/'.$nohaz,'Anexar contrato','target=_blank');
        }
        $hazplan =  anchor('agenda/alterar/'.base64_encode('e_'.$servname.'_'.$idserv.'_'.time()).'/3','Ver planilha','target=_blank');
        
        $this->cabecalho['js'] .= '
        if($("ul#opcoes-dropdown-modmenu").length)
        {
            var html = "";
            html += \'<li>'.$hazdoc.'</li>\';
            html += "<li><a href=\''.site_url('arquivos/listarserv/'.$idserv).'\' target=\'_blank\'>Arquivos deste serviço</a></li>";
            html += \'<li>'.$hazplan.'</li>\';
            html += "<li class=\'divider\'></li><li><a href=\''.site_url('home').'\'>Voltar à lista de serviços</a></li>"
            $("ul#opcoes-dropdown-modmenu").html(html);
        }';
        
        return;
    }
    
    public function geraModMenu($links)
    {
        if(empty($links)) return '';
        
        if($this->session->userdata('esta_logado'))
            $logocli = $this->banco->campo('usersdata','logo','iduser = '.$this->session->userdata('esta_logado'));
        if(empty($logocli))
            $logocli = site_url('img').'/semfoto.jpg';
        else
            $logocli = site_url('uploads').'/'.$logocli;
        
        $modmenu = '
        <div class="navbar navbar-inverse">
              <div class="navbar-inner">
                <div id="barrainversa" class="container">
             
                  <!-- .navbar-btn is used as the toggle for collapsed navbar content -->
                  <a class="btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="glyphicon-bar"></span>
                    <span class="glyphicon-bar"></span>
                    <span class="glyphicon-bar"></span>
                  </a>
             
                  <!-- Be sure to leave the brand out there if you want it shown -->
                  <a class="brand" href="'.$links[0]['url'].'">'.$links[0]['name'].'</a>
             
                  <!-- Everything you want hidden at 940px or less, place within here -->
                  <div class="navbar-collapse collapse">
                      <ul class="nav">';
        
        for($i=1;$i<count($links);$i++)
            $modmenu .= '<li class="nav-item '.(current_url() == $links[$i]['url'] ? 'active' : '').'">
            <a href="'.$links[$i]['url'].'">'.$links[$i]['name'].'</a>
            </li>';
        
        $modmenu .= '</ul>
                  </div>
                  
                  
                  <div class="navbar-collapse collapse pull-right">
                    <div id="atualmente" style="float:left; margin-right:10px;">
                    <a href="'.site_url('cadastro/eu').'" title="Logado como '.$this->session->userdata('nome_usuario').'"><img src="'.$logocli.'" style="height:30px;padding:5px 0px;"></a>
                    </div>';
        
        if($this->banco->isUser('coordenador',$this->session->userdata('esta_logado')) ||
        $this->banco->isUser('consultor',$this->session->userdata('esta_logado')) ||
        $this->banco->isUser('administrador',$this->session->userdata('esta_logado')))
        {
            $modmenu .= '<ul class="navbar pull-right">
                        <li class="nav-item dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Opções <b class="caret"></b></a>
                            <ul class="dropdown-menu" id="opcoes-dropdown-modmenu">
                              
                            </ul>
                          </li>
                         </ul>';
        }
        
        $modmenu .= '</div>
                  
                </div>
              </div>
        </div>
        ';
        
        return $modmenu;
    }
	
	public function geraRodape()
	{
		// ******* VERSAO DO SISTEMA *********
		$versao = '0.2';
		return '&copy;'.date('Y').' Argument Tecnologia -- Versão ' . $versao;
	}
	
    public function geraForm($campos,$rules,$atributos=array(),$readonly=false)
	{
		$labels = array_keys($rules);
		$a = 0;
		foreach($campos as $name => $val)
		{
			if(is_numeric($name))
			{
				$name = $val;
				$val = 'text';
			}
			
			// Buscando o valor default
			$vlr = '';
			if(!empty($atributos[$name]))
			{
				$vlr = ( empty($atributos[$name]['value']) ? '' : $atributos[$name]['value'] );
			}

			// Montando de acordo com o tipo
			switch($val)
			{
				case 'textlarge' :
					$attr = array('class' => 'input-xxlarge');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
				
				case 'textsmall' :
					$attr = array('class' => 'input-small');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
				
				case 'textxsmall' :
					$attr = array('class' => 'input-mini','style' => 'text-transform:uppercase;');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;	
				
				case 'text' :
					$attr = array('class' => 'input-xlarge');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
                
                case 'text_autocomplete' :
                    $attr = array('class' => 'input-xlarge');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
                    $this->form->text($name,$labels[$a],'',$vlr,$attr);
                    $this->form->html('
                    <script>
                    $(function() {
                        var elementos = '.$rules[$labels[$a]].';
                        $(\'#'.$name.'\').typeahead({
                            source: elementos
                        });
                    });
                    </script>
                    ');
                    break;
                    
                case 'date' :
                    $attr = array('class' => 'input-small', 'size' => '16');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
                    $this->form->html('<div style="display:block" class="input-append date" id="dp3" data-date="'.date('d/m/Y',strtotime('now')).'" data-date-format="dd/mm/yyyy">');
                    $this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
                    $this->form->html('<span class="add-on"><i class="fas fa-calendar-alt"></i></i></span>');
                    $this->form->html('</div>');
                    $this->form->html('
                    <script>
                    $(function() {
                        $(\'#dp3\').datepicker();
                    });
                    </script>
                    ');
                    break;
					
				case 'textcnpj' :
					$attr = array('class' => 'input-large','alt' => 'cnpj');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
				
				case 'textcpf' :
					$attr = array('class' => 'input-large','alt' => 'cpf');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
					
				case 'textphone' :
					$attr = array('class' => 'input-large','alt' => 'phone', 'placeholder' => '(XX) XXXX-XXXX');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
					
				case 'textmail' :
					$attr = array('class' => 'input-large','placeholder' => 'exemplo@email.com');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
                
                case 'texturl' :
                    $attr = array('class' => 'input-large','placeholder' => 'http://');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
                    $this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
                    break;
				
                case 'textcep' :
                    $attr = array('class' => 'input-large','alt' => 'cep', 'placeholder' => '00000-000');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
                    $this->form->text($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
                    break;
                
				case 'pass' :
                    if(!$readonly)
					$this->form->password($name,$labels[$a],$rules[$labels[$a]],$vlr);
					break;
					
				case 'hidden' :
					$this->form->hidden($name,$rules[$labels[$a]]);
					break;
				
				case 'textarea' :
					$attr = array('class' => 'input-xxlarge');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->textarea($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
					break;
					
				case 'textarea_advanced' :
					$attr = array('class' => 'input-xxlarge ckeditor');
                    ($readonly ? $attr['readonly'] = 'readonly' : '');
					$this->form->textarea($name,$labels[$a],$rules[$labels[$a]],$vlr,$attr);
                    
					break;
					
				case 'iupload' :
                    $attr = array('class' => 'input-xlarge');
                    if(!$readonly)
					$this->form->iupload($name,$labels[$a],'',$attr);
					break;
				
                case 'checkbox' :
                    $attr = array();
                    $this->form->html('<p><label>');
                    $this->form->checkbox($name,$rules[$labels[$a]],$labels[$a],$vlr,$attr);
                    $this->form->html('</label></p>');
                    break;
                
				case 'select' :
					$attr = array();
                    if(!$readonly)
					   $this->form->select($name,$rules[$labels[$a]],$labels[$a],$vlr,'',$attr);
                    else
                       $this->form->html('<p><strong>'.$labels[$a].'</strong>: '.$rules[$labels[$a]][$vlr].'</p>');
					break;
				
				case 'select_required' :
					$attr = array();
                    if(!$readonly)
					   $this->form->select($name,$rules[$labels[$a]],$labels[$a],$vlr,'required',$attr);
                    else
                       $this->form->html('<p><strong>'.$labels[$a].'</strong>: '.$rules[$labels[$a]][$vlr].'</p>');
					break;
					
				case 'checkgroup' :
					$attr = array('class' => 'inline');
					//$this->form->html('<div>');
					if(!$readonly)
					   $this->form->checkgroup($name,$rules[$labels[$a]],$labels[$a],$vlr,'',$attr);
                    else
                       $this->form->html('<p><strong>'.$labels[$a].'</strong>: '.$rules[$labels[$a]][$vlr].'</p>');
					//$this->form->html('</div>');
					break;
					
				case 'checkgroup_required' :
					$attr = array('class' => 'inline');
					//$this->form->html('<div>');
					if(!$readonly)
					   $this->form->checkgroup($name,$rules[$labels[$a]],$labels[$a],$vlr,'required',$attr);
                    else
                       $this->form->html('<p><strong>'.$labels[$a].'</strong>: '.$rules[$labels[$a]][$vlr].'</p>');
					//$this->form->html('</div>');
					break;
			}
			$a++;
		}
	}

	public function geraStringSeguranca($section='',$extra=0,$cod=0)
	{
		if(empty($section))
		{
			$section = 'site';
		}
		
		$lestring = $section.'_'.$extra.'_'.$cod.'_'.time();
		$resultado = base64_encode($lestring);
		
		return $resultado;
	}


    public function geraArvore($condicao="")
    {
        $this->cabecalho['arvore'] = '
        <div id="arvore-div">
        <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pastas</h3>
        <div id="arvore" style="padding: 2px 6px">
        </div>
        </div>
        <p>&nbsp;</p>
        ';
        $this->geraJavascript('arvore');
    }

	public function geraPerfil($condicao="")
    {
        if(!empty($condicao))
        {
            echo '<p class="loud">'.$this->session->userdata('nome_usuario').'<br>[ '. anchor('login/encerra_sessao','logout').' ]</p>';
        }
        else
            {
                $nivel = array('yes' => 'administrador', 'no' => 'usuário');
                $grupos = array();
                foreach($this->banco->listagem('groups') as $item) {
                    if (in_array($item->id,$this->session->userdata('grupos')))
                        $grupos[$item->id] = $item->name;
                }
                
                
                $this->dados['perfil'] = '
                <p class="loud">'.$this->session->userdata('nome_usuario').'
                <br><small>'. $this->session->userdata('e-mail').'</small></p>
                <p class="small">Nível <em>'. $nivel[$this->session->userdata('nivel')] .'</em>
                <br>Grupo(s) <u>'.implode(', ',$grupos).'</u></p>
                <hr />
                ';
                
                $orgdata = $this->session->userdata('orgdata');
                //$this->dados['pasta_org'] = '/'. basename(dirname(dirname(dirname(__FILE__)))) . '/' . $orgdata[0]['folder'];
                $raiz = str_replace(array('index.php','/'),array('',''),$_SERVER['SCRIPT_NAME']);
                $raiz = (empty($raiz) ? '' : '/'.$raiz) . '/';
                $this->dados['pasta_org'] = $raiz . $orgdata[0]['folder'];
            }
        
    }

	public function geraHistorico($condicao="")
	{
		if (empty($condicao)) {
			$condicao = 'idusu = '. $this->session->userdata('esta_logado');
		}
		$this->dados['historico'] = $this->mostra_historico(true,$condicao);
	}

	public function geraDestaques($condicao="")
	{
		if (empty($condicao)) {
			$condicao = '(action like "criou%" or action like "enviou%") and (module like "document" or module like "folder")';
		}
		$this->dados['destaques_box'] = $this->mostra_historico(false,$condicao);
	}
	
	public function geraJavaScript($tipo='',$param="")
	{
		//$pasta_sistema = '/'. basename(dirname(dirname(dirname(__FILE__)))) . '/';
        $raiz = str_replace(array('index.php','/'),array('',''),$_SERVER['SCRIPT_NAME']);
        $pasta_sistema = (empty($raiz) ? '' : '/'.$raiz) . '/';
		switch($tipo)
		{
			case 'arvore':
                $sis = $pasta_sistema . 'documentos/';
                
                $jscript = '
                setInterval("checkAnchor()", 300);
                
                $(function () {';
                //var xmlstring = "'.$this->geraFolderList(true).'";
                $jscript .= '
                $("#arvore")
                    .jstree({
                        "xml_data" : {
                            "ajax" : {
                                "url" : "'.$pasta_sistema.'pastas/folderlist/true/"+Math.random()
                                }
                            },
                            "plugins" : ["themes","xml_data","ui"] })
                    .bind("select_node.jstree", function (event, data) { 
                        if (data.rslt.obj.attr("id") == "arvore_item0")
                        $("div.conteudo").html("<h3></h3><p>Selecione uma subpasta.</p>");
                        else mostraArquivos(data.rslt.obj.attr("id"));
                    })
                    .delegate("a", "click", function (event, data) { event.preventDefault(); })
                });
                var icons = {
                    header: "ui-glyphicon-circle-arrow-e",
                    headerSelected: "ui-glyphicon-circle-arrow-s"
                };
                $( "#arvore-div" ).accordion( { icons: icons });
                $( "#arvore").css( {"height" : "200px"});
                ';
                
                $js_out = '
                function mostraArquivos(linkid)
                {
                    $("div.span-3").remove();
                    $("div.span-14").removeClass("span-14").addClass("span-17").addClass("last");
                    $(".span-24 h2").html("/ <a href=\''.$pasta_sistema.'home\'>home</a> / pastas");
                    $.ajax({
                        url : "'.$sis.'listar/"+linkid+"/"+Math.random(),
                        success: function(arquivos) {
                            $("div.conteudo").html(arquivos);
                            $.ajax({
                            url : "'.$pasta_sistema.'mensagens/listar/"+linkid+"/"+Math.random(),
                            success: function(html) {
                                $("#sol_list").html(html);
                                $(\'#datatable\').dataTable( {
                                    "oLanguage" : {"sUrl" : "'. $pasta_sistema .'js/media/language/pt_BR.txt" }
                                } );
                            }
                        });
                            
                        }
                    });
                    return false;
                }
                
                var currentAnchor = null;
                
                function checkAnchor()
                {
                    if(currentAnchor != document.location.hash)
                    {
                        currentAnchor = document.location.hash;
                        if(currentAnchor)
                        {
                            var local = currentAnchor.substring(1);
                            if(local.length == 6)
                            return mostraArquivos(local);
                        }
                    }
                    return false;
                }
                ';
                break;
		    
		    case 'listagem':
                $jscript = '
                $(\'#datatable\').dataTable( {
                    "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" }
                } );
                ';
                $js_out = '';
                break;
            
            case 'usercreate' :
                $jscript = '
                $("div#username_ok").hide();
                $("input#username").blur(function() {
                    if ($("input#username").val().length > 3)
                    {
                        $("#username_ok").show();
                        var valor = $("input#username").val();
                        $("#username_ok").load("'. site_url('usuarios/check_username') .'/"+valor);
                    }
                    else
                    {
                        $("#username_ok").show();
                        $("#username_ok").html("<small>Obs: Precisa ter pelo menos 4 caracteres.</small>");
                    }
                });
                ';
                $js_out = '';
                break;
            
            // Formatacao para os documentos baseados em templates de campo unico
            case 'template_regedit' :
                $j = "$(\"textarea#editor\").ckeditor(function(){},{skin: 'office2003', contentsCss : '". site_url('template/editor_css/'.$this->session->userdata('bg_image_tmp')) ."'});";
                $jscript = $j;
                $js_out = '';
                break;
            
            // Formatacao para os documentos baseados em templates de multiplos campos
            case 'template_regedit2' :
                $j = 'var $editors = $("textarea.editor");
                        if ($editors.length) {
                            $editors.each(function() {
                                var editorID = $(this).attr("id");
                                var instance = CKEDITOR.instances[editorID];
                                if (instance) { CKEDITOR.remove(instance); }
                                CKEDITOR.replace(editorID,{toolbar: \'Simples\', width: \'420px\', height: \'200px\', forcePasteAsPlainText: \'true\'});
                            });
                        }
                ';
                //$j = "$(\"textarea#editor\").ckeditor(function(){},{toolbar: 'Simples', width: '420px', height: '200px', forcePasteAsPlainText: 'true'});";
                $jscript = $j;
                $js_out = '';
                break;
            
            case 'solicitacoes':
                $jscript = '$("#sol_list").load("'.$pasta_sistema.'mensagens/listar?"+Math.random());';
                $js_out = '';
                break;
                
            case 'relatorios':
                $jscript = '
                $(\'#datatable\').dataTable( {
                    "oLanguage" : {"sUrl" : "'. base_url() .'js/media/language/pt_BR.txt" }
                } );
                var dates = $( "#datainicial, #datafinal" ).datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    numberOfMonths: 2,
                    onSelect: function( selectedDate ) {
                        var option = this.id == "datainicial" ? "minDate" : "maxDate",
                            instance = $( this ).data( "datepicker" ),
                            date = $.datepicker.parseDate(
                                instance.settings.dateFormat ||
                                $.datepicker._defaults.dateFormat,
                                selectedDate, instance.settings );
                        dates.not( this ).datepicker( "option", option, date );
                    }
                });
                $("select#hist_folder_div").change(function(){$("#hist_files_div").load("'.$pasta_sistema.'relatorios/listaDocumentos/"+$("select#hist_folder_div option:selected").val());});
                ';
                $js_out = '
                ';
                break;
            
            case 'autocomplete':
                $j = 'var $editors = $("textarea.editor");
                        if ($editors.length) {
                            $editors.each(function() {
                                var editorID = $(this).attr("id");
                                var instance = CKEDITOR.instances[editorID];
                                if (instance) { CKEDITOR.remove(instance); }
                                CKEDITOR.replace(editorID,{toolbar: \'Simples\', width: \'420px\', height: \'200px\', forcePasteAsPlainText: \'true\'});
                            });
                        }
                ';
                if (!empty($param))
                {
                    $jscript = '
                    $( "#name" ).autocomplete({
                        source: "'.$pasta_sistema.'documentos/buscaNomes/'.$param.'",
                        minLength: 2
                    });
                    '.$j;
                }
                else {
                  $jscript = $j;
                }
                $js_out = '';
                break;
            
            case 'multiupload':
                $jscript= '$("#lista_arquivos").load("'.$pasta_sistema.'documentos/lista_multi_arquivos/"+(new Date).getTime());';
                $js_out = '';
                break;
            
            case 'ocultar_cadastros' :
                $jscript = '';
                if($param == 4):
                        $jscript = '
                        $("div.navbar-fixed-top>div.navbar-inner>div.container>div.collapse>ul.nav>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('custos').'")
                                $(this).parent().remove();
                            if($(this).attr("href")=="'.site_url('produtos').'")
                                $(this).parent().remove();
                        });
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('cadastro/inserirgrupo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listargrupo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/coord').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/insumo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listar/insumos').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                        if(strstr(current_url(),'cadastro/'))
                        $jscript .= 'if($("select#groups"))
                        {
                            $("select#groups option[value=5]").remove();
                            $("select#groups option[value=6]").remove();
                        }
                        ';
                endif;
                if($param == 3):
                        $jscript = '
                        // Oculta a porra toda
                        $("div.navbar-fixed-top>div.navbar-inner>div.container>div.collapse>ul.nav>li>ul.dropdown-menu").remove();
                        // Coloca nomes legais
                        $("div.navbar-fixed-top>div.navbar-inner>div.container>div.collapse>ul.nav>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('cadastro/eu').'")
                                $(this).text("Cadastro");
                            if($(this).attr("href")=="'.site_url('custos').'")
                                $(this).parent().remove();
                            if($(this).attr("href")=="'.site_url('produtos').'")
                                $(this).parent().remove();
                            if($(this).attr("href")=="'.site_url('agenda/listar').'")
                                $(this).parent().remove();
                            $(this).parent().removeClass("dropdown");
                            $(this).removeClass("dropdown-toggle").removeAttr("data-toggle");
                        });
                        
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('cadastro/inserirgrupo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listargrupo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/inserir').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listar').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listar/cliente').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/insumo').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/listar/insumos').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/coord').'" ||
                               $(this).attr("href")=="'.site_url('cadastro/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                        if(strstr(current_url(),'cadastro/'))
                        $jscript .= '
                        if($("select#groups"))
                        {
                            $("select#groups").hide().prev().hide();
                        }
                        if($("input#username"))
                        {
                            $("input#username").hide().prev().hide();
                        }
                        ';
                endif;
                $js_out = '';
                break;
            
            case 'ocultar_custos' :
                
                $jscript = '';
                if($param == 4):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('custos/encargos').'" ||
                               $(this).attr("href")=="'.site_url('custos/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        
                        ';
                endif;
                if($param == 3):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('custos/atribuir').'" ||
                               $(this).attr("href")=="'.site_url('custos/encargos').'" ||
                               $(this).attr("href")=="'.site_url('custos/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        ';
                endif;
                if($param < 3):
                    $jscript = '
                        $("div.navbar-collapse ul.nav li.dropdown a.dropdown-toggle").each(function(){
                            if($(this).attr("href")=="'.site_url('custos').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                endif;
                $js_out = '';
                break;
            
            case 'ocultar_agenda' :
                
                $jscript = '';
                // Somente os que participa/coordena
                if($param == 4):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('agenda/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        
                        ';
                endif;
                // Somente os que participa
                if($param == 3):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('agenda/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        ';
                endif;
                // Apenas listagem (para status)
                if($param == 2):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('agenda/novo').'" ||
                               $(this).attr("href")=="'.site_url('agenda/status').'" ||
                               $(this).attr("href")=="'.site_url('agenda/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        ';
                endif;
                $js_out = '';
                break;
            
            case 'ocultar_produtos' :
                
                $jscript = '';
                if($param == 4):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('produtos/novo').'" ||
                               $(this).attr("href")=="'.site_url('produtos/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                        if(strstr(current_url(),'produtos/'))
                        $jscript .= '
                        if($("input#salvar"))
                        {
                            $("input#salvar").hide();
                        }
                        if($("form#produtos"))
                        {
                            $("#produtos input").attr("readonly", "readonly");
                            $("#produtos textarea").attr("readonly", "readonly");
                        }
                        ';
                endif;
                if($param < 4):
                    $jscript = '
                        $("div.navbar-collapse ul.nav :not(#interna) li.dropdown a.dropdown-toggle").each(function(){
                            if($(this).attr("href")=="'.site_url('produtos').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                endif;
                
                $js_out = '';
                break;
            
            case 'ocultar_financeiro' :
                
                $jscript = '';
                // Somente os que participa
                if($param == 4):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('financeiro/cliente').'" ||
                            $(this).attr("href")=="'.site_url('financeiro/funcionario').'" ||
                            $(this).attr("href")=="'.site_url('financeiro/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                        if(strstr(current_url(),'financeiro/'))
                        $jscript .= '
                        if($("form#financeirocli"))
                        {
                            $("#financeirocli input").attr("disabled", "disabled");
                        }
                        if($("form#financeirofun"))
                        {
                            $("#financeirofun input").attr("disabled", "disabled");
                        }
                        if($("form#financeiroger"))
                        {
                            $("#financeiroger input").attr("disabled", "disabled");
                        }
                        ';
                endif;
                // Somente listagem
                if($param == 3):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('financeiro/cliente').'" ||
                            $(this).attr("href")=="'.site_url('financeiro/funcionario').'" ||
                            $(this).attr("href")=="'.site_url('financeiro/relatorios').'")
                               {
                                   $(this).parent().hide();
                               }
                        });';
                        if(strstr(current_url(),'financeiro/'))
                        $jscript .= '
                        if($("form#financeirocli"))
                        {
                            $("#financeirocli input").attr("disabled", "disabled");
                        }
                        if($("form#financeirofun"))
                        {
                            $("#financeirofun input").attr("disabled", "disabled");
                        }
                        if($("form#financeiroger"))
                        {
                            $("#financeiroger input").attr("disabled", "disabled");
                        }
                        ';
                endif;
                
                $js_out = '';
                break;
            
            case 'ocultar_documentos' :
                
                $jscript = '';
                $js_out = '';
                break;
            
            case 'ocultar_arquivos' :
                
                $jscript = '';
                // Somente o dele
                if($param == 3):
                        $jscript = '
                        $("ul.nav>li>a, ul.dropdown-menu>li>a").each(function(){
                            if($(this).attr("href")=="'.site_url('arquivos/listarserv').'" ||
                            $(this).attr("href")=="'.site_url('arquivos/listarcli').'")
                               {
                                   $(this).parent().hide();
                               }
                        });
                        
                        
                        
                        ';
                endif;
                $js_out = '';
                break;
            
            default:
                $jscript = '';
                $js_out = '';
                break;
        }
        
		$this->cabecalho['js'] .= $jscript; // funcoes jquery
		$this->cabecalho['js_out'] .= $js_out; // outras funcoes javascripts
		//return $jscript;
	}

    function geraFileTypes($fullpath=false)
    {
        $this->load->helper('file');
        
        $sis = './js/kcfinder/themes/oxygen/img/files/small/';
        if (!$fullpath)
        $lista = get_filenames($sis);
        else
            {
                $lista = get_filenames($sis);
                foreach($lista as $item)
                {
                    //$sis = '/'. basename(dirname(dirname(dirname(__FILE__)))) . '/js/kcfinder/themes/oxygen/img/files/small/';
                    $raiz = str_replace(array('index.php','/'),array('',''),$_SERVER['SCRIPT_NAME']);
                    $raiz = (empty($raiz) ? '' : '/'.$raiz) . '/';
                    //$sis = $raiz . 'js/kcfinder/themes/oxygen/img/files/small/';
                    $sis = site_url('js/kcfinder/themes/oxygen/img/files/small/') . '/';
                    $newlista[strtolower(basename($item,'.png'))] = $sis.$item;
                }
                $lista = $newlista;
            }
        
        return $lista;
    }
    
    
    
    function listaArqServico($idserv,$justqtde=false,$exclusao=false,$selectable=false)
    {
        // Lista os serviços disponíveis e seus arquivos
        $filt = "";
        
        $lista = $this->banco->listagem('docs','id','idserv = '. (int) $idserv);
        if(empty($lista))
        {
            if($justqtde) return 0;
            //echo '<p>Nenhum arquivo encontrado.</p>';
            return false;
        }

        $tbl = '<table width="100%" border="0">
          <thead class="ui-state-default">
          <tr>
            <td>Nome</td>
            <td>Descrição</td>
            <td>Data de criação</td>
            <td>&nbsp;</td>
          </tr>
          </thead><tbody>';
        $c = 0;
        
        // Lista de icons
        $icons = $this->geraFileTypes(true);
        
        foreach($lista as $item)
        {
            $icon = '<img src="'.$icons[$this->verificaMimeType($item->type)].'" alt="'.$item->type.'" title="'.$item->type.'" border="0" align="left" />';
            $baixar = base64_encode($item->id . '|||' . $item->date_created);
            $checked = ($this->banco->campo('services','document','id = '. (int) $idserv) == $baixar ? 'checked="checked"' : '');
            if($c == 1) $checked = 'checked="checked"'; else $checked = '';
            $delLink = '';
            if($this->session->userdata('nivel') == 'yes')
                $delLink = '<a href="#" onclick="if(confirm(\'Tem certeza que deseja remover este documento? Não há como desfazer esta ação!\')){removedoc(\''.$baixar.'\')}else{return false;}"><i class="fas fa-times"></i>&nbsp;Remover</a>';
            
            $tbl .= '
            <tr>
                <td>
                '. (empty($item->content) ? $item->name . "<br /><small>Obs: nenhum arquivo anexo no registro!</small>" : $icon).'&nbsp;
                '.(($selectable && $c > 0) ? '<input type="checkbox" onchange="verificaChecks()" '.$checked.' name="selfile[]" id="selfile_'.$item->id.'" value="'.$baixar.'">&nbsp;' : '').'
                <strong>'.(($item->locked == 'yes') ? '<em>Documento indisponível.</em>' : anchor('documentos/download/'.$baixar,$item->name.
                ($this->verificaMimeType($item->type) == 'txt' ? '' : '.'.$this->verificaMimeType($item->type)))).'</strong>
                </td>
                <td>'. (strstr($item->desc,'1') ? '...' : word_limiter($item->desc,15)).'</td>
                <td>'. date('d/m/Y H:i',strtotime($item->date_created)) .'</td>
                <td>
                '.(($item->idtpl > 0) ? '' : $delLink).'
                </td>
            </tr>
            ';
            $c++;
        }
        $tbl .= "</tbody></table>";
        $tbl .= '
        <script>
        function removedoc(code)
        {
            $.ajax({
                url : "'.base_url().'arquivos/remover/"+code+"/"+Math.random(),
                success: function(html) {
                    if(html.length > 0)
                        alert(html);
                    $.get("'.site_url('arquivos/listarservAjax/'.$idserv).'",function(data){ $("#listaArqs").html("").html(data); });
                }});
        }
        </script>';
        
        if($justqtde) return $c;
        return $tbl;
    }

    function verificaMimeType($mime,$inverse=false)
    {
        if (defined('ENVIRONMENT') AND is_file(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT))
        {
            include(APPPATH.'config/'.ENVIRONMENT.'/mimes'.EXT);
        }
        elseif (is_file(APPPATH.'config/mimes'.EXT))
        {
            include(APPPATH.'config/mimes'.EXT);
        }
        
        if ($inverse)
        {
            if(empty($mimes[$mime])) return "txt";
            $val = $mimes[$mime];
            if (is_array($val))
                return $val[0];
            else 
              return $val;
        }
        // Verificando os tipos mais comuns primeiro
        if (stristr($mime,'excel')) return "xls";
        if (stristr($mime,'jpeg')) return "jpg";
        if (stristr($mime,'jpg')) return "jpg";
        if (stristr($mime,'png')) return "png";
        
        $extensao = $this->array_searchKey2($mime,$mimes);
        if (is_array($extensao)) $extensao = $extensao[0]; // TODO: não está funcionando bem...
        if (strlen($extensao) > 1)
        {
            return $extensao;
        }
        else
        {
            return "txt";
        }
    }

    function array_searchKey2($needle, $haystack,$strict=false,$path=array())
    {
        if( !is_array($haystack) ) {
        return false;
        }
     
        foreach( $haystack as $key => $val ) {
            if( is_array($val) && $subPath = $this->array_searchKey2($needle, $val, $strict, $path) ) {
                $path = array_merge($path, array($key), $subPath);
                return $path;
            } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
                $path[] = $key;
                return $path;
            }
        }
        return false;
    }
    
    function geraFolderList($xml=false,$condicao="")
    {
        if ($condicao)  
            $res = $this->banco->listagem('folders','name','idorg = '. $this->org['id'] .' and '. $condicao);
        else
            $res = $this->banco->listagem('folders','name','idorg = '. $this->org['id']);
        
        $lista[0] = $this->org['name'];
        $nivel = array('','-','--','---','----','-----','------','-------','--------','---------','----------');
        
        foreach($res as $item)
        {
            // guarda o id de todos os niveis
            $niveis[$item->folder_level][] = $item->nameid;
            // guarda o id de todos os parentes (exceto do nivel 0)
            if ($item->folder_parent > 0)
                $parentes[$item->folder_parent][] = $item->nameid;
            // guarda o nome de todas as pastas, indexadas pelo id
            $lista[$item->nameid] = $nivel[$item->folder_level] . ' ' . $item->name;
        }
        ksort($niveis);
        
        // guarda o numero de pastas
        $quantidade = count($lista) - 1;
        $folderNivel = 1;
        $newLista = array();
        $newOrdem = array();
        while($quantidade > 0)
        {
            // Primeiro nivel é folderNivel = 1
            if (!empty($niveis[$folderNivel]))
            foreach($niveis[$folderNivel] as $itemLista)
            {
                if (!array_key_exists($itemLista,$newLista))
                {
                    $newLista[$itemLista] = $lista[$itemLista];
                    $newOrdem[$itemLista] = $itemLista;
                }
                // verifica se possui subfolders
                if (!empty($parentes[$itemLista]))
                {
                    foreach($parentes[$itemLista] as $itemParente)
                    {
                        $newLista[$itemParente] = $lista[$itemParente];
                        $newOrdem[$itemParente] = $itemLista;
                    }
                }
                $quantidade--;
            }
            $folderNivel++;
        }
                
        // Reordenando a lista
        foreach($newOrdem as $dir => $dirpai)
        {
            if ($dir != $dirpai)
            {
                // Copia o valor do filho antes
                $tmp = $newLista[$dir];
                // Remove o filho do array
                unset($newLista[$dir]);
                // Insere o filho logo depois do pai
                $newListaTmp = array();
                foreach($newLista as $ch => $vl)
                {
                    $newListaTmp[$ch] = $vl;
                    if ($ch == $dirpai)
                    {
                        $newListaTmp[$dir] = $tmp;
                    }
                }
                asort($newListaTmp);
                // Salva a alteracao
                $newLista = $newListaTmp;
                // Apaga a variavel temporaria
                unset($tmp);
            }
        }       
        
        // Criando a versao XML
        if ($xml) :
            $saida = '<?xml version="1.0" encoding="UTF-8"?>';
            $saida .= '<root>';
            foreach($newLista as $nameid => $name)
            {
                // elimina os caracteres indicando subfolders
                $itemp = explode('- ',$name);
                if ($itemp && count($itemp) > 1)
                    $name = $itemp[1];
                else
                    $name = $itemp[0];
                    
                // monta o xml
                $parente = $this->banco->campo('folders','folder_parent','nameid like "'.$nameid.'"');
                if ($parente > 0) $parente = " parent_id='".$parente."'"; else $parente = '';
                $saida .= "<item id='".$nameid."'".$parente.">";
                $saida .= "<content><name><![CDATA[".$name."]]></name></content>";
                $saida .= "</item>";
            }
            $saida .= '</root>';
            $newLista = $saida;
        endif;
        /*
        // DEBUG
        if (!empty($niveis)) {
            echo 'NIVEIS : <br>'; print_r($niveis);
        }
        if (!empty($parentes)) {
            echo '<br><br>PARENTES : <br>'; print_r($parentes);
        }
        echo '<br><br>Quantidade: ' . $quantidade = count($lista) - 1;
        echo '<br><br>Ordem da lista final : <br>'; print_r($newOrdem);
        echo '<br><br>Lista final : <br>'; print_r($newLista);
        exit;
        */

        return $newLista;
    }
    
    public function isServReadOnly($idserv)
    {
        // verifica se o serviço pode ser alterado
        if(empty($idserv)) return false;
        $ids = $this->banco->campo('services','idstatus','id = '. (int) $idserv);
        if($ids == 7)
            return true;
        else
            return false;
    }
    
    public function checaPermissao($folderid="")
    {
        /*
         * Função que verifica os privilégios de acesso do usuário logado
         * 
         * 
         * 
         * 5 = nivel maximo
         * 4
         * .
         * .
         * 1 = nível baixo
         * 
         */
        
        // Busca o array de permissões
        $perms = $this->session->userdata('permissoes');
        $pagina_atual = $this->session->userdata('pagina_atual');

        /*
         Atualmente, o sistema não trata permissões, logo, todo mundo é administrador.
        */
        
        return 5; // 5 = nivel maximo (pode tudo)
        
        //if ($this->session->userdata('nivel') == 'yes') 
        //    return 5;
        
        switch($pagina_atual)
        {
            case 'home' :
                // Vai de acordo com o grupo
                $this->cabecalho['submenu'] = '';
                
                break;
            
                default :
                
                    $valor = $perms->$pagina_atual;
                    
                    return $valor;
                
                break;
                
                
        }
        
    }

    private function search_in_array($needle,$haystack)
    {
        if (is_array($needle))
        {
            foreach($needle as $item)
            {
                if (in_array($item,$haystack))
                    $ok = true;
                else
                    $ok = false;
            }
        }
        else
            $ok = in_array($needle,$haystack);
        return $ok;
    }
    
    public function mostra_solicitacoes($foldername="")
    {
        $userid = $this->session->userdata('esta_logado');
        if (empty($userid)) {
            echo 'Falha ao mostrar solicitações.';
            return false;
        }
        // lista de usuarios e emails
        $res = $this->banco->listagem('users','name','islocked = "no"');
        foreach($res as $row)
        {
            $dados['usuarios'][$row->id] = $row->name;
            $dados['emails'][$row->id] = $row->email;
        }
        
        // lista de status com cores
        $res = $this->banco->listagem('messages_status','name');
        foreach($res as $row)
        {
            $dados['statuses'][$row->id] = '<span style="color:'.$row->color.'">'. $row->name . '</span>';
        }
        if (empty($foldername))
            $dados['resultado_query'] = $this->banco->listagem('messages','date_created','from_user = '. $userid .' or to_user = '. $userid);
        else
            {
                // Busca as mensagens da pasta selecionada
                $folderid = $this->banco->campo('folders','id','nameid = '.$foldername);
                $tmpmsgs = $this->banco->relacao('messages','','folders',$folderid);
                if (!empty($tmpmsgs))
                {
                    $dados['resultado_query'] = $this->banco->listagem('messages','date_created','(from_user = '. $userid .' or to_user = '. $userid .') and id in ('.implode(',',$tmpmsgs).')');
                }
            }
        $this->load->view('mensagens/listar',$dados);
    }
    
    public function enviar_email($de,$para,$tipo,$params=array())
    {
        $this->load->library('email');

        if (is_numeric($de)) // busca o email pelo o codigo do usuario
            $de = $this->banco->campo('users','email','id = '. $de);
        //$this->email->from($de);
        $this->email->from('no-reply@luizotavio.info');
        
        if (is_numeric($para)) // busca o email pelo o codigo do usuario
            $para = $this->banco->campo('users','email','id = '. $para);
        $this->email->to($para);
        
        // busca todos os dados dos usuarios envolvidos
        
        switch($tipo) :
            case 'nova_solicitacao' :
                $assunto = 'Nova solicitação por '.$params['usuario'];
                $mensagem = "\n";
                $mensagem .= "O usuário " . $params['usuario'] . " (".$de.") solicita um documento na pasta ". $params['pasta'];
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e responda à solicitação deste usuário.";
            break;
            case 'editar_solicitacao' :
                $assunto = 'Solicitação de '.$params['usuario'] . ' foi atualizada';
                $mensagem = "\n";
                $mensagem .= "O usuário " . $params['responsavel'] . " (".$de.") alterou o staus da solicitacao criada por ".$params['usuario'] . ", feita na pasta ". $params['pasta'] .". ";
                $mensagem .= "\n\n";
                $mensagem .= "Status: " . strtoupper($params['status']);
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            case 'comentar_solicitacao' :
                $assunto = 'Novo comentário na solicitação de '.$params['usuario'];
                $mensagem = "\n";
                $mensagem .= "O usuário " . $params['usuario'] . " (".$de.") enviou um novo comentário sobre a solicitacao feita na pasta ". $params['pasta'] .". ";
                $mensagem .= "\n\n";
                $mensagem .= "Comentário: " . $params['comentario'];
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            case 'novo_arquivo' :
                $assunto = 'Novo arquivo na pasta '.$params['pasta'];
                $mensagem = "\n";
                $mensagem .= "O usuário " . $params['usuario'] . " (".$de.") enviou um novo arquivo para a pasta ". $params['pasta'] .". ";
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            case 'novo_servico' :
                $assunto = 'Um novo serviço foi criado';
                $mensagem = "\n";
                $mensagem .= "Você está recebendo este e-mail porque o usuário " . $params['usuario'] . " (".$de.") criou um novo serviço intitulado " . $params['nome'] . " (cód: ".$params['num']."), no qual você fará parte. ";
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            case 'novo_status' :
                $assunto = 'Novo status para o serviço '.$params['num'];
                $mensagem = "\n";
                $mensagem .= "O serviço intitulado " . $params['nome'] . " (cód: ".$params['num'].") foi alterado para o status " . $params['status'] . " por " . $params['usuario'] . ". ";
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            case 'financeiro_atualizado' :
                $assunto = 'A planilha financeira foi atualizada';
                $mensagem = "\n";
                $mensagem .= "Um dado da planilha financeira associada a seu perfil foi atualizada por " . $params['usuario'] . ". ";
                $mensagem .= "\n\n";
                $mensagem .= "Por favor, acesse o GESTOR DE CONSULTORIAS e veja mais detalhes.";
            break;
            default :
                $assunto = $params['assunto'];
                $mensagem = $params['mensagem'];
            break;
        endswitch;
        
        $rodape = "\n\n---\n\nEnviado pelo GESTOR DE CONSULTORIAS em ". date('d/m/Y H:i:s');

        $this->email->subject('['.$this->org['name'].'] '.$assunto);
        $this->email->message($mensagem . $rodape);
        
        if ($this->email->send())
            return true;
        else
            return false;
    }
    
    public function historico($module,$action="")
    {
        // Gera um registro no histórico de ações no sistema
        /* Registra:
         *  - Data/hora do login, com dados do ip, browser e sistema operacional do usuário que acabou de logar.
            - Data/hora do documento ou arquivo baixado.
            - Data/hora do template que foi criado, editado ou removido.
            - Data/hora do documento que foi criado, editado ou removido.
            - Data/hora da pasta que foi criada, editada ou removida.
            - Data/hora do comentário que foi criado ou removido.
            - Data/hora da solicitação que foi criada, editada ou removida.
         * 
         */
        if(empty($module)) return false;
        $data = date('Y-m-d H:i:s');
        $idusu = $this->session->userdata('esta_logado');
        
        switch($module) :
            case 'login' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'login',
                    'action' => 'entrou no sistema utilizando ' . $this->session->userdata('user_agent'),
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
                // Atualiza o campo de ultimo login
                $this->banco->atualiza('users',array('last_login' => date('Y-m-d H:i:s'), 'id' => $idusu));
            break;
            case 'logout' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'login',
                    'action' => 'saiu do sistema utilizando ' . $this->session->userdata('user_agent'),
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            case 'download' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'download',
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            case 'template' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'template',
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            case 'document' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'document',
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            case 'folder' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'folder',
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            case 'comment' :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => 'comment',
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
            break;
            default :
                $dados = array(
                    'idusu' => $idusu,
                    'module' => $module,
                    'action' => $action,
                    'date' => $data,
                    'info' => $this->session->userdata('ip_address'),
                    'idorg' => $this->org['id']
                );
                break;
        endswitch;
        $id = $this->banco->insere('historic',$dados);
        // Atualiza o campo de ultimo historico gravado
        $this->banco->atualiza('users',array('last_hist_action' => date('Y-m-d H:i:s'), 'id' => $idusu));
        return true;
    }
    
    public function who_is_online()
    {
        $this->load->model('sistema','banco',TRUE);
        $t = $this->banco->campo('users','id','islocked = "no" and idorg = 1');
        $qtde = 0;
        foreach($t as $i)
        {
            if($i != $this->session->userdata('esta_logado'))
            {
                if(!$this->banco->offline($i)) $qtde++;
            }
        }
        
        if($qtde == 0) return '';
        
        return '<span title="'.$qtde.' online" class="badge badge-success" onclick="document.location.href=\''.site_url('chat').'\'" style="cursor:pointer;position:absolute; top:10px; margin-left:90px; opacity:0.6;">'.$qtde.'</span>';
    }
    
    public function nao_lidas()
    {
        $this->load->model('sistema','banco',TRUE);
        if(!$this->session->userdata('esta_logado')) return '';
        $qtde = $this->banco->naolidas($this->session->userdata('esta_logado'));
        
        if($qtde == 0) return '';
        
        return '<span title="'.$qtde.' mensagens não-lidas" class="badge badge-success" onclick="document.location.href=\''.site_url('chat').'\'" style="cursor:pointer;position:absolute; top:10px; margin-left:90px; opacity:0.6;">'.$qtde.'</span>';
    }
    
    private function mostra_historico($resumido=true,$condicao,$itens=10)
    {
        if($resumido)
            $msg = '<h3>Histórico</h3><div id="page_container1"><ul class="content">';      
        else
            $msg = '<ul style="margin:0.padding:0">';
        $lista = $this->banco->listar('historic',$itens,0,'date desc',$condicao);
        $i = $this->session->userdata('esta_logado');
        foreach($lista as $item) :
            if ($i != $item->idusu) {
                $nome_usuario = $this->banco->campo('users','name','id = '.$item->idusu) . ' ';
                if (empty($nome_usuario)) $nome_usuario = 'Usuário indefinido (removido?) ';
            } else { $nome_usuario = 'Você '; }
            $acao = $item->action;
            // Tratamento das mensagens de acao
            switch($item->module)
            {
                case 'login':
                case 'logout':
                    $titulo = '<span style="color:red">Autenticação</span><br>';
                    break;
                
                case 'document':
                    $titulo = '<span style="color:blue">Documentos</span><br>';
                    break;
                
                case 'template':
                    $titulo = '<span style="color:green">Templates</span><br>';
                    break;
                    
                case 'folder':
                    $titulo = '<span style="color:orange">Pastas</span><br>';
                    break;
                
                case 'comment':
                    $titulo = '<span style="color:tomato">Comentários</span><br>';
                    break;
                    
                case 'download':
                    $titulo = '<span style="color:slategrey">Download</span><br>';
                    break;
                
                default:
                    $titulo = '<span style="color:purple">'.$item->module.'</span><br>';
                    break;
            }
            if ($resumido)
                $msg .= '<li>'. $titulo .'<div style="border-bottom: 1px dotted black; text-align:right; font-size:10px">'. date('d/m \à\s H\hi',strtotime($item->date)) . '</div><div style="padding-top:8px"><small> '. $nome_usuario . $acao .'</small></div></p></li>';// . ' ('.$item->info.')';
            else
                $msg .= '<li style="list-style:none;">'. $titulo .'<div style="border-bottom: 1px dotted black; text-align:right; float:right; font-size:10px">'. date('d/m/Y \à\s H\hi',strtotime($item->date)) . '</div><div style="padding-top:2px"> '. $nome_usuario . $acao . /*'<br>(IP: '.($item->info == '0.0.0.0' ? 'Não identificado' : $item->info).')' .*/'</div></p></li>';// . ' ('.$item->info.')';
        endforeach;
        if ($resumido)
            $msg .= '</ul><div class="page_navigation"></div></div><hr />';
        else
            $msg .= '</ul>';
        return $msg;
    }
    
    public function limpa_historico($meses)
    {
        if (empty($meses)) return false;
        $res = $this->banco->remover('historic_clean',$meses);
        return $res;
    }
	
	public function remove_acentos($string, $slug = false) 
	{
		$string = strtolower($string);
	
		// Código ASCII das vogais
		$ascii['a'] = range(224, 230);
		$ascii['e'] = range(232, 235);
		$ascii['i'] = range(236, 239);
		$ascii['o'] = array_merge(range(242, 246), array(240, 248));
		$ascii['u'] = range(249, 252);
	
		// Código ASCII dos outros caracteres
		$ascii['b'] = array(223);
		$ascii['c'] = array(231);
		$ascii['d'] = array(208);
		$ascii['n'] = array(241);
		$ascii['y'] = array(253, 255);
	
		foreach ($ascii as $key=>$item) {
			$acentos = '';
			foreach ($item AS $codigo) $acentos .= chr($codigo);
			$troca[$key] = '/['.$acentos.']/i';
		}
	
		$string = preg_replace(array_values($troca), array_keys($troca), $string);
	
		// Slug?
		if ($slug) {
			// Troca tudo que não for letra ou número por um caractere ($slug)
			$string = preg_replace('/[^a-z0-9]/i', $slug, $string);
			// Tira os caracteres ($slug) repetidos
			$string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
			$string = trim($string, $slug);
		}
	
		return $string;
	}

	function remove_enter($comm)
	{
		$comm = trim($comm,"\t\n[]\"'");
		$comm = (string)str_replace(array("\r", "\r\n", "\n"), ' ', $comm);
		$comm = (string)str_replace(array("\"", "'"), "\\\"", $comm);
		return $comm;
	}
    
    function preco($valor,$reais=true)
    {
        //if($valor == 'n/d') return 'n/d';
            
        if (empty($valor))
        return ($reais ? 'R$ ' : '') . '0,00';
        
        
        
        // Se o numero já estiver correto
        $test = explode(".",$valor);
        if(strstr($valor,',') || count($test) > 2)
        {
            $valor = $this->limpapreco($valor);
        }
        
        //echo '<p>'.$valor.'</p>';
        
                //echo $valor . '<br>';
        return ($reais ? 'R$ ' : '') . number_format(trim($valor), 2, ',', '.');
    }
    
    function limpapreco($valor)
    {
        //if($valor == 'n/d') return 'n/d';
        
        if (empty($valor))
        return '0.00';
        
        // O numero ja esta no formato correto
        if(strstr(substr($valor,-3),'.'))
        {
            $valor = trim(str_replace(array('R$','%',' '),array('','',''),$valor));
            // Porém, é bom verificar os pontos no separador de milhar - 22/08/2013
            $test = explode(".",$valor);
            if(count($test) > 2)
            {
                $ntest = "";
                foreach($test as $i => $t)
                {
                    if($i != (count($test)-1))
                        $ntest .= $t;
                }
                $valor = $ntest . '.' . $test[count($test)-1];
            }
        }
        else
        {
            $valor = trim(str_replace(array('R$','%','.',' ',','),array('','','','','.'),$valor));
        }
        return number_format($valor, 2, '.', '');
    }
    
    function nbsp($q=1)
    {
        $n = '';
        for($i=1;$i<=$q;$i++)
            $n .= '&nbsp;';
        return $n;
    }
    
    function geraMsg($tipo,$mensagem,$redirect)
    {
        $this->session->set_flashdata($tipo,$mensagem);
        redirect($redirect);
        return false;
    }
    
    function geraUsersByGroupKind($kind)
    {
        $lis = $this->banco->listagem('groups','id','kind = '.$kind);
        $res = array();
        foreach($lis as $l)
        {
            $lisusu = $this->banco->relacao('users','','groups',$l->id);
            $res = array_merge((array)$lisusu,(array)$res);
        }
        return $res;
    }
    
    function removeEnter($body)
    {
        $breaks = array("\r\n", "\n", "\r");
        $newtext = str_replace($breaks, "", $body);
        $newtext = mysql_real_escape_string($newtext);
        return $newtext;
    }
    
    function calc_meses($data_inicial,$data_final)
    {
        $diff = abs($data_final - $data_inicial);
        $anos = floor($diff / (365*60*60*24));
        $meses = floor(($diff - $anos * 365*60*60*24) / (30*60*60*24));
        $meses = $meses + ($anos > 0 ? ($anos*12) : $anos);
        
        return ($meses > 1 ? $meses+1 : $meses);
    }
    
    function change_case_recursive($arr){ 
        foreach ($arr as $key=>$val){ 
            if (!is_array($arr[$key])){ 
                $arr[$key]=mb_strtoupper($arr[$key]); 
            }else{ 
                $arr[$key]=$this->change_case_recursive($arr[$key]); 
            } 
        } 
        return $arr;
    }
    
    function &array_find_element_by_key($key, &$form) 
    {
      $ret = false;
      if (array_key_exists($key, $form)) {
        $ret =& $form[$key];
        return $ret;
      }
      foreach ($form as $k => $v) {
        if (is_array($v)) {
          $ret =& $this->array_find_element_by_key($key, $form[$k]);
          if ($ret) {
            return $ret;
          }
        }
      }
      return $ret;
    }
}
?>
