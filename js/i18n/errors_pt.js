/**
 * locale pt
 */
$.extend($.idealforms.errors, {

  required: 'Este campo é obrigatório.',
  number: 'Precisa ser um número',
  digits: 'Deve conter apenas digitos.',
  name: 'Precisa conter pelo menos 3 letras. Apenas letras (sem números).',
  username: 'Deve conter entre 4 a 32 caracteres e comecçar com uma letra. Pode-se usar letras, números, traços e pontos. Evite palavras acentuadas.',
  pass: 'Precisa conter pelo menos 6 caracteres, sendo obrigatório ter pelo menos um número, uma letra maiúscula e uma minúscula.',
  strongpass: 'Precisa conter pelo menos 8 caracteres, sendo obrigatório ter uma letra maiúscula, uma minúscula e um número ou caracter especial.',
  email: 'Precisa ser um endereço de e-mail válido. <em>(e.g. user@gmail.com)</em>',
  phone: 'Precisa ser um número de telefone válido. <em>(ex. 31-3123-4567)</em>',
  zip: 'Precisa ser um CEP válido. <em>(ex. 33245-000)</em>',
  url: 'Precisa ser uma URL válida. <em>(ex. www.google.com)</em>',
  minChar: 'Deve conter pelo menos <strong>{0}</strong> caracteres.',
  minOption: 'Selecione pelo menos <strong>{0}</strong> opção(ões).',
  maxChar: 'Não pode ocnter mais do que <strong>{0}</strong> caracteres.',
  maxOption: 'Não pode haver mais do que <strong>{0}</strong> opções selecionadas.',
  range: 'Deve ser um número entre {0} e {1}.',
  date: 'Precisa ser uma data válida. <em>(e.g. {0})</em>',
  dob: 'Precisa ser uma data de nascimento válida.',
  exclude: '"{0}" não está disponível.',
  excludeOption: '{0}',
  equalto: 'Precisa ser idêntico a <strong>"{0}"</strong>',
  extension: 'Os arquivos devem ter uma extensão válida. <em>(ex. "{0}")</em>'

})
