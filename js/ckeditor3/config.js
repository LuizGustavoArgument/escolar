/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
   //config.filebrowserBrowseUrl = '../../js/kcfinder/browse.php?type=files';
   //config.filebrowserImageBrowseUrl = '../../js/kcfinder/browse.php?type=images';
   //config.filebrowserFlashBrowseUrl = '../../js/kcfinder/browse.php?type=flash';
   //config.filebrowserUploadUrl = '../../js/kcfinder/upload.php?type=files';
   //config.filebrowserImageUploadUrl = '../../js/kcfinder/upload.php?type=images';
   //config.filebrowserFlashUploadUrl = '../../js/kcfinder/upload.php?type=flash';
   
   //config.contentsCss = 'templates/padrao.css';
   config.toolbar = 'Luiz2';
   config.forcePasteAsPlainText = false;
   config.fullPage = false;
   
   config.width = '700px';
   config.height = '400px';

    config.toolbar_Full =
	[
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];
   
    config.toolbar_Luiz =
    [
        ['Source','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Image','Flash','Table','SpecialChar'],
        '/',
        ['Bold','Italic','Strike','Underline','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','Anchor']
    ];
	
	config.toolbar_Luiz2 =
    [
        ['Source','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
        '/',
        ['Styles','Format','FontSize'],
        ['Bold','Italic','Strike','Underline','-','Subscript','Superscript'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Link','Unlink','Anchor']
    ];
	
	config.toolbar_Simples =
    [
        ['Cut','Copy','PasteText','PasteFromWord','-','NumberedList','BulletedList'],
        ['Bold','Italic','Strike','Underline','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ['Styles','Format','FontSize','-','Image','Link','Unlink','-','Table']
    ];
	
	config.toolbar_Simples2 =
    [
        ['Source','Link','Unlink'],
		['Image','Flash']
    ];
};
